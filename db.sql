/*
SQLyog Ultimate v10.42 
MySQL - 5.5.53-0ubuntu0.14.04.1 : Database - calculator
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`calculator` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `calculator`;

/*Table structure for table `additional_description` */

DROP TABLE IF EXISTS `additional_description`;

CREATE TABLE `additional_description` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `material_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `additional_description` */

/*Table structure for table `additional_description_label` */

DROP TABLE IF EXISTS `additional_description_label`;

CREATE TABLE `additional_description_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `additional_description_label` */

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`id`,`username`,`password`,`first_name`,`last_name`,`phone`,`last_activity`) values (1,'admin','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e','Ruzanna','Avanesyan','+37455248778','2017-01-11 14:13:23');

/*Table structure for table `building_type` */

DROP TABLE IF EXISTS `building_type`;

CREATE TABLE `building_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL,
  `price` float(11,2) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `building_type_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `building_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

/*Data for the table `building_type` */

insert  into `building_type`(`id`,`parent_id`,`price`,`created_date`,`sort_order`) values (0,0,0.00,'0000-00-00 00:00:00',0),(1,0,50.00,'2016-11-04 16:29:17',0),(2,0,120.00,'2016-11-04 16:29:17',0),(3,0,150.00,'2016-11-04 16:29:18',0),(10,1,NULL,'2016-11-07 11:23:07',0),(11,1,NULL,'2016-11-07 11:23:07',0),(12,1,NULL,'2016-11-07 11:23:07',0),(13,1,NULL,'2016-11-07 11:23:07',0),(14,1,NULL,'2016-11-07 11:25:57',0),(15,10,NULL,'2016-11-07 11:28:10',0),(16,10,NULL,'2016-11-07 11:28:10',0),(17,10,NULL,'2016-11-07 11:28:10',0),(18,10,NULL,'2016-11-07 11:28:10',0),(19,10,2.00,'2016-11-07 11:28:10',0),(20,10,2.00,'2016-11-07 11:28:10',0),(21,10,3.00,'2016-11-07 11:28:10',0),(22,10,3.00,'2016-11-07 11:28:10',0),(23,11,NULL,'2016-11-07 11:39:39',0),(24,11,NULL,'2016-11-07 11:39:40',0),(25,11,NULL,'2016-11-07 11:39:40',0),(26,11,4.00,'2016-11-07 11:39:40',0),(27,11,5.00,'2016-11-07 11:39:40',0),(28,12,NULL,'2016-11-07 11:40:44',0),(29,12,NULL,'2016-11-07 11:40:44',0),(30,13,NULL,'2016-11-07 11:41:39',0),(31,13,NULL,'2016-11-07 11:41:39',0),(32,13,2.00,'2016-11-07 11:41:39',0),(33,13,5.00,'2016-11-07 11:41:39',0),(34,14,NULL,'2016-11-07 11:42:22',0),(35,14,NULL,'2016-11-07 11:42:22',0),(36,2,NULL,'2016-11-07 11:47:04',0),(37,2,NULL,'2016-11-07 11:47:04',0),(38,2,NULL,'2016-11-07 11:47:05',0),(39,36,NULL,'2016-11-07 11:48:07',0),(40,36,NULL,'2016-11-07 11:48:07',0),(41,36,2.00,'2016-11-07 11:48:07',0),(42,36,3.00,'2016-11-07 11:48:07',0),(43,37,NULL,'2016-11-07 11:49:15',0),(44,37,NULL,'2016-11-07 11:49:15',0),(45,38,NULL,'2016-11-07 11:49:15',0),(46,38,NULL,'2016-11-07 11:49:15',0);

/*Table structure for table `building_type_label` */

DROP TABLE IF EXISTS `building_type_label`;

CREATE TABLE `building_type_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `building_type_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `building_type_id` (`building_type_id`),
  CONSTRAINT `building_type_label_ibfk_1` FOREIGN KEY (`building_type_id`) REFERENCES `building_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/*Data for the table `building_type_label` */

insert  into `building_type_label`(`id`,`building_type_id`,`language_id`,`name`) values (1,1,1,'Appartament1'),(2,2,1,'Maison'),(3,3,1,'Garde'),(9,10,1,'Etage'),(10,11,1,'Ascenseur'),(11,12,1,'Monte meuble nécessaire'),(12,13,1,'Distance logement-rue'),(13,14,1,'Cave'),(14,15,1,'RDC'),(15,16,1,'1er'),(16,17,1,'2ème'),(17,18,1,'3ème'),(18,19,1,'4ème'),(19,20,1,'5ème'),(20,21,1,'6ème'),(21,22,1,'7ème'),(22,23,1,'Non'),(23,24,1,'1 personne Max'),(24,25,1,'2 personnes Max'),(25,26,1,'3 personnes Max'),(26,27,1,'4 personnes Max'),(27,28,1,'Oui'),(28,29,1,'Non'),(29,30,1,'Aucune'),(30,31,1,'0 à 30 mètres'),(31,32,1,'30 à 50 mètres'),(32,33,1,'50 à 100 mètres'),(33,34,1,'Oui'),(34,35,1,'Non'),(35,36,1,'Distance logement-rue'),(36,37,1,'Cave'),(37,38,1,'Monte meuble nécessaire'),(38,39,1,'Aucune'),(39,40,1,'0 à 30 mètres'),(40,41,1,'30 à 50 mètres'),(41,42,1,'50 à 100 mètres'),(42,43,1,'Oui'),(43,44,1,'Non'),(44,45,1,'Oui'),(45,46,1,'Non');

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-active,1-disable',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`id`,`status`,`sort_order`) values (12,0,0),(13,0,0),(14,0,0),(15,0,0),(16,0,0);

/*Table structure for table `category_admin_label` */

DROP TABLE IF EXISTS `category_admin_label`;

CREATE TABLE `category_admin_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `category_admin_label_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `category_admin_label` */

insert  into `category_admin_label`(`id`,`category_id`,`language_id`,`name`,`text`) values (7,12,1,'Carton de déménagement','<p>Pour un&nbsp;<strong>d&eacute;m&eacute;nagement pas cher</strong>, Des bras en plus vous propose&nbsp;<strong>des&nbsp;cartons de d&eacute;m&eacute;nagement de qualit&eacute; &agrave; prix low-cost</strong>.&nbsp;<br />\r\nSur le march&eacute; du d&eacute;m&eacute;nagement, tous les cartons ne sont pas de qualit&eacute; &eacute;quivalente, les notres sont extremement&nbsp;r&eacute;sistants, et sont utilis&eacute;s aussi bien par&nbsp;des professionnels du d&eacute;m&eacute;nagement que par des particuliers&nbsp;sur des milliers de chantiers tous les ans.&nbsp;<br />\r\nPour vous offrir toujours la meilleure qualit&eacute; de carton de d&eacute;m&eacute;nagement&nbsp;au meilleur prix, nous travaillons en&nbsp;direct avec les fabriquants de cartons&nbsp;avec lesquels nous proc&eacute;dons r&eacute;guli&egrave;rement &agrave; des tests de r&eacute;sistance.<br />\r\nVendus &agrave; l&#39;unit&eacute;&nbsp;ou en pack de diff&eacute;rentes tailles, profitez de&nbsp;tarifs avantageux sur l&#39;int&eacute;gralit&eacute; de nos cartons de d&eacute;m&eacute;nagement.<br />\r\n<strong>Carton&nbsp;standard, carton&nbsp;livre, carton&nbsp;assiette, carton&nbsp;verre, carton bouteille, caisse de d&eacute;m&eacute;nagement, tout est l&agrave; pour simplifier votre d&eacute;m&eacute;nagement.&nbsp;</strong><br />\r\nB&eacute;n&eacute;ficiez en plus de la livraison partout en France &agrave; tarif unique et express en 48H.</p>\r\n'),(8,13,1,'Pack carton déménagement','<p>&nbsp;</p>\r\n\r\n<p>Vous cherchez du<strong>&nbsp;mat&eacute;riel de d&eacute;m&eacute;nagement au meilleur prix,</strong>&nbsp;vous allez aimer nos&nbsp;<strong>packs d&eacute;m&eacute;nagement pas cher</strong>.<br />\r\nCartons&nbsp;standard, cartons livre, scotch (adh&eacute;sif), papier bulle (rouleau de bulle de d&eacute;m&eacute;nagement), tout y est.<br />\r\nQue ce soit pour le d&eacute;m&eacute;nagement d&#39;un studio, le d&eacute;m&eacute;nagement d&#39;un 2 pi&egrave;ces ou le d&eacute;m&eacute;nagement d&#39;un logement plus grand, vous trouverez le pack de d&eacute;m&eacute;nagement pas cher qu&#39;il vous faut.<br />\r\nL&#39;avantage du pack&nbsp;de cartons de d&eacute;m&eacute;nagement pas cher Des bras en plus, c&#39;est qu&#39;il vous propose une solution tout compris et qui va vous &eacute;viter d&#39;oublier l&#39;achat d&#39;un des &eacute;lements indispensables au bon d&eacute;roulement de votre d&eacute;m&eacute;nagement. De plus l&#39;achat par pack est moins cher que l&#39;achat de carton&nbsp;&agrave; l&#39;unit&eacute;, alors pourquoi se priver?</p>\r\n'),(9,14,1,'Bulle de déménagement','<p>Pour la protection de vos&nbsp;objets&nbsp;fragiles pendant votre d&eacute;m&eacute;nagement, n&#39;h&eacute;sitez plus, et utilisez du bulle sp&eacute;cial d&eacute;m&eacute;nagement. Vendu par&nbsp;rouleaux de diff&eacute;rentes tailles,&nbsp;le papier bulle vous permettra de prot&eacute;ger assiettes, verres, bouteilles en verre, cadres et tous vos objets fragiles.<br />\r\nLe papier bulle est pratique et l&eacute;ger, et vous permet d&#39;&eacute;viter la collision&nbsp;de vos affaires. Utiliser un bulle de qualit&eacute; est important car il faut que les compartiments d&#39;air soient&nbsp;suffisamment r&eacute;sistants pour faire face aux chocs les plus importants.<br />\r\nDe la m&ecirc;me mani&egrave;re que pour nos cartons de d&eacute;m&eacute;nagement, nous effectuons r&eacute;gulierement des tests de r&eacute;sistance, et nos rouleaux de papier bulles sont utilis&eacute;s&nbsp;aussi bien par des particuliers qui d&eacute;m&eacute;nagent&nbsp;que par des professionels du d&eacute;m&eacute;nagement qui l&#39;utilisent sur des milliers de chantiers tous les ans.</p>\r\n'),(10,15,1,'Scotch et Adhésif de déménagement','<p>&nbsp;</p>\r\n\r\n<p>Pour la fermeture de tous vos cartons de d&eacute;m&eacute;nagement, utilisez le scotch de d&eacute;m&eacute;nagement (adh&eacute;sif de d&eacute;m&eacute;nagement) Des bras en plus.<br />\r\nUtilis&eacute; aussi bien par des particuliers que par des professionnels du d&eacute;m&eacute;nagement, le scotch de d&eacute;m&eacute;nagement pas cher Des bras en plus est l&#39;un des adh&eacute;sifs de d&eacute;m&eacute;nagement les moins cher du march&eacute;. Pratique, il vous permettra de sceller jusqu&#39;&agrave; 20 cartons en toute s&eacute;curit&eacute;.</p>\r\n'),(11,16,1,'Fournitures de déménagement','<p>Pour un d&eacute;m&eacute;nagement pas cher, n&#39;h&eacute;sitez plus et achetez tout votre mat&eacute;riel&nbsp;et vos fournitures de d&eacute;m&eacute;nagement chez Des bras en plus.<br />\r\nD&eacute;vidoir &agrave; scotch, marqueur, housse pour matelas (canap&eacute;), tout est l&agrave; pour simplifier votre d&eacute;m&eacute;nagenement au meilleur prix.<br />\r\nNos tarifs font partie des moins cher du march&eacute;, et notre service de livraison rapide vous assure une reception express de vos fournitures de d&eacute;m&eacute;nagement.</p>\r\n');

/*Table structure for table `category_label` */

DROP TABLE IF EXISTS `category_label`;

CREATE TABLE `category_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) unsigned NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`),
  CONSTRAINT `category_label_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `category_label` */

insert  into `category_label`(`id`,`product_category_id`,`language_id`,`name`) values (11,11,1,'Mobilier'),(12,12,1,'Appareils'),(13,13,1,'Divers');

/*Table structure for table `days_price` */

DROP TABLE IF EXISTS `days_price`;

CREATE TABLE `days_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `week_day` tinyint(1) NOT NULL,
  `price` float(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `days_price` */

insert  into `days_price`(`id`,`week_day`,`price`) values (3,1,14.00),(4,2,14.00),(5,3,15.00),(6,4,14.00),(7,5,14.00),(8,6,14.00),(9,7,14.00),(10,8,15.00);

/*Table structure for table `delivery` */

DROP TABLE IF EXISTS `delivery`;

CREATE TABLE `delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `delivery` */

insert  into `delivery`(`id`,`price`) values (1,9.90),(2,0.00),(3,0.00);

/*Table structure for table `delivery_label` */

DROP TABLE IF EXISTS `delivery_label`;

CREATE TABLE `delivery_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `delivery_id` (`delivery_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `delivery_label_ibfk_1` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `delivery_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `delivery_label` */

insert  into `delivery_label`(`id`,`delivery_id`,`language_id`,`text`) values (1,1,1,'<p>Livraison &agrave; domicile&nbsp;<br />\r\nCo&ucirc;t de l&rsquo;option :&nbsp;9.90&euro;</p>\r\n'),(2,2,1,'<p>Retrait en agence&nbsp;<br />\r\nCo&ucirc;t de l&rsquo;option :&nbsp;Gratuit</p>\r\n'),(3,3,1,'<p>Livraison le jour du d&eacute;m&eacute;nagement&nbsp;<br />\r\nCo&ucirc;t de l&rsquo;option :&nbsp;Gratuit</p>\r\n');

/*Table structure for table `hours_info` */

DROP TABLE IF EXISTS `hours_info`;

CREATE TABLE `hours_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(250) NOT NULL,
  `hours` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `hours_info` */

insert  into `hours_info`(`id`,`image`,`hours`,`price`) values (4,'cbf2e527817678b818b4aaf1a8de47bd.png',4,5.00),(5,'6ef343228b1e01d56cf057a6373e8058.png',7,0.00);

/*Table structure for table `insurance` */

DROP TABLE IF EXISTS `insurance`;

CREATE TABLE `insurance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `insurance` */

insert  into `insurance`(`id`,`price`) values (5,69.00),(6,0.00);

/*Table structure for table `insurance_label` */

DROP TABLE IF EXISTS `insurance_label`;

CREATE TABLE `insurance_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `insurance_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `insurance_id` (`insurance_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `insurance_label_ibfk_1` FOREIGN KEY (`insurance_id`) REFERENCES `insurance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `insurance_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `insurance_label` */

insert  into `insurance_label`(`id`,`insurance_id`,`language_id`,`text`) values (5,5,1,'<p>Oui j&rsquo;assure mes biens<br />\r\nCo&ucirc;t de l&rsquo;option :&nbsp;69.00&euro;<br />\r\nGarantie totale : 5000&euro; / 500&euro; par objet / Franchise 250&euro;</p>\r\n'),(6,6,1,'<p>Non je ne souhaite pas d&#39;assurance<br />\r\nCouverture&nbsp;incluse&nbsp;: 70&euro; max/objet</p>\r\n');

/*Table structure for table `language` */

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `iso` varchar(10) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `language` */

insert  into `language`(`id`,`name`,`iso`,`status`) values (1,'français','fr',0);

/*Table structure for table `material_label` */

DROP TABLE IF EXISTS `material_label`;

CREATE TABLE `material_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `material_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `additional_description` text NOT NULL,
  `small_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `material_id` (`material_id`),
  CONSTRAINT `material_label_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `material_label_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `materials` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `material_label` */

insert  into `material_label`(`id`,`material_id`,`language_id`,`name`,`description`,`additional_description`,`small_description`) values (4,10,1,'Carton Standard','<p>C&rsquo;est le carton de r&eacute;f&eacute;rence dans le d&eacute;m&eacute;nagement. Il sert &agrave; emballer l&rsquo;essentiel de vos effets &agrave; commencer par le linge pli&eacute;, vos jeans, t-shirts et autres pulls.</p>\r\n\r\n<p>Il est renforc&eacute; et permet le transport en toute s&eacute;curit&eacute; de vos effets. Il est aussi moins sujet &agrave; l&rsquo;affaissement&nbsp;; r&eacute;sultat, vous pouvez les empiler sans crainte.</p>\r\n\r\n<p>Attention cependant &agrave; ne pas le remplir de papiers, documents et autres livres. Le carton supporterait le poids mais votre dos s&rsquo;en souviendrait&nbsp;! Il n&rsquo;est en effet pas con&ccedil;u pour le transport de livre (il existe un carton sp&eacute;cifique pour cela) ou d&rsquo;objets tr&egrave;s lourds.</p>\r\n\r\n<p>Il vous est en revanche tout &agrave; fait possible de mixer linge pli&eacute; et livre de mani&egrave;re &agrave;&nbsp;&nbsp;mod&eacute;rer le poids final &agrave; transporter et &eacute;viter de vous faire mal lors du transport de ce carton. Utilisez ce carton pour toutes les pi&egrave;ces &agrave; d&eacute;m&eacute;nager, il conviendra aussi bien aux v&ecirc;tements, qu&rsquo;aux jouets en passant par les chaussures et bibelots.</p>\r\n\r\n<p>Le secret lors du remplissage est de ne laisser aucun vide. Ainsi, vos affaires ne se baladent pas &agrave; l&rsquo;int&eacute;rieur et le carton garde sa forme originelle m&ecirc;me en cas d&rsquo;empilement.</p>\r\n\r\n<p>Les dimensions de ce carton (55 x 35 x 30 cm) sont parfaitement &eacute;tudi&eacute;es pour supporter un poids de 25 kgs et nous vous recommandons l&rsquo;utilisation conjointe de ce carton avec du papier bulle pour une protection optimale.</p>\r\n\r\n<p>Pour mesurer le volume, comptez 7 cartons de ce type pour 1m3.</p>\r\n\r\n<p>Ce carton est 100% biod&eacute;gradable et recyclable. Renforc&eacute;, il pourra &ecirc;tre r&eacute;utilis&eacute; pour plusieurs d&eacute;m&eacute;nagements. Il contient une grille de rep&eacute;rage pour vous permettre d&rsquo;indiquer sans effort sa destination et &eacute;viter de se tromper&nbsp;!</p>\r\n\r\n<p>Il vous suffit de l&rsquo;utiliser avec un adh&eacute;sif standard pour le d&eacute;m&eacute;nagement pour l&rsquo;attacher sans effort et assurer un transport en toute s&eacute;curit&eacute; de vos effets&nbsp;!</p>\r\n','<table>\r\n	<tbody>\r\n		<tr>\r\n			<th>Taille :</th>\r\n			<td>55 x 35 x 30 cm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n','<p>C&rsquo;est le carton de r&eacute;f&eacute;rence dans le d&eacute;m&eacute;nagement. Il sert &agrave; emballer l&rsquo;essentiel de vos effets &agrave; commencer par le linge pli&eacute;, vos jeans, t-shirts et autres pulls. Dimensions : 55 x 35 x 30 cms</p>\r\n'),(5,11,1,'Pack Starter','<p>L&rsquo;&eacute;l&eacute;ment indispensable, qui vous suivra, pendant tout votre d&eacute;m&eacute;nagement, c&rsquo;est LE CARTON. De petite, moyenne ou grande taille c&rsquo;est le seul objet qui pourra vous permettre de ranger et prot&eacute;ger v&ecirc;tements, livres, objets, vaisselles, linge&hellip; Seulement le carton lors du d&eacute;m&eacute;nagement ne voyage jamais seul, il est accompagn&eacute; de toute sa famille de carton et dur de savoir combien de cartons vous faudra-t-il pour emballer l&rsquo;ensemble des pi&egrave;ces de chez vous, en fonction de la superficie de l&rsquo;habitation&nbsp;? Du nombre d&rsquo;objets objets en votre possession&nbsp;? Et d&rsquo;autres questions que vous vous poserez encore&nbsp;: combien de rouleau adh&eacute;sif faut-il pour pouvoir fermer tous ces cartons&nbsp;? Le carton est-il suffisant pour prot&eacute;ger certains de mes objets&nbsp;? Et c&rsquo;est &agrave; toutes ces questions que Des Bras en Plus vous apporte une r&eacute;ponse, notre professionnalisme et notre exp&eacute;rience nous donne envie de vous aider tous les jours un peu plus dans votre d&eacute;m&eacute;nagement, c&rsquo;est pour cela que nous avons cr&eacute;&eacute; des&nbsp;<strong><u>PACKS DE CARTONS</u></strong>&nbsp;qui rassemblent le n&eacute;cessaire du d&eacute;m&eacute;nagement et &agrave; chaque client son pack de carton id&eacute;al selon la superficie du domicile.</p>\r\n\r\n<p><strong><u>LE PACK STARTER</u></strong>&nbsp;pr&eacute;sente un lot de 12 cartons conseill&eacute;s pour un studio jusqu&rsquo;&agrave; 20m2 il comporte&nbsp;: 7 cartons&nbsp;<strong>STANDARD</strong>&nbsp;(55x, 35x30cm) cartons&nbsp;<strong>LIVRES&nbsp;</strong>(35x34x27cm) indispensables pour vos livres mais pas que, on peut y mettre CD,DVD&hellip;&nbsp;<strong><u>LE PACK STARTER</u></strong>&nbsp;contient aussi 1 rouleau&nbsp;<strong>BULLE</strong>&nbsp;(0.8x10m) qui vous permettra d&rsquo;emballer tout objet fragile avec ces coussinets d&rsquo;air qui amortissent le choc et donc rend l&rsquo;objet incassable et on vous fourmi &eacute;galement dans&nbsp;<strong><u>LE PACK STARTER</u></strong>&nbsp;1 rouleau&nbsp;<strong>ADHESIF</strong>&nbsp;suffisant pour refermer tous vos cartons. Et comme la soci&eacute;t&eacute; Des Bras en Plus vous apporte toujours un&nbsp;<strong><u>+</u></strong>&nbsp;ce pack est &agrave; prix r&eacute;duit &agrave; 25&euro;.</p>\r\n','','<p>7 x Carton standard, 5 x Carton livre, 1 x Rouleau bulle (0,8x10m) , 1 x Adhesif standard</p>\r\n'),(6,12,1,'Bulle de protection','<p>Eh oui, finies les protections traditionnelles tel que le papier journal froiss&eacute;, les objets entour&eacute;s de vieux tissus&hellip;Aujourd&rsquo;hui, le papier bulle s&rsquo;impose face &agrave; toutes ces options expos&eacute;es pr&eacute;c&eacute;demment.</p>\r\n\r\n<p>Indispensable pour emballer et prot&eacute;ger vos objets fragiles (tableaux, bibelots, vaisselles, lampes, t&eacute;l&eacute;phones, miroirs&hellip;), Des bras en plus vous propose de prendre vos pr&eacute;cautions et de vous munir de bulle de protection.</p>\r\n\r\n<p>En effet le papier bulle, pratique et l&eacute;ger vous sera utile pour vos d&eacute;m&eacute;nagements ou tout simplement pour &eacute;viter les collisions de vos produits qui n&rsquo;ont pas n&eacute;cessairement besoin d&rsquo;un volume aussi grand que celui d&rsquo;un carton ou du moins pour les prot&eacute;ger lorsqu&rsquo;ils seront dans ce dernier. Facile &agrave; mettre, il va s&rsquo;adapter aux diff&eacute;rentes formes des mat&eacute;riaux &agrave; prot&eacute;ger et ainsi &eacute;viter les chocs qu&rsquo;ils pourront subir d&rsquo;un point A &agrave; un point B gr&acirc;ce aux bulles d&rsquo;air qui feront office de &laquo;&nbsp;coussins&nbsp;&raquo; et donc d&rsquo;amortisseurs. De plus, par sa couleur transparente il permettra de rep&eacute;rer facilement les objets que vous cherchez et qui sont d&eacute;j&agrave; emball&eacute;s.</p>\r\n\r\n<p>Confectionn&eacute; sous diff&eacute;rents formats (enveloppe, pochette, rouleau&hellip;), vous le trouverez en vente chez Des bras en plus (en agence ou sur le site&nbsp;<a href=\"http://www.desbrasenplus.com/\">www.desbrasenplus.com</a>&nbsp;) sur 0,8m &times; 10m &agrave; seulement 9,90&euro;.</p>\r\n\r\n<p>Enfin, dernier avantage que peut vous apporter le bulle de protection&nbsp;: il vous &eacute;vitera de d&eacute;penser votre argent inutilement chez un psychologue qui va vous prendre 100&euro; pour une consultation d&rsquo;anti stress&nbsp;; alors venez vous faire prescrire le meilleur anti stress chez Des bras en plus pour 9,90&euro; un rouleau de papier bulle que vous &eacute;claterez avec vos doigts&nbsp;; cela vous donnera une occupation utile pour que vous puissiez vous d&eacute;tendre d&egrave;s que l&rsquo;envie vous en prendra&nbsp;; en effet il n&rsquo;a aucune date de p&eacute;remption vous en avez pour 0,8m &times; 10m&nbsp;!!!</p>\r\n','',''),(7,13,1,'Adhésif Standard','<p>La plupart des mat&eacute;riaux ne collent pas entres eux. Pourquoi? Pour que 2 objets adh&egrave;rent (on se passera de vous faire une explication chimique et physique) il faut tout simplement de L&#39;ADHESIF. Petit rouleau l&eacute;ger et discret il est l&#39;un des &eacute;l&eacute;ments indispensables durant votre d&eacute;m&eacute;nagement, du moins particuli&egrave;rement &agrave; la p&eacute;riode la plus redoutable de cette action de d&eacute;m&eacute;nagement qui est bien s&ucirc;r le RANGEMENT. &Ocirc;ter tous les biens mobiliers, v&ecirc;tements, vaisselles... qui doivent &ecirc;tre ensuite tri&eacute;s et l&agrave; se posera la question qui restera parmi vous dans votre futur logement ou alors de qui souhaitez-vous vous s&eacute;parer. Avant derni&egrave;re &eacute;tape tout mettre en carton et enfin le dernier coup de ma&icirc;tre enfermer tous ces cartons avec de L&#39;ADHESIF qui sera le clap de fin et un soulagement suite &agrave; des heures et &agrave; des jours de tris intensifs.</p>\r\n\r\n<p>Pour ce qu&#39;il reste du d&eacute;ballage, &quot;bon courage&quot; car il est dit que l&#39;adh&eacute;sif est d&#39;une forte dissipation, en effet un adh&eacute;sif r&eacute;siste au d&eacute;collement non seulement parce qu&#39;il faut pr&eacute;tendre avoir une force &eacute;lev&eacute;e pour pouvoir essayer de le d&eacute;coller, mais plus encore parce qu&#39;il faut fournir une &eacute;nergie importante pour y arriver. Sacr&eacute; ADHESIF il ne se laisse pas faire, mais sa reste une garantie, vos cartons ne s&#39;ouvriront en aucun cas et aucun de vos petits objets pr&eacute;cieux seront perdus.</p>\r\n','','<p>L&#39;adh&eacute;sif standard de d&eacute;m&eacute;nagement Des bras en plus est la r&eacute;f&eacute;rence. Parfait pour les particuliers et les professionnels. Dimensions :</p>\r\n'),(8,14,1,'Marqueur noir','<p>Plut&ocirc;t que de vous &quot;amuser&quot; &agrave; ouvrir l&#39;ensemble de ces grosses boites pour savoir quel carton correspond &agrave; quelle pi&egrave;ce de votre nouveau logement ou encore d&#39;avoir auparavant utiliser un simple stylo pour noter sur vos cartons c&#39;est &agrave; ce moment que vous regretterez ce geste vu l&#39;&eacute;criture &agrave; moiti&eacute; effac&eacute; et incompr&eacute;hensible du sylo.</p>\r\n\r\n<p>De plus, l&#39;ensemble de nos cartons mis en vente sont marqu&eacute;s d&#39;une petite liste qui rassemble l&#39;ensemble des pi&egrave;ces (Bureau, Chambre, Chambre 2, Chambre 3, Chambre 4, Chambre 5, Cuisine, Entr&eacute;e, Salle &agrave; manger, Salle de bain, Salon, Cave, Cellier, Garage, Grenier) il ne vous restera plus qu&#39;&agrave; cocher &agrave; quel pi&egrave;ce est destin&eacute;e ce carton. Un gain de temps consid&eacute;rable, surtout en pleine p&eacute;riode de d&eacute;m&eacute;nagement.</p>\r\n\r\n<p>C&#39;est aussi un moyen de faire participer les plus petits, adeptes de feutre en tout genre et exp&eacute;riment&eacute;s en dessin. Mais attention, le MARQUEUR PERMANENT comme son nom l&#39;indique est ind&eacute;l&eacute;bile. A vous donc de surveiller que leur grande &acirc;me d&#39;artiste ne s&#39;entendent pas sur les murs de votre futur ancien logement, qui sera peut &ecirc;tre per&ccedil;u comme un signe de bienvenue &agrave; ces futurs locataires ou propri&eacute;taires qui viendront s&#39;installer dans ce qui a &eacute;t&eacute; votre &quot;chez vous&quot;.</p>\r\n\r\n<p>Et pour toujours rester professionnels le MARQUEUR que nous vous proposons est de marque&nbsp; &quot;BIC&quot;, un MARQUEUR PERMANENT &agrave; faible odeur, qui ne s&egrave;che pas m&ecirc;me ouvert pendant 2 semaines. MARQUEUR que vous trouverez en vente sur notre site&nbsp;<a href=\"http://www.desbrasenplus.com/\">www.desbrasenplus.com</a>&nbsp;ou directement en agence dans le 18&egrave;me arrondissement de Paris : 60 Rue Ramey et dans le 17&egrave;me arrondissement : 141 Rue Cardinet o&ugrave; vous retrouverez l&#39;indispensable du d&eacute;m&eacute;nagement et o&ugrave; nos conseillers sont pr&eacute;sents pour vous aider et r&eacute;pondre &agrave; l&#39;ensemble de vos questions.</p>\r\n','<p>&nbsp;&nbsp;</p>\r\n','<p>Id&eacute;al pour marquer, le MARQUEUR PERMANENT vous permettra de vous y retrouver face &agrave; cette pile de cartons que vous devez soigneusement d&eacute;baller et ranger.</p>\r\n');

/*Table structure for table `materials` */

DROP TABLE IF EXISTS `materials`;

CREATE TABLE `materials` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `price` double(10,2) NOT NULL,
  `image` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `materials_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `materials` */

insert  into `materials`(`id`,`category_id`,`price`,`image`) values (10,12,1.80,'60baa8e3584ebf928b11ecafe3557218.png'),(11,13,25.00,'608cae99f15ff76a35cb5783e0459ce7.png'),(12,14,9.90,'a47132dfe899d0f1b326e15acc4e31df.png'),(13,15,2.90,'c241c7f6f4002bc511b41b321b498782.png'),(14,16,3.00,'b2cb1148f177474fdd82712e1907a959.png');

/*Table structure for table `order_info` */

DROP TABLE IF EXISTS `order_info`;

CREATE TABLE `order_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `information_table` text NOT NULL,
  `pdf` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_info_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

/*Data for the table `order_info` */

insert  into `order_info`(`id`,`order_id`,`information_table`,`pdf`) values (132,166,'<div style=\"text-align: center\"><table style=\"width: 100%;border: 1px solid black;border-collapse: collapse;\"><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">User information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">statut</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">nom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aranush</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">prenom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Test</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">email</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">a.grigoryan@voodoo.pro</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">portable</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">telephone</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Depart Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Yutz, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">57970</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Yutz</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Arrive Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">7 Rue de Rivoli, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"3\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Products</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aide emballage</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Carton Standard</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr></table></div>','YOG4PULQCU.pdf'),(133,167,'<div style=\"text-align: center\"><table style=\"width: 100%;border: 1px solid black;border-collapse: collapse;\"><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">User information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">statut</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">nom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Test</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">prenom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Grigoryan</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">email</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">a.grigoryan@voodoo.pro</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">portable</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">telephone</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Depart Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">8 Place Louis-Armand, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">75012</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Arrive Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">118 France, Gaja-et-Villedieu</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Gaja-et-Villedieu</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"3\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Products</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aide emballage</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Carton Standard</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr></table></div>','VFI2V9JS8P.pdf'),(134,168,'<div style=\"text-align: center\"><table style=\"width: 100%;border: 1px solid black;border-collapse: collapse;\"><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">User information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">statut</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">nom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aranush</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">prenom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">dfrhy</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">email</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">a.grigoryan@voodoo.pro</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">portable</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">telephone</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Depart Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">4 Rue de Dunkerque, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">75010</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Arrive Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">7 Rue de Dunkerque, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris-10E-Arrondissement</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"3\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Products</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aide emballage</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Carton Standard</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr></table></div>','K5M1V6QXIP.pdf'),(135,169,'<div style=\"text-align: center\"><table style=\"width: 100%;border: 1px solid black;border-collapse: collapse;\"><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">User information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">statut</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">nom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aranush</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">prenom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">dfrhy</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">email</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">a.grigoryan@voodoo.pro</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">portable</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">telephone</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Depart Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">6 Rue de Rivoli, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">75004</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Arrive Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">6 Rue de Dunkerque, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"3\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Products</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aide emballage</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Carton Standard</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr></table></div>','9BS1SBHB8V.pdf'),(136,170,'<div style=\"text-align: center\"><table style=\"width: 100%;border: 1px solid black;border-collapse: collapse;\"><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">User information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">statut</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">nom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aranush</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">prenom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">dfrhy</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">email</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">a.grigoryan@voodoo.pro</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">portable</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">telephone</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Depart Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">06200 France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">06200</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Nice</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Arrive Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">4 Rue de Dunkerque, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><th colspan=\"3\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Products</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Déménagement de piano</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Carton Standard</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr></table></div>','YHRZFM1QFM.pdf'),(137,171,'<div style=\"text-align: center\"><table style=\"width: 100%;border: 1px solid black;border-collapse: collapse;\"><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">User information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">add_users</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">1</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">statut</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">nom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Aranush</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">prenom</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">dgsg</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">email</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">a.grigoryan@voodoo.pro</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">portable</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">telephone</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">0612345678</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Tarif</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">STANDARD</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Depart Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">6 Rue de Rivoli, Paris, France</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">75004</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Paris</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Garde</td></tr><th colspan=\"2\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Arrive Information</th><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Address</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Post</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Ville</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Surface</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Depart Url</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Country</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\"></td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\">House type</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">Garde</td></tr><th colspan=\"3\" style=\"border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc\">Products</th><tr><td style=\"padding: 10px;text-align: center; background-color: rgb(210, 214, 222)\" colspan =\"3\">Mobilier</td></tr><tr><td style=\"border: 1px solid black;padding: 10px;text-align: left\"> Moyenne table</td><td style=\"border: 1px solid black;padding: 10px;text-align: left\">2</td></tr></table></div>','FXQ5XN8ESM.pdf');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `INVOICE` (`invoice_number`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

/*Data for the table `orders` */

insert  into `orders`(`id`,`invoice_number`) values (169,'9BS1SBHB8V'),(171,'FXQ5XN8ESM'),(168,'K5M1V6QXIP'),(172,'N8XSUWO7J9'),(167,'VFI2V9JS8P'),(170,'YHRZFM1QFM'),(166,'YOG4PULQCU');

/*Table structure for table `person_number` */

DROP TABLE IF EXISTS `person_number`;

CREATE TABLE `person_number` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_number` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `history_image` varchar(250) NOT NULL,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `person_number` */

insert  into `person_number`(`id`,`person_number`,`image`,`history_image`,`price`) values (5,1,'06cccadc12508f73128912e585679d87.png','3ecc0e5d038aba756ad7adbc343f7c41.png',5.00),(6,2,'10c46c605960ca76177ddcb277776d72.png','117766637d4d4dfe968acb2b24574fb3.png',0.00),(7,3,'b945f2874740563dc53e03266883cfd2.png','de6e48532e22961fe0be17f157f368b4.png',0.00),(8,4,'6f33cca55f5d140aed0d953594bcf2fb.png','ea820798c6b851f53d5b5dd0f82669ef.png',0.00),(9,5,'b558cbb76f32138ac7c8726855872ba5.png','89e4a8813a8f13010346f791862f50d8.png',0.00),(10,6,'2228d9284aee4632ae218433a340d5f6.png','b1b8faa8f4a999fd231062e718f1a1c9.png',0.00);

/*Table structure for table `prices` */

DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(250) NOT NULL,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `prices` */

/*Table structure for table `product_category` */

DROP TABLE IF EXISTS `product_category`;

CREATE TABLE `product_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-active, 1-disable',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `product_category` */

insert  into `product_category`(`id`,`status`,`sort_order`) values (11,0,0),(12,0,0),(13,0,0);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `volume` float(10,2) NOT NULL DEFAULT '1.00',
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `sort_order` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-active,1-disable',
  `category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`id`,`image`,`volume`,`price`,`sort_order`,`status`,`category`) values (8,'40f6e02ddac0705c6dae4689b3393a18.png',1.00,0.00,0,0,11),(9,'eece9243cb6551abecfe68bbd6454593.png',1.00,0.00,0,0,11),(10,'808591a8a2af95897a8452963b54dfce.png',1.00,0.00,0,0,11),(11,'82e3a880be995abe771ea90e53fb5567.png',1.00,0.00,0,0,11),(12,'5a69ef2ada6416d9ffb3f0cf2882ca52.png',1.00,0.00,0,0,11),(13,'89696b5f3c0b8b33c21ce280106afe08.png',1.00,0.00,0,0,11),(14,'f273e8dccd8a925cd5a7f889fafbb9b2.png',1.00,0.00,0,0,11),(15,'8fbb9fef2a34d474f3b57fcbe37b939c.png',1.00,0.00,0,0,11),(17,'0867819ec170283d8258525cee3265ea.png',1.00,0.00,0,0,11),(18,'bdaa2c0054f3af67fee786160bb1b670.png',1.00,0.00,0,0,11),(19,'68f755e65212748ca5fb913129b7af20.png',1.00,0.00,0,0,11),(20,'c670bbf39bf4dba84a4b30523daca6b9.png',1.00,0.00,0,0,11),(21,'aae65c08015a59873ae0b8db4453fb72.png',1.00,0.00,0,0,12),(22,'1f638be36d11e9475467dfce69380e63.png',1.00,0.00,0,0,12),(23,'4a347f8fcc8ac14aba515b29972c0811.png',1.00,0.00,0,0,12),(24,'58354ed7cedffa7ee7a27ff528a8755b.png',1.00,0.00,0,0,12),(25,'af77558941f4e70534f5382cf0d91cf6.png',1.00,0.00,0,0,12),(26,'dde50cb2eaf6883fa8dcea2d4874a4cc.png',1.00,0.00,0,0,13),(27,'78a282c43aad4d9a862cd55cb329a691.png',1.00,0.00,0,0,13),(28,'63ceba9b3b96bdae3fea2e98100e1cad.png',1.00,0.00,0,0,13),(30,'272a796b17db9942c126fb26c323a768.png',1.00,0.00,0,0,13);

/*Table structure for table `products_label` */

DROP TABLE IF EXISTS `products_label`;

CREATE TABLE `products_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `products_label_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `products_label` */

insert  into `products_label`(`id`,`product_id`,`language_id`,`name`) values (13,8,1,'Lit simple'),(14,9,1,'Lit double'),(15,10,1,' Lit superposé'),(16,11,1,'Lit bébé'),(17,12,1,' Canapé 2 places'),(18,13,1,' Canapé 3 places'),(19,14,1,' Canapé d\'angle'),(20,15,1,'Fauteuil'),(22,17,1,'Chaise'),(23,18,1,'Luminaire'),(24,19,1,' Petite table'),(25,20,1,' Moyenne table'),(26,21,1,'Congélateur bac'),(27,22,1,'Frigo-congélateur'),(28,23,1,'Frigo simple'),(29,24,1,' Frigo américain'),(30,25,1,' Gazinière'),(31,26,1,'Cave à vin'),(32,27,1,'Poussette'),(33,28,1,' Vélo d\'intérieur'),(34,27,1,'Poussette'),(36,30,1,'Vélo'),(37,8,1,'Lit simple'),(38,8,1,'Lit simple');

/*Table structure for table `service_option` */

DROP TABLE IF EXISTS `service_option`;

CREATE TABLE `service_option` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(11) unsigned NOT NULL,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_option_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `service_option` */

insert  into `service_option`(`id`,`service_id`,`price`) values (8,2,250.00),(9,2,264.00),(10,2,288.00),(11,2,336.00),(12,2,360.00),(13,2,384.00),(14,2,408.00),(15,2,432.00),(16,2,456.00),(17,2,480.00),(18,2,504.00),(19,3,199.00),(20,4,299.00),(21,4,499.00);

/*Table structure for table `service_option_label` */

DROP TABLE IF EXISTS `service_option_label`;

CREATE TABLE `service_option_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `option_id` (`option_id`),
  CONSTRAINT `service_option_label_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_option_label_ibfk_2` FOREIGN KEY (`option_id`) REFERENCES `service_option` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `service_option_label` */

insert  into `service_option_label`(`id`,`option_id`,`language_id`,`text`) values (8,8,1,'<p>0 &eacute;tage</p>\r\n'),(9,9,1,'<p>1 &eacute;tage</p>\r\n'),(10,10,1,'<p>2 &eacute;tage</p>\r\n'),(11,11,1,'<p>3 &eacute;tage</p>\r\n'),(12,12,1,'<p>4 &eacute;tage</p>\r\n'),(13,13,1,'<p>5 &eacute;tage</p>\r\n'),(14,14,1,'<p>6 &eacute;tage</p>\r\n'),(15,15,1,'<p>7 &eacute;tage</p>\r\n'),(16,16,1,'<p>8 &eacute;tage</p>\r\n'),(17,17,1,'<p>9 &eacute;tage</p>\r\n'),(18,18,1,'<p>10 &eacute;tages</p>\r\n'),(19,19,1,'<p>4H</p>\r\n'),(20,20,1,'<p>Demi journ&eacute;e</p>\r\n'),(21,21,1,'<p>Journ&eacute;e</p>\r\n');

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` float(10,2) NOT NULL,
  `image` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `services` */

insert  into `services`(`id`,`price`,`image`) values (1,8.90,'1de4ca3f6ba5880517dff806354c9660.png'),(2,250.00,'faa3eb63949769272b92facfaf3953e3.png'),(3,199.00,'072888589044792aed46da2587c533a4.png'),(4,299.00,'49df277800ffaa950228881c965606d2.png');

/*Table structure for table `services_label` */

DROP TABLE IF EXISTS `services_label`;

CREATE TABLE `services_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) unsigned NOT NULL,
  `services_id` int(11) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `small_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `services_id` (`services_id`),
  CONSTRAINT `services_label_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `services_label_ibfk_2` FOREIGN KEY (`services_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `services_label` */

insert  into `services_label`(`id`,`language_id`,`services_id`,`name`,`description`,`small_description`) values (1,1,1,'Transport en penderie','<p>Pour transporter les v&ecirc;tements sur cintres, deux possibilit&eacute;s s&rsquo;offrent &agrave; vous : soit tout mettre en cartons ou alors opter plus une solution plus pratique, la location pendant la prestation de penderie en plastique. Cette seconde solution vous permet de transporter vos effets sans les froisser en ins&eacute;rant simplement vos cintres directement dans la penderies. Vos v&ecirc;tements sont alors maintenus &agrave; l&rsquo;aide d&rsquo;une tringle de suspension. Finies les longues s&eacute;ances de repassage post d&eacute;m&eacute;nagement !</p>\r\n\r\n<p>Avec 50 centim&egrave;tres de lin&eacute;aire et un m&egrave;tre de hauteur, transportez vos chemises, manteaux et autres robes plus facilement facilement.</p>\r\n\r\n<p>La mise &agrave; disposition de penderie en plastique pendant la prestation est aussi une solution plus &eacute;conomique que l&rsquo;achat d&rsquo;un carton sp&eacute;cifique. Mat&eacute;riel de conditionnement &eacute;conomique et pratique, la penderie fait partie des indispensables pour un d&eacute;m&eacute;nagement sans stress.</p>\r\n','Avec le transport en penderie, finie la corvée de repassage ou les dépenses de pressing après le déménagement. Mise à disposition d’une penderie en plastique robuste et facile à transporter pendant la prestation. Dimensions (intérieur) : 50 x 50 x 100 cm Placez y jusqu’à 20 chemises, 10 costumes complets ou encore 10 manteaux (Matériel récupéré à l\'issue de la prestation)'),(2,1,2,'Déménagement de piano','<p>Parce que transporter un piano droit requiert des comp&eacute;tences particuli&egrave;res, une &eacute;quipe sp&eacute;cialis&eacute;e et d&eacute;di&eacute;e les prend en charge dans le cadre de nos prestations. Pour votre piano droit dont le poids n&rsquo;exc&egrave;de pas 200 kilos et la hauteur est de 120 cm maximum, optez pour l&rsquo;option transport de piano droit afin de profiter du savoir faire d&rsquo;une &eacute;quipe agu&eacute;rrie et habitu&eacute;e au transport d&rsquo;objets lourds (pianos, coffres,&hellip;).</p>\r\n\r\n<p>Le prix de cette option est fonction des &eacute;tages au d&eacute;part et &agrave; la livraison. Comptez 0 pour un rez de chauss&eacute;e et 0 &eacute;galement si le piano passe dans les ascenseurs. La somme des &eacute;tages au chargement et au d&eacute;chargement vous permet d&rsquo;obtenir le prix final de cette prestation.</p>\r\n\r\n<p>A noter que cette option concerne uniquement les piano dits &laquo;&nbsp;droits&nbsp;&raquo; et que pour les autres types de piano (tout comme pour les autres objets lourds dont le poids d&eacute;passe 80 kilos), nous vous invitons &agrave; vous rapprocher du service client pour obtenir un tarif sur mesure.</p>\r\n','Pour le transport de votre piano droit, nous faisons intervenir une équipe qui s\'occupe exclusivement du transport de votre précieux instrument. Pour connaître le tarif de ce service et sélectionner cette option, additionnez l\'étage de votre adresse de départ et l\'étage de votre adresse d\'arrivée (0 pour un RDC) et sélectionnez la somme des deux dans l\'onglet déroulant. (prix applicables pour un piano droit d\'une hauteur maximale de 120 cm dont le poids n\'excède pas 200 kg, sous reserve que les accès permettent le passage. Nous pouvons aussi prendre en charge les autres pianos, appelez nous au 01 84 16 18 00 )'),(3,1,3,'Aide emballage','<p>Avec cette option, un d&eacute;m&eacute;nageur professionnel s&rsquo;occupe de l&rsquo;emballage de vos effets : mise en cartons, protection avec papier bulle, emballage de la vaisselles, des bibelots,&hellip;</p>\r\n\r\n<p>Il peut aussi vous aider dans le d&eacute;montage de mobilier.</p>\r\n\r\n<p>Un d&eacute;m&eacute;nagement r&eacute;ussi, c&rsquo;est d&rsquo;abord un conditionnement adapt&eacute; ! Avec l&rsquo;option &laquo;&nbsp;Aide &agrave; l&rsquo;emballage&nbsp;&raquo;, le d&eacute;m&eacute;nageur est pr&eacute;sent sur une demi-journ&eacute;e (4 heures) le matin ou l&rsquo;apr&egrave;s-midi &agrave; une date souhait&eacute;e. Que ce soit la veille du d&eacute;m&eacute;nagement ou quelques jours avant, cette option&nbsp;</p>\r\n\r\n<p>Astuce : si vous le souhaitez, optez &eacute;galement pour cette option pour vous aider dans le d&eacute;ballage et la remise en place de vos effets apr&egrave;s le d&eacute;m&eacute;nagement !</p>\r\n','Profitez de l\'aide d\'un déménageur confirmé la veille de votre déménagement pour emballer, conditionner et protéger vos effets. Le professionel pourra aussi vous aider à démonter vos meubles volumineux pour en faciliter le transport. nb: le déménageur n\'est pas habilité à décrocher les éléments fixés (murs et plafonds) Forfait aide à l\'emballage demi-journée (4H) : 199 €'),(4,1,4,'Location d’un monte-meubles','<p>Quand le passage par le fen&ecirc;tre est possible, nous vous conseillons l&#39;utilisation de monte meubles avec technicien. L&rsquo;appareil vous permet d&rsquo;&eacute;viter la cage d&rsquo;escalier et les risques de salissure et de d&eacute;gradations qui vont avec. Le monte meubles est aussi indispensable lorsque les acc&egrave;s &laquo;&nbsp;classiques&nbsp;&raquo; ne permettent pas d&rsquo;enlever tous les &eacute;l&eacute;ments.</p>\r\n\r\n<p>Il facilite en g&eacute;n&eacute;ral beaucoup le d&eacute;m&eacute;nagement et permet d&rsquo;optimiser le temps de prestation.&nbsp;</p>\r\n\r\n<p>Le monte meubles est reservable sur une demi-journ&eacute;e soit 4 heures (ou une adresse) et la journ&eacute;e soit 7 heures (ou 2 adresses). Un technicien vient avec et l&rsquo;installe. Il est aussi charg&eacute; de manipuler l&rsquo;appareil.&nbsp;</p>\r\n\r\n<p>Il peut &ecirc;tre install&eacute; &agrave; l&rsquo;adresse de chargement, &agrave; l&rsquo;adresse de livraison ou aux deux selon votre besoin.</p>\r\n\r\n<p>En moyenne, il est possible de vider un appartement de 50m2 en une demi-journ&eacute;e (temps d&rsquo;installation compris).&nbsp;</p>\r\n\r\n<p>Vous pouvez placer des charges de 100 kilos maximum sur le plateau de l&rsquo;appareil.</p>\r\n\r\n<p>Assurez-vous simplement que le passage est possible et d&rsquo;avoir toutes les autorisations aupr&egrave;s du service voirie de votre commune (nous pouvons vous donner un document attestant votre d&eacute;m&eacute;nagement prochaine sur simple demande).</p>\r\n','Vous souhaitez un passage par fenêtre ? Optez pour la location d\'un monte meubles avec technicien. Un minimum de deux déménageurs (hors technicien monte-meubles) est requis pour la location d\'un monte-meubles Forfait demi-journée (4H - une adresse) : 299 € Forfait journée (7H - jusqu\'à deux adresses) : 499 €');

/*Table structure for table `tarifs` */

DROP TABLE IF EXISTS `tarifs`;

CREATE TABLE `tarifs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` float(10,0) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-active,1-disable',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `tarifs` */

insert  into `tarifs`(`id`,`price`,`status`,`sort_order`) values (3,35,0,0),(5,56,0,1),(10,42,0,0);

/*Table structure for table `tarifs_label` */

DROP TABLE IF EXISTS `tarifs_label`;

CREATE TABLE `tarifs_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tarifs_id` int(11) unsigned NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `features_user_Comment` text,
  PRIMARY KEY (`id`),
  KEY `tarifs_id` (`tarifs_id`),
  CONSTRAINT `tarifs_label_ibfk_1` FOREIGN KEY (`tarifs_id`) REFERENCES `tarifs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tarifs_label` */

insert  into `tarifs_label`(`id`,`tarifs_id`,`language_id`,`name`,`comment`,`features_user_Comment`) values (3,3,1,'ECONOMIQUE','','<p><strong>Je m&rsquo;occupe de tout !</strong><br />\r\nJe fais mes cartons et je d&eacute;monte mes meubles.<br />\r\nLes d&eacute;m&eacute;nageurs prot&egrave;gent, transportent, chargent, d&eacute;chargent et remettent en place le mobilier.</p>\r\n'),(4,5,1,'CONFORT','','<p><strong>Je ne m&rsquo;occupe de rien !</strong><br />\r\nLes avantages de la formule standard + emballage de l&#39;int&eacute;gralit&eacute; de vos affaires.</p>\r\n'),(8,10,1,'STANDARD','LE PLUS POPULAIRE','<p><strong>Le meilleur des deux mondes !</strong><br />\r\nJe fais mes cartons.<br />\r\nLes avantages de la formule &eacute;conomique + d&eacute;montage / remontage du mobilier + protection des &eacute;l&eacute;ments&nbsp;fragiles par les d&eacute;m&eacute;nageurs.</p>\r\n');

/*Table structure for table `terms_conditions` */

DROP TABLE IF EXISTS `terms_conditions`;

CREATE TABLE `terms_conditions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `terms_conditions` */

insert  into `terms_conditions`(`id`) values (3);

/*Table structure for table `terms_contaitions_label` */

DROP TABLE IF EXISTS `terms_contaitions_label`;

CREATE TABLE `terms_contaitions_label` (
  `id` int(11) unsigned NOT NULL,
  `terms_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `terms_id` (`terms_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `terms_contaitions_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `terms_contaitions_label_ibfk_1` FOREIGN KEY (`terms_id`) REFERENCES `terms_conditions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `terms_contaitions_label` */

insert  into `terms_contaitions_label`(`id`,`terms_id`,`language_id`,`text`) values (0,3,1,'<p><strong>CONDITIONS GÉNÉRALES DE VENTE ET DE PRESTATION DE SERVICES (vente à distance ou en ligne)</strong></p>\r\n\r\n<p><strong>APPLICABLES AU SEIN DU RÉSEAU DES BRAS EN PLUS</strong></p>\r\n\r\n<p>A jour au 1er Septembre 2016</p>\r\n\r\n<p><strong>Article 1 - Application des conditions générales</strong><br />\r\n<strong>DES BRAS EN PLUS&nbsp;</strong>est un réseau d&rsquo;entreprises indépendantes (ci-après ensemble</p>\r\n\r\n<p>dénommées &laquo;&nbsp;<strong>DES BRAS EN PLUS&nbsp;</strong>&raquo; ou &laquo;&nbsp;<strong>DBEP&nbsp;</strong>&raquo;) ayant pour activité :</p>\r\n\r\n<p>- La réalisation de prestations de services de déménagement sous la marque&nbsp;<strong>DES BRAS EN PLUS&nbsp;</strong>au profit de clients consommateurs (ci-après dénommés le(s) &laquo; Client(s) &raquo;) par le biais d&rsquo;une mise à disposition de déménageurs, de matériel(s) et de véhicule(s) pour une durée prédéterminée par le Client ; ou de prestations de services de déménagement via un inventaire ou un volume.</p>\r\n\r\n<p>- Accessoirement, la vente de produits sous la marque&nbsp;<strong>DES BRAS EN PLUS&nbsp;</strong>(matériels, fournitures, accessoires dédiés au déménagement).</p>\r\n\r\n<p>Les prestations de services de déménagement réalisées par&nbsp;<strong>DBEP&nbsp;</strong>sont exclusivement les suivantes : (en fonction de l&rsquo;offre choisie, certaines peuvent être en option avec supplément)</p>\r\n\r\n<p>- Manutention ,</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>- &nbsp;Conduite du camion DBEP,</p>\r\n	</li>\r\n	<li>\r\n	<p>- &nbsp;Chargement du mobilier dans le camion DBEP,</p>\r\n	</li>\r\n	<li>\r\n	<p>- &nbsp;Emballage et déballage des effets fragiles et non-fragiles,</p>\r\n	</li>\r\n	<li>\r\n	<p>- &nbsp;Démontage et remontage du mobilier,</p>\r\n	</li>\r\n	<li>\r\n	<p>- &nbsp;Remise en place du mobilier.</p>\r\n	</li>\r\n	<li>\r\n	<p>- &nbsp;Les équipes régulières DBEP sont compétentes pour la prise en charge d&rsquo;effets dont le poids n&rsquo;excède par 80kg. Quand le poids excède 80kg. (mobilier, piano, coffre...), le Client s&rsquo;engage à informer DBEP qui fera intervenir des équipes spécialisées (engendre un supplément).</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>- Passage par la fenêtre à l&rsquo;aide d&rsquo;un monte meubles avec technicien dans le cadre d&rsquo;une mise à disposition de moyens. Charge au Client de s&rsquo;assurer que le passage est possible et d&rsquo;avoir toutes les autorisations auprès du service voirie de votre commune. A la demande du Client, l&rsquo;organisation d&rsquo;une visite technique sur site pour valider la faisabilité est possible.</p>\r\n\r\n<p>Les présentes Conditions Générales de Vente et de Service (ci-après dénommées les &laquo; CGV &raquo;) convenues entre&nbsp;<strong>DES BRAS EN PLUS&nbsp;</strong>et le Client déterminent les droits et obligations de chacun d&rsquo;eux.</p>\r\n\r\n<p>Elles s&rsquo;appliquent de plein droit aux prestations de déménagement, et vente de produits relatifs au déménagement objet des présentes CGV.</p>\r\n\r\n<p>DBEP se réserve la possibilité de modifier ses CGV à tout moment. Les CGV applicables seront celles en vigueur à la date de la commande du Client.</p>\r\n\r\n<p>Si une condition venait à faire défaut, elle serait considérée être régie par les usages en vigueur.</p>\r\n\r\n<p><strong>Article 2 - Commande</strong></p>\r\n\r\n<p>2.1. Étapes de la commande</p>\r\n\r\n<p>1/ Pré-réservation</p>\r\n\r\n<p>Le Client a la possibilité de pré-réserver sa prestation de déménagement sur le site web www.desbrasenplus.com, ou par téléphone au 01.44.16.18.00 ou directement en agence DBEP.</p>\r\n\r\n<p>- Le Client choisit, personnalise son déménagement et paye en ligne, par téléphone ou directement en agence selon une tarification en temps réel.</p>\r\n\r\n<p>- DBEP adresse par courriel ou directement en main propre au Client en cas de pré-réservation en agence un reçu de pré-réservation avec le montant encaissé.</p>\r\n\r\n<p>2/ Confirmation de la réservation</p>\r\n\r\n<p>S&rsquo;agissant d&rsquo;une pré-réservation, la prestation de déménagement fait l&rsquo;objet d&rsquo;une confirmation de DBEP au Client par tous moyens.</p>\r\n\r\n<p>La commande est donc parfaite à réception de cette confirmation.<br />\r\n2.2. Modification / Annulation de la commande<br />\r\n1/ Modification de la commande<br />\r\nEn cas de changement de date du déménagement à la demande du Client :</p>\r\n\r\n<p>&bull; Le Client a la possibilité de demander à DBEP la modification de la date du déménagement sans frais s&rsquo;il en fait la demande au moins 10 (dix) jours ouvrés avant la date de son déménagement.</p>\r\n\r\n<p>&bull; Si le Client demande à DBEP la modification de la date du déménagement dans les 10 (dix) jours ouvrés avant la date du déménagement, les frais de modification à la charge du Client sont de 99 (quatre-vingt-dix-neuf) Euros TTC.</p>\r\n\r\n<p>Toute modification de la commande n&rsquo;est effective qu&rsquo;après confirmation écrite de DBEP au Client.</p>\r\n\r\n<p>La différence de prix entre la prestation initiale et la prestation modifiée ainsi que les éventuels frais supplémentaires sont dus à la confirmation de la modification de la commande par DBEP.</p>\r\n\r\n<p>2/ Annulation de la commande<br />\r\nDBEP perçoit l&rsquo;intégralité du prix de la prestation :<br />\r\n- En cas d&rsquo;annulation de la commande à l&rsquo;initiative du Client :</p>\r\n\r\n<p>&bull; Le Client a la faculté de résilier sans frais dans les 7 (sept) jours ouvrés suivants sa commande à condition qu&rsquo;il y ait un délai minimum de 10 (dix) jours ouvrés entre la date de la résiliation et la date de la réalisation de la prestation.</p>\r\n\r\n<p>&bull; Dans tous les autres cas, un montant forfaitaire de 99 (quatre-vingt dix-neuf) Euros sera retenu par DBEP qui remboursera le Client de la somme restante dans un délai de 10 (dix) jours ouvrés à compter de la notification par le Client de sa volonté d&rsquo;annulation de sa commande à DBEP .</p>\r\n\r\n<p>- En cas d&rsquo;annulation de la commande à l&rsquo;initiative de DBEP, DBEP restitue l&rsquo;intégralité des sommes perçues au titre de la prestation commandée par le Client.</p>\r\n\r\n<p><strong>Article 3 - Prix</strong></p>\r\n\r\n<p>Les prix des prestations de déménagement et des ventes des produits sont fournis aux prix en vigueur au moment de la passation de la commande exprimés en euros et tenant compte de la TVA applicable au jour de la commande. Tout changement du taux pourra être répercuté sur le prix des prestations ou des produits.</p>\r\n\r\n<p>Le Client et DBEP peuvent toutefois décider d&rsquo;un commun accord de modifier les dispositions prévues au contrat.</p>\r\n\r\n<p><strong>Article 4 - Paiement</strong></p>\r\n\r\n<p>L&rsquo;intégralité du prix de la prestation de déménagement est payée par le Client lors de la pré- réservation.</p>\r\n\r\n<p>S&rsquo;agissant de la vente des produits, le Client les paye à leur commande.<br />\r\nUne facture est remise au Client par courriel par DBEP après la prestation de déménagement.</p>\r\n\r\n<p><strong>Article 5 - Réalisation des prestations</strong></p>\r\n\r\n<p>Les prestations sont convenues avec le Client préalablement à chaque opération.</p>\r\n\r\n<p>5.1. Information sur les conditions de réalisation de la prestation</p>\r\n\r\n<p>A la demande de DBEP, le Client doit fournir toutes informations dont il a connaissance permettant la réalisation matérielle de la prestation, tant au lieu de chargement que de livraison (condition d&rsquo;accès pour le personnel et le véhicule, possibilité de stationnement, travaux en cours ou autre particularité).</p>\r\n\r\n<p>5.2. Réalisation par une tierce entreprise</p>\r\n\r\n<p>DBEP étant un réseau d&rsquo;entreprises indépendantes, le Client est informé que son déménagement sera réalisé par une des entreprises du réseau DBEP.</p>\r\n\r\n<p>Il est précisé que DBEP ou toute entreprise du réseau DBEP conserve la faculté de confier, sous son entière responsabilité, la réalisation de prestations totales ou partielles spécifiques du déménagement à une tierce entreprise dénommée &laquo; entreprise exécutante &raquo;, sans que la liste soit exhaustive, monte meuble, piano, objet très lourd....</p>\r\n\r\n<p>5.3. Présence obligatoire du Client<br />\r\nLe Client ou son mandataire doit être présent pendant toute la durée de la prestation.</p>\r\n\r\n<p>DBEP est en droit d&rsquo;exiger du Client la constatation par écrit de toute détérioration antérieure à la réalisation de la prestation.</p>\r\n\r\n<p>5.4. Contenus spécifiques</p>\r\n\r\n<p>DBEP n&rsquo;assume pas la prise en charge des personnes, des animaux, des végétaux, des matières dangereuses, infectes, explosives ou inflammables, des bijoux, monnaies, métaux précieux ou de valeur.</p>\r\n\r\n<p>Toute exception à cette règle doit faire l&rsquo;objet d&rsquo;un accord écrit entre DBEP et le Client avant la réalisation de la prestation.</p>\r\n\r\n<p>5.5. Déménagement à l&rsquo;heure : Dépassement de durée ou kilométrique</p>\r\n\r\n<p>Si la prestation effectuée par DBEP est une mise à disposition de déménageurs, de matériel(s) et de véhicule(s) pour une durée prédéterminée par le Client, tout dépassement non prévu au préalable mais souhaité par le Client ou rendu obligatoire par des circonstances particulières (impossibilité de décharger, sécurité des employés, respect des réglementations...) en cours de réalisation des prestations de déménagement, engendrera des coûts supplémentaires pour le Client précisés selon la nature du dépassement :</p>\r\n\r\n<p>&bull; Dépassement de durée : Tout dépassement de durée souhaité par le Client ou jugé nécessaire par DBEP selon les conditions exprimées ci-dessus donnera lieu à un supplément de 70 Euros TTC (soixante-dix Euros toutes taxes comprises) par heure et par déménageur (heure et nombre de déménageur non divisibles).</p>\r\n\r\n<p>&bull; Dépassement kilométrique : Tout dépassement kilométrique constaté par DBEP ou souhaité par le Client donnera lieu à un supplément de 4 Euros TTC (quatre Euros toutes taxes comprises) par kilomètre non divisible.</p>\r\n\r\n<p>En cours de réalisation de la prestation prévue initialement, le Client est averti des coûts supplémentaires engendrés par un éventuel dépassement souhaité par lui-même, ou rendu nécessaire (impossibilité de décharger, sécurité des employés, respect des réglementations...) Il aura la possibilité soit de faire arrêter le chargement, soit d&rsquo;accepter le dépassement selon les conditions de dépassement ci-dessus définies.</p>\r\n\r\n<p>Ces dépassements seront à régler avant 12 h le jour de la prestation et le cas échéant au plus tard avant la fin de la prestation par le Client à DBEP.</p>\r\n\r\n<p>5.5. Déménagement sur inventaire ou sur forfait de volume : Dépassement du nombre d&rsquo;éléments à transporter ou du volume</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>● &nbsp;inventaire établi en ligne ou par téléphone: Pour ces prestations, le client communique à des bras en plus un inventaire précis des éléments qu&rsquo;il souhaite déménager, une adresse précise de chargement et de livraison, ainsi que les accès pour accéder au lieu de chargement et de livraison. Le client recevra par courriel un récapitulatif de l&rsquo;opération et l&rsquo;inventaire des éléments à transporter.</p>\r\n	</li>\r\n	<li>\r\n	<p>● &nbsp;Après validation de ce devis, tout ajout d&#39;élément à transporter, sera soumis à un nouveau devis que le client sera libre d&rsquo;accepter ou de refuser. Un montant minimal de 75&euro;/m3 (non divisible) est applicable. Ce montant peut être révisé par DBEP en fonction des difficultés d&rsquo;accès, de la distance et des moyens (temps, humains, opérationnels) nécessaires pour la prise en charge du ou des éléments supplémentaires.</p>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<p>● &nbsp;La suppression par le Client d&rsquo;élément à transporter le jour de la prestation n&#39;entraîne pas de baisse du tarif</p>\r\n	</li>\r\n	<li>\r\n	<p>● &nbsp;En cas de panne d&rsquo;ascenseur, portage non signalé ou tout autre évènement modifiant les conditions d&rsquo;accès, les suppléments suivants peuvent être pris :</p>\r\n\r\n	<ul>\r\n		<li>\r\n		<p>○ &nbsp;160 &euro; HT par déménageur supplémentaire par demi journée (4h)</p>\r\n		</li>\r\n		<li>\r\n		<p>○ &nbsp;110 &euro; HT par véhicule utiliaire (&lt;3,5T)</p>\r\n		</li>\r\n		<li>\r\n		<p>○ &nbsp;290&euro; HT pour la mise a disposition d&rsquo;un monte meuble sur 4h</p>\r\n\r\n		<p><strong>Article 6 - Livraison du mobilier et formalités en cas de dommage</strong></p>\r\n\r\n		<p>6.1. Livraison du mobilier à domicile</p>\r\n\r\n		<p>Le Client est responsable de ses effets meubles et objets personnels durant toute la durée de la prestation. DBEP peut proposer au Client de souscrire une assurance dommage destinée à garantir le mobilier contre certains risques pour lesquels elle n&rsquo;assume légalement aucune responsabilité. Les modalités de cette assurance complémentaire sont communiquées au Client lors de la commande et peut être souscrite jusqu&rsquo;à 5 jours avant la date du déménagement.</p>\r\n\r\n		<p>A réception, le Client doit vérifier l&rsquo;état de son mobilier et en donner décharge dès la prestation terminée à l&rsquo;aide de la déclaration de fin de travail.</p>\r\n\r\n		<p>6.2. Livraison du mobilier au garde-meubles à la demande du Client</p>\r\n\r\n		<p>La livraison en garde-meubles est assimilée à une livraison à domicile et met fin au contrat de déménagement.</p>\r\n\r\n		<p>Les frais d&rsquo;entrée en garde-meubles sont distincts et facturés au Client par le garde-meubles qui assume la garde du mobilier.</p>\r\n\r\n		<p>6.3. Dépôt nécessaire par suite d&rsquo;empêchement à la livraison</p>\r\n		</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<p>En cas d&rsquo;absence du Client aux adresses, par lui indiquées, ou d&rsquo;impossibilité matérielle n&rsquo;étant pas le fait de DBEP, le mobilier est placé d&rsquo;office dans un garde-meuble, à la diligence de DBEP et aux frais du Client.</p>\r\n\r\n<p>Par tout moyen approprié, DBEP rend compte au Client de cette opération de dépôt, qui met fin au contrat de déménagement.</p>\r\n\r\n<p><strong>Article 7 - Responsabilité de DBEP</strong></p>\r\n\r\n<p>7.1. Responsabilité pour retard</p>\r\n\r\n<p>DBEP est tenue de réaliser la prestation à la date prévue dans la commande.</p>\r\n\r\n<p>DBEP ne peut s&rsquo;engager sur un horaire précis d&rsquo;arrivée de l&rsquo;équipe. Une tranche horaire prévisionnelle sera communiquée au Client (le plus souvent, arrivée entre 8h et 9h, ou entre 12h et 14h). En cas de délai inférieur à une heure par rapport au créneau annoncé, le client se verra remettre une indemnité forfaitaire de 15&euro; TTC. En cas de retard supérieur à une heure, le client se verra remettre une indemnité de 15% du montant de la commande (hors assurance ou option)</p>\r\n\r\n<p>7.2. Responsabilité pour pertes et avaries</p>\r\n\r\n<p>DBEP est responsable des meubles et objets qui lui sont confiés, sauf cas de force majeure, vice propre de la chose ou faute du Client.</p>\r\n\r\n<p>DBEP décline toute responsabilité en ce qui concerne les opérations qui ne seraient pas exécutées par ses préposés ou ses intermédiaires substitués.</p>\r\n\r\n<p>En cas de perte ou d&rsquo;avaries, pour sauvegarder ses droits et moyens de preuve, le Client a intérêt à émettre dès la fin de la prestation, en présence d&rsquo;un représentant de DBEP, des</p>\r\n\r\n<p>réserves précises écrites et détaillées. Que ces réserves aient été prises ou non, le Client doit adresser à DBEP, en cas de perte partielle ou d&rsquo;avaries, une lettre recommandée dans laquelle il décrit le dommage constaté.</p>\r\n\r\n<p>Ces formalités doivent être accomplies dans les dix jours calendaires à compter de la réception des objets transportés tel que prévu par l&rsquo;article L. 121-95 du Code de la consommation. A défaut, le Client est privé du droit d&rsquo;agir contre DBEP (article L. 133-3 du Code de commerce).</p>\r\n\r\n<p>Suivant la nature des dommages causés par la société, les pertes et avaries peuvent donner lieu à réparation, remplacement ou indemnité compensatrice (dans la limite de 300 &euro; au total, 70 &euro; par objet).</p>\r\n\r\n<p>Le Client peut souscrire une assurance, suivant la nature des dommages, les pertes et avaries donnent lieu à réparation, remplacement ou indemnité compensatrice selon les conditions de l&#39;assurance souscrite.</p>\r\n\r\n<p>7.3. Prescription</p>\r\n\r\n<p>Les actions en justice pour avaries, perte ou retard auxquelles peut donner lieu le contrat de déménagement doivent être intentées dans l&rsquo;année qui suit la livraison du mobilier (article L. 133-6 du Code de commerce).</p>\r\n\r\n<p><strong>Article 8 - Loi applicable / Compétence</strong></p>\r\n\r\n<p>Les présentes CGV sont soumises au droit français.<br />\r\nTous les litiges pouvant découler des présentes CGV seront soumis aux tribunaux compétents.&nbsp;</p>\r\n\r\n<p>----</p>\r\n\r\n<p><strong>Donn&eacute;es personnelles :</strong></p>\r\n\r\n<p>Les informations recueillies &agrave; partir de ce site font l&rsquo;objet d&rsquo;un traitement informatique destin&eacute;&nbsp;&agrave; :<strong>Des bras en plus.&nbsp;</strong>Pour la ou les finalit&eacute;(s) suivante(s) :&nbsp;<strong>Gestion de la client&egrave;le.&nbsp;</strong>Le ou les&nbsp;destinataire(s) des donn&eacute;es sont :</p>\r\n\r\n<p><strong>le service commercial et exploitation.&nbsp;</strong>Conform&eacute;ment &agrave; la&nbsp;loi &laquo; informatique et libert&eacute;s &raquo; du 6 janvier 1978 modifi&eacute;e, vous disposez d&rsquo;un&nbsp;droit d&rsquo;acc&egrave;s&nbsp;et&nbsp;de rectification&nbsp;aux informations qui vous concernent.&nbsp;</p>\r\n\r\n<p>Vous pouvez acc&egrave;der aux informations vous concernant&nbsp;en vous adressant &agrave;&nbsp;: <strong>example@example.com</strong></p>\r\n\r\n<p>Vous pouvez &eacute;galement, pour des motifs l&eacute;gitimes,&nbsp;vous opposer&nbsp;au traitement des donn&eacute;es vous concernant.</p>\r\n\r\n<p>Pour en savoir plus,&nbsp;consultez&nbsp;vos droits sur le site de la CNIL.</p>\r\n');

/*Table structure for table `translation` */

DROP TABLE IF EXISTS `translation`;

CREATE TABLE `translation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

/*Data for the table `translation` */

insert  into `translation`(`id`,`key`) values (56,'additional_service'),(51,'address_arrive'),(52,'address_chargement'),(53,'Address_dechargement'),(50,'address_depart'),(31,'arrive'),(80,'assemble'),(79,'assembly'),(47,'auto_info'),(48,'auto_type'),(67,'available'),(60,'carrying_distance'),(66,'client_stignature'),(74,'comments'),(41,'date'),(78,'debale'),(71,'demonte'),(30,'depart'),(70,'dismantling'),(49,'distance'),(72,'emballage'),(54,'escale_1'),(55,'escale_2'),(63,'etage_count'),(45,'finishing_hour'),(1,'first_word'),(59,'halteverbotszone'),(76,'lost'),(2,'month_01'),(3,'month_02'),(4,'month_03'),(6,'month_04'),(8,'month_05'),(9,'month_06'),(10,'month_07'),(11,'month_08'),(12,'month_09'),(13,'month_10'),(14,'month_11'),(15,'month_12'),(43,'nombre_demenageur'),(68,'object'),(57,'ordered'),(73,'packed'),(28,'page_1_header'),(82,'page_7_comment'),(83,'page_7_comment_point1'),(84,'page_7_comment_point2'),(85,'page_7_comment_point3'),(86,'page_sign'),(39,'pdf_3page_header'),(40,'pdf_info_rel'),(33,'pdf_items'),(35,'pdf_items_list_text'),(34,'pdf_list_items'),(38,'pdf_product_count'),(36,'pdf_product_name'),(37,'pdf_product_unit'),(61,'planned'),(69,'prior'),(58,'put_in_place'),(62,'Real'),(75,'received'),(42,'reference_number'),(32,'service'),(65,'signature'),(64,'signature_text'),(44,'starting_hour'),(81,'stricken'),(77,'unpacking'),(29,'user_name'),(46,'volume'),(16,'week_day_0'),(19,'week_day_1'),(22,'week_day_2'),(23,'week_day_3'),(25,'week_day_4'),(26,'week_day_5'),(27,'week_day_6');

/*Table structure for table `translation_label` */

DROP TABLE IF EXISTS `translation_label`;

CREATE TABLE `translation_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `translation_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `translation_id` (`translation_id`),
  CONSTRAINT `translation_label_ibfk_1` FOREIGN KEY (`translation_id`) REFERENCES `translation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

/*Data for the table `translation_label` */

insert  into `translation_label`(`id`,`translation_id`,`language_id`,`value`) values (1,1,1,'Test\r\n'),(2,2,1,'<p>January</p>\r\n'),(3,3,1,'<p>February</p>\r\n'),(4,4,1,'<p>March</p>\r\n'),(5,6,1,'<p>April</p>\r\n'),(6,8,1,'<p>May</p>\r\n'),(7,9,1,'<p>June</p>\r\n'),(8,10,1,'<p>July</p>\r\n'),(9,11,1,'<p>August</p>\r\n'),(10,12,1,'<p>September</p>\r\n'),(11,13,1,'<p>October</p>\r\n'),(12,14,1,'<p>November</p>\r\n'),(13,15,1,'<p>December</p>\r\n'),(14,16,1,'<p>Sunday</p>\r\n'),(15,19,1,'<p>Monday</p>\r\n'),(16,22,1,'<p>Tuesday</p>\r\n'),(17,23,1,'<p>Wednesday</p>\r\n'),(18,25,1,'<p>Thursday</p>\r\n'),(19,26,1,'<p>Friday</p>\r\n'),(20,27,1,'<p>Saturday</p>\r\n'),(21,28,1,'Aperçu du service:\r\n'),(22,29,1,'Donnees personnelles'),(23,30,1,'Départ'),(24,31,1,'Arrivée'),(25,32,1,'Service'),(26,33,1,'ITEMS'),(27,34,1,'Liste de colisage'),(28,35,1,'Assurez-vous que toutes les prestations requises sont bien mentionnées.'),(29,36,1,'nom'),(30,37,1,'Unité'),(31,38,1,'Nombre'),(32,39,1,'LETTRE DE VOITURE'),(33,40,1,'Informations relatives au déménagement'),(34,41,1,'date'),(35,42,1,'N° de reference Futurama:'),(36,43,1,'Nombre de déménageurs présents:'),(37,44,1,'Heure de début:'),(38,45,1,'Heure de fin:'),(39,46,1,'Volume transporté:'),(40,47,1,'Immatriculation véhicule:'),(41,48,1,'Type de véhicule:'),(42,49,1,'Distance:'),(43,50,1,'ADRESSE DE DÉPART'),(44,51,1,'ADRESSE D´ARRIVÉE'),(45,52,1,'Adresse de chargement'),(46,53,1,'Adresse de déchargement'),(47,54,1,'ESCALE 1 '),(48,55,1,'ESCALE 2'),(49,56,1,'Service supplémentaire'),(50,57,1,'Ordered'),(51,58,1,'Mis en place'),(52,59,1,'Halteverbotszone'),(53,60,1,'Distance de portage'),(54,61,1,'Prévue'),(55,62,1,'Réelle'),(56,63,1,'Etage à monter'),(57,64,1,'Nous confirmons que les informations mentionnées ci-dessus sont exactes'),(58,65,1,'Signature du déménageur*:'),(59,66,1,'Signature du client:'),(60,67,1,'Disponible'),(61,68,1,'Objet'),(62,69,1,'Dégâts préalables'),(63,70,1,'Démontage'),(64,71,1,'Démonté'),(65,72,1,'Emballage'),(66,73,1,'Emballé'),(67,74,1,'Commentaires'),(68,75,1,'Reçu'),(69,76,1,'Perdu'),(70,77,1,'Déballage'),(71,78,1,'Déballé'),(72,79,1,'Assemblage'),(73,80,1,'Assemblé'),(74,81,1,'sinistré'),(75,82,1,'Veuillez indiquer ici:'),(76,83,1,'Les éléments déménagés qui ne figuraient pas sur la liste de colisage (par exemple: 10 cartons en plus, un fauteuil en plus, etc.)'),(77,84,1,'Tout service supplémentaire fourni par l´équipe de déménagement (par exemple: emballage, utilisation d´un monte-charge, etc.)'),(78,85,1,'Tout dommage causé pendant le déménagement'),(79,86,1,'*Les objets déménagés mentionnés dans la lettre de voiture, ont été pris par le partenaire en bon état et ont été livrés endommagés. Futurama tient le partenaire comme responsable pour tous dégâts et autres coûts occasionnés.');

/*Table structure for table `vehicle` */

DROP TABLE IF EXISTS `vehicle`;

CREATE TABLE `vehicle` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(250) NOT NULL,
  `history_image` varchar(250) NOT NULL,
  `vehicle_volume` varchar(250) NOT NULL,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `vehicle` */

insert  into `vehicle`(`id`,`image`,`history_image`,`vehicle_volume`,`price`) values (12,'a1cf41e430a4070ba59c5a071a51db91.png','6379af641bf9a0cdf3fd9b0b505a0c0d.png','0',5.00),(13,'3ec18ad42bcaf727bad449aad4054f41.png','2448abe8a8f04c7dfbb5d910604a5cd8.png','12',0.00),(14,'4259e88405f8c9a84a869d4e516fffe8.png','acd773285f20b4e58c6cf2d49313bdf9.png','22',0.00),(15,'96bd09c5c84ec9b9c5d34abe12264308.png','c3cda9d4bb94b721f47b110481a2da4e.png','40',0.00);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
