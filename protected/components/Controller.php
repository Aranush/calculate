<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
define('bDIR', dirname(Yii::app()->request->scriptFile));
define('UPLOADS', bDIR."/upload/");

class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	public $menu=array();
	public $breadcrumbs=array();

	public $currentLanguage ;
	public $currentLangIso ;
	public $languagesModel;
	public $langList;
	public $langId;

	public $translationModel;
	public $translationLabel;
	public $translation;

	public $msg_permission;
	public $companies;

	public function __construct($id,$module=null){
		parent::__construct($id,$module);


		if(isset($_POST['language'])) {
			$lang = $_POST['language'];
			$MultilangReturnUrl = $_POST[$lang];
			$this->redirect($MultilangReturnUrl);
		}
		if(isset($_GET['language'])) {
			Yii::app()->language = $_GET['language'];
			Yii::app()->user->setState('language', $_GET['language']);
			$cookie = new CHttpCookie('language', $_GET['language']);
			$cookie->expire = time() + (60*60*24*365); // (1 year)
			Yii::app()->request->cookies['language'] = $cookie;
		}
		else if (Yii::app()->user->hasState('language'))
			Yii::app()->language = Yii::app()->user->getState('language');
		else if(isset(Yii::app()->request->cookies['language']))
			Yii::app()->language = Yii::app()->request->cookies['language']->value;


	}
	public function createMultilanguageReturnUrl($lang='en'){
		if (count($_GET)>0){
			$arr = $_GET;
			$arr['language']= $lang;
		}
		else
			$arr = array('language'=>$lang);
		return $this->createUrl('', $arr);
	}
	public function beforeAction($action){

		$this->languagesModel = Language::model()->findAllByAttributes(array('status' => 0));


		foreach($this->languagesModel AS $value) {
			$this->langList[$value->iso] = $value->name;
		}

		if(isset($_GET['language'])){
			foreach($this->languagesModel AS $value) {
				if($value->iso == $_GET['language']) {
					$this->langId = $value->id;
					break;
				}
			}
		}else{
			$def_lang=Yii::app()->language;

			foreach($this->languagesModel AS $value) {
				if($value->iso == $def_lang) {
					$this->langId = $value->id;
					break;
				}
			}
		}


		$this->translationLabel = TranslationLabel::model()->with('translation')->findAllByAttributes(array("language_id" => $this->langId));
		if(!empty($this->translationLabel)) {
			foreach ($this->translationLabel AS $value) {
				$this->translation[$value->translation->key] = $value->value;
			}
		}


		if(isset($_GET['language'])){
			if(isset($this->langList)){
				foreach($this->langList  as $key => $value) {
					if ($key == $_GET['language']) {
						$this->currentLanguage = $value;
						$this->currentLangIso = $key;
					}
				}
			}
		}else{
			$this->currentLanguage = Yii::app()->language;
			if(isset($this->langList)) {
				foreach ($this->langList as $key => $value) {
					if ($key == $this->currentLanguage) {
						$this->currentLanguage = $value;
						$this->currentLangIso = $key;
					}
				}
			}
		}



		return parent::beforeAction($action);

	}
}