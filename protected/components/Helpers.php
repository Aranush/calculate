<?php
class Helpers {




    public static function dump($params=array()){

        $bt = debug_backtrace();
        $caller = array_shift($bt);

        echo "<pre>";
        print_r($params);
        print_r("\nCalled in ".$caller['file'].", at line".$caller['line']."\n");
        echo "</pre>";
    }


    public static function languages(){
        $languages = Language::model()->findAll();
        return $languages;
    }

    public static function date(){
        return date('Y-m-d H:i:s');
    }

    function Name($data,$key,$value)
    {
        $name = CHtml::listData($data->$key, 'id', $value);
        return implode('<br/><br/>', $name);
    }


    public static  function uniqueAssocArray($array, $uniqueKey) {
        if (!is_array($array)) {
            return array();
        }

        $uniqueKeys = array();
        foreach ($array as $key => $item) {
            foreach($item as $k=>$v)
                if (!in_array($v->$uniqueKey, $uniqueKeys)) {
                    $uniqueKeys[] = $v->id;
                }
        }
        return $uniqueKeys;
    }



    public function get_random_string($length)
    {
        // start with an empty random string
        $random_string = "";
        $numbers = range(0,9);
        $letters =  range('A', 'Z');
        $valid_chars = array_merge($letters,$numbers);
//        print_r($valid_chars);die;
        // count the number of chars in the valid chars string so we know how many choices we have
//        $num_valid_chars = strlen($valid_chars);

        // repeat the steps until we've created a string of the right length
        for ($i = 0; $i < $length; $i++)
        {
            // pick a random number from 1 up to the number of valid chars
            $random_pick = array_rand($valid_chars,1);

            // take the random character out of the string of valid chars
            // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
            $random_char = $valid_chars[$random_pick];

            // add the randomly-chosen char onto the end of our string so far
            $random_string .= $random_char;
        }

        // return our finished random string
        return $random_string;
    }


    function createDateRangeArray($strDateFrom,$strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange=array();

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }

}