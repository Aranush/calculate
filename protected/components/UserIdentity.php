<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$admin = Admin::model()->findByAttributes(array('username'=>$this->username));

		if(!empty($admin)){
			if($admin->password == $this->password){
				$this->setState('name', $admin->username);
				$this->errorCode = self::ERROR_NONE;
				return !$this->errorCode;
			}
		}elseif($admin->password !== $this->password)
		{
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}
	}
}