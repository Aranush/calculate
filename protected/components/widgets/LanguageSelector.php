<?php
class LanguageSelector extends CWidget
{
    public function run()
    {
        $currentLang = Yii::app()->language; //default language

        $languages =Languages::model()->findAll();

        $this->render('languageSelector', array('currentLang' => $currentLang, 'languages'=>$languages));

    }
}
?>