<?php

class SiteController extends Controller
{



    public function actionSendMail(){
        if(!empty($_POST)){
            $a = array();
            Yii::app()->session['user'] = array();
            foreach ($_POST as $key =>$value){
                $a[$key] = $value;
            }
            Yii::app()->session->add("user", $a);

        }
        $model = new Orders();
        Yii::app()->session['invoiceNumber'] = $this->get_random_string(10);
        $model->invoice_number = Yii::app()->session['invoiceNumber'];
        while(!$model->save()){
            Yii::app()->session['invoiceNumber'] = $this->get_random_string(10);
            $model->invoice_number = Yii::app()->session['invoiceNumber'];
            $model->save();
        }

        require_once(bDIR."/protected/components/mpdf60/mpdf.php");
        $mpdf=new mPDF();
        $strVar= '<!DOCTYPE html>';
        $strVar .=   '<html lang="en">';
        $strVar .=   '<head>';
        $strVar .=   '<meta charset="UTF-8">';
        $strVar .=   '<title>Title</title>';
        $strVar .=   '</head>';
        $strVar .=   '<body style="padding: 0;margin: 0">';
        $strVar .=   '<div style="width: 100%; margin: auto; padding: 10px;">';
        $strVar .=   '<div style="width: 40%;float:right;">';
        $strVar .=   '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .=   '<table>';
        $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
        $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">Hotline client:</td>     <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">01 84 88 52 12</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
        $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">Lundi - Vendredi:</td>   <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">09:00 - 21:00</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
        $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">Samedi - Dimanche:</td>  <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">10:00 - 18:00</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
        $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">E-Mail:</td>             <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">service_fr@move24.fr</td>';
        $strVar .=   '</tr>';
        $strVar .=   '</table>';
        $strVar .=   '</div>';

        $strVar .=   '<div style="margin-top: 130px; float: left; width: 100%">';
        $strVar .=   '<p style="color: #616161;font-size: 14px;font-weight: bold;font-family: Sans-Serif">' . $this->translation['page_1_header'] . ' Ref '. Yii::app()->session['invoiceNumber'] .'</p>';
        $strVar .=   '<p style="font-family: sans-serif; color: #797979;"><span style="display: block">1. '. $this->translation['service'].' <span style="text-transform: capitalize">'.Yii::app()->session['tarif']['name'].'</span> (Volume Total: '.Yii::app()->session['volume'].' m<sup>3</sup>)</span>';
//        $strVar .=   '<span style="display: block">Chargement*, D?chargement*, Transport**,</span>';
//        $strVar .=   '<span style="display: block">Assurance***</span>';
        $strVar .=   '</p>';
        $strVar .=   '<div style="float:left;width:100%"><p style="color: #616161;margin: 0;margin-top: 10px;font-size: 16px;font-weight: bold;font-family: Sans-Serif"> '. $this->translation['user_name'] .'</p>';

        $strVar .=   '<hr style ="border: 0;border-bottom: 1px solid #858585;border-top: 1px solid #b5b5b5;"/>';

        $strVar .=   '<div style="width: 50%;float: left;">';
        $strVar .=   '<table>';
        $strVar .=   '<tr >';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;width:130px;">Nom:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;min-width:130px;">'.Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'].'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '</table>';
        $strVar .=   '</div>';
        $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
        $strVar .=   '</div></div>';

        $strVar .=   '<div style="float:left;width:100%"><p style="color: #616161;font-size: 16px;margin: 0;margin-top: 10px;font-weight: bold;font-family: Sans-Serif">'  . $this->translation['depart'] . '</p>';

        $strVar .=   '<hr style ="border: 0;border-bottom: 1px solid #858585;border-top: 1px solid #b5b5b5;"/>';

        $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
        $strVar .=   '<table style="margin-bottom: 16px;">';
        $strVar .=   '<tr >';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;width:130px;">Date:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' . Yii::app()->session['date'] . '</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr>';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Rue:</td>   <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'. explode(",",Yii::app()->session['depart']['adresse_depart'])[0] .'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr>';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Ville:</td>  <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['depart']['cp_depart'].' '.Yii::app()->session['depart']['ville_depart'].'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr>';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Type:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['depart']['type'].'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '</table>';
        $strVar .=   '</div>';
        $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
        if(isset(Yii::app()->session['maison_depart'])){
            $strVar .=   '<table style="margin-bottom: 16px;">';
            foreach( Yii::app()->session['maison_depart'] as $key=> $value){
                $strVar .=   '<tr >';
                $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.$key.':</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.$value.'</td>';
                $strVar .=   '</tr>';
            }
            $strVar .=   '</table>';
        }
        $strVar .=   '</div>';

        $strVar .=   '</div>';
        $strVar .=   '<div style="float:left;width:100%"><p style="color: #616161;font-size: 16px;margin: 0;margin-top: 10px;font-weight: bold;font-family: Sans-Serif">' . $this->translation['arrive'] . '</p>';

        $strVar .=   '<hr style ="border: 0;border-bottom: 1px solid #858585;border-top: 1px solid #b5b5b5;"/>';

        $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
        $strVar .=   '<table style="margin-bottom: 16px;">';
        $strVar .=   '<tr >';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;width:130px;">Date:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' . Yii::app()->session['date'] . '</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr>';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Rue:</td>   <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'. explode(",",Yii::app()->session['arrive']['adresse_arrivee'])[0] .'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr>';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Ville:</td>  <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'. Yii::app()->session['arrive']['cp_arrivee'] .'  '.explode(",",Yii::app()->session['arrive']['adresse_arrivee'])[1].'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '<tr>';
        $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Type:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['arrive']['type'].'</td>';
        $strVar .=   '</tr>';
        $strVar .=   '</table>';
        $strVar .=   '</div>';
        $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
        $strVar .=   '<table style="margin-bottom: 16px;">';
        if(isset(Yii::app()->session['maison_arrive'])){
            foreach( Yii::app()->session['maison_arrive'] as $key=> $value){
                $strVar .=   '<tr >';
                $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.$key.':</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.$value.'</td>';
                $strVar .=   '</tr>';
            }
        }

        $strVar .=   '</table>';

        $strVar .=   '</div>';

        $strVar .=   '</div>';

        $strVar .=   '</div></div>';

//        Helpers::dump($strVar);die;
        $mpdf->WriteHTML($strVar);

        $strVar .= '<div style="width: 100%; margin:120px 0 500px 0; padding: 10px;">';
        $strVar .= '<div style="width: 40%;float:right;">';
        $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .= '</div>';
        $strVar .= '<div style="margin-top: 130px; float: left; width: 100%">';
        $strVar .= '<p style="font-family: sans-serif; color: #797979;"><span style="display: block">1. ' . $this->translation['service'] . ' <span style="text-transform: capitalize">'.Yii::app()->session['tarif']['name'].'</span></span></p>';
        $strVar .= '<div style="float: left; width: 100%;border: 2px solid #858585">';
        $strVar .= '<p style="font-family: sans-serif; color: #797979; margin: 5px;font-size: 14px"> ' .  $this->translation['pdf_items'] . ' : ';
        foreach($_SESSION['selectedProducts'] as $key => $value){
            foreach($value as $k =>$v){
                $strVar .=  '<span>'. $v["value"].' '.$v["name"].'  </span>';
            }
        }
        $strVar .=  '</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .=  '<div style="width: 100%; margin: auto; padding: 30px;">';
        $strVar .=  '<div style="width: 40%;float:right;">';
        $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .=  '</div>';
        $strVar .=  '<p style="margin-top: 130px; float: left; width: 80%">';
        $strVar .=  '<p style="color: #616161;width:100%;font-size: 16px;font-weight: bold;font-family: Sans-Serif"> ' . $this->translation ['pdf_list_items'] . ' </p>';
        $strVar .=  '<hr style ="border: 0; border-bottom:2px solid #264d9c; border-top:2px solid #264d9c; color: #264d9c;height: 2px;background-color: #264d9c"/>';
        $strVar .=  '<p style="font-family: sans-serif; color: #797979;"> ' . $this->translation ['pdf_items_list_text'] . ' </p>';
        $strVar .=  '</p>';
        $strVar .=  '<table style="width: 100%;border-spacing:0">';
        $strVar .=  '<tr style="max-height: 20px;">';
        $strVar .=  '<td style="border: 0; color: #264d9c;font-family: sans-serif">  ' . $this->translation ['pdf_product_name'] . ' </td><td style="border: 0; color: #264d9c;font-family: sans-serif">' . $this->translation['pdf_product_unit'] . '</td><td style="font-family: sans-serif;border: 0; color: #264d9c;"> ' . $this->translation['pdf_product_count'] . ' </td>';
        $strVar .=  '</tr>';
        foreach ($_SESSION['selectedProducts'] as $key =>$value){
            $strVar .=  '<tr style="">';
            $strVar .=  '<td style="height: 5px;font-family: sans-serif;color: #5b5b5b;background-color: #efefef;border-bottom:1px solid #000; font-weight: bold">'.$key.'</td><td style="height: 5px;border-bottom:1px solid #000;font-family: sans-serif;color: #5b5b5b;background-color: #efefef; font-weight: bold"></td><td style="height: 5px;border-bottom:1px solid #000;font-family: sans-serif;color: #5b5b5b;background-color: #efefef; font-weight: bold"></td>';
            $strVar .=  '</tr>';

            foreach ($value as $k=>$v){
                $strVar .=  '<tr style="max-height: 20px;">';
                $strVar .=  '<td style="font-family: sans-serif;color: #5b5b5b;border-bottom:1px solid #000;">'. $v['name'] .'</td>   <td style="font-family: sans-serif;border-bottom:1px solid #000;color: #5b5b5b;"></td> <td style="font-family: sans-serif;border-bottom:1px solid #000;color: #5b5b5b;">'.$v['value'].'</td>';
                $strVar .=  '</tr>';
            }
        }
        $strVar .=  '</table>';
        $strVar .=  '</div>';
        $strVar .=  '</body>';
        $strVar .=  '</html>';

//        Helpers::dump($strVar);


        $mpdf=new mPDF();
        $mpdf->WriteHTML($strVar);


        $strVar ='';

        $strVar .= '<!DOCTYPE html>';
        $strVar .= '<html lang="en">';
        $strVar .= '<head>';
        $strVar .= '<meta charset="UTF-8">';
        $strVar .= '<title>Title</title>';
        $strVar .= '</head>';
        $strVar .= '<body style="padding: 0;margin: 0">';
        $strVar .= '<div style="width: 100%;margin: auto; padding: 10px;">';
        $strVar .= '<div style="width: 60%;float:left;">';
        $strVar .= '<p style="font-family: sans-serif;text-transform: uppercase;font-size: 23px;color: #3c3c3d;font-weight: bold;">' . $this->translation['pdf_3page_header'] . '</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 15%;float:right;">';
        $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;border: 2px solid #000;float: left">';
        $strVar .= '<div style="width: 14%;padding: 7px 0 0 5px;float: left;">';
        $strVar .= '<p style="font-size: 12px;font-weight: bold;padding: 0;margin: 0;" >' . $this->translation['pdf_info_rel'] . '</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 27%;padding: 0 5px;float: left;font-size: 12px">';
        $strVar .= '<table style="width: 100%; border-spacing:0">';
        $strVar .= '<tr >';
        $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-family: Sans-serif;font-size: 10px;">' . $this->translation['date'] . '</td>     <td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-family: Sans-serif">' . Yii::app()->session['date'] . '</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">' . $this->translation['reference_number'] . '</td>   <td style="border-bottom: 1px solid #000; font-weight: bold;font-family: Sans-serif">'. Yii::app()->session['invoiceNumber'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">'. $this->translation['nombre_demenageur'] .'</td>  <td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-family: Sans-serif"></td>';
        $strVar .= '</tr>';
        $strVar .= '</table>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 27%;padding: 0 5px;float: left;font-size: 12px">';
        $strVar .= '<table style="width: 100%; border-spacing:0">';
        $strVar .= '<tr >';
        $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">'. $this->translation['starting_hour'] .'</td><td style="font-family: Sans-serif;padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">'. $this->translation['finishing_hour'] .'</td><td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">' . $this->translation['volume'] . '</td><td style="padding: 5px;border-bottom: 1px solid #000;font-family: Sans-serif">'.Yii::app()->session['volume'].'m<sup>3</sup></td>';
        $strVar .= '</tr>';
        $strVar .= '</table>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 27%;padding: 0 5px;float: left;font-size: 12px">';
        $strVar .= '<table style="width: 100%; border-spacing:0">';
        $strVar .= '<tr >';
        $strVar .= '<td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-size: 10px;font-family: Sans-serif">' . $this->translation['auto_info'] . '</td><td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-family: Sans-serif"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-size: 10px;font-family: Sans-serif">' . $this->translation['auto_type'] . '</td><td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-family: Sans-serif" ></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-size: 10px;font-family: Sans-serif">' . $this->translation['distance'] . '</td><td style="padding: 5px;border-bottom: 1px solid #000;font-family: Sans-serif">3km</td>';
        $strVar .= '</tr>';
        $strVar .= '</table>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 55%;float: left;font-family: sans-serif;">';
        $strVar .= '<p style="text-transform: uppercase; font-family: sans-serif;color: #264d9c;font-weight: bold;">'. $this->translation['address_depart'] .'</p>';
        $strVar .= '<p style="font-size: 14px; color: #676767;">';
        $strVar .= '<span style="display: block;font-weight: bold;">' .$this->translation['address_chargement']. '</span> <br />';
        $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['depart']['adresse_depart'].'</span><br />';
        $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['depart']['cp_depart'].' '. Yii::app()->session['depart']['ville_depart'] .' </span> <br />';
        $strVar .= '<span style="display: block;width: 100%">'. explode(",", Yii::app()->session['depart']['adresse_depart'])[2] .'</span> <br />';
//        $strVar .= '<span style="display: block">Etage:10</span>';
        $strVar .= '</p>';
        $strVar .= '<p style="text-transform: uppercase; font-family: sans-serif;color: #264d9c;font-weight: bold;"> ' . $this->translation['escale_1'] . ' </p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;float: right;font-family: sans-serif;">';
        $strVar .= '<p style="text-transform: uppercase; display:block;font-family: sans-serif;color: #264d9c;font-weight: bold;">'. $this->translation['address_arrive'] .'</p>';
        $strVar .= '<p style="font-size: 14px; color: #676767;">';
        $strVar .= '<span style="display: block;font-weight: bold;">'. $this->translation['Address_dechargement'] .'</span><br />';

        $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['arrive']['adresse_arrivee'].'</span><br />';
        $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['arrive']['cp_arrivee'].' '. explode(",", Yii::app()->session['arrive']['adresse_arrivee'])[1] .' </span><br />';
        $strVar .= '<span style="display: block;width: 100%">'. explode(",", Yii::app()->session['arrive']['adresse_arrivee'])[2] .'</span> <br />';
//        $strVar .= '<span style="display: block">Etage:10</span>';
        $strVar .= '</p>';
        $strVar .= '<p style="text-transform: uppercase; font-family: sans-serif;color: #264d9c;font-weight: bold;">'. $this->translation['escale_2'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="float: left;margin-top: 30px;margin-bottom:20px;width: 100%">';
        $strVar .= '<div style="width: 55%;float: left;font-size: 14px">';
        $strVar .= '<table style="margin-bottom: 10px;border-spacing:0">';
        $strVar .= '<tr style="background-color: #e4e1e0;">';
        $strVar .= '<td style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">' . $this->translation['additional_service'] . '</td><td style="font-weight: 100">' . $this->translation['ordered'] . '</td><td style="font-weight: 100">' . $this->translation['put_in_place'] . '</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['halteverbotszone'] .'</td><td style ="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr style="background-color: #e4e1e0;">';
        $strVar .= '<th style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">'. $this->translation['carrying_distance'] .'</th><th style="font-weight: 100">' . $this->translation['planned'] . '</th><th style="font-weight: 100">' . $this->translation['Real'] . '</th>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['distance'] .'</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border-bottom:1px solid #000;">' . $this->translation['etage_count'] . '</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
        $strVar .= '</tr>';
        $strVar .= '</table>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;float: right;font-size: 14px">';
        $strVar .= '<table style="margin-bottom: 10px;border-spacing:0">';
        $strVar .= '<tr style="background-color: #e4e1e0;">';
        $strVar .= '<td style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">' . $this->translation['additional_service'] . '</td><td style="font-weight: 100">' . $this->translation['ordered'] . '</td><td style="font-weight: 100">' . $this->translation['put_in_place'] . '</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['halteverbotszone'] .'</td><td style ="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr style="background-color: #e4e1e0;">';
        $strVar .= '<th style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">'. $this->translation['carrying_distance'] .'</th><th style="font-weight: 100">' . $this->translation['planned'] . '</th><th style="font-weight: 100">' . $this->translation['Real'] . '</th>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['distance'] .'</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border-bottom:1px solid #000;">' . $this->translation['etage_count'] . '</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
        $strVar .= '</tr>';
        $strVar .= '</table>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;float: left;position: relative">';
        $strVar .= '<div style="width: 100%;bottom: 30px;">';
        $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
        $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] .'</span></li>';
        $strVar .= '</ul>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
        $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display: inline;float: right;padding-right: 15px;"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] . '</span></li>';
        $strVar .= '</ul>        </div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .= '<div style="width: 100%; margin: auto; padding: 10px;">';
        $strVar .= '<div style="width: 60%;float:left;">';
        $strVar .= '<p style="font-family: sans-serif;text-transform: uppercase;font-size: 23px;color: #3c3c3d;font-weight: bold;">lettre de voiture</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 15%;float:right;">';
        $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;float: left;position: absolute;height: 100%">';
        $strVar .= '<div style="width: 55%;float: left;">';
        $strVar .= '<table style="border-spacing:0;margin-bottom: 100px">';
        $strVar .= '<tr>';
        $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;width: 140px;font-weight:100;text-align: left">';
        $strVar .= $this->translation['object'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['available'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['prior'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['dismantling'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['demonte'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['emballage'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['packed'];
        $strVar .= '</th>';
        $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;font-weight:100;width: 150px;text-align: center">';
        $strVar .= $this->translation['comments'];
        $strVar .= '</th>';
        $strVar .= '</tr>';
        foreach($_SESSION['selectedProducts'] as $key=>$value){
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0;"> '. $key .' </td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;"></td>';
            $strVar .= '</tr>';

            foreach($value as $k=>$v){
                for($i=0;$i<$v['value'];$i++){
                    $strVar .= '<tr >';
                    $strVar .= '<td style="border-bottom:1px solid #000"> '.$v['name'].' </td>';
                    $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
                    $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000"></td>';
                    $strVar .= '</tr>';
                }
            }

        }
        $strVar .= '</table >';
        $strVar .= '</div>';


        $strVar .= '<div style="width: 42%;float: right;">';
        $strVar .= '<table style="border-spacing:0;margin-top: 4px;margin-bottom: 100px ">';
        $strVar .= '<tr>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['received'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['lost'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['unpacking'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['debale'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['assembly'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['assemble'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['stricken'];
        $strVar .= '</th>';
        $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;font-weight:100;width: 150px;text-align: center">';
        $strVar .= $this->translation['comments'];
        $strVar .= '</th>';
        $strVar .= '</tr>';
        foreach($_SESSION['selectedProducts'] as $key=>$value){
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';

            foreach($value as $k=>$v){
                for($i=0;$i<$v['value'];$i++){
                    $strVar .= '<tr >';
                    $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
                    $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
                    $strVar .= '</tr>';
                }
            }

        }
        $strVar .= '</table>';
        $strVar .= '</div>';

        $strVar .= '<div style="width: 3%;float: left;">';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;float: left;">';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;float: left;position: relative">';
        $strVar .= '<div style="width: 100%;bottom: 30px;">';
        $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
        $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] .'</span></li>';
        $strVar .= '</ul>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
        $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display: inline;float: right;padding-right: 15px;"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] . '</span></li>';
        $strVar .= '</ul>        </div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';



        $strVar .= '<div style="width: 100%; margin: auto;">';
        $strVar .= '<div style="width: 15%;float:right;">';
        $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;float: left;position: absolute;height: 100%">';
        $strVar .= '<div style="width: 55%;float: left;margin-top: -25px;">';
        $strVar .= '<table style="border-spacing:0;width:100%;">';
        $strVar .= '<tr>';
        $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;width: 140px;font-weight:100;text-align: left">';
        $strVar .= $this->translation['object'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['available'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['prior'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['dismantling'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['packed'];
        $strVar .= '</th>';
        $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;font-weight:100;width: 150px;text-align: center">';
        $strVar .= $this->translation['comments'];
        $strVar .= '</th>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';

        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '</table >';
        $strVar .= '</div>';


        $strVar .= '<div style="width: 42%;float: right;">';
        $strVar .= '<table style="border-spacing:0;width: 100%;margin-bottom: 10px">';
        $strVar .= '<tr>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['received'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['lost'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['unpacking'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['assembly'];
        $strVar .= '</th>';
        $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
        $strVar .= $this->translation['stricken'];
        $strVar .= '</th>';
        $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;font-weight:100;width: 150px;text-align: center">';
        $strVar .= $this->translation['comments'];
        $strVar .= '</th>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
        $strVar .= '</tr>';
        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';

        $strVar .= '<tr >';
        $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
        $strVar .= '</tr>';
        $strVar .= '</table>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 3%;float: left;">';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;float: left;">';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;float: left;position: relative">';
        $strVar .= '<div style="width: 100%;bottom: 30px;">';
        $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
        $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] .'</span></li>';
        $strVar .= '</ul>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
        $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display: inline;float: right;padding-right: 15px"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] . '</span></li>';
        $strVar .= '</ul>        </div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="position: absolute;bottom: -10px;width: calc(100% - 60px);">';
        $strVar .= '<p style="font-size: 10px;">'. $this->translation['page_sign'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';




        $strVar .= '<div style="width:100%; margin: auto;position: relative">';
        $strVar .= '<div style="width: 10%;float:right;">';
        $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
        $strVar .= '</div>';
        $strVar .= '<div style="float: left; width: 100%">';
        $strVar .= '<div>';
        $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;">'. $this->translation['page_7_comment'] .'</p>';
        $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;"> - '. $this->translation['page_7_comment_point1'] .'</p>';
        $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;"> - '. $this->translation['page_7_comment_point2'] .'</p>';
        $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;"> - '. $this->translation['page_7_comment_point3'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;height: 300px;border: 2px solid #000;margin-top: 15px;margin-bottom: 15px;">';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 100%;position: absolute">';
        $strVar .= '<div style="width: 100%;bottom: 30px;">';
        $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
        $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] .'</span></li>';
        $strVar .= '</ul>';
        $strVar .= '</div>';
        $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
        $strVar .= '<p style="color: #264c9c;font-size: 9px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
        $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
        $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
        $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
        $strVar .= '<li style="display: inline;float: right;padding-right: 15px;margin-top: 2px"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['user']['nom'].' '. Yii::app()->session['user']['nom'] . '</span></li>';
        $strVar .= '</ul>        </div>';
        $strVar .= '</div>';
        $strVar .= '<div style="position: absolute;bottom: -10px;width: calc(100% - 60px);">';
        $strVar .= '<p style="font-size: 10px;">'. $this->translation['page_sign'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</body>';
        $strVar .= '</html>';


        $mpdf->AddPage('c', 'A4-L');
        $mpdf->WriteHTML($strVar);

        $file = fopen(UPLOADS."/pdfFiles/".Yii::app()->session['invoiceNumber'].".pdf", "wb");
        $content = $mpdf->Output(UPLOADS."/pdfFiles/".Yii::app()->session['invoiceNumber'].".pdf",'F');



        $strVar = '';



        $strVar .= '<html>';
        $strVar .= '<head>';
        $strVar .= '</head>';
        $strVar .= '<body style="font-family: sans-serif;padding: 0px;margin: 0px ">';
        $strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 70px;padding: 0px">';
        $strVar .= '<div style="width:45%;float:left"><img style="width: 70px;height: 40px" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" /></div>';
        $strVar .= '<div style="width:20%;float:left;margin-top:25px"><i class="fa fa-phone" aria-hidden="true"></i><span style="color:#909090;font-size: 14px;font-weight: 600">' . Yii::app()->session['user']['telephone']. '</span></div>';
        $strVar .= '<div style="width:20%;float:left;text-align:right;margin-top:25px"><span style="color:#909090;font-size: 12px;font-style: italic;text-align:right">Exemplaire Déménageur</span></div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 80px">';
        $strVar.='<div style="width:100%;float:left;margin-top: 0">';
        $strVar .= '<div style="width:48%;float:left;margin-top: -15px;"><span style="color:#909090;font-size: 22px;font-weight: 600">Lettre de Voiture</span></div>';
        $strVar .= '<div style="width:100%;float:left;text-align:left;margin-top:-10px"><p style="color:#909090;font-size: 16px;width:100%">ref.<span style="color:#264d9c;font-size: 14px;">'. Yii::app()->session['user']['prenom'] . ' _ '. Yii::app()->session['invoiceNumber'] .'</span></p></p>';
        $strVar.='</div><br/>';
//$strVar .= '<hr style="width:100%;height: 0px;display:inline-block;border-top: 4px solid #264d9c" />';
        $strVar .= '<div style="font-size: 10px;width:100%;color:#909090;margin-top:-21px;text-align: left">Document obligatoire par lŽarreté du '.date("d",Yii::app()->session['date'] ).' '. date("m",Yii::app()->session['date'] ) .' '.date("Y",Yii::app()->session['date'] ).'</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="padding: 0px">';
        $strVar .= '<div ><p style="font-size:13px;margin-top:0px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">1. Informations Générales</p></div>';
        $strVar .= '<div style="height: 65px;width:100%;">';
        $strVar .= '<div style="width:49%;float:left;border: 1px solid #000;height: 60px;">';
        $strVar .= '<div style="width:50%;float:left;height: 60px;border-right: 1px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Client</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Téléphone Client</p>';
        $strVar .= '<p style="margin:0px;font-size: 10px;padding: 2px;font-weight: 600">Société de déménagement</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600">' . Yii::app()->session['user']['nom']. ' ' . Yii::app()->session['user']['prenom'] . '</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;color:#264d9c">' . Yii::app()->session['user']['portable']. '</p>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Futurama Express</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;border: 1px solid #000;height: 60px;">';
        $strVar .= '<div style="width:49%;float:left;height: 60px;border-right: 0.5px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Formule</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Volume total</p>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Distance</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600">' . Yii::app()->session['distance'] . '</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;margin-top:5px;height: 182px;text-align: center">';
        $strVar .= '<div style="width:49.2%;float:left">';
        $strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;clear: both">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
        $strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['date'])  . ' '.Yii::app()->session['dayPart'].'</p>';
        $strVar .= '<div style="width:100%">';
        $strVar .= '<p style="margin:4px;font-size: 12px;padding: 1px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['depart']['adresse_depart'] .'</p> <br/>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['depart']['cp_depart'] .'</p> <br/>';
        $strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['depart']['ville_depart'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
        $strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
        $strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 1</p>';
        $strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
        $strVar .= '<div style="width:100%;height:60px">';
        $strVar .= 'contract signed';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49.2%;float:right">';
        $strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 2</p>';
        $strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
        $strVar .= '<div style="width:100%;height:60px">';
        $strVar .= 'contract signed';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
        $strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['date'])  . ' '.Yii::app()->session['dayPart'].'</p>';
        $strVar .= '<div style="width:100%">';
        $strVar .= '<p style="margin:4px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['arrive']['adresse_arrivee'].'</p> <br/>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['arrive']['cp_arrivee'] .'</p>';
        $strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['arrive']['ville_arrivee'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
        $strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
        $strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">2. Prise en charge du Chargement</p></div>';
        $strVar .= '<div style="width:100%;margin-top:2px;height: 75px;text-align: center">';
        $strVar .= '<div style="width:49.2%;float:left;border:1px solid #000;height:75px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Dommages constatés avant chargement (réservé déménageur)</p>';
        $strVar .= '<div style="width:90%;margin:0px auto">';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49.2%;float:right;border:1px solid #000;height:75px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Signature client (décharge) au chargement</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">3. Déclaration de fin de travaux</p></div>';

        $strVar .= '<div style="width:100%;margin-top:2px;height: 240px;text-align: center;float: left">';
        $strVar .= '<div style="width:49%;float:left;border:1px solid #000;height:240px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du client</p>';
        $strVar .= '<div style="width:90%;margin:7px auto">';
        $strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
        $strVar .= '<p style="display:block;width:55%;float:left;text-align: left;margin: 0px">Recu mon mobilier : </p>';
        $strVar .= '<div style="float:right;width:45%;">';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Au complet et sans réserves</span></p>';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block"> Avec les réserves ci-dessous</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
        $strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
        $strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature client</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .= '<div style="width:49%;float:right;border:1px solid #000;height:240px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du déménageur</p>';
        $strVar .= '<div style="width:90%;margin:7px auto;clear:both;">';
        $strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
        $strVar .= '<p style="display:block;width:45%;float:left;font-size:12px;text-align: left;margin: 0px">Immatriculation : </p>';
        $strVar .= '<div style="float:right;width:45%;">';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 1 :</span></p>';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 2 :</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
        $strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
        $strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature dem.</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .= '<div style="width:100%;height: 37px;border:1px solid #000;margin-top: 10px;font-size: 8px;padding: 1.5px;text-align: center">';
        $strVar .= '<p style="margin: 0px">La livraison donne lieu à des formalités impératives. Dans tous les cas, vous devez donner décharge à lentreprise en fin de livraison en signant ce document. En cas de dommages, identifier avec précision les pertes et';
        $strVar .= 'avaries constatées, la mention "sous réserve de déballage ou de contrôle" nayant aucune valeur de preuve. Si vos réserves émises à la réception du mobilier ne sont pas acceptées par le professionel, ou si vous navez émis';
        $strVar .= 'aucune réserve à la livraison,vous ne disposez alors que dun délai de 7 jours calendaires à compter de la réception des objets transportés pour émettre par lettre recommandée une protestation motivée sur létat du';
        $strVar .= 'mobilier réceptionné en application de larticle L. 121-95 du code de la consommation.';
        $strVar .= '</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;height: 25px;margin-top: 5px;font-size: 10px;padding: 1.5px;text-align: center">';
        $strVar .= '<p style="margin: 0px">Move24 Group GmbH I Chausseestrasse 86 I 10115 Berlin | HRB 168985 B | Gérants: Marcel Rangnow, Ante Krsanac';
        $strVar .= 'HypoVereinsbank | IBAN: DE57100208900025392566 | BIC: HYVEDEMM488 | St-Nr: 37/243/22935 | USt-IdNr DE 301320311';
        $strVar .= '</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 70px;padding: 0px">';
        $strVar .= '<div style="width:45%;float:left"><img style="width: 70px;height: 40px" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" /></div>';
        $strVar .= '<div style="width:20%;float:left;margin-top:25px"><i class="fa fa-phone" aria-hidden="true"></i><span style="color:#909090;font-size: 14px;font-weight: 600">' . Yii::app()->session['user']['telephone']. '</span></div>';
        $strVar .= '<div style="width:20%;float:left;text-align:right;margin-top:25px"><span style="color:#909090;font-size: 12px;font-style: italic;text-align:right">Exemplaire Client</span></div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 80px">';
        $strVar.='<div style="width:100%;float:left;margin-top: 0">';
        $strVar .= '<div style="width:48%;float:left;margin-top: -10px"><span style="color:#909090;font-size: 22px;font-weight: 600">Lettre de Voiture</span></div>';
        $strVar .= '<div style="width:100%;float:left;text-align:left;margin-top:-10px"><p style="color:#909090;font-size: 16px;width:100%">ref.<span style="color:#264d9c;font-size: 14px;">'. Yii::app()->session['user']['prenom'] . ' _ '. Yii::app()->session['invoiceNumber'] .'</span></p></p>';
        $strVar.='</div><br/>';
//$strVar .= '<hr style="width:100%;height: 0px;display:inline-block;border-top: 4px solid #264d9c" />';
        $strVar .= '<div style="font-size: 10px;width:100%;color:#909090;margin-top:-21px;text-align: left">Document obligatoire par lŽarreté du '.date("d",Yii::app()->session['date'] ).' '. date("m",Yii::app()->session['date'] ) .' '.date("Y",Yii::app()->session['date'] ).'</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="padding: 0px">';
        $strVar .= '<div ><p style="font-size:13px;margin-top:0px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">1. Informations Générales</p></div>';
        $strVar .= '<div style="height: 65px;width:100%;">';
        $strVar .= '<div style="width:49%;float:left;border: 1px solid #000;height: 60px;">';
        $strVar .= '<div style="width:50%;float:left;height: 60px;border-right: 1px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Client</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Téléphone Client</p>';
        $strVar .= '<p style="margin:0px;font-size: 10px;padding: 2px;font-weight: 600">Société de déménagement</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600">' . Yii::app()->session['User']['nom']. ' ' . Yii::app()->session['user']['prenom'] . '</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;color:#264d9c">' . Yii::app()->session['user']['portable']. '</p>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Futurama Express</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;border: 1px solid #000;height: 60px;">';
        $strVar .= '<div style="width:49%;float:left;height: 60px;border-right: 0.5px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Formule</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Volume total</p>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Distance</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600">' . Yii::app()->session['distance'] . '</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;margin-top:5px;height: 182px;text-align: center">';
        $strVar .= '<div style="width:49.2%;float:left">';
        $strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;clear: both">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
        $strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['date'])  . ' '.Yii::app()->session['dayPart'].'</p>';
        $strVar .= '<div style="width:100%">';
        $strVar .= '<p style="margin:4px;font-size: 12px;padding: 1px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['depart']['adresse_depart'] .'</p> <br/>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['depart']['cp_depart'] .'</p> <br/>';
        $strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['ville_depart'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
        $strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
        $strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 1</p>';
        $strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
        $strVar .= '<div style="width:100%;height:60px">';
        $strVar .= 'contract signed';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49.2%;float:right">';
        $strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 2</p>';
        $strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
        $strVar .= '<div style="width:100%;height:60px">';
        $strVar .= 'contract signed';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
        $strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['date'])  . ' '.Yii::app()->session['dayPart'].'</p>';
        $strVar .= '<div style="width:100%">';
        $strVar .= '<p style="margin:4px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['arrive']['adresse_arrivee'] .'</p> <br/>';
        $strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['arrive']['cp_arrivee'] .'</p>';
        $strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['arrive']['ville_arrivee'] .'</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
        $strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
        $strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">2. Prise en charge du Chargement</p></div>';
        $strVar .= '<div style="width:100%;margin-top:2px;height: 75px;text-align: center">';
        $strVar .= '<div style="width:49.2%;float:left;border:1px solid #000;height:75px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Dommages constatés avant chargement (réservé déménageur)</p>';
        $strVar .= '<div style="width:90%;margin:0px auto">';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:49.2%;float:right;border:1px solid #000;height:75px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Signature client (décharge) au chargement</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">3. Déclaration de fin de travaux</p></div>';

        $strVar .= '<div style="width:100%;margin-top:2px;height: 240px;text-align: center;float: left">';
        $strVar .= '<div style="width:49%;float:left;border:1px solid #000;height:240px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du client</p>';
        $strVar .= '<div style="width:90%;margin:7px auto">';
        $strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
        $strVar .= '<p style="display:block;width:55%;float:left;text-align: left;margin: 0px">Recu mon mobilier : </p>';
        $strVar .= '<div style="float:right;width:45%;">';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Au complet et sans réserves</span></p>';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block"> Avec les réserves ci-dessous</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
        $strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
        $strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature client</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .= '<div style="width:49%;float:right;border:1px solid #000;height:240px">';
        $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du déménageur</p>';
        $strVar .= '<div style="width:90%;margin:7px auto;clear:both;">';
        $strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
        $strVar .= '<p style="display:block;width:45%;float:left;font-size:12px;text-align: left;margin: 0px">Immatriculation : </p>';
        $strVar .= '<div style="float:right;width:45%;">';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 1 :</span></p>';
        $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 2 :</span></p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
        $strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
        $strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature dem.</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
        $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</div>';

        $strVar .= '<div style="width:100%;height: 37px;border:1px solid #000;margin-top: 10px;font-size: 8px;padding: 1.5px;text-align: center">';
        $strVar .= '<p style="margin: 0px">La livraison donne lieu à des formalités impératives. Dans tous les cas, vous devez donner décharge à lentreprise en fin de livraison en signant ce document. En cas de dommages, identifier avec précision les pertes et';
        $strVar .= 'avaries constatées, la mention "sous réserve de déballage ou de contrôle" nayant aucune valeur de preuve. Si vos réserves émises à la réception du mobilier ne sont pas acceptées par le professionel, ou si vous navez émis';
        $strVar .= 'aucune réserve à la livraison,vous ne disposez alors que dun délai de 7 jours calendaires à compter de la réception des objets transportés pour émettre par lettre recommandée une protestation motivée sur létat du';
        $strVar .= 'mobilier réceptionné en application de larticle L. 121-95 du code de la consommation.';
        $strVar .= '</p>';
        $strVar .= '</div>';
        $strVar .= '<div style="width:100%;height: 25px;margin-top: 5px;font-size: 10px;padding: 1.5px;text-align: center">';
        $strVar .= '<p style="margin: 0px">Move24 Group GmbH I Chausseestrasse 86 I 10115 Berlin | HRB 168985 B | Gérants: Marcel Rangnow, Ante Krsanac';
        $strVar .= 'HypoVereinsbank | IBAN: DE57100208900025392566 | BIC: HYVEDEMM488 | St-Nr: 37/243/22935 | USt-IdNr DE 301320311';
        $strVar .= '</p>';
        $strVar .= '</div>';
        $strVar .= '</div>';
        $strVar .= '</body>';
        $strVar .= '</html>';


//            Helpers::dump($strVar);die;

        $mpdf=new mPDF();
        $mpdf->WriteHTML($strVar);

        $file = fopen(UPLOADS.'/pdfFiles/1_'.Yii::app()->session['invoiceNumber'].'.pdf', "wb");
        $content = $mpdf->Output(UPLOADS.'/pdfFiles/1_'.Yii::app()->session['invoiceNumber'].'.pdf','F');

        $model_order = new OrderInfo();

        $model_order->order_id = $model->id;
        $model_order->pdf = $model->invoice_number . '.pdf';
        $model_order->information_table = $this->drawTable();
        if($model_order->save()){
            require bDIR.'/protected/extensions/PHPMailer-master/PHPMailerAutoload.php';

            $mail = new PHPMailer;

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = Yii::app()->params->smtp;  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = Yii::app()->params->contactEmail;                 // SMTP username
            $mail->Password = Yii::app()->params->password;                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            $mail->setFrom(Yii::app()->params->contactEmail, 'Futurama Express');
            $mail->addAddress(Yii::app()->session['user']['email'], Yii::app()->session['user']['nom']);     // Add a recipient
            $mail->addReplyTo(Yii::app()->params->contactEmail, 'Information');
            $mail->addAttachment(UPLOADS.'/pdfFiles/'.Yii::app()->session['invoiceNumber'].'.pdf');         // Add attachments
            $mail->addAttachment(UPLOADS.'/pdfFiles/1_'.Yii::app()->session['invoiceNumber'].'.pdf');         // Add attachments
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'Your order letter';
            $mail->Body    = 'Futurama Express';
            $mail->AltBody = 'Futurama Express';
            $mail->SMTPDebug = 1;
            if(!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            }else {
                echo 'Message has been sent';
                Yii::app()->user->setFlash('success','Thanks, your message sent!');
                $this->redirect(array('/site/index'));
            }

        }

        die;
    }

    public function drawTable(){

        $strVar = '';
        $strVar .= '<div style="text-align: center">';
        $strVar .= '<table style="width: 100%;border: 1px solid black;border-collapse: collapse;">';
        $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">User information</th>';
        foreach (Yii::app()->session['user'] as $key => $value ){
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $key .'</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $value .'</td>';
            $strVar .= '</tr>';
        }
            $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Tarif</th>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Type</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['tarif']['name'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Depart Information</th>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Address</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['adresse_depart'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Post</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['cp_depart'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Ville</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['ville_depart'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Surface</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['surface_depart'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['depart_url'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['depart_geo'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Country</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['pays_dep'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">House type</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['depart']['type'] .'</td>';
            $strVar .= '</tr>';

            $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Arrive Information</th>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Address</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['adresse_arrive'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Post</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['cp_arrive'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Ville</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['ville_arrive'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Surface</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['surface_arrive'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['arrive_url'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['arrive_geo'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Country</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['pays_arr'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">House type</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['arrive']['type'] .'</td>';
            $strVar .= '</tr>';



            $strVar .= '<th colspan="3" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Products</th>';
            foreach($_SESSION['selectedProducts'] as $key =>$value){
                $strVar .= '<tr>';
                $strVar .= '<td style="padding: 10px;text-align: center; background-color: rgb(210, 214, 222)" colspan ="3">' . $key . '</td>';
                $strVar .= '</tr>';
                foreach($value as $k =>$v){
                    $strVar .= '<tr>';
                    $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">' . $v['name'] . '</td>';
                    $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $v['value'] .'</td>';
                    $strVar .= '</tr>';
                }
            }

        $strVar .= '</table>';
        $strVar .= '</div>';

        return $strVar;

    }


    public function actionIndex(){
        session_destroy();
        $building_type = BuildingType::model()->with('buildingTypeLabel',array('condition'=>array('language_id' => $this->langId)))->findAllByAttributes(array('parent_id' => 0 ));
        $data = array(
            'building_type' => $building_type
        );


        $criteria = new CDbCriteria();
        $criteria->with= array(
          'categoryLabels'=>array(

          )
        );

        $category = ProductCategory::model()->findAll($criteria);



        $criteria = new CDbCriteria();
        $criteria->condition = "status = 0 ";
        $criteria->order="sort_order ASC";
        $criteria->with = array(
            'productsLabels' =>array(

            )
        );

        $products = Products::model()->findAll($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = "status = 0 ";
        $criteria->order="sort_order ASC";
        $criteria->with = array(
          'tarifsLabels' => array(
          )
        );

        $tarifs = Tarifs::model()->findAll($criteria);

        $empty = 0;

        $this->render('calculator',array('data' => $data,'empty'=>$empty, 'category'=>$category, 'products'=>$products,'tarifs'=>$tarifs));

    }

	public function actionIndex1()
	{

		$building_type = BuildingType::model()->with('buildingTypeLabel',array('condition'=>array('language_id' => $this->langId)))->findAllByAttributes(array('parent_id' => 0 ));
		$data = array(
			'building_type' => $building_type
		);
		$this->render('details',array('data' => $data));
	}

	public function actionInventory(){
		$this->render('inventory');
}

	public function actionPayment(){
		$this->render('payment');
	}
    public function getDayPrice($day){

        $criteria = new CDbCriteria();
        $criteria->select = 'price';
        $criteria->condition = 'week_day = :day';
        $criteria->params =array(
            ':day'=>$day
        );
        $price = DaysPrice::model()->find($criteria);
        return $price['price'];
    }

	public function actionTest(){
		$this->render('test');
	}

	public function actionGetBuildType(){
		if(isset($_POST['id'])){
			$json_array = array();
			$building_type = BuildingType::model()->with('buildingTypeLabel',array('condition'=>array('language_id' => $this->langId)))->findAllByAttributes(array(),array('order' => 'sort_order ASC'));
			$building_type_recursive = $this->RecursiveChild($building_type,$_POST['id']);
				foreach($building_type_recursive as $key => $value){
					$json_array[$key]['id'] = $value->id;
					$json_array[$key]['price'] = $value->price;
					$json_array[$key]['name_label'] = $value->buildingTypeLabel->name;
					if($value->name){
						foreach($value->name as $elems => $item){
							$json_array[$key]['childs'][$elems]['id'] = $item->id;
							$json_array[$key]['childs'][$elems]['price'] = $item->price;
							$json_array[$key]['childs'][$elems]['name_label'] = $item->buildingTypeLabel->name;
						}
					}
				}
			echo json_encode($json_array);
		}
	}

	public function RecursiveChild($array, $parent = 0){
		$array2 = array();
		foreach ($array as $val){
			if($val['parent_id'] == $parent){
				$array2[$val['id']] = $val;
				$a=$this->RecursiveChild($array, $val['id']);
				if(!empty($a))
					$array2[$val['id']]['name'] = $a;
			}
		}
		return $array2;
	}


	public function actionProductsCount(){

        $selected = array();
        if(!empty($_POST)){
            $id = $_POST['id'];
            $value = $_POST['valeur'];

            if($id == '00'){
                $currentProduct = array(

                    'value'  => $value,
                    'volume' => '(45*55*35)',
                    'price'  => '0',
                    'name'   => 'Carton standards (45*55*35)'

                );
                if(!isset($_SESSION['selectedProducts'])){
                    $_SESSION['selectedProducts'] = array();
                }
                $_SESSION['selectedProducts']['Cartons de déménagements'][2] = $currentProduct;
            }


            $criteria =  new CDbCriteria();
            $criteria -> condition = " t.id = :id ";
            $criteria -> params = array(
                ':id' => $id,
            );
            $criteria ->with =array(
                'productsLabel'=>array(
                    'alias' => 't1',
                    'condition' => 't1.language_id = :langId',
                    'params' => array(':langId' => $this->langId)
                )
            );


            $product = Products::model()->find($criteria);

//            Helpers::dump($_POST);die;
            $currentProduct = array(

                    'value'  => $value,
                    'volume' => $product['volume'],
                    'price'  => $product['price'],
                    'name'   => $product['productsLabel']['name']

            );


            $criteria =  new CDbCriteria();
            $criteria -> condition = " t.id = :id ";
            $criteria -> params = array(
                ':id' => $product['category'],
            );
            $criteria ->with =array(
                'categoryLabel'=>array(
                    'alias' => 't1',
                    'condition' => 't1.language_id = :langId',
                    'params' => array(':langId' => $this->langId)
                )
            );

            $categoryInfo = ProductCategory::model()->find($criteria);


            if(!isset($_SESSION['selectedProducts'])){
                $_SESSION['selectedProducts'] = array();
            }
            $_SESSION['selectedProducts'][$categoryInfo['categoryLabel']['name']][$id] = $currentProduct;

            Yii::app()->session['volume'] = 0;
            foreach ($_SESSION['selectedProducts'] as $key=>$value){
                foreach($value as $k =>$v){
                    Yii::app()->session['volume'] = Yii::app()->session['volume'] + $v['value'] * $v['volume'];
                    Yii::app()->session['price'] = Yii::app()->session['price'] + $v['value'] * $v['price'];
                }
            }
            $return_value = array();
            $return_value['volume'] = Yii::app()->session['volume'];

            echo Yii::app()->session['volume']; die;

        }


    }

    public function actionChooseTarif(){

        if(!empty($_POST) && $_POST['tarif']){

            $tarif= $_POST['tarif'];

            $criteria = new CDbCriteria();
            $criteria->select = "price";
            $criteria->condition = "t.id = :id";
            $criteria->params =array(
              'id'=>$tarif
            );
            $criteria->with=array(
                'tarifsLabel'=>array(
                    'alias' => 't1',
                    'condition' => 't1.language_id = :langId',
                    'params' => array(':langId' => $this->langId)
                )
            );
            $tarif= Tarifs::model()->find($criteria);
            Yii::app()->session['tarif'] = array();
            $k = array();
            $k['price']=$tarif['price'];
            $k['name']=$tarif['tarifsLabel']['name'];
            Yii::app()->session->add("tarif", $k);
            Yii::app()->session['price'] = Yii::app()->session['tarif']['price'] + Yii::app()->session['price'];
            $return_value = array();
            $return_value['response'] = 'true';
            $return_value['price'] = Yii::app()->session['price'];
            echo json_encode($return_value);
        }else{
            $return_value = array();
            $return_value['response'] = 'false';
            $return_value['price'] = '';
            echo json_encode($return_value);
        }
    }
    public function getInfoById($id){

        $criteria = new CDbCriteria();
        $criteria -> condition = "t.id = :id";
        $criteria -> params = array(
            'id' => $id
        );
        $criteria->with = array(
            'buildingTypeLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId)
            )
        );

        $type = BuildingType::model()->find($criteria);
        return $depart['type'] = $type['buildingTypeLabel']['name'];

    }
	public function actionPriceList(){

        $empty=0;

        if(!empty($_POST)){



            if(empty($_POST['name']['date_choisie']) || empty($_POST['name']['adresse_depart']) || empty($_POST['name']['adresse_arrivee']) || $_POST['name']['type_logement_depart'] == 999999 || $_POST['name']['type_logement_arrivee'] == 999999 ){
                $empty = 1;
                $return_value = array();
                $return_value['response'] = 'false';
                $return_value['error_message'] = 'Date, depart and arrive, home type, addresses are required!';
                echo json_encode($return_value);
                die;
            }else{
                if($_POST['name']['type_logement_depart']!= 3){

                    Yii::app()->session['etage_depart'] = array();
                    $k = array();
//                    $criteria = new CDbCriteria;

                    foreach($_POST['name']['etage_depart'] as $key=>$value){
                        $criteria = new CDbCriteria;
                        $criteria->select = "name";
                        $criteria->alias = "t1";
                        $criteria->condition = "t1.building_type_id = :id and t1.language_id = :langId";
                        $criteria->params =array(
                            ':id'=>$value,
                            ':langId' => $this->langId
                        );
                        $criteria -> with = array(
                            'buildingType'
                        );
                        $itemName = BuildingTypeLabel::model()->find($criteria);
                        $parentId =  $itemName['buildingType']['parent_id'];
                        $criteria = new CDbCriteria;
                        $criteria->alias = "t1";
                        $criteria->condition = "building_type_id = :parentId and t1.language_id = :langId";
                        $criteria->params =array(
                            ':parentId'=>$parentId,
                            ':langId' => $this->langId
                        );
                        $parent_name = BuildingTypeLabel::model()->find($criteria);
                        $k[$parent_name['name']] =  $itemName['name'];

                    }
                    Yii::app()->session->add("maison_depart", $k);
                }

                if($_POST['name']['type_logement_arrivee']!=3){
                    $k=array();
                    foreach($_POST['name']['etage_arrive'] as $key=>$value){
                        $criteria = new CDbCriteria;
                        $criteria->select = "name";
                        $criteria->alias = "t1";
                        $criteria->condition = "t1.building_type_id = :id and t1.language_id = :langId";
                        $criteria->params =array(
                            ':id'=>$value,
                            ':langId' => $this->langId
                        );
                        $criteria -> with = array(
                            'buildingType'
                        );

                        $itemName = BuildingTypeLabel::model()->find($criteria);
                        $parentId =  $itemName['buildingType']['parent_id'];
                        $criteria = new CDbCriteria;
                        $criteria->alias = "t1";
                        $criteria->condition = "building_type_id = :parentId and t1.language_id = :langId";
                        $criteria->params =array(
                            ':parentId'=>$parentId,
                            ':langId' => $this->langId
                        );
                        $parent_name = BuildingTypeLabel::model()->find($criteria);
                        $k[$parent_name['name']] =  $itemName['name'];
                    }

                    Yii::app()->session->add("maison_arrive", $k);
                }else{
                    Yii::app()->session['etage_arrive'] = '';
                }


                Yii::app()->session['price'] = $this->getDayPrice(date('N',strtotime($_POST['name']['date_choisie']))) + $this->getDayPrice(8) * $_POST['name']['distance'];


                Yii::app()->session['distance'] = $_POST['name']['distance'];

                Yii::app()->session['date'] =$_POST['name']['date_choisie'];


                $arrive = array();
                Yii::app()->session['arrive'] = array();
                $depart = array();
                Yii::app()->session['depart'] = array();


                $arrive['arrivee_url'] = $_POST['name']['arrivee_url'];
                $arrive['arrivee_geo'] = $_POST['name']['arrivee_geo'];
                $arrive['adresse_arrivee'] = $_POST['name']['adresse_arrivee'];
                $arrive['cp_arrivee'] = $_POST['name']['cp_arrivee'];
                $arrive['ville_arrivee'] = $_POST['name']['ville_arrivee'];
                $arrive['type_logement_arrivee'] = $_POST['name']['type_logement_arrivee'];
                $arrive['surface_arrivee'] = $_POST['name']['surface_arrivee'];
                $arrive['street_number_arr'] = $_POST['name']['street_number_arr'];
                $arrive['route_arr'] = $_POST['name']['route_arr'];
                $arrive['pays_arr'] = $_POST['name']['pays_arr'];
                $depart['adresse_depart'] = $_POST['name']['adresse_depart'];
                $depart['cp_depart'] = $_POST['name']['cp_depart'];
                $depart['ville_depart'] = $_POST['name']['ville_depart'];
                $depart['surface_depart'] = $_POST['name']['surface_depart'];
                $depart['depart_url'] = $_POST['name']['depart_url'];
                $depart['depart_geo'] = $_POST['name']['depart_geo'];
                $depart['pays_dep'] = $_POST['name']['pays_dep'];
                $depart['type'] = $this->getInfoById($_POST['name']['type_logement_depart']);
                $arrive['type'] = $this->getInfoById($_POST['name']['type_logement_arrivee']);

//                Helpers::dump($arrive['type']);die;

                Yii::app()->session->add("arrive",$arrive);
                Yii::app()->session->add("depart",$depart);

                $return_value = array();
                $return_value['response'] = 'true';
                $return_value['error_message'] = '';
                echo json_encode($return_value);
            }

        }


	}
	/**
	 * This is the action to handle external exceptions.
	 */
    public function get_random_string($length)
    {
        // start with an empty random string
        $random_string = "";
        $numbers = range(0,9);
        $letters =  range('A', 'Z');
        $valid_chars = array_merge($letters,$numbers);
//        print_r($valid_chars);die;
        // count the number of chars in the valid chars string so we know how many choices we have
//        $num_valid_chars = strlen($valid_chars);

        // repeat the steps until we've created a string of the right length
        for ($i = 0; $i < $length; $i++)
        {
            // pick a random number from 1 up to the number of valid chars
            $random_pick = array_rand($valid_chars,1);

            // take the random character out of the string of valid chars
            // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
            $random_char = $valid_chars[$random_pick];

            // add the randomly-chosen char onto the end of our string so far
            $random_string .= $random_char;
        }

        // return our finished random string
        return $random_string;
    }




	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
    protected function sendEmail($email,$subject,$message){

        $mail=Yii::app()->params->contactEmail;
        $password = Yii::app()->params->password;
        $smtp =Yii::app()->params->smtp;
        $mailSMTP = new SendMailSmtpClass( $mail, $password, 'ssl://'.$smtp, $mail, 465);
        $headers= "MIME-Version: 1.0\r\n";
//        $file_to_attach = UPLOADS."/pdfFiles/";
        $message = "asdas";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From:  <$email>\r\n";

        $result   =  $mailSMTP->send("$email",
            "
            : ".$subject,
            $message,
            $headers);
        if($result === true){
            return true;
        }else {
            return false;
        }

    }
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}