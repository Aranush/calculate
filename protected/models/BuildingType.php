<?php

/**
 * This is the model class for table "building_type".
 *
 * The followings are the available columns in table 'building_type':
 * @property string $id
 * @property string $parent_id
 * @property string $name
 * @property double $price
 * @property string $created_date
 * @property integer $sort_order
 *
 * The followings are the available model relations:
 * @property BuildingTypeLabel[] $buildingTypeLabels
 */
class BuildingType extends CActiveRecord
{

	public $name;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'building_type';
	}
	public function getName(){
		if(isset($this->name)){
			return $this->name;
		}else{
			return null;
		}
	}

	public function setName($val){
		$this->name = $val;
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_id, created_date', 'required'),
			array('sort_order', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('parent_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, price, created_date, sort_order, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buildingTypeLabels' => array(self::HAS_MANY, 'BuildingTypeLabel', 'building_type_id'),
			'buildingTypeLabel' => array(self::HAS_ONE, 'BuildingTypeLabel', 'building_type_id'),
			'parent' => array(self::BELONGS_TO, 'BuildingType', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'price' => 'Price',
			'created_date' => 'Created Date',
			'sort_order' => 'Sort Order',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with= array('buildingTypeLabel');
		$criteria->together = true;
		$criteria->compare('buildingTypeLabel.name',$this->name,true);

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.parent_id',$this->parent_id,true);
		$criteria->compare('t.price',$this->price);
		$criteria->compare('t.created_date',$this->created_date,true);
		$criteria->compare('t.sort_order',$this->sort_order);
		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BuildingType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
