<?php

/**
 * This is the model class for table "language".
 *
 * The followings are the available columns in table 'language':
 * @property string $id
 * @property string $name
 * @property string $iso
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property DeliveryLabel[] $deliveryLabels
 * @property InsuranceLabel[] $insuranceLabels
 * @property MaterialLabel[] $materialLabels
 * @property ServiceOptionLabel[] $serviceOptionLabels
 * @property ServicesLabel[] $servicesLabels
 * @property TermsContaitionsLabel[] $termsContaitionsLabels
 */
class Language extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'language';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, iso', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			array('iso', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, iso, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deliveryLabels' => array(self::HAS_MANY, 'DeliveryLabel', 'language_id'),
			'insuranceLabels' => array(self::HAS_MANY, 'InsuranceLabel', 'language_id'),
			'materialLabels' => array(self::HAS_MANY, 'MaterialLabel', 'language_id'),
			'serviceOptionLabels' => array(self::HAS_MANY, 'ServiceOptionLabel', 'language_id'),
			'servicesLabels' => array(self::HAS_MANY, 'ServicesLabel', 'language_id'),
			'termsContaitionsLabels' => array(self::HAS_MANY, 'TermsContaitionsLabel', 'language_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'iso' => 'Iso',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('iso',$this->iso,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Language the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
