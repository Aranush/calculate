<?php

/**
 * This is the model class for table "materials".
 *
 * The followings are the available columns in table 'materials':
 * @property string $id
 * @property string $category_id
 * @property double $price
 * @property string $image
 *
 * The followings are the available model relations:
 * @property MaterialLabel[] $materialLabels
 * @property Category $category
 */
class Materials extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $name;

    public function getName(){
        if(isset($this->name)){
            return $this->name;
        }else{
            return null;
        }
    }

    public function setName($val){
        $this->name = $val;
    }

    public function tableName()
	{
		return 'materials';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price, image', 'required'),
			array('price', 'numerical'),
			array('category_id', 'length', 'max'=>11),
			array('image', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, price, image, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'materialLabels' => array(self::HAS_MANY, 'MaterialLabel', 'material_id'),
			'materialLabel' => array(self::HAS_ONE, 'MaterialLabel', 'material_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
            'name'=>'Name',
			'price' => 'Price',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
        $criteria->with =array('materialLabel');
        $criteria->together=true;
        $criteria->compare('categoryLabels.name',$this->name,true);
        $criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('image',$this->image,true);
        $criteria->order = 't.id DESC';
        $criteria->limit = 50;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Materials the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
