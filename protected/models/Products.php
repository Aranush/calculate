<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property string $image
 * @property double $volume
 * @property double $price
 * @property integer $sort_order
 * @property integer $status
 * @property integer $category
 *
 * The followings are the available model relations:
 * @property ProductsLabel[] $productsLabels
 */
class Products extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $name;

    public function getName(){
        if(isset($this->name)){
            return $this->name;
        }else{
            return null;
        }
    }

    public function setName($val){
        $this->name = $val;
    }


	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('image, category', 'required'),
			array('sort_order, status, category', 'numerical', 'integerOnly'=>true),
			array('volume, price', 'numerical'),
			array('image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image, volume, price, sort_order, status, category, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productsLabels' => array(self::HAS_MANY, 'ProductsLabel', 'product_id'),
            'productsLabel' => array(self::HAS_ONE, 'ProductsLabel', 'product_id'),

        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'volume' => 'Volume',
			'price' => 'Price',
			'sort_order' => 'Sort Order',
			'status' => 'Status',
            'name'=>'Name',
			'category' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with= array('productsLabels');
        $criteria->together = true;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('price',$this->price);
        $criteria->compare('productsLabels.name',$this->name,true);
        $criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('status',$this->status);
		$criteria->compare('category',$this->category);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
