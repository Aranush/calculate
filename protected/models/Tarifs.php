<?php

/**
 * This is the model class for table "tarifs".
 *
 * The followings are the available columns in table 'tarifs':
 * @property string $id
 * @property double $price
 * @property integer $status
 * @property integer $sort_order
 *
 * The followings are the available model relations:
 * @property TarifsLabel[] $tarifsLabels
 */
class Tarifs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

    public $name;

    public function getName(){
        if(isset($this->name)){
            return $this->name;
        }else{
            return null;
        }
    }

    public function setName($val){
        $this->name = $val;
    }

	public function tableName()
	{
		return 'tarifs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price, sort_order', 'required'),
			array('status, sort_order', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, price, status, sort_order, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarifsLabels' => array(self::HAS_MANY, 'TarifsLabel', 'tarifs_id'),
			'tarifsLabel' => array(self::HAS_ONE, 'TarifsLabel', 'tarifs_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'price' => 'Price',
			'status' => 'Status',
            'name'=>'Name',
			'sort_order' => 'Sort Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with =array('tarifsLabels');
        $criteria->together =true;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('tarifsLabels.name',$this->name);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tarifs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
