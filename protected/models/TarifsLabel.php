<?php

/**
 * This is the model class for table "tarifs_label".
 *
 * The followings are the available columns in table 'tarifs_label':
 * @property string $id
 * @property string $tarifs_id
 * @property integer $language_id
 * @property string $name
 * @property string $comment
 * @property string $features_user_Comment
 *
 * The followings are the available model relations:
 * @property Tarifs $tarifs
 */
class TarifsLabel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarifs_label';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tarifs_id, language_id, name', 'required'),
			array('language_id', 'numerical', 'integerOnly'=>true),
			array('tarifs_id', 'length', 'max'=>11),
			array('name, comment', 'length', 'max'=>255),
			array('features_user_Comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tarifs_id, language_id, name, comment, features_user_Comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarifs' => array(self::BELONGS_TO, 'Tarifs', 'tarifs_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tarifs_id' => 'Tarifs',
			'language_id' => 'Language',
			'name' => 'Name',
			'comment' => 'Comment',
			'features_user_Comment' => 'Features User Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tarifs_id',$this->tarifs_id,true);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('features_user_Comment',$this->features_user_Comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifsLabel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
