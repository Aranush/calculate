<?php

class DefaultController extends Controller
{
    public function actionIndex()
    {

        $persons = PersonNumber::model()->findAll();
        $hours = HoursInfo::model()->findAll();
        $vehicles = Vehicle::model()->findAll();
        $empty = 0;

        $criteria = new CDbCriteria();
        $criteria->with = array(
            'insuranceLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId)
            )
        );
        $insurance = Insurance::model()->findAll($criteria);
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'deliveryLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId)
            )
        );
        $delivery = Delivery::model()->findAll($criteria);

        $this->render('index',array('empty'=>$empty, 'persons'=>$persons,'hours'=>$hours,'vehicles'=>$vehicles,'insurance'=>$insurance,'delivery'=>$delivery));
    }
    public function actionServices(){
        $criteria = new CDbCriteria();
        $criteria->with=array(
            'servicesLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId)
            )
        );
        $services = Services::model()->findAll($criteria);
//        Helpers::dump($services);die;


        $this->render('services',array('services'=>$services));
    }
    public function actionServicesInner($id){

        $criteria = new CDbCriteria();
        $criteria->limit = 4;
        $criteria->condition= "t.id <> $id";
        $criteria->with=array(
            'servicesLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId)
            )
        );
        $services = Services::model()->findAll($criteria);

        $criteria = new CDbCriteria();
        $criteria ->condition = 't.id = :id';
        $criteria->params = array(
            ':id'=>$id
        );
        $criteria->with = array(
            'servicesLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId),

            ),

            'serviceOptions'=>array(

                'with' =>array(
                    'serviceOptionLabels'=>array(
                        'condition' => 'serviceOptionLabels.language_id = :langId',
                        'params'    => array(':langId' => $this->langId),
                        'together'  => false,
                    ),
                ),
            )
        );

        $serviceInfo = Services::model()->findAll($criteria);


        $this->render('services_inner',array('services'=>$services,'serviceInfo'=>$serviceInfo));
    }

    public function actionAddProduct(){
        if(isset($_POST['data'])){
            $data = $_POST['data'];
            $id = $_POST['id'];
            if($_POST['type']){
                $criteria = new CDbCriteria();
                $criteria->condition = "t.id = $id";
                $criteria->with = array(
                    'servicesLabel' => array(
                        'alias' => 't1',
                        'condition' => 't1.language_id = :langId',
                        'params' => array(':langId' => $this->langId),

                    ),
                );

                $servicesInfo = Services::model()->find($criteria);

            }else{
                $criteria = new CDbCriteria();
                $criteria->condition = "t.id = $id";
                $criteria->with = array(
                    'servicesLabel' => array(
                        'alias' => 't1',
                        'condition' => 't1.language_id = :langId',
                        'params' => array(':langId' => $this->langId),

                    ),
                    'serviceOption' => array(
                        'condition' => "serviceOption.id = $data",
                        'with' => array(
                            'serviceOptionLabels' => array(
                                'condition' => "serviceOptionLabels.language_id = $this->langId",
                                'together'  => false
                            )
                        ),
                        'together' => false
                    )

                );

                $servicesInfo = Services::model()->find($criteria);

            }

            if($servicesInfo){
                $dataInfo = array(
                    'product_name' => $servicesInfo->servicesLabel->name,
                    'product_id' => $servicesInfo->id,
                );
                if($_POST['type']) {
                    $dataInfo['option_count'] = $data;
                    $dataInfo['price'] = $data*$servicesInfo->price;
                }else{
                    $dataInfo['option_count'] =  $servicesInfo->serviceOption->serviceOptionLabel->text;
                    $dataInfo['price'] = $servicesInfo->serviceOption->price;
                    $dataInfo['option'][] = ServiceOption::model()->with('serviceOptionLabel',array('condition' => "serviceOptionLabel.language_id = $this->langId"))->findAllByAttributes(array('service_id' => $id));
                    $dataInfo['option_id'] = $servicesInfo->serviceOption->id;
                }

                    $dataInfo['type'] = $_POST['type']; //1- (+-), 0 - select
                if(!isset($_SESSION['products'])){
                    $_SESSION['products'] = array();
                }
                $_SESSION['products'][$servicesInfo->id] = $dataInfo;
                echo 1; die;

            }
            echo 0;die;
        }
    }

    public function actionCgv(){

        $criteria = new CDbCriteria();
        $criteria->with = array(
            'termsContaitionsLabel'
        );
        $terms = TermsConditions::model()->find($criteria);
        $this->render('cgv',array('terms'=>$terms['termsContaitionsLabel']['text']));
    }
    public function actionReservation()
    {
        $return_value = array();
        if(isset($_POST['Page_1'])){


//            Helpers::dump($_POST);die;

            if(is_numeric($_POST['Page_1']['distance'])){
                $return_value['distance'] = $_POST['Page_1']['distance'];
                Yii::app()->session['distance'] = $_POST['Page_1']['distance'];
            }else{

                $return_value['response'] = 'false';
                $return_value['error_message'] = 'Distance have to be number';
                echo json_encode($return_value);
            }

            $personNumber = new PersonNumber();
            $hoursInfo = new HoursInfo();
            $vehicle_model = new Vehicle();


            Yii::app()->session['user_count'] = $_POST['Page_1']['user'];
            Yii::app()->session['hour'] = $_POST['Page_1']['hour'];
            Yii::app()->session['vehicle'] = $_POST['Page_1']['vehicle'];
            Yii::app()->session['day'] = $_POST['Page_1']['day'];
            Yii::app()->session['dayPart'] = $_POST['Page_1']['dayPart'];


            $user = $this->queryByIdModel(Yii::app()->session['user_count'],$personNumber);
            $hour = $this->queryByIdModel(Yii::app()->session['hour'],$hoursInfo);
            $vehicle = $this->queryByIdModel(Yii::app()->session['vehicle'],$vehicle_model);
//            Helpers::dump($vehicle);die;
            $return_value['userImage']=$user['history_image'];
            $return_value['vehicleImage']=$vehicle['history_image'];
            $return_value['hours']=$hour['hours'];
            $return_value['weekDay'] = date("N",Yii::app()->session['day'] );
            $return_value['months'] = date("m",Yii::app()->session['day'] );
            $return_value['year'] = date("Y",Yii::app()->session['day'] );
            $return_value['monthsDay'] = date("d",Yii::app()->session['day'] );
            $return_value['dayPart'] = Yii::app()->session['dayPart'];

            Yii::app()->session->add('dayInformation',$return_value);
            Yii::app()->session->add('personNumber',$user);
            Yii::app()->session->add('vehicle',$vehicle);
            Yii::app()->session->add('hour',$hour);
            Yii::app()->session->add('userImage',$user['history_image']);
            Yii::app()->session->add('vehicleImage',$vehicle['history_image']);
            echo json_encode($return_value);

        }
    }
    public function actionMaterial($id=""){


        $criteria = new CDbCriteria();
        if(!empty($id )){
            $criteria->condition="t.id = :id";
            $criteria->params = array(
                ':id'=>$id
            );
            $material_limit = '100';
        }else{
            $material_limit = '3';
        }
        $criteria->with=array(
            'categoryAdminLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId),
            ),
            'materials'=>array(
                'limit' => $material_limit,
                'with'=>array(
                    'materialLabel'=>array(
                        'alias' => 't2',
                        'condition' => 't2.language_id = :langId',
                        'params' => array(':langId' => $this->langId),
                    )
                )

            )
        );



        $selectedCategory = Category::model()->findAll($criteria);

        $criteria = new CDbCriteria();
        $criteria->with=array(
            'categoryAdminLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId),
            ),

        );


        $categories = Category::model()->findAll($criteria);

        $data = array(
            'selectedCategory' => $selectedCategory,
            'categories' => $categories,
        );


        $this->render('material',array('data'=>$data));

    }

    public function actionMaterialInfo($id){
        $criteria = new CDbCriteria();
        $criteria->limit = 4;
        $criteria->condition= "t.id <> $id";
        $criteria->with=array(
            'materialLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId)
            )
        );
        $materials = Materials::model()->findAll($criteria);

        $criteria = new CDbCriteria();
        $criteria ->condition = 't.id = :id';
        $criteria->params = array(
            ':id'=>$id
        );
        $criteria->with = array(
            'materialLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId),

            ),
        );

        $materialsInfo = Materials::model()->find($criteria);

        $criteria = new CDbCriteria();
        $criteria->with=array(
            'categoryAdminLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = :langId',
                'params' => array(':langId' => $this->langId),
            ),

        );


        $categories = Category::model()->findAll($criteria);



        $data = array(
            'materials' => $materials,
            'materialsInfo' => $materialsInfo,
            'categories' => $categories,
        );
        $this->render('material_info',array('data' => $data));
    }

    public function actionAddMaterial(){
        if(isset($_POST['data'])){
            $id = $_POST['id'];
            $data = $_POST['data'];
            $criteria = new CDbCriteria();
            $criteria->condition = "t.id = $id";
            $criteria->with = array(
                'materialLabel' => array(
                    'alias' => 't1',
                    'condition' => 't1.language_id = :langId',
                    'params' => array(':langId' => $this->langId),
                ),
            );

                $materialInfo = Materials::model()->find($criteria);


            if($materialInfo){
                $dataInfo = array(
                    'material_name' => $materialInfo->materialLabel->name,
                    'material_id' => $materialInfo->id,
                );
                    $dataInfo['option_count'] = $data;
                    $dataInfo['price'] = $data*$materialInfo->price;

                if(!isset($_SESSION['materials'])){
                    $_SESSION['materials'] = array();
                }
                $_SESSION['materials'][$materialInfo->id] = $dataInfo;
                echo 1; die;

            }
            echo 0;die;
        }
    }

    public function actionAddInfoCoord(){
        if(isset($_POST['assurance_id'])){

            $issurance = Insurance::model()->with('insuranceLabel',array('condition' => "insuranceLabel.language_id = $this->langId"))->findByPk($_POST['assurance_id']);
            Yii::app()->session['insurance'] = $issurance;
            Helpers::dump(Yii::app()->session['insurance']);die;
        }
        if(isset($_POST['all_materials'])){

            foreach ($_POST['all_materials'] as $elems =>$item) {
                unset($_SESSION['materials'][$elems]);
                $criteria = new CDbCriteria();
                $criteria->condition = "t.id = $elems";
                $criteria->with = array(
                    'materialLabel' => array(
                        'alias' => 't1',
                        'condition' => 't1.language_id = :langId',
                        'params' => array(':langId' => $this->langId),
                    ),
                );

                $materialInfo = Materials::model()->find($criteria);


                if($materialInfo){
                    $dataInfo = array(
                        'material_name' => $materialInfo->materialLabel->name,
                    );
                    $dataInfo['option_count'] = $item;
                    $dataInfo['price'] = $item*$materialInfo->price;

                    if(!isset($_SESSION['materials'])){
                        $_SESSION['materials'] = array();
                    }
                    $_SESSION['materials'][$materialInfo->id] = $dataInfo;

                }
            }

        }
        if(isset($_POST['all_services_option'])){

            foreach($_POST['all_services_option'] as $key => $value) {
                unset($_SESSION['products'][$key]);
                $criteria = new CDbCriteria();
                $criteria->condition = "t.id = $key";
                $criteria->with = array(
                    'servicesLabel' => array(
                        'alias' => 't1',
                        'condition' => 't1.language_id = :langId',
                        'params' => array(':langId' => $this->langId),

                    ),
                    'serviceOption' => array(
                        'condition' => "serviceOption.id = $value",
                        'with' => array(
                            'serviceOptionLabels' => array(
                                'condition' => "serviceOptionLabels.language_id = $this->langId",
                                'together' => false
                            )
                        ),
                        'together' => false
                    )

                );

                $servicesInfo = Services::model()->find($criteria);
                if($servicesInfo){
                    $dataInfo = array(
                        'product_name' => $servicesInfo->servicesLabel->name,
                        'product_id' => $servicesInfo->id,
                    );

                        $dataInfo['option_count'] =  $servicesInfo->serviceOption->serviceOptionLabel->text;
                        $dataInfo['price'] = $servicesInfo->serviceOption->price;
                        $dataInfo['option'][] = ServiceOption::model()->with('serviceOptionLabel',array('condition' => "serviceOptionLabel.language_id = $this->langId"))->findAllByAttributes(array('service_id' => $key));
                        $dataInfo['option_id'] = $servicesInfo->serviceOption->id;

                    $dataInfo['type'] = 0; //1- (+-), 0 - select
                    if(!isset($_SESSION['products'])){
                        $_SESSION['products'] = array();
                    }
                    $_SESSION['products'][$servicesInfo->id] = $dataInfo;

                }

            }

        }
        if(isset($_POST['all_services_count'])){
            foreach($_POST['all_services_count'] as $key => $value) {
                unset($_SESSION['products'][$key]);
                    $criteria = new CDbCriteria();
                    $criteria->condition = "t.id = $key";
                    $criteria->with = array(
                        'servicesLabel' => array(
                            'alias' => 't1',
                            'condition' => 't1.language_id = :langId',
                            'params' => array(':langId' => $this->langId),

                        ),
                    );

                    $servicesInfo = Services::model()->find($criteria);




                if ($servicesInfo) {
                    $dataInfo = array(
                        'product_name' => $servicesInfo->servicesLabel->name,
                        'product_id' => $servicesInfo->id,
                    );
                        $dataInfo['option_count'] = $value;
                        $dataInfo['price'] = $value * $servicesInfo->price;

                    $dataInfo['type'] =1; //1- (+-), 0 - select
                    if (!isset($_SESSION['products'])) {
                        $_SESSION['products'] = array();
                    }
                    $_SESSION['products'][$servicesInfo->id] = $dataInfo;

                }
            }
        }
    }

    public function queryByIdModel($id,$model,$modelRel = "")
    {
        $criteria = new CDbCriteria();
        if($modelRel) {
            $criteria ->with =array(
                $modelRel
            );
        }
        $criteria->condition = "t.id = :id";
        $criteria->params = array(
            ':id'=>$id
        );
        return $model::model()->find($criteria);

    }


    public function actionDeleteProduct(){
        if(isset($_POST['key'])){
            if($_POST['type']){
                unset($_SESSION['materials'][$_POST['key']]);
            }else{
                unset($_SESSION['products'][$_POST['key']]);
            }
            echo 1;die;

        }
    }
}