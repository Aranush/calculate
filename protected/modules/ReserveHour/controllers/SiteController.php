<?php

class SiteController extends Controller
{
    public function get_random_string($length)
    {
        // start with an empty random string
        $random_string = "";
        $numbers = range(0,9);
        $letters =  range('A', 'Z');
        $valid_chars = array_merge($letters,$numbers);
//        print_r($valid_chars);die;
        // count the number of chars in the valid chars string so we know how many choices we have
//        $num_valid_chars = strlen($valid_chars);

        // repeat the steps until we've created a string of the right length
        for ($i = 0; $i < $length; $i++)
        {
            // pick a random number from 1 up to the number of valid chars
            $random_pick = array_rand($valid_chars,1);

            // take the random character out of the string of valid chars
            // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
            $random_char = $valid_chars[$random_pick];

            // add the randomly-chosen char onto the end of our string so far
            $random_string .= $random_char;
        }

        // return our finished random string
        return $random_string;
    }
    public function actionSendMail(){
        if(!empty($_POST)){

            Yii::app()->session->add("User",$_POST['User']);
            Yii::app()->session->add("Depart",$_POST['Depart']);
            Yii::app()->session->add("Arrive",$_POST['Arrive']);
            Yii::app()->session['invoiceNumber'] = $this->get_random_string(10);

            $model = new Orders();
            Yii::app()->session['invoiceNumber'] = $this->get_random_string(10);
            $model->invoice_number = Yii::app()->session['invoiceNumber'];
            while(!$model->save()){
                Yii::app()->session['invoiceNumber'] = $this->get_random_string(10);
                $model->invoice_number = Yii::app()->session['invoiceNumber'];
                $model->save();
            }

            require_once(bDIR."/protected/components/mpdf60/mpdf.php");
            $mpdf=new mPDF();
            $strVar= '<!DOCTYPE html>';
            $strVar .=   '<html lang="en">';
            $strVar .=   '<head>';
            $strVar .=   '<meta charset="UTF-8">';
            $strVar .=   '<title>Title</title>';
            $strVar .=   '</head>';
            $strVar .=   '<body style="padding: 0;margin: 0">';
            $strVar .=   '<div style="width: 100%; margin: auto; padding: 10px;">';
            $strVar .=   '<div style="width: 40%;float:right;">';
            $strVar .=   '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
            $strVar .=   '<table>';
            $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
            $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">Hotline client:</td>     <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">01 84 88 52 12</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
            $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">Lundi - Vendredi:</td>   <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">09:00 - 21:00</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
            $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">Samedi - Dimanche:</td>  <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">10:00 - 18:00</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">';
            $strVar .=   '<td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">E-Mail:</td>             <td style="color: #b7b5b5; font-family: sans-serif; font-size: 12px;">service_fr@move24.fr</td>';
            $strVar .=   '</tr>';
            $strVar .=   '</table>';
            $strVar .=   '</div>';

            $strVar .=   '<div style="margin-top: 130px; float: left; width: 100%">';
            $strVar .=   '<p style="color: #616161;font-size: 14px;font-weight: bold;font-family: Sans-Serif">' . $this->translation['page_1_header'] . ' Ref '.Yii::app()->session['invoiceNumber'].'</p>';
//            $strVar .=   '<p style="font-family: sans-serif; color: #797979;"><span style="display: block">Volume Total: '.Yii::app()->session['volume'].' m<sup>3</sup>)</span>';
//        $strVar .=   '<span style="display: block">Chargement*, D?chargement*, Transport**,</span>';
        if(Yii::app()->session['insurance']['id']==5):
            $strVar .=   '<p>Assurance***</p>';
         endif;
            $strVar .=   '<div style="float:left;width:100%"><p style="color: #616161;margin: 0;margin-top: 10px;font-size: 16px;font-weight: bold;font-family: Sans-Serif"> '. $this->translation['User_name'] .'</p>';

            $strVar .=   '<hr style ="border: 0;border-bottom: 1px solid #858585;border-top: 1px solid #b5b5b5;"/>';

            $strVar .=   '<div style="width: 50%;float: left;">';
            $strVar .=   '<table>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;width:130px;">Nom:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;min-width:130px;">'.Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['prenom'].'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '</table>';
            $strVar .=   '</div>';
            $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
            $strVar .=   '</div></div>';

            $strVar .=   '<div style="float:left;width:100%"><p style="color: #616161;font-size: 16px;margin: 0;margin-top: 10px;font-weight: bold;font-family: Sans-Serif">'  . $this->translation['depart'] . '</p>';

            $strVar .=   '<hr style ="border: 0;border-bottom: 1px solid #858585;border-top: 1px solid #b5b5b5;"/>';

            $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
            $strVar .=   '<table style="margin-bottom: 16px;">';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;width:130px;">Date:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' .date('d.m.Y',Yii::app()->session['day'])  . '</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Rue:</td>   <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'. explode(",",Yii::app()->session['Depart']['adresse'])[0] .'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Ville:</td>  <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['Depart']['cp_dep'].' '.Yii::app()->session['Depart']['ville'].'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Type:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Région:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Pièces:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Étage:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' . Yii::app()->session['Depart']['etage'] . '</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Personnes:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' . Yii::app()->session['User_count'] . '</td>';
            $strVar .=   '</tr>';

            $strVar .=   '</table>';
            $strVar .=   '</div>';
            $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';

            $strVar .=   '<table style="margin-bottom: 16px;">';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Ascenseur:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['Depart']['ascenseur'].'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Autorisation de stationnemen:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Emballage:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Démontage Mobilier:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Démontage Cuisine:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Cave/Grenier:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Distance jusque à lentrée:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['distance'].'</td>';
            $strVar .=   '</tr>';

            $strVar .=   '</table>';
            $strVar .=   '</div>';

            $strVar .=   '</div>';
            $strVar .=   '<div style="float:left;width:100%"><p style="color: #616161;font-size: 16px;margin: 0;margin-top: 10px;font-weight: bold;font-family: Sans-Serif">' . $this->translation['arrive'] . '</p>';

            $strVar .=   '<hr style ="border: 0;border-bottom: 1px solid #858585;border-top: 1px solid #b5b5b5;"/>';

            $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';
            $strVar .=   '<table style="margin-bottom: 16px;">';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;width:130px;">Date:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' . date('d.m.Y',Yii::app()->session['day'])  . '</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Rue:</td>   <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'. explode(",",Yii::app()->session['Arrive']['adresse'])[0] .'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Ville:</td>  <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['Arrive']['cp_arr'].' '.Yii::app()->session['Arrive']['ville'].'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Type:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Région:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Pièces:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr>';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Étage:</td>             <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">' . Yii::app()->session['Arrive']['etage'] . '</td>';
            $strVar .=   '</tr>';

            $strVar .=   '</table>';
            $strVar .=   '</div>';
            $strVar .=   '<div style="width: 50%;float: left;color: #595959;font-size: 14px;font-family: sans-serif;font-weight: 400">';

            $strVar .=   '<table style="margin-bottom: 16px;">';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Ascenseur:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['Arrive']['ascenseur'].'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Autorisation de stationnemen:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Déballage:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Assemblage du mobilier: </td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Assemblage du Cuisine: </td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;"></td>';
            $strVar .=   '</tr>';
            $strVar .=   '<tr >';
            $strVar .=   '<td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">Distance jusque à lentrée:</td>     <td style="color: #595959;font-size: 12px;font-family: sans-serif;font-weight: 400;">'.Yii::app()->session['distance'].'</td>';
            $strVar .=   '</tr>';
            $strVar .=   '</table>';
            $strVar .=   '</div>';

            $strVar .=   '</div>';

            $strVar .=   '</div>';

            $strVar .=   '</div></div>';


            $strVar .= '<div style="width: 100%; margin:120px 0 500px 0; padding: 10px;">';
            $strVar .= '<div style="width: 40%;float:right;">';
            $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
            $strVar .= '</div>';
            $strVar .= '<div style="margin-top: 130px; float: left; width: 100%">';
            $strVar .= '<p style="font-family: sans-serif; color: #797979;"><span style="display: block">1. ' . $this->translation['service'] . ' <span style="text-transform: capitalize">'.Yii::app()->session['tarif']['name'].'</span></span></p>';
            $strVar .= '<div style="float: left; width: 100%;border: 2px solid #858585">';
            $strVar .= '<p style="font-family: sans-serif; color: #797979; margin: 5px;font-size: 14px"> ' .  $this->translation['pdf_items'] . ' : ';
            foreach($_SESSION['products'] as $key => $value){
                    $strVar .=  '<span>'.strip_tags( $value["option_count"],'<p>') .' '.$value["product_name"].'  </span>';
            }
            foreach($_SESSION['materials'] as $key => $value){
                    $strVar .=  '<span>'. $value["option_count"].' '.$value["material_name"].'  </span>';
            }

            $strVar .=  '</p>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</div>';



            $strVar .=  '<div style="width: 100%; margin: auto; padding: 30px;">';
            $strVar .=  '<div style="width: 40%;float:right;">';
            $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
            $strVar .=  '</div>';
            $strVar .=  '<p style="margin-top: 130px; float: left; width: 80%">';
            $strVar .=  '<p style="color: #616161;width:100%;font-size: 16px;font-weight: bold;font-family: Sans-Serif"> ' . $this->translation ['pdf_list_items'] . ' </p>';
            $strVar .=  '<hr style ="border: 0; border-bottom:2px solid #264d9c; border-top:2px solid #264d9c; color: #264d9c;height: 2px;background-color: #264d9c"/>';
            $strVar .=  '<p style="font-family: sans-serif; color: #797979;"> ' . $this->translation ['pdf_items_list_text'] . ' </p>';
            $strVar .=  '</p>';
            $strVar .=  '<table style="width: 100%;border-spacing:0">';
            $strVar .=  '<tr style="max-height: 20px;">';
            $strVar .=  '<td style="border: 0; color: #264d9c;font-family: sans-serif">  ' . $this->translation ['pdf_product_name'] . ' </td><td style="border: 0; color: #264d9c;font-family: sans-serif">' . $this->translation['pdf_product_unit'] . '</td><td style="font-family: sans-serif;border: 0; color: #264d9c;"> ' . $this->translation['pdf_product_count'] . ' </td>';
            $strVar .=  '</tr>';
            foreach ($_SESSION['products'] as $key =>$value){
                $strVar .=  '<tr style="">';
                $strVar .=  '<td style="height: 5px;font-family: sans-serif;color: #5b5b5b;border-bottom:1px solid #000; font-weight: bold">'.$value["product_name"].'</td><td style="height: 5px;border-bottom:1px solid #000;font-family: sans-serif;color: #5b5b5b;font-weight: bold"></td><td style="height: 5px;border-bottom:1px solid #000;font-family: sans-serif;color: #5b5b5b; font-weight: bold">'.$value["option_count"].'</td>';
                $strVar .=  '</tr>';
            }
             foreach ($_SESSION['materials'] as $key =>$value){
                $strVar .=  '<tr style="">';
                $strVar .=  '<td style="height: 5px;font-family: sans-serif;color: #5b5b5b;border-bottom:1px solid #000; font-weight: bold">'.$value["material_name"].'</td><td style="height: 5px;border-bottom:1px solid #000;font-family: sans-serif;color: #5b5b5b;font-weight: bold"></td><td style="height: 5px;border-bottom:1px solid #000;font-family: sans-serif;color: #5b5b5b;font-weight: bold">'.$value["option_count"].'</td>';
                $strVar .=  '</tr>';
            }

            $strVar .=  '</table>';
            $strVar .=  '</div>';
            $strVar .=  '</body>';
            $strVar .=  '</html>';



            $mpdf=new mPDF();
            $mpdf->WriteHTML($strVar);


            $strVar ='';

            $strVar .= '<!DOCTYPE html>';
            $strVar .= '<html lang="en">';
            $strVar .= '<head>';
            $strVar .= '<meta charset="UTF-8">';
            $strVar .= '<title>Title</title>';
            $strVar .= '</head>';
            $strVar .= '<body style="padding: 0;margin: 0">';
            $strVar .= '<div style="width: 100%;margin: auto; padding: 10px;">';
            $strVar .= '<div style="width: 60%;float:left;">';
            $strVar .= '<p style="font-family: sans-serif;text-transform: uppercase;font-size: 23px;color: #3c3c3d;font-weight: bold;">' . $this->translation['pdf_3page_header'] . '</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 15%;float:right;">';
            $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 100%;border: 2px solid #000;float: left">';
            $strVar .= '<div style="width: 14%;padding: 7px 0 0 5px;float: left;">';
            $strVar .= '<p style="font-size: 12px;font-weight: bold;padding: 0;margin: 0;" >' . $this->translation['pdf_info_rel'] . '</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 27%;padding: 0 5px;float: left;font-size: 12px">';
            $strVar .= '<table style="width: 100%; border-spacing:0">';
            $strVar .= '<tr >';
            $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-family: Sans-serif;font-size: 10px;">' . $this->translation['date'] . '</td>     <td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-family: Sans-serif">' . date('d.m.Y',Yii::app()->session['day']) . '</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">' . $this->translation['reference_number'] . '</td>   <td style="border-bottom: 1px solid #000; font-weight: bold;font-family: Sans-serif">'. Yii::app()->session['invoiceNumber'] .'</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">'. $this->translation['nombre_demenageur'] .'</td>  <td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-family: Sans-serif">'.Yii::app()->session['User_count'].'</td>';
            $strVar .= '</tr>';
            $strVar .= '</table>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 27%;padding: 0 5px;float: left;font-size: 12px">';
            $strVar .= '<table style="width: 100%; border-spacing:0">';
            $strVar .= '<tr >';
            $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">'. $this->translation['starting_hour'] .'</td><td style="font-family: Sans-serif;padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">'. $this->translation['finishing_hour'] .'</td><td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="padding: 5px;border-bottom: 1px solid #000;color: #a9a5a9;font-size: 10px;font-family: Sans-serif">' . $this->translation['volume'] . '</td><td style="padding: 5px;border-bottom: 1px solid #000;font-family: Sans-serif">'.Yii::app()->session['volume'].'m<sup>3</sup></td>';
            $strVar .= '</tr>';
            $strVar .= '</table>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 27%;padding: 0 5px;float: left;font-size: 12px">';
            $strVar .= '<table style="width: 100%; border-spacing:0">';
            $strVar .= '<tr >';
            $strVar .= '<td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-size: 10px;font-family: Sans-serif">' . $this->translation['auto_info'] . '</td><td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-family: Sans-serif"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-size: 10px;font-family: Sans-serif">' . $this->translation['auto_type'] . '</td><td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-family: Sans-serif" ></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="padding: 5px;color: #a9a5a9;border-bottom: 1px solid #000;font-size: 10px;font-family: Sans-serif">' . $this->translation['distance'] . '</td><td style="padding: 5px;border-bottom: 1px solid #000;font-family: Sans-serif">'.Yii::app()->session['distance'].'</td>';
            $strVar .= '</tr>';
            $strVar .= '</table>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 55%;float: left;font-family: sans-serif;">';
            $strVar .= '<p style="text-transform: uppercase; font-family: sans-serif;color: #264d9c;font-weight: bold;">'. $this->translation['address_depart'] .'</p>';
            $strVar .= '<p style="font-size: 14px; color: #676767;">';
            $strVar .= '<span style="display: block;font-weight: bold;">' .$this->translation['address_chargement']. '</span> <br />';
            $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['Depart']['adresse'].'</span><br />';
            $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['Depart']['cp_dep'].' '. Yii::app()->session['Depart']['ville'] .' </span> <br />';
            $strVar .= '<span style="display: block;width: 100%">'. explode(",", Yii::app()->session['Depart']['adresse'])[2] .'</span> <br />';
            $strVar .= '<span style="display: block">Etage:'. Yii::app()->session['Depart']['etage'] .'</span>';
            $strVar .= '</p>';
            $strVar .= '<p style="text-transform: uppercase; font-family: sans-serif;color: #264d9c;font-weight: bold;"> ' . $this->translation['escale_1'] . ' </p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 42%;float: right;font-family: sans-serif;">';
            $strVar .= '<p style="text-transform: uppercase; display:block;font-family: sans-serif;color: #264d9c;font-weight: bold;">'. $this->translation['address_arrive'] .'</p>';
            $strVar .= '<p style="font-size: 14px; color: #676767;">';
            $strVar .= '<span style="display: block;font-weight: bold;">'. $this->translation['Address_dechargement'] .'</span><br />';

            $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['Arrive']['adresse'].'</span><br />';
            $strVar .= '<span style="display: block;width: 100%">'.Yii::app()->session['Arrive']['cp_arr'].' '. explode(",", Yii::app()->session['Arrive']['adresse'])[1] .' </span><br />';
            $strVar .= '<span style="display: block;width: 100%">'. explode(",", Yii::app()->session['Arrive']['adresse'])[2] .'</span> <br />';
            $strVar .= '<span style="display: block">Etage:'. Yii::app()->session['Arrive']['etage'] .'</span>';
            $strVar .= '</p>';
            $strVar .= '<p style="text-transform: uppercase; font-family: sans-serif;color: #264d9c;font-weight: bold;">'. $this->translation['escale_2'] .'</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="float: left;margin-top: 30px;margin-bottom:20px;width: 100%">';
            $strVar .= '<div style="width: 55%;float: left;font-size: 14px">';
            $strVar .= '<table style="margin-bottom: 10px;border-spacing:0">';
            $strVar .= '<tr style="background-color: #e4e1e0;">';
            $strVar .= '<td style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">' . $this->translation['additional_service'] . '</td><td style="font-weight: 100">' . $this->translation['ordered'] . '</td><td style="font-weight: 100">' . $this->translation['put_in_place'] . '</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['halteverbotszone'] .'</td><td style ="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr style="background-color: #e4e1e0;">';
            $strVar .= '<th style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">'. $this->translation['carrying_distance'] .'</th><th style="font-weight: 100">' . $this->translation['planned'] . '</th><th style="font-weight: 100">' . $this->translation['Real'] . '</th>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['distance'] .'</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;">'.Yii::app()->session['distance'].'</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border-bottom:1px solid #000;">' . $this->translation['etage_count'] . '</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
            $strVar .= '</tr>';
            $strVar .= '</table>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 42%;float: right;font-size: 14px">';
            $strVar .= '<table style="margin-bottom: 10px;border-spacing:0">';
            $strVar .= '<tr style="background-color: #e4e1e0;">';
            $strVar .= '<td style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">' . $this->translation['additional_service'] . '</td><td style="font-weight: 100">' . $this->translation['ordered'] . '</td><td style="font-weight: 100">' . $this->translation['put_in_place'] . '</td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['halteverbotszone'] .'</td><td style ="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr style="background-color: #e4e1e0;">';
            $strVar .= '<th style="color: #264d9c; color: #264d9c;font-weight: bold;padding: 4px 3px;">'. $this->translation['carrying_distance'] .'</th><th style="font-weight: 100">' . $this->translation['planned'] . '</th><th style="font-weight: 100">' . $this->translation['Real'] . '</th>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border-bottom:1px solid #000;">'. $this->translation['distance'] .'</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;">'.Yii::app()->session['distance'].'</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="border-bottom:1px solid #000;">' . $this->translation['etage_count'] . '</td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td><td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>';
            $strVar .= '</tr>';
            $strVar .= '</table>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 100%;float: left;position: relative">';
            $strVar .= '<div style="width: 100%;bottom: 30px;">';
            $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
            $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
            $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
            $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
            $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
            $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['prenom'] .'</span></li>';
            $strVar .= '</ul>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
            $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
            $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
            $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
            $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
            $strVar .= '<li style="display: inline;float: right;padding-right: 15px;"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['nom'] . '</span></li>';
            $strVar .= '</ul>        </div>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</div>';



            $strVar .= '<div style="width: 100%; margin: auto;">';
            $strVar .= '<div style="width: 15%;float:right;">';
            $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 100%;float: left;position: absolute;height: 100%">';
            $strVar .= '<div style="width: 55%;float: left;margin-top: -25px;">';
            $strVar .= '<table style="border-spacing:0;width:100%;">';
            $strVar .= '<tr>';
            $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;width: 140px;font-weight:100;text-align: left">';
            $strVar .= $this->translation['object'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['available'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['prior'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['dismantling'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['packed'];
            $strVar .= '</th>';
            $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;font-weight:100;width: 150px;text-align: center">';
            $strVar .= $this->translation['comments'];
            $strVar .= '</th>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';

            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '</table >';
            $strVar .= '</div>';


            $strVar .= '<div style="width: 42%;float: right;">';
            $strVar .= '<table style="border-spacing:0;width: 100%;margin-bottom: 10px">';
            $strVar .= '<tr>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['received'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['lost'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['unpacking'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['assembly'];
            $strVar .= '</th>';
            $strVar .= '<th style="height: 140px;text-rotate: 75;color: #264d9c;font-family:Sans-serif;font-weight:100;padding: 0 10px;white-space: nowrap;">';
            $strVar .= $this->translation['stricken'];
            $strVar .= '</th>';
            $strVar .= '<th style="vertical-align: bottom;color: #264d9c;font-family:Sans-serif;font-weight:100;width: 150px;text-align: center">';
            $strVar .= $this->translation['comments'];
            $strVar .= '</th>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr>';
            $strVar .= '<td style="background-color: #e4e0e0; height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '<td style="background-color: #e4e0e0;height: 21px"></td>';
            $strVar .= '</tr>';
            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';

            $strVar .= '<tr >';
            $strVar .= '<td style="border-bottom:1px solid #000;height: 20px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-bottom:1px solid #000;border-left:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '<td style="border-left:1px solid #000;border-bottom:1px solid #000;height: 22.5px"></td>';
            $strVar .= '</tr>';
            $strVar .= '</table>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 3%;float: left;">';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 42%;float: left;">';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 100%;float: left;position: relative">';
            $strVar .= '<div style="width: 100%;bottom: 30px;">';
            $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
            $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
            $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
            $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
            $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
            $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['nom'] .'</span></li>';
            $strVar .= '</ul>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
            $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
            $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
            $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
            $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
            $strVar .= '<li style="display: inline;float: right;padding-right: 15px"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['nom'] . '</span></li>';
            $strVar .= '</ul>        </div>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '<div style="position: absolute;bottom: -10px;width: calc(100% - 60px);">';
            $strVar .= '<p style="font-size: 10px;">'. $this->translation['page_sign'] .'</p>';
            $strVar .= '</div>';
            $strVar .= '</div>';




            $strVar .= '<div style="width:100%; margin: auto;position: relative">';
            $strVar .= '<div style="width: 10%;float:right;">';
            $strVar .= '<img style="width: 100%;height: auto" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" />';
            $strVar .= '</div>';
            $strVar .= '<div style="float: left; width: 100%">';
            $strVar .= '<div>';
            $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;">'. $this->translation['page_7_comment'] .'</p>';
            $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;"> - '. $this->translation['page_7_comment_point1'] .'</p>';
            $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;"> - '. $this->translation['page_7_comment_point2'] .'</p>';
            $strVar .= '<p style="font-size: 12px;padding: 0; margin: 0;font-family: sans-serif;"> - '. $this->translation['page_7_comment_point3'] .'</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 100%;height: 300px;border: 2px solid #000;margin-top: 15px;margin-bottom: 15px;">';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 100%;position: absolute">';
            $strVar .= '<div style="width: 100%;bottom: 30px;">';
            $strVar .= '<div style="width: 55%;height: 80px;float: left;border: 2px solid #264d9c;">';
            $strVar .= '<p style="color: #264c9c;font-size: 10px;font-family: sans-serif;padding-left: 14px;">' . $this->translation['signature_text'] . '</p>';
            $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
            $strVar .= '<li style="display:inline-block;padding-right: 70px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['date'] . ' :</span> </li>';
            $strVar .= '<li style="display:inline-block;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
            $strVar .= '<li style="display:inline-block;float: right;margin-top:5px;font-size: 10px;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['client_stignature'] . '</span> <span style="display: block;font-size:10px;font-family: Sans-serif;color: #747474;font-weight: bold;">'. Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['nom'] .'</span></li>';
            $strVar .= '</ul>';
            $strVar .= '</div>';
            $strVar .= '<div style="width: 42%;height: 80px;float: right;border: 2px solid #264d9c;">';
            $strVar .= '<p style="color: #264c9c;font-size: 9px;font-family: sans-serif;padding-left: 14px;"> ' . $this->translation['signature_text'] . '</p>';
            $strVar .= '<ul style="list-style-type: none; padding-left: 15px;">';
            $strVar .= '<li style="display: inline;padding-right: 30px;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">'.  $this->translation['date']  .' :</span> </li>';
            $strVar .= '<li style="display: inline;"><span style="font-size: 10px;color: #aeacad;font-family: Sans-serif">' . $this->translation['signature'] . '</span></li>';
            $strVar .= '<li style="display: inline;float: right;padding-right: 15px;margin-top: 2px"><span style="font-size: 10px;font-family: Sans-serif;color: #aeacad">' . $this->translation['client_stignature'] . '</span> <span style="font-family: Sans-serif;font-size:10px;display: block;color: #747474;font-weight: bold;">' . Yii::app()->session['User']['nom'].' '. Yii::app()->session['User']['nom'] . '</span></li>';
            $strVar .= '</ul>        </div>';
            $strVar .= '</div>';
            $strVar .= '<div style="position: absolute;bottom: -10px;width: calc(100% - 60px);">';
            $strVar .= '<p style="font-size: 10px;">'. $this->translation['page_sign'] .'</p>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</body>';
            $strVar .= '</html>';


            $mpdf->AddPage('c', 'A4-L');
            $mpdf->WriteHTML($strVar);

            $file = fopen(UPLOADS."pdfFiles/".Yii::app()->session['invoiceNumber'].".pdf", "wb");
            $content = $mpdf->Output(UPLOADS."pdfFiles/".Yii::app()->session['invoiceNumber'].".pdf",'F');




            $strVar = '';



$strVar .= '<html>';
$strVar .= '<head>';
$strVar .= '</head>';
$strVar .= '<body style="font-family: sans-serif;padding: 0px;margin: 0px ">';
$strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 70px;padding: 0px">';
$strVar .= '<div style="width:45%;float:left"><img style="width: 70px;height: 40px" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" /></div>';
$strVar .= '<div style="width:20%;float:left;margin-top:25px"><i class="fa fa-phone" aria-hidden="true"></i><span style="color:#909090;font-size: 14px;font-weight: 600">' . Yii::app()->session['User']['telephone']. '</span></div>';
$strVar .= '<div style="width:20%;float:left;text-align:right;margin-top:25px"><span style="color:#909090;font-size: 12px;font-style: italic;text-align:right">Exemplaire Déménageur</span></div>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 80px">';
            $strVar.='<div style="width:100%;float:left;margin-top: 0">';
            $strVar .= '<div style="width:48%;float:left;margin-top: -15px;"><span style="color:#909090;font-size: 22px;font-weight: 600">Lettre de Voiture</span></div>';
            $strVar .= '<div style="width:100%;float:left;text-align:left;margin-top:-10px"><p style="color:#909090;font-size: 16px;width:100%">ref.<span style="color:#264d9c;font-size: 14px;">'. Yii::app()->session['User']['prenom'] . ' _ '. Yii::app()->session['invoiceNumber'] .'</span></p></p>';
            $strVar.='</div><br/>';
//$strVar .= '<hr style="width:100%;height: 0px;display:inline-block;border-top: 4px solid #264d9c" />';
$strVar .= '<div style="font-size: 10px;width:100%;color:#909090;margin-top:-21px;text-align: left">Document obligatoire par lŽarreté du '.date("d",Yii::app()->session['day'] ).' '. date("m",Yii::app()->session['day'] ) .' '.date("Y",Yii::app()->session['day'] ).'</div>';
$strVar .= '</div>';
$strVar .= '<div style="padding: 0px">';
$strVar .= '<div ><p style="font-size:13px;margin-top:0px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">1. Informations Générales</p></div>';
$strVar .= '<div style="height: 65px;width:100%;">';
$strVar .= '<div style="width:49%;float:left;border: 1px solid #000;height: 60px;">';
$strVar .= '<div style="width:50%;float:left;height: 60px;border-right: 1px solid #000;background-color: #e7e6e6">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Client</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Téléphone Client</p>';
$strVar .= '<p style="margin:0px;font-size: 10px;padding: 2px;font-weight: 600">Société de déménagement</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600">' . Yii::app()->session['User']['nom']. ' ' . Yii::app()->session['User']['prenom'] . '</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;color:#264d9c">' . Yii::app()->session['User']['portable']. '</p>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Futurama Express</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;border: 1px solid #000;height: 60px;">';
$strVar .= '<div style="width:49%;float:left;height: 60px;border-right: 0.5px solid #000;background-color: #e7e6e6">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Formule</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Volume total</p>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Distance</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600">' . Yii::app()->session['distance'] . '</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;margin-top:5px;height: 182px;text-align: center">';
$strVar .= '<div style="width:49.2%;float:left">';
$strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;clear: both">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
$strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['day'])  . ' '.Yii::app()->session['dayPart'].'</p>';
$strVar .= '<div style="width:100%">';
$strVar .= '<p style="margin:4px;font-size: 12px;padding: 1px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['adresse'] .'</p> <br/>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['cp_dep'] .'</p> <br/>';
$strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['ville'] .'</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
$strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
$strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
$strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Depart']['etage'] .'</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Depart']['ascenseur'] .'</p>';
$strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 1</p>';
$strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
$strVar .= '<div style="width:100%;height:60px">';
$strVar .= 'contract signed';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:49.2%;float:right">';
$strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 2</p>';
$strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
$strVar .= '<div style="width:100%;height:60px">';
$strVar .= 'contract signed';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
$strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['day'])  . ' '.Yii::app()->session['dayPart'].'</p>';
$strVar .= '<div style="width:100%">';
$strVar .= '<p style="margin:4px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Arrive']['adresse'] .'</p> <br/>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Arrive']['cp_arr'] .'</p>';
$strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Arrive']['ville'] .'</p>';
$strVar .= '</div>';
            $strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
            $strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
            $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
            $strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Arrive']['etage'] .'</p>';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Arrive']['ascenseur'] .'</p>';
            $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
            $strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">2. Prise en charge du Chargement</p></div>';
$strVar .= '<div style="width:100%;margin-top:2px;height: 75px;text-align: center">';
$strVar .= '<div style="width:49.2%;float:left;border:1px solid #000;height:75px">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Dommages constatés avant chargement (réservé déménageur)</p>';
$strVar .= '<div style="width:90%;margin:0px auto">';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:49.2%;float:right;border:1px solid #000;height:75px">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Signature client (décharge) au chargement</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">3. Déclaration de fin de travaux</p></div>';

$strVar .= '<div style="width:100%;margin-top:2px;height: 240px;text-align: center;float: left">';
$strVar .= '<div style="width:49%;float:left;border:1px solid #000;height:240px">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du client</p>';
$strVar .= '<div style="width:90%;margin:7px auto">';
$strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
$strVar .= '<p style="display:block;width:55%;float:left;text-align: left;margin: 0px">Recu mon mobilier : </p>';
$strVar .= '<div style="float:right;width:45%;">';
$strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Au complet et sans réserves</span></p>';
$strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block"> Avec les réserves ci-dessous</span></p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
$strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
$strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature client</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
$strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
$strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';

            $strVar .= '<div style="width:49%;float:right;border:1px solid #000;height:240px">';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du déménageur</p>';
            $strVar .= '<div style="width:90%;margin:7px auto;clear:both;">';
            $strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
            $strVar .= '<p style="display:block;width:45%;float:left;font-size:12px;text-align: left;margin: 0px">Immatriculation : </p>';
            $strVar .= '<div style="float:right;width:45%;">';
            $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 1 :</span></p>';
            $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 2 :</span></p>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '</div>';
            $strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
            $strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
            $strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature dem.</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
            $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
            $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</div>';

$strVar .= '<div style="width:100%;height: 37px;border:1px solid #000;margin-top: 10px;font-size: 8px;padding: 1.5px;text-align: center">';
$strVar .= '<p style="margin: 0px">La livraison donne lieu à des formalités impératives. Dans tous les cas, vous devez donner décharge à lentreprise en fin de livraison en signant ce document. En cas de dommages, identifier avec précision les pertes et';
$strVar .= 'avaries constatées, la mention "sous réserve de déballage ou de contrôle" nayant aucune valeur de preuve. Si vos réserves émises à la réception du mobilier ne sont pas acceptées par le professionel, ou si vous navez émis';
$strVar .= 'aucune réserve à la livraison,vous ne disposez alors que dun délai de 7 jours calendaires à compter de la réception des objets transportés pour émettre par lettre recommandée une protestation motivée sur létat du';
$strVar .= 'mobilier réceptionné en application de larticle L. 121-95 du code de la consommation.';
$strVar .= '</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;height: 25px;margin-top: 5px;font-size: 10px;padding: 1.5px;text-align: center">';
$strVar .= '<p style="margin: 0px">Move24 Group GmbH I Chausseestrasse 86 I 10115 Berlin | HRB 168985 B | Gérants: Marcel Rangnow, Ante Krsanac';
$strVar .= 'HypoVereinsbank | IBAN: DE57100208900025392566 | BIC: HYVEDEMM488 | St-Nr: 37/243/22935 | USt-IdNr DE 301320311';
$strVar .= '</p>';
$strVar .= '</div>';
$strVar .= '</div>';

$strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 70px;padding: 0px">';
$strVar .= '<div style="width:45%;float:left"><img style="width: 70px;height: 40px" src="'.Yii::app()->request->baseUrl.'/vendor/assets/images/logo.jpg" /></div>';
$strVar .= '<div style="width:20%;float:left;margin-top:25px"><i class="fa fa-phone" aria-hidden="true"></i><span style="color:#909090;font-size: 14px;font-weight: 600">' . Yii::app()->session['User']['telephone']. '</span></div>';
$strVar .= '<div style="width:20%;float:left;text-align:right;margin-top:25px"><span style="color:#909090;font-size: 12px;font-style: italic;text-align:right">Exemplaire Client</span></div>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;margin:0px auto;overflow: hidden;height: 80px">';
            $strVar.='<div style="width:100%;float:left;margin-top: 0">';
            $strVar .= '<div style="width:48%;float:left;margin-top: -10px"><span style="color:#909090;font-size: 22px;font-weight: 600">Lettre de Voiture</span></div>';
            $strVar .= '<div style="width:100%;float:left;text-align:left;margin-top:-10px"><p style="color:#909090;font-size: 16px;width:100%">ref.<span style="color:#264d9c;font-size: 14px;">'. Yii::app()->session['User']['prenom'] . ' _ '. Yii::app()->session['invoiceNumber'] .'</span></p></p>';
            $strVar.='</div><br/>';
//$strVar .= '<hr style="width:100%;height: 0px;display:inline-block;border-top: 4px solid #264d9c" />';
$strVar .= '<div style="font-size: 10px;width:100%;color:#909090;margin-top:-21px;text-align: left">Document obligatoire par lŽarreté du '.date("d",Yii::app()->session['day'] ).' '. date("m",Yii::app()->session['day'] ) .' '.date("Y",Yii::app()->session['day'] ).'</div>';
$strVar .= '</div>';
$strVar .= '<div style="padding: 0px">';
$strVar .= '<div ><p style="font-size:13px;margin-top:0px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">1. Informations Générales</p></div>';
$strVar .= '<div style="height: 65px;width:100%;">';
$strVar .= '<div style="width:49%;float:left;border: 1px solid #000;height: 60px;">';
$strVar .= '<div style="width:50%;float:left;height: 60px;border-right: 1px solid #000;background-color: #e7e6e6">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Client</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Téléphone Client</p>';
$strVar .= '<p style="margin:0px;font-size: 10px;padding: 2px;font-weight: 600">Société de déménagement</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600">' . Yii::app()->session['User']['nom']. ' ' . Yii::app()->session['User']['prenom'] . '</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;color:#264d9c">' . Yii::app()->session['User']['portable']. '</p>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Futurama Express</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;border: 1px solid #000;height: 60px;">';
$strVar .= '<div style="width:49%;float:left;height: 60px;border-right: 0.5px solid #000;background-color: #e7e6e6">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Formule</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600">Volume total</p>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 1px;font-weight: 600">Distance</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;height: 60px;text-align: center">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 10px;padding: 2px;font-weight: 600"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600">' . Yii::app()->session['distance'] . '</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;margin-top:5px;height: 182px;text-align: center">';
$strVar .= '<div style="width:49.2%;float:left">';
$strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;clear: both">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
$strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['day'])  . ' '.Yii::app()->session['dayPart'].'</p>';
$strVar .= '<div style="width:100%">';
$strVar .= '<p style="margin:4px;font-size: 12px;padding: 1px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['adresse'] .'</p> <br/>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['cp_dep'] .'</p> <br/>';
$strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Depart']['ville'] .'</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
$strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
$strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
$strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Depart']['etage'] .'</p>';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Depart']['ascenseur'] .'</p>';
$strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 1</p>';
$strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
$strVar .= '<div style="width:100%;height:60px">';
$strVar .= 'contract signed';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:49.2%;float:right">';
$strVar .= '<div style="width:49.5%;float:left;border:1px solid #000;height:182px;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Escale 2</p>';
$strVar .= '<p style="margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 1px;font-weight: 600;height: 16px"></p>';
$strVar .= '<div style="width:100%;height:60px">';
$strVar .= 'contract signed';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;float:right;border-top: 0.5px solid #000;height: 75px;">';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:47%;float:right;border:1px solid #000;height:182px;">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Chargement</p>';
$strVar .= '<p style="margin-bottom:1px;margin-top:0px;border-bottom: 1px solid #000;font-size: 12px;padding: 2px;font-weight: 600">' .date('d.m.Y',Yii::app()->session['day'])  . ' '.Yii::app()->session['dayPart'].'</p>';
$strVar .= '<div style="width:100%">';
$strVar .= '<p style="margin:4px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Arrive']['adresse'] .'</p> <br/>';
$strVar .= '<p style="margin:0px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Arrive']['cp_arr'] .'</p>';
$strVar .= '<p style="margin:3px;font-size: 12px;padding: 2px;font-weight: 600;background-color: #ffffff">'. Yii::app()->session['Arrive']['ville'] .'</p>';
$strVar .= '</div>';
            $strVar .= '<div style="width:100%;float:right;border-top: 1px solid #000;height: 69px;font-size: 10px;">';
            $strVar .= '<div style="width:49%;float:left;height: 70px;border-right: 1px solid #000;background-color: #e7e6e6">';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Etage </p>';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">Ascenseur</p>';
            $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;">Portage (m)</p>';
            $strVar .= '<p style="margin:0px;padding: 2px;">Monte-meuble</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width:49%;float:right;height: 70px;text-align: center;">';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Arrive']['etage'] .'</p>';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;padding: 2px;">'. Yii::app()->session['Arrive']['ascenseur'] .'</p>';
            $strVar .= '<p style="margin:0px;border-bottom: 0.5px solid #000;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
            $strVar .= '<p style="margin:0px;padding: 2px;"><span style="visibility: hidden">sadas</span></p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">2. Prise en charge du Chargement</p></div>';
$strVar .= '<div style="width:100%;margin-top:2px;height: 75px;text-align: center">';
$strVar .= '<div style="width:49.2%;float:left;border:1px solid #000;height:75px">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Dommages constatés avant chargement (réservé déménageur)</p>';
$strVar .= '<div style="width:90%;margin:0px auto">';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div style="width:49.2%;float:right;border:1px solid #000;height:75px">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 2px;font-weight: 600;background-color: #e7e6e6">Signature client (décharge) au chargement</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<div ><p style="margin-top:10px;font-size:13px;background-color:#595959;height: 20px;border:1px solid #000;line-height:18px;color:#ffffff">3. Déclaration de fin de travaux</p></div>';

$strVar .= '<div style="width:100%;margin-top:2px;height: 240px;text-align: center;float: left">';
$strVar .= '<div style="width:49%;float:left;border:1px solid #000;height:240px">';
$strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du client</p>';
$strVar .= '<div style="width:90%;margin:7px auto">';
$strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
$strVar .= '<p style="display:block;width:55%;float:left;text-align: left;margin: 0px">Recu mon mobilier : </p>';
$strVar .= '<div style="float:right;width:45%;">';
$strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Au complet et sans réserves</span></p>';
$strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block"> Avec les réserves ci-dessous</span></p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
$strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
$strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature client</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
$strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
$strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</div>';

            $strVar .= '<div style="width:49%;float:right;border:1px solid #000;height:240px">';
            $strVar .= '<p style="margin:0px;border-bottom: 1px solid #000;font-size: 11px;padding: 3px;font-weight: 600;background-color: #e7e6e6">Observations du déménageur</p>';
            $strVar .= '<div style="width:90%;margin:7px auto;clear:both;">';
            $strVar .= '<div style="width:100%;height:35px;font-size: 10px">';
            $strVar .= '<p style="display:block;width:45%;float:left;font-size:12px;text-align: left;margin: 0px">Immatriculation : </p>';
            $strVar .= '<div style="float:right;width:45%;">';
            $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 1 :</span></p>';
            $strVar .= '<p style="margin: 0px"><span style="margin-top:3px;display:inline-block;height: 5px;width:5px;border: 1px solid #000"></span><span style="display:inline-block">Véhicule 2 :</span></p>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block ;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '<hr style="height: 0px;border-top:1px;display: inline-block;margin-top:10px "/>';
            $strVar .= '</div>';
            $strVar .= '<div style="width:100%;height: 30px;margin-top: 10px;border-top:1px solid #000">';
            $strVar .= '<div style="width:28%;height: 40px;border-right:1px solid #000;float: left;">';
            $strVar .= '<p style="border-top:0.5px solid #000;margin:0px;font-size: 11px;padding:15px 2px 2px 2px;font-weight: 600;background-color: #e7e6e6;height:24px">Signature dem.</p>';
            $strVar .= '</div>';
            $strVar .= '<div style="width:68%;height: 30px;float: right;text-align: left;font-size: 12px">';
            $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:left;">A:</p>';
            $strVar .= '<p style="height:30px;margin:2px;font-weight: 600;width:48%;float:right;">Le:</p>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</div>';
            $strVar .= '</div>';

$strVar .= '<div style="width:100%;height: 37px;border:1px solid #000;margin-top: 10px;font-size: 8px;padding: 1.5px;text-align: center">';
$strVar .= '<p style="margin: 0px">La livraison donne lieu à des formalités impératives. Dans tous les cas, vous devez donner décharge à lentreprise en fin de livraison en signant ce document. En cas de dommages, identifier avec précision les pertes et';
$strVar .= 'avaries constatées, la mention "sous réserve de déballage ou de contrôle" nayant aucune valeur de preuve. Si vos réserves émises à la réception du mobilier ne sont pas acceptées par le professionel, ou si vous navez émis';
$strVar .= 'aucune réserve à la livraison,vous ne disposez alors que dun délai de 7 jours calendaires à compter de la réception des objets transportés pour émettre par lettre recommandée une protestation motivée sur létat du';
$strVar .= 'mobilier réceptionné en application de larticle L. 121-95 du code de la consommation.';
$strVar .= '</p>';
$strVar .= '</div>';
$strVar .= '<div style="width:100%;height: 25px;margin-top: 5px;font-size: 10px;padding: 1.5px;text-align: center">';
$strVar .= '<p style="margin: 0px">Move24 Group GmbH I Chausseestrasse 86 I 10115 Berlin | HRB 168985 B | Gérants: Marcel Rangnow, Ante Krsanac';
$strVar .= 'HypoVereinsbank | IBAN: DE57100208900025392566 | BIC: HYVEDEMM488 | St-Nr: 37/243/22935 | USt-IdNr DE 301320311';
$strVar .= '</p>';
$strVar .= '</div>';
$strVar .= '</div>';
$strVar .= '</body>';
$strVar .= '</html>';


//            Helpers::dump($strVar);die;

            $mpdf=new mPDF();
            $mpdf->WriteHTML($strVar);

            $file = fopen(UPLOADS.'/pdfFiles/1_'.Yii::app()->session['invoiceNumber'].'.pdf', "wb");
            $content = $mpdf->Output(UPLOADS.'/pdfFiles/1_'.Yii::app()->session['invoiceNumber'].'.pdf','F');

            $model_order = new OrderInfo();

            $model_order->order_id = $model->id;
            $model_order->pdf = $model->invoice_number . '.pdf';
            $model_order->information_table = $this->drawTable();
            if($model_order->save()){
                require bDIR.'/protected/extensions/PHPMailer-master/PHPMailerAutoload.php';

                $mail = new PHPMailer;

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = Yii::app()->params->smtp;  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = Yii::app()->params->contactEmail;                 // SMTP Username
                $mail->Password = Yii::app()->params->password;                           // SMTP password
                $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 465;                                    // TCP port to connect to

                $mail->setFrom(Yii::app()->params->contactEmail, 'Futurama Express');
                $mail->addAddress(Yii::app()->session['User']['email'], Yii::app()->session['User']['nom']);     // Add a recipient
                $mail->addReplyTo(Yii::app()->params->contactEmail, 'Information');
                $mail->addAttachment(UPLOADS.'/pdfFiles/'.Yii::app()->session['invoiceNumber'].'.pdf');         // Add attachments
                $mail->addAttachment(UPLOADS.'/pdfFiles/1_'.Yii::app()->session['invoiceNumber'].'.pdf');         // Add attachments
                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = 'Your order letter';
                $mail->Body    = 'Futurama Express';
                $mail->AltBody = 'Futurama Express';
                $mail->SMTPDebug = 1;

//
                if(!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                }else {
                    echo 'Message has been sent';
                    Yii::app()->user->setFlash('success','Thanks, your message sent!');
                    $this->redirect(array('default/index'));

                }

            }

            die;
        }


    }
    public function drawTable(){

        $strVar = '';
        $strVar .= '<div style="text-align: center">';
        $strVar .= '<table style="width: 100%;border: 1px solid black;border-collapse: collapse;">';
        $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">User information</th>';
        foreach (Yii::app()->session['User'] as $key => $value ){
            $strVar .= '<tr>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $key .'</td>';
            $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $value .'</td>';
            $strVar .= '</tr>';
        }
        $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Depart Information</th>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Address</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['adresse'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Post</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['cp_dep'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Ville</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['ville'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Surface</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['surface_depart'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['depart_url'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['depart_geo'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Country</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['pays_dep'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">House type</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Depart']['type'] .'</td>';
        $strVar .= '</tr>';

        $strVar .= '<th colspan="2" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Arrive Information</th>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Address</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['adresse'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Post</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['cp_dep'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Ville</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['ville'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Surface</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['surface_depart'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['depart_url'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Depart Url</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['depart_geo'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">Country</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['pays_dep'] .'</td>';
        $strVar .= '</tr>';
        $strVar .= '<tr>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">House type</td>';
        $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. Yii::app()->session['Arrive']['type'] .'</td>';
        $strVar .= '</tr>';



        $strVar .= '<th colspan="3" style="border: 1px solid black;padding: 10px;text-align: center;color: #3c8dbc">Products</th>';
        foreach($_SESSION['products'] as $key =>$value){

                $strVar .= '<tr>';
                $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">' . $value["product_name"] . '</td>';
                $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $value["object_count"] .'</td>';
                $strVar .= '</tr>';
        }
        foreach($_SESSION['materials'] as $key =>$value){

                $strVar .= '<tr>';
                $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">' . $value["material_name"] . '</td>';
                $strVar .= '<td style="border: 1px solid black;padding: 10px;text-align: left">'. $value["object_count"] .'</td>';
                $strVar .= '</tr>';
        }

        $strVar .= '</table>';
        $strVar .= '</div>';

        return $strVar;

    }
//    public function actionContact()
//    {
//        $model=new ContactForm;
//        if(isset($_POST['ContactForm']))
//        {
//            $model->attributes=$_POST['ContactForm'];
//            if($model->validate())
//            {
//                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
//                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
//                $headers="From: $name <{$model->email}>\r\n".
//                    "Reply-To: {$model->email}\r\n".
//                    "MIME-Version: 1.0\r\n".
//                    "Content-Type: text/plain; charset=UTF-8";
//
//                mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
//                Yii::app()->User->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
//                $this->refresh();
//            }
//        }
//        $this->render('contact',array('model'=>$model));
//    }
//
//    /**
//     * Displays the login page
//     */
//    protected function sendEmail($email,$subject,$message){
//
//        $mail=Yii::app()->params->contactEmail;
//        $password = Yii::app()->params->password;
//        $smtp =Yii::app()->params->smtp;
//        $mailSMTP = new SendMailSmtpClass( $mail, $password, 'ssl://'.$smtp, $mail, 465);
//        $headers= "MIME-Version: 1.0\r\n";
////        $file_to_attach = UPLOADS."/pdfFiles/";
//        $message = "asdas";
//        $headers .= "Content-type: text/html; charset=utf-8\r\n";
//        $headers .= "From:  <$email>\r\n";
//
//        $result   =  $mailSMTP->send("$email",
//            "
//            : ".$subject,
//            $message,
//            $headers);
//        if($result === true){
//            return true;
//        }else {
//            return false;
//        }
//
//    }
}