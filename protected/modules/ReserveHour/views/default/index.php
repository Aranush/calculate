<div id="wrapper" class="clearfix">
	<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/tunnel-commande.js"></script>

	<section class="bg_fond_comande" id="content_">
		<div class="content-wrap">
			<div class="container clearfix nopadding_responsive">
				<br clear="all">
				<div class="row">
					<div class="col-xs-12">
						<section class="bg_fond_comande" id="content_">
							<div class="content-wrap-padding-top content-wrap">
								<div class="container clearfix nopadding">
									<div class="col-md-9 commande nomargin nopadding" id="information">
										<div>
										</div>
										<div class="col-md-12 bg_white border_div">
											<div class="heading-block center" style="margin:10px auto">
												<h1 class="orange nopadding nomargin font_20" style="font-size:20px; color:#ff5900;">PERSONNALISEZ VOTRE FORMULE DE DEMENAGEMENT ET RESERVEZ DIRECTEMENT EN LIGNE - Déménagement à la carte</h1>
											</div>
											<div class="center">
												<p>
													Avec Des bras en plus, réservez votre déménagement directement en ligne grâce à nos offres à la carte. On ne vous impose aucun devis de déménagement, c’est vous, selon votre budget et selon vos besoins qui configurez votre prestation de déménagement :<br />
													Nombre de déménageurs professionnels, taille du camion, durée de la prestation et bien sur, tous les services et le matériel nécessaire à votre déménagement.
												</p>
											</div>
										</div>
									</div>
									<div class="col-xs-12 hidden" id="selectors">
										<section class="panel form-wizard" id="w4">
											<div class="wizard-progress wizard-progress-lg">
												<div class="steps-progress">
													<div class="progress-indicator"></div>
												</div>
												<ul class="wizard-steps">
													<li class="active tunnel">
														<a href="#basket"><span>1</span>Panier</a>
													</li>
													<li class=" tunnel">
														<a href="/coordonnee"><span>2</span>Coordonnées</a>
													</li>
													<li class=" tunnel">
														<a href="#" onclick="return false"><span>3</span>Paiement</a>
													</li>
												</ul>
											</div>
										</section>
									</div>
									<div class="col-xs-12">&nbsp;</div>
									<div class="col-md-12">&nbsp;</div>
									<div class="col-md-9 commande nomargin nopadding">
										<?php if(Yii::app()->user->hasFlash('success')): ?>
										<div class="alert alert-danger calc-alert alert-dismissible fade in" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong> <?php echo Yii::app()->user->getFlash('success'); ?></strong>
										</div>
										<?php endif;?>
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="home">
												<div class="col-md-12 bg_white border_div">
													<div class="col-md-12 encart_web">&nbsp;</div>
													<h2 class="font_18 gris maj border_bottom_gris"><i class="fa fa-truck"></i>  Configuration du déménagement</h2>
													<form id="page_1" method="POST">
														<input type="hidden" value="<?=(!empty(Yii::app()->session['personNumber'])) ? Yii::app()->session['personNumber']['id'] : "5"?>" id="p1_user" name="Page_1[user]" />
														<input type="hidden" value="<?=(!empty(Yii::app()->session['hour'])) ? Yii::app()->session['hour']['hours'] : "4"?>" id="p1_hour" name="Page_1[hour]" />
														<input type="hidden" value="<?=(!empty(Yii::app()->session['vehicle'])) ? Yii::app()->session['vehicle']['id'] : "12"?>" id="p1_vehicle" name="Page_1[vehicle]" />
														<input type="hidden" value="<?=(!empty(Yii::app()->session['day'])) ? Yii::app()->session['day'] : ""?>" id="p1_day" name="Page_1[day]" />
														<input type="hidden" value="<?=(!empty(Yii::app()->session['distance'])) ? Yii::app()->session['distance'] : ""?>" id="p1_distance" name="Page_1[distance]" />
														<input type="hidden" value="<?=(!empty(Yii::app()->session['dayPart'])) ? Yii::app()->session['dayPart'] : ""?>" id="p1_dayPart" name="Page_1[dayPart]" />
													</form>

													<div class="col-md-4 bg_white border_right_gris min_230" id="prestation_1">
														<div class="col-md-12 block margin margin_bottom_10 tab-content">
															<?php
															foreach($persons as $key=>$value):
																?>

																<?php
																	$active = "";

																	if(Yii::app()->session['personNumber']){
																		if(Yii::app()->session['personNumber']['id'] == $value['id']) {
																			$active = "active";
																		}
																	}else if($key==0) {
																		$active = "active";
																	}
																?>
																<div role="tabpanel" class="tab-pane <?=$active?>" id="user-<?php echo $value['id']?>">
																	<img class="center_picture img-responsive" src="<?=Yii::app()->request->BaseUrl?>/upload/persones/<?php echo $value['image']?>" alt="" />
																</div>
															<?php endforeach;?>
														</div>
														<div class="encart_home_texte">
															<h4 class="gris center">Nombre de déménageurs <a href="#" id="div_aide_1" class="aide" rel="nofollow" onclick="return false"><i class="fa fa-question-circle silver"></i></a></h4>
															<div style="text-align:center;" class="class_1">
																<?php foreach($persons as $key=>$value):?>
																	<?php
																	$active = "no_actif";

																	if(Yii::app()->session['personNumber']){
																		if(Yii::app()->session['personNumber']['id'] == $value['id']) {
																			$active = "actif";
																		}
																	}else if($key==0) {
																		$active = "actif";
																	}
																	?>


																	<a href="#<?php echo $key?>" data-id="<?php echo $value['id']?>" aria-controls="1" role="tab" data-toggle="tab" class="<?php if($key>2):?>person-active hidden_user <?php endif;?>btn_demenageur <?=$active?> btn"  ><?php echo $value['person_number']?></a>
																<?php endforeach;?>
																<?php if(count($persons)>3):?>
																	<div class="col-md-12" id="plus_demenageur" style="margin-top:8px;">
																		<div class="orange bold" id="btn_demenageur" data-rel="+" style="cursor:pointer;">+ Plus de choix</div>
																	</div>
																<?php endif;?>


															</div>
															<br clear="all" />
														</div>
													</div>

													<div class="col-md-4 bg_white border_right_gris min_230" id="prestation_2">
														<div class="col-md-12 block margin margin_bottom_10 tab-content">
															<?php foreach($hours as $key=>$value):?>
																<?php
																$active = "";
//																	Helpers::dump($value);die;
																if(Yii::app()->session['hour']){
																	if(Yii::app()->session['hour']['id'] == $value['id']) {
																		$active = "active";
																	}
																}else if($key==0) {
																	$active = "active";
																}
																?>

																<div role="tabpanel" class="tab-pane <?=$active?>" id="hours-<?php echo $value['id']?>">
																	<img class="center_picture img-responsive" src="<?=Yii::app()->request->BaseUrl?>/upload/hours/<?php echo $value['image']?>" alt="" />
																</div>
															<?php endforeach;?>
														</div>
														<div class="encart_home_texte">
															<h4 class="gris center">Nombre de déménageurs <a href="#" id="div_aide_1" class="aide" rel="nofollow" onclick="return false"><i class="fa fa-question-circle silver"></i></a></h4>
															<div style="text-align:center;" class="class_1">
																<?php foreach($hours as $key=>$value):?>
																	<?php
																		$active = "no_actif";
	//																	Helpers::dump($value);die;
																		if(Yii::app()->session['hour']){
																			if(Yii::app()->session['hour']['id'] == $value['id']) {
																				$active = "actif";
																			}
																		}else if($key==0) {
																			$active = "actif";
																		}
																	?>
																	<a href="#<?php echo $key?>" data-id="<?php echo $value['id']?>" aria-controls="1" role="tab" data-toggle="tab" class="<?php if($key>2):?>hour-active hidden_hour <?php endif;?>btn_hour <?=$active?> btn"  ><?php echo $value['hours']?>h</a>
																<?php endforeach;?>
																<?php if(count($hours)>3):?>
																	<div class="col-md-12" id="plus_hour" style="margin-top:8px;">
																		<div class="orange bold" id="btn_hour" data-rel="+" style="cursor:pointer;">+ Plus de choix</div>
																	</div>
																<?php endif;?>


															</div>
															<br clear="all" />
														</div>
													</div>

													<div class="col-md-4 bg_white" id="prestation_3">
														<div class="col-md-12 block margin margin_bottom_10 tab-content">
															<?php foreach($vehicles as $key=>$value):?>
																<?php
																$active = "";
																if(Yii::app()->session['vehicle']){
																	if(Yii::app()->session['vehicle']['id'] == $value['id']) {
																		$active = "active";
																	}
																}else if($key==0) {
																	$active = "active";
																}
																?>
																<div role="tabpanel" class="tab-pane <?=$active?>" id="vehicle-<?php echo $value['id']?>">
																	<img class="center_picture img-responsive" src="<?=Yii::app()->request->BaseUrl?>/upload/vehicle/<?php echo $value['image']?>" alt="" />
																</div>
															<?php endforeach;?>
														</div>
														<div class="encart_home_texte">
															<h4 class="gris center">Nombre de déménageurs <a href="#" id="div_aide_1" class="aide" rel="nofollow" onclick="return false"><i class="fa fa-question-circle silver"></i></a></h4>
															<div style="text-align:center;" class="class_1">
																<?php foreach($vehicles as $key=>$value):?>
																	<?php
																	$active = "no_actif";
																	if(Yii::app()->session['vehicle']){
																		if(Yii::app()->session['vehicle']['id'] == $value['id']) {
																			$active = "actif";
																		}
																	}else if($key==0) {
																		$active = "actif";
																	}
																	?>
																	<a href="#<?php echo $key?>" data-id="<?php echo $value['id']?>" aria-controls="1" role="tab" data-toggle="tab" class="<?php if($key>2):?>vehicle-active hidden_vehicle <?php endif;?>btn_vehicle <?=$active?> btn"  ><?php echo  $value['vehicle_volume']?>M <sup>3</sup></a>
																<?php endforeach;?>
																<?php if(count($vehicles)>3):?>
																	<div class="col-md-12" id="plus_vehicle" style="margin-top:8px;">
																		<div class="orange bold" id="btn_vehicle" data-rel="+" style="cursor:pointer;">+ Plus de choix</div>
																	</div>
																<?php endif;?>


															</div>
															<br clear="all" />
														</div>

													</div>

													<!--
                                                    </form>
                                                    -->
													<br clear="all" />
													<div class="distance_km">
														<div class="col-md-12 encart_web">&nbsp;</div>
														<div class="col-md-12 col-xs-12 border_top_gris">
															<div class="col-md-9" style="margin-top:5px;">
																<h4 class="gris" style="font-size:1.2em;">Distance entre l’adresse de départ et d’arrivée - 30 km offerts <a href="#" id="div_aide_4" class="aide" rel="nofollow" onclick="return false"><i class="fa fa-question-circle"></i></a></h4>
															</div>
															<div class="col-md-3">
																<div class="form-group has-feedback">
																	<span class="yes-km glyphicon glyphicon-ok form-control-feedback" aria-hidden="true" style="display:none;"></span>
																	<span class="no-km glyphicon glyphicon-remove form-control-feedback" aria-hidden="true" ></span>
																	<input type="text" class="form-control" id="distance" placeholder="Km" name="km" id="nb_km" value="<?=(Yii::app()->session['distance']) ? intval(Yii::app()->session['distance']) : "" ?>" required>
																	<span class="result_km red"></span>
																</div>
															</div>
														</div>
														<br clear="all">
														<div class="col-md-12 texte_mobile">&nbsp;</div>
													</div>
												</div>
												<div class="col-md-12">&nbsp;</div>
												<div class="col-md-12 bg_white border_div div_aide div_aide_1" style="display:none;">
													<div class="col-md-12 right">
														<a href="#" class="ferme_aide font_12 italic">fermer</a>
													</div>
													<div class="col-md-12">
														<h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Nombre de déménageurs</h4>
														<p><span style="font-family: Arial; ">Un doute sur la personnalisation de votre formule? Voici quelques crit&egrave;res &agrave; prendre en consid&eacute;ration:</span></p>
														<ul>
															<li style="margin: 0px; font-family: Arial; ">Combien de personnes pourront pr&ecirc;ter main forte aux d&eacute;m&eacute;nageurs ?</li>
															<li style="margin: 0px; font-family: Arial; ">Quelles sont les difficult&eacute;s (acc&egrave;s, &eacute;tage, pr&eacute;sence d&#39;un ascenseur ...) ?</li>
															<li style="margin: 0px; font-family: Arial; ">O&ugrave; en serez-vous le jour du d&eacute;m&eacute;nagement (avancement de l&#39;emballage, d&eacute;montage/remontage &agrave; pr&eacute;voir...) ?</li>
															<li style="margin: 0px; font-family: Arial; ">A quelles t&acirc;ches seront affect&eacute;s les d&eacute;m&eacute;nageurs (chargement, emballage, d&eacute;montage ...) ?</li>
														</ul>
														<p style="margin: 0px 0px 12px; font-family: Arial; ">Cas pratique: 2 d&eacute;m&eacute;nageurs sur 1/2 journ&eacute;e pour un studio de 20 &agrave; 25m<span style="font-size: 10px; "><sup>2</sup></span> peut se transformer en 1 d&eacute;m&eacute;nageur + 2 copains (selon le copain ;-) c&rsquo;est &ccedil;a le d&eacute;m&eacute;nagement participatif !</p>
														<p style="margin: 0px 0px 12px; font-size: 10px; font-family: Arial; ">(Cas pratique &agrave; titre d&#39;exemple uniquement)</p>
														<br clear="all">
													</div>
												</div>
												<div class="col-md-12 div_aide div_aide_1" style="display:none;">&nbsp;</div>
												<div class="col-md-12 bg_white border_div div_aide div_aide_2" style="display:none;">
													<div class="col-md-12 right">
														<a href="#" class="ferme_aide font_12 italic">fermer</a>
													</div>
													<div class="col-md-12">
														<h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Durée de la prestation</h4>
														<p>Dans un d&eacute;lai de 48h apr&egrave;s votre commande, un conseiller <strong>Des bras en plus</strong> vous contactera pour convenir avec vous de l&rsquo;horaire de votre prestation.</p>
														<p>Besoin de plus de temps le jour du d&eacute;m&eacute;nagement ?&nbsp;Pas de probl&egrave;me, il vous est possible de prolonger la dur&eacute;e de votre prestation le jour m&ecirc;me au prix de 70 &euro; TTC par heure (non divisible et par d&eacute;m&eacute;nageur)</p>
														<br clear="all">
													</div>
												</div>
												<div class="col-md-12 div_aide div_aide_2" style="display:none;">&nbsp;</div>
												<div class="col-md-12 bg_white border_div div_aide div_aide_3" style="display:none;">
													<div class="col-md-12 right">
														<a href="#" class="ferme_aide font_12 italic">fermer</a>
													</div>
													<div class="col-md-12">
														<h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Taille du camion</h4>
														<p style="text-align: justify; "><span style="font-family: Arial; ">Le camion de 22m</span><span style="font-family: Arial; font-size: 10px; "><sup>3</sup></span><span style="font-family: Arial; "> est le v&eacute;hicule le plus couramment utilis&eacute; dans le d&eacute;m&eacute;nagement. Il peut contenir en moyenne l&#39;&eacute;quivalent d&#39;un appartement de 40/45m</span><span style="font-family: Arial; font-size: 10px; "><sup>2</sup></span><span style="font-family: Arial; ">.</span></p>
														<p style="margin: 0px 0px 12px; font-family: Arial; ">Une camionnette de 12m<span style="font-size: 10px; "><sup>3</sup></span> suffit en moyenne &agrave; transporter l&#39;&eacute;quivalent d&#39;un studio de 20/25m<span style="font-size: 10px; "><sup>2</sup></span>.</p>
														<p style="margin: 0px 0px 12px; font-family: Arial; ">Caract&eacute;ristiques des v&eacute;hicules (donn&eacute;es &agrave; titre indicatif)</p>
														<table border="1" cellpadding="0" cellspacing="0">
															<thead>
															<tr>
																<th class="col-md-4"><p align="center">&nbsp;</p></th>
																<th class="col-md-4"><p align="center">12 m<sup>3</sup></p></th>
																<th class="col-md-4"><p align="center">22 m<sup>3</sup></p></th>
															</tr>
															</thead>
															<tbody>
															<tr>
																<td class="col-md-4"><p>Dimensions ext&eacute;rieures (m)</p></td>
																<td class="col-md-4">
																	<p>Longueur 6,00</p>
																	<p>Hauteur 2,80</p>
																	<p>Largeur 2,00</p>
																</td>
																<td class="col-md-4">
																	<p>Longueur 6,85</p>
																	<p>Hauteur 3,40</p>
																	<p>Largeur 2,30</p>
																</td>
															</tr>
															<tr>
																<td class="col-md-4"><p>Dimensions int&eacute;rieures (m)</p></td>
																<td class="col-md-4">
																	<p>Longueur 3,52</p>
																	<p>Hauteur 1,90</p>
																	<p>Largeur 1,80</p>
																</td>
																<td class="col-md-4">
																	<p>Longueur 4,28</p>
																	<p>Hauteur 2,20</p>
																	<p>Largeur 2,02</p>
																</td>
															</tr>
															<tr>
																<td class="col-md-4">
																	<p>Visuel des v&eacute;hicules</p>
																	<p>(photos non contractuelles)</p>
																</td>
																<td class="col-md-4">
																	<img class="col-md-12" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/site/Master12m3.jpg" alt="Camion 12m3" />
																	<p>(c) image constructeur</p>
																</td>
																<td class="col-md-4">
																	<img class="col-md-12" alt="Camion demenagement 22m3" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/site/renault-master_22m3.jpg" />
																	<p>(c) image constructeur</p>
																</td>
															</tr>
															</tbody>
														</table>
														<p style="text-align: justify; ">Astuce : Si vous h&eacute;sitez entre les deux tailles de camion, prenez la taille la plus grande, cela vous &eacute;vitera de perdre du temps &agrave; faire des allers-retours.</p>
														<br clear="all">
													</div>
												</div>
												<div class="col-md-12 div_aide div_aide_3" style="display:none;">&nbsp;</div>
												<div class="col-md-12 bg_white border_div div_aide div_aide_4" style="display:none;">
													<div class="col-md-12 right">
														<a href="#" class="ferme_aide font_12 italic">fermer</a>
													</div>
													<div class="col-md-12">
														<h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Distance</h4>
														<p> La distance vous est demandée uniquement si vous avez besoin d'un véhicule.</p>
														<p>30 kilomètres sont inclus à votre prestation sans supplément de prix. Le km supplémentaire est quand à lui facturé 3€ TTC .</p>
														<p>S'il y a plusieurs adresses de chargement/livraison, indiquez la distance totale à parcourir avec le véhicule.</p>
														<p>Si vous ne connaissez pas la distance entre votre adresse de départ et d'arrivée, n'hésitez pas à utiliser notre outil d'estimation de distance.</p>                                    <br clear="all">
														<div class="col-md-12">
															<form name="form_outils" id="form_outils" class="form_info validation form-horizontal row-border" method="post" action="#">
																<div class="col-md-4">
																	<input type="text" class="form-control required" name="cp_depart" id="cp_depart" placeholder="Code postal de départ - ex : 75001" />
																</div>
																<div class="col-md-4">
																	<input type="text" class="form-control required" name="cp_arrivee" id="cp_arrivee" placeholder="Code postal d'arrivée - ex : 13001"/>
																</div>
																<div class="col-md-4">
																	<input type="submit" value="ESTIMER MA DISTANCE" class="height_30 btn btn-block btn-primary">
																</div>
															</form>
															<h4 style="display:none" class="resultat_estimation_km center"></h4>
														</div>
														<div class="col-md-12">&nbsp;</div>
														<br clear="all">
													</div>
												</div>
												<div class="col-md-12 div_aide div_aide_4" style="display:none;">&nbsp;</div>
												<div class="col-md-12 bg_white border_div">
													<div class="col-md-12">&nbsp;</div>
													<h4 class="gris maj border_bottom_gris">
														<i class="fa fa-calendar-check-o"></i>  Date de votre déménagement
													</h4>
													<div class="resultat"></div>
													<div id="calendrier" style="min-height:180px;">
														<div id="owl-demo" class="owl-carousel owl-theme">
															<?php
															$begin =date('Y-m-d');
															$end = date('Y-m-d', strtotime('+200 days'));
															$days = Helpers::createDateRangeArray($begin,$end);
															?>
															<?php
															foreach($days as $key => $value):

																?>
																<?php
																$active = "";
//																Helpers::dump(date('Y-m-d H:i:s',Yii::app()->session['day']));die;
																if(Yii::app()->session['day']){
																	if(Yii::app()->session['day'] == strtotime($value)) {
																		$active = "selected";
																	}
																}
																?>
																<div id="col_<?=strtotime($value)?>" class="hover cal_<?=$value?> owl-item <?=$active?> item date danger">
																	<a href="#" id="" name="<?=$value?>" data-day="<?=strtotime($value)?>" class="<?php if($empty):?>error<?php endif;?> ">
																		<div class="col-md-12 center">
																			<h4 style="margin-bottom:10px;font-weight: 600;" class="gris"><?=strip_tags($this->translation['week_day_'.date('w',strtotime($value))])?></h4>
																		</div>
																		<div class="col-md-12 center vert <?php if((date('N',strtotime($value)) == 6) || (date('N',strtotime($value)) == 7)){echo "special-price";}?>">
																			<h4 style="margin:10px;font-weight: 600;" class="chiffre"><?=date('d',strtotime($value))?></h4>
																		</div>
																		<div class="col-md-12 center">
																			<p class="gris"><?=strip_tags($this->translation['month_'.date('m',strtotime($value))])?><br/> <?=date('Y',strtotime($value))?></p>
																		</div>
																	</a>
																	<br clear="all" />
																</div>
																<?php
															endforeach;
															?>
														</div>
														<br clear="all" />
														<?php
															$hide = "hidden";
															$empty = "";
															$matin = "";
															$aprem = "";
															if(Yii::app()->session['hour']['id'] == 4){
																$hide = "";
																$dayPart = Yii::app()->session['dayPart'];
																switch ($dayPart){
																	case "":
																		$empty = "selected";
																		break;
																	case "matin":
																		$matin = "selected";
																		break;
																	case "aprem":
																		$aprem = "selected";
																		break;
																}
															}
														?>
														<div class="col-md-12 nopadding <?=$hide?>" id="hours">
															<div class="col-md-12">&nbsp;</div>
															<div class="col-md-6 ">
																<label class="gris maj border_bottom_gris">Créneau d'intervention des déménageurs</label>
															</div>
															<div class="col-md-6">
																<select class="form-control_2 " id="hours-select">
																	<option value="" <?=$empty?>>Horaire</option>
																	<option value="matin" <?=$matin?>>Matin</option>
																	<option value="aprem" <?=$aprem?>>Aprés-Midi</option>
																</select>
															</div>
														</div>
													</div>

													<br clear="all">

													<div class="col-md-12"><i class="fa fa-lightbulb-o"></i> <i>Pour un déménagement économique, privilégiez les dates vertes</i></div>
													<div class="col-md-12">&nbsp;</div>
													<br clear="all">
												</div>
												<div class="col-md-12">&nbsp;</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="basket">
												<div id="panier">
													<div class="col-md-12 nomargin nopadding_responsive">


														<div class="col-md-12 no-padding-left livraison">
														</div>
														<br clear="all">
													</div>
													<div class="col-md-12 nomargin nopadding_responsive">
														<div class="col-md-6 no-padding-left margin_bottom_10 nopadding_responsive">
															<div class="bg_white border_div div_prestation">
																<div class="col-md-12 right">
																	<a href="<?=$this->CreateUrl('default/index')?>" class="go_back_home font_12 italic"><i class="fa fa-cog"> Modifier</i></a>
																</div>
																<h4 class="center no-padding no-margin bold orange">
																	Ma prestation
																</h4>
																<div class="details_panier2">
																	<div class="col-md-12">&nbsp;</div>
																	<div class="col-md-12 center font_20 padding-top-bottom">
																		<div class="row">
																			<span class="weekDay"><?=(Yii::app()->session['dayInformation']['weekDay']) ? Yii::app()->session['dayInformation']['weekDay'] : ""?></span>
																			<span class="monthsDay"><?=(Yii::app()->session['dayInformation']['monthsDay']) ? Yii::app()->session['dayInformation']['monthsDay'] : ""?></span>
																			<span class="monthName"><?=(Yii::app()->session['dayInformation']['months']) ? Yii::app()->session['dayInformation']['months'] : ""?></span>
																			<span class="year"><?=(Yii::app()->session['dayInformation']['year']) ? Yii::app()->session['dayInformation']['year'] : ""?></span>
																			<span class="dayPart"><?=(Yii::app()->session['dayInformation']['dayPart']) ? Yii::app()->session['dayInformation']['dayPart'] : ""?></span>
																			<br />
																			<span class="hoursHistory"></span> Heures
																		</div>
																	</div>
																	<div class="col-md-12">&nbsp;</div>
																	<div class="col-md-12 font_10_i center">
																		<img class="col-md-12 no-padding no-margin" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inclus.jpg" />
																	</div>
																	<div class="col-md-6 col-xs-6 margin_bottom_10 nopadding">

																		<img class="col-md-12 img-responsive" id="user_history" src="<?=(Yii::app()->session['userImage']) ? Yii::app()->request->BaseUrl.'/upload/persones/'.Yii::app()->session['userImage'] : "" ?>"/>
																	</div>
																	<div class="col-md-6 col-xs-6 margin_bottom_10 nopadding">

																		<img class="col-md-12 img-responsive" id="vehicle_history" src="<?=(Yii::app()->session['vehicleImage']) ? Yii::app()->request->BaseUrl.'/upload/vehicle/'.Yii::app()->session['vehicleImage'] : "" ?>"/>
																	</div>
																	<div class="col-md-12 center font_20 maj no-margin">
																		<i class="fa fa-road"></i><span class="km"><?=(Yii::app()->session['distance']) ? Yii::app()->session['distance'] : "" ?></span>
																	</div>
																</div>
																<br clear="all">
															</div>
														</div>
														<div class="col-md-6 margin_bottom_10 nopadding_responsive">
																<?php
																$carton_hover = "hover_panier";
																$carton_hide = "";
																$material_hide  = "hide";
																if(isset($_SESSION['materials']) && !empty($_SESSION['materials'])){
																	$carton_hide = "hide";
																	$material_hide = "";
																	$carton_hover = "";

																}

																?>
															<div class="encart_panier_height bg_white border_div materiel div_materiel <?=$carton_hover?>">
																<div class="details_materiel center">
																	<a href="<?=$this->CreateUrl('default/Material')?>" class="<?=$carton_hide?>">
																		<img class="col-md-12 img-responsive" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/no-carton.png" onmouseover="this.src='<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/no-carton-white.png';" onmouseout="this.src='<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/no-carton.png';" alt="" />
																	</a>
																	<div class="col-md-12 right <?=$material_hide?>">
																		<a href="<?=$this->CreateUrl('default/Material')?>" class="font_12 italic"><i class="fa fa-cog"> Ajouter</i></a>
																	</div>
																	<h4 class="center no-padding no-margin bold orange <?=$material_hide?>">
																		Mon matériel
																	</h4>
																	<div class="col-md-12 shop <?=$material_hide?>">
																		<form action="#" method="post" class="cart id_3" id="add_material_form">
																		<table class="table table-hover table-striped table-bordered table-highlight-head">
																			<tbody>
																		<?php
																		if(!empty($_SESSION['materials'])):
																			foreach($_SESSION['materials'] as $key => $value):

																				?>
																				<tr class="materiel_ligne_id_<?=$key?>">
																					<td class="left" style="vertical-align:middle; padding:5px;"><?=$value['material_name']?><emballage></emballage></td>
																					<td style="width:50%; text-align:center;vertical-align:middle; padding:5px;">
																							<div class="quantity">
																								<input type="button" id="id_634" class="plus_moins_basket minus" value="-">
																								<input type="text" class="input-text input-text-basket qty text materials_input" data-id = '<?=$key?>' disabled="disabled" value="<?=$value['option_count']?>" name="quantite" min="1" step="1">
																								<input type="button" id="id_634" class="plus_moins_basket plus" value="+">
																							</div>


																					</td>
																					<td style="width:5%; padding:5px;vertical-align:middle;">
																						<a href="#" class="materiel_delete_basket" data-id="<?=$key?>" data-type = '1' >
																							<i class="fa fa-trash-o red"></i>
																						</a>
																					</td>

																				</tr>
																				<?php
																			endforeach;;
																		endif;
																		?>
																			</tbody>
																		</table>
																		</form>

																		<br clear="all">
																	</div>
																	<br clear="all">
																</div>
															</div>
														</div>
														<div class="col-md-6 margin_bottom_10 nopadding_responsive">
															<?php
																$basket_hover = "hover_panier";
																$basket_hide = "";
																$product_hide = "hide";
																if(isset($_SESSION['products']) && !empty($_SESSION['products'])){
																	$basket_hide = "hide";
																	$product_hide = "";
																	$basket_hover = "";

																}

															?>
															<div class="encart_panier_height bg_white border_div materiel div_materiel <?=$basket_hover?>">
																<div class="details_materiel center">
																	<a href="<?php echo $this->CreateUrl('default/services') ?>" class="<?=$basket_hide?>">
																		<img class="col-md-12 img-responsive" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/no-service.png" onmouseover="this.src='<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/no-service-white.png';" onmouseout="this.src='<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/no-service.png';" alt="" />
																	</a>
																	<div class="col-md-12 right <?=$product_hide?>">
																		<a href="<?=$this->CreateUrl('default/services')?>" class="font_12 italic "><i class="fa fa-cog"> Ajouter</i></a>
																	</div>
																	<h4 class="center no-padding no-margin  bold orange <?=$product_hide?>">
																		Mes services
																	</h4>
																	<div class="col-md-12 shop <?=$product_hide?>">
																		<form action="#" method="post" class="cart id_3" id="add_product_form">
																		<table class="table table-hover table-striped table-bordered table-highlight-head">
																			<tbody>

																			<?php
																				if(!empty($_SESSION['products'])):
																					foreach($_SESSION['products'] as $key => $value):

																					?>
																				<tr class="materiel_ligne_id_<?=$key?>">
																					<td class="left" style="vertical-align:middle; padding:5px;"><?=$value['product_name']?><emballage></emballage></td>
																					<td style="width:50%; text-align:center;vertical-align:middle; padding:5px;">

																							<?php
																								if($value['type']):
																							?>
																									<div class="quantity">
																										<input type="button" id="id_6<?=$key?>" class="plus_moins_basket minus" value="-">
																										<input type="text" class="input-text input-text-basket qty text service_count" data-id = '<?=$key?>' disabled="disabled" value="<?=$value['option_count']?>" name="quantite" min="1" step="1">
																										<input type="button" id="id_6<?=$key?>" class="plus_moins_basket plus" value="+">
																									</div>
																							<?php else:?>
																									<div class="col-md-11 no-margin no-padding">
																										<select title="3" id="_3" name="option_select_3" class="form-control_3 service_option" data-id = '<?=$key?>'>
																											<?php
																												if(!empty($value['option'])):
																													foreach ($value['option'][0] as $elems => $item):
																														$selected = "";
																														if($item->id == $value['option_id']){
																															$selected = "selected";
																														}
																											?>
																														<option value="<?=$item->id?>" <?=$selected?>> <?=$item->serviceOptionLabel->text?></option>
																											<?php
																													endforeach;
																												endif;
																											?>

																										</select>
																									</div>
																							<?php
																								endif;
																							?>


																					</td>
																					<td style="width:5%; padding:5px;vertical-align:middle;">
																						<a href="#" class="materiel_delete_basket" data-id="<?=$key?>" data-type = '0' >
																							<i class="fa fa-trash-o red"></i>
																						</a>
																					</td>

																				</tr>
																			<?php
																					endforeach;;
																				endif;
																			?>

																			</tbody>
																		</table>
																		</form>
																		<br clear="all">
																	</div>
																	<br clear="all">
																</div>
															</div>
														</div>
														<div class="col-md-12 no-padding-left margin_bottom_10 nopadding_responsive">
															<div class="bg_white border_div padding">
																<h4 class="title_encart_gris padding-top-bottom"><i class="fa fa-umbrella"></i>
																	Assurance de déménagement
																</h4>
																<?php if(isset($insurance)):?>
																	<?php foreach($insurance as $key => $value): ?>
																		<div class="col-md-6 col-xd-6">
																			<label class="radio-inline">
																				<input type="radio" name="option_assurance" class="insurane" value="<?= $value['id']?>" <?=($key == 0)  ? "checked":""?> />
																				<?=$value['insuranceLabel']['text']?>
																			</label>
																		</div>
																	<?php endforeach;?>
																<?php endif;?>
																<br clear="all">

															</div>
														</div>
														<div class="col-md-12 no-padding-left livraison nopadding_responsive hide">
															<div class="bg_white border_div padding">
																<h4 class="title_encart_gris padding-top-bottom"><i class="fa fa-truck"></i>

																	Livraison

																</h4>
																<?php if(isset($delivery)):?>
																	<?php foreach($delivery as $value): ?>
																		<div class="col-md-4 col-xd-4">
																			<label class="radio-inline">
																				<input type="radio" name="option_livraison" id="checkbox_livraision_<?=$value['id']?>" value="<?=$value['id']?>"/>
																				<?=$value['deliveryLabel']['text']?>
																			</label>
																		</div>
																	<?php endforeach;?>
																<?php endif;?>
																<br clear="all">

															</div>
														</div>
													</div>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="coordinates">
												<div class="col-md-12 bg_white border_div margin_bottom_10 ">
													<div class="col-md-12">
														<h3 class="title_encart_gris padding-top-bottom"><i class="fa fa-user"></i>Informations
														</h3>
														<form method="post" action="<?=$this->CreateUrl('site/sendMail')?>" class="validation">
															<input type="hidden" name="add_users" value="1">

															<div class="row" style="margin-bottom:15px;">
																<div class="col-md-6 col-sm-6">
																	<div class="form-group">
																		<select name="User[statut]" id="statut" class="required sm-form-control" required>
																			<option value="">Vous ?tes un</option>
																			<option value="0">Particulier</option>
																			<option value="1">Professionnel</option>
																		</select>
																		<span class="help-block"></span>
																	</div>
																</div>
															</div>
															<div class="row" style="margin-bottom:20px;">
																<div class="col-md-6 col-sm-6 col-xs-6">
																	<div class="form-group">
																		<input placeholder="Nom" type="text" name="User[nom]" class="required sm-form-control" value="" required>
																	</div>
																</div>
																<div class="col-md-6 col-sm-6 col-xs-6">
																	<div class="form-group">
																		<input placeholder="Prenom" type="text" class="required sm-form-control" name="User[prenom]" value="" required>
																	</div>
																</div>
															</div>
															<div class="row" style="margin-bottom:20px;">
																<div class="col-md-6 col-sm-6">
																	<div class="form-group">
																		<input placeholder="Votre adresse email" type="email" class="required sm-form-control" name="User[email]" value="" required>
																	</div>
																</div>
															</div>

															<div class="row" style="margin-bottom:20px;">
																<div class="col-md-6 col-sm-6 col-xs-6">
																	<div class="form-group">
																		<input placeholder="T?l?phone portable" type="text" class="required sm-form-control" name="User[portable]" data-mask="0999999999" value=""  required>
																		<span class="help-block" style="font-style:italic; color:#ccc;">0612345678</span>
																	</div>
																</div>
																<div class="col-md-6 col-sm-6 col-xs-6">
																	<div class="form-group">
																		<input placeholder="T?l?phone secondaire" type="text" class="required sm-form-control" name="User[telephone]" data-mask="0999999999" value="" required>
																		<span class="help-block" style="font-style:italic; color:#ccc;">0112345678</span>
																	</div>
																</div>

															</div>
															<h3 class="title_encart_gris padding-top-bottom"><i class="fa fa-map-marker"></i> Informations de départ
															</h3>
															<div class="form-group">
																<div class="col-md-6 col-xs-12">
																	<input type="text"  id="autocomplete" name="Depart[adresse]" placeholder="Adresse de départ *" class="required sm-form-control margin_bottom_10" value  required/>
																</div>
																<div class="col-md-3 col-xs-6">
																	<input type="text"  id="postal_code" name="Depart[cp_dep]" placeholder="Code Postal *" class="required sm-form-control margin_bottom_10 margin_bottom_responsive" value required/>
																</div>
																<div class="col-md-3 col-xs-6">
																	<input type="text"  id="locality" name="Depart[ville]" placeholder="Ville *" class="required sm-form-control margin_bottom_10 margin_bottom_responsive" value required/>
																</div>
															</div>
															<div class="form-group">
																<div class="col-md-6 col-xs-12">
																	<input type="text"  name="Depart[etage]" placeholder="Etage *" class="required sm-form-control margin_bottom_10" value required/>
																</div>
																<div class="col-md-6 col-xs-12">
																	<select name="Depart[ascenseur]" class="required sm-form-control" required>
																		<option value>Ascenseur</option>
																		<option value="Oui">Oui</option>
																		<option value="Non">Non</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<div class="col-md-12">
																	<textarea class="sm-form-control" name="Depart[complement]" cols="5" rows="3" placeholder="Complément d'adresse" ></textarea>
																</div>
															</div>
															<h3 class="title_encart_gris padding-top-bottom"><i class="fa fa-map-marker"></i>  Informations d'arrivée
															</h3>
															<div class="form-group">
																<div class="col-md-6 col-xs-12">
																	<input type="text"  id="autocomplete_arrivee" name="Arrive[adresse]" placeholder="Adresse de départ *" class="required sm-form-control margin_bottom_10" value  required/>
																</div>
																<div class="col-md-3 col-xs-6">
																	<input type="text"  id="arrive_postal_code" name="Arrive[cp_arr]" placeholder="Code Postal *" class="required sm-form-control margin_bottom_10 margin_bottom_responsive" value required/>
																</div>
																<div class="col-md-3 col-xs-6">
																	<input type="text"  id="arrive_locality" name="Arrive[ville]" placeholder="Ville *" class="required sm-form-control margin_bottom_10 margin_bottom_responsive" value required/>
																</div>
															</div>
															<div class="form-group">
																<div class="col-md-6 col-xs-12">
																	<input type="text"  name="Arrive[etage]" placeholder="Etage *" class="required sm-form-control margin_bottom_10" value required/>
																</div>
																<div class="col-md-6 col-xs-12">
																	<select name="Arrive[ascenseur]" class="required sm-form-control" required>
																		<option value>Ascenseur</option>
																		<option value="Oui">Oui</option>
																		<option value="Non">Non</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<div class="col-md-12">
																	<textarea class="sm-form-control" name="Arrive[complement]" cols="5" rows="3" placeholder="Complément d'adresse"></textarea>
																</div>
															</div>
															<div class="col-md-12 row_cgv">
																<label style=" text-transform:none;">
																	En cochant cette case vous validez les <a href="<?=$this->CreateUrl('default/cgv') ?>" target="_blank" CGV> concernant votre réservation
																		<input type="checkbox" name="cgv" class="required uniform" value="1" required></a></label>
															</div>
															<div class="form-group encart_web" >
																<div class="col-md-4 pull-right no-margin no-padding">
																	<input type="submit" class="add-to-cart button" value="Proc?der au paiement">
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="settings">...</div>
										</div>

									</div>
									<div class="col-md-3 center right_prix nopadding_responsive">
										<div class="panel-body-">
											<aside class="sidebar bg_rotate_prix padding_9">
												<div class="border_div bg_white single-product">
													<div class="center right_prix">
														<div class="single-product">
															<div class="col-md-12">&nbsp;</div>
															<div class="col-md-12">
																<div class="feature-box fbox-plain fbox-dark fbox-small">
																	<div class="col-md-2 col-xs-2">
																		<div class="fbox-icon">
																			<i class="fa fa-question-circle-o"></i>
																		</div>
																		<br clear="all" />
																	</div>
																	<div class="col-md-10 col-xs-10">
																		<h3>Une question? </h3>
																		<p class="nomargin nopadding">09.72.47.50.47</p>
																	</div>
																</div>
																<br clear="all" />
																<div class="feature-box fbox-plain fbox-dark fbox-small">
																	<div class="col-md-2 col-xs-2">
																		<div class="fbox-icon">
																			<i class="fa fa-thumbs-up"></i>
																		</div>
																		<br clear="all" />
																	</div>
																	<div class="col-md-10 col-xs-10">
																		<h3>Paiement 100% sécurisé</h3>
																	</div>
																</div>
																<br clear="all" />
																<div class="feature-box fbox-plain fbox-dark fbox-small">
																	<div class="col-md-2 col-xs-2">
																		<div class="fbox-icon">
																			<i class="fa fa-undo"></i>
																		</div>
																		<br clear="all" />
																	</div>
																	<div class="col-md-10 col-xs-10">
																		<h3>ANNULATION POSSIBLE</h3>
																	</div>
																</div>
																<br clear="all" />

															</div>
															<div class="col-md-12">&nbsp;</div>

															<br clear="all">
														</div>
													</div>
												</div>
												<br clear="all">
												<div class="border_div bg_white single-product no-bg no-border email-form hidden">
													<div class="col-md-12">&nbsp;</div>
													<div class="center right_prix simulation_div">
														<p>Recevoir mon devis par email</p>
														<div class="col-md-12">
															<form class="nomargin nopadding" action="#" role="form" id="form_simulation" method="post">
																<div class="col-md-8 col-sm-8 col-xs-8 nomargin nopadding">
																	<input type="text" placeholder="Votre E-mail" value class="border_orange sm-form-control not-dark required" id="champ_email" name="champ_email"/>
																</div>
																<div class="col-md-4 col-sm-4 col-xs-4 nomargin nopadding">
																	<button class="button button-orange uppercase nomargin btn-block" type="submit"> OK</button>
																</div>
															</form>
														</div>
													</div>
													<br clear="all">
													<br clear="all">

												</div>
												<br clear="all">

												<div class="center">
													<div class="col-md-12">&nbsp;</div>
													<a href="#go_basket" id="go_basket" onclick="return false "class="add-to-cart button nomargin">Ajouter au panier</a>
												</div>
												<div class="center">
													<div class="col-md-12">&nbsp;</div>
													<a href="#coordinates" id="go_coord" class="add-to-cart coordinates_btn button nomargin hidden">
														Valider mon panier
													</a>
												</div>
											</aside>
										</div>
									</div>
								</div>

								<div class="container conditions">
									<p>Avec l’offre de déménagement à l’heure Des bras en plus, tous les types de déménagement sont envisageable :</p>
									<ul>
										<li>Vous souhaitez laisser les déménageurs s’occuper de votre déménagement, dans ce cas là, pas de soucis, nos déménageurs professionnels s’occuperont de tout. Pensez juste à réserver suffisamment de déménageurs, sur la bonne durée (si vous avez besoin d’aide, n’hésitez pas à consulter notre service client au 01.84.16.1800)</li>
										<li>Vous souhaitez déménager seul et que vous avez déjà un camion de déménagement : Des amis et de la famille vont venir vous prêter main forte le jour de votre déménagement et vous avez déjà un camion. Vous avez surtout besoin d’aide pour vous le transport des objets les plus lourds, les plus encombrants, et les plus difficiles à manipuler. Pas de problème, vous pouvez facilement configurer votre prestation de manière à réserver les services de 1 ou 2 déménageurs sur 4 ou 7 heures sans véhicules selon vos besoin.</li>
										<li>Vous souhaitez déménager seul et vous n’avez pas encore de camion de déménagement : Des bras en plus est la solution parfaite. Réservez les services d’un unique chauffeur-déménageur avec un véhicule de 12 ou 22m3. Plus besoin d’aller à l’agence de location de camion de déménagement, de laisser un dépôt de garantie, de devoir passer par la pompe à essence et surtout, plus besoin de conduire un camion de déménagement :) le chauffeur-déménageur s’en occupe pour vous.</li>
									</ul>
								</div>
							</div>
						</section>

					</div>

				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" charset="utf-8">var ju_num="AA736390-0E3A-4FBD-8050-5FBB98799D92";var asset_host=(("https:"==document.location.protocol)?"https":"http")+'://d2j3qa5nc37287.cloudfront.net/';(function() {var s=document.createElement('script');s.type='text/javascript';s.async=true;s.src=asset_host+'coupon_code1.js';var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);})();</script>




</div>
<div class="bootbox modal fade" id="userVehicle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="bootbox-body">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle-o red"></i></span></button>
						<h4 class="modal-title" id="myModalLabel"><i class="icon fa fa-exclamation-triangle red"></i> Important</h4>
					</div>
					<div style="margin:5px 5px 5px 15px; display:block;">
						Vous ne pouvez pas sélectioner 1 déménageur avec un camion de 40 M<sup>3</sup>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script>

	jQuery(document).ready(function($){

		var owl = $(".owl-carousel");
		owl.owlCarousel({
			items :7, //10 items above 1000px browser width
			itemsCustom : false,
			itemsDesktop : [1000,7], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,5], // 3 items betweem 900px and 601px
			itemsTablet: [600,3], //2 items between 600 and 0;
			itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
			navigation : true,
			navigationText : ["<i class=\"fa fa-chevron-left bg_bt_left\"></i>","<i class=\"fa fa-chevron-right bg_bt_right\"></i>"],
			rewindNav : true,
			scrollPerPage : false,
			pagination : false,
			dragBeforeAnimFinish :false,
			mouseDrag :true,
			touchDrag :true,
			lazyLoad : true
		});


	});

</script>
<!--<script src="--><?//=Yii::app()->request->BaseUrl?><!--/vendor/assets/js/tunnel-commande.js"></script>-->
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/jcalendar.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/validation/additional-methods.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/bootbox/bootbox.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery(".validation").validate();
	});
</script>
<!-- Google Analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-30590788-1', 'auto');
	ga('send', 'pageview');
</script>


<!-- Code Google de la balise de remarketing -->
<!--------------------------------------------------
Les balises de remarketing ne peuvent pas être associées aux informations personnelles ou placées sur des pages liées aux catégories à caractère sensible. Pour comprendre et savoir comment configurer la balise, rendez-vous sur la page http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 983060310;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/conversion.js">
</script>
<noscript>
	<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="/googleads.g.doubleclick.net/pagead/viewthroughconversion/983060310/?guid=ON&amp;script=0">
	</div>
</noscript>


<script>
	$(document).ready(function(){

		$('#go_basket').on('click',function(){
			var p1_user = $('#p1_user').val();
			var p1_hour = $('#p1_hour').val();
			var p1_vehicle = $('#p1_vehicle').val();
			var p1_day = $('#p1_day').val();
			var p1_dayPart = $('#p1_dayPart').val();
			var p1_distance = $('#p1_distance').val();


			if(p1_vehicle =='' || p1_day ==''){

				if(p1_hour==4 && p1_dayPart =='' ){
					var box = ""
						//+"<div class=\"modal_\">"
						//+"	<div class=\"modal-dialog\">"
						//+"		<div class=\"modal-content\">"
						+"			<div class=\"modal-header\">"
						+"				<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><span aria-hidden=\"true\"><i class=\"fa fa-times-circle-o red\"></i></span></button>"
						+"				<h4 class=\"modal-title\"><i class=\"icon icon-warning\"></i> Important</h4>"
						+"			</div>"
						+"			<p>Vous ne pouvez pas sélectioner 1 déménageur avec un camion de 40 M3</p>"
						+"			</div>"
					//+"		</div>"
					//+"	</div>"
					//+"</div>"
					var box = bootbox.alert(box);

				}
				else if(p1_distance == ''){
					alert(111);
					var box = ""
						//+"<div class=\"modal_\">"
						//+"	<div class=\"modal-dialog\">"
						//+"		<div class=\"modal-content\">"
						+"			<div class=\"modal-header\">"
						+"				<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><span aria-hidden=\"true\"><i class=\"fa fa-times-circle-o red\"></i></span></button>"
						+"				<h4 class=\"modal-title\"><i class=\"icon icon-warning\"></i> Important</h4>"
						+"			</div>"
						+"			<p>Distance error</p>"
						+"			</div>"
					//+"		</div>"
					//+"	</div>"
					//+"</div>"
					var box = bootbox.alert(box);
				}

			}else{
				$.ajax({
					url: '<?=$this->CreateUrl('default/Reservation')?>',
					data: $('#page_1').serialize(),
					type: "POST",
					dataType: 'JSON',
					success: function (result) {
						console.log(111);
						console.log(result);
						$('.km').html(result.distance);
						$('#user_history').attr('src','<?=Yii::app()->request->BaseUrl?>/upload/persones/'+result.userImage+'');
						$('#vehicle_history').attr('src','<?=Yii::app()->request->BaseUrl?>/upload/vehicle/'+result.vehicleImage+'');
						$('.weekDay').html(result.weekDay);
						$('.monthsDay').html(result.monthsDay);
						$('.monthName').html(result.months);
						$('.year').html(result.year);
						$('.hoursHistory').html(result.hours);
						$('.dayPart').html(result.dayPart);
					}
				});
				$('.tab-pane').removeClass('active');
				$('#basket').addClass('active');
				$('#information').addClass('hidden');
				$('.conditions').addClass('hidden');
				$('#selectors').removeClass('hidden');
				$('.email-form').removeClass('hidden');
				$('#go_basket').addClass('hidden');
				$('#go_coord').removeClass('hidden');

			}

		});

	});
	$(document).ready(function(){
		$('#go_coord').on('click',function(){
			$('.tab-pane').removeClass('active');
			$('#coordinates').addClass('active');
			$('#go_coord').addClass('hidden');
		});
	});
</script>
<script>
	$(document).ready(function () {
		$(document).on("click",'.btn_demenageur', function(){
			if(($('.btn_vehicle.actif').data('id') == 3) && ($(this).data('id') == 0)){
				$('#userVehicle').modal('show');
			}else{
				var currentTab = '#user-' + $(this).data('id');
				$('.btn_demenageur').removeClass('actif').addClass('no_actif');
				$(this).removeClass('no_actif').addClass('actif');
				$('#prestation_1 .tab-pane').removeClass('active');
				$(currentTab).addClass('active');
				$('#p1_user').val($(this).data('id'));
			}
		});
		$("#btn_demenageur").on("click",function(){
			$('.hidden_user').toggleClass('person-active');
			if($(this).attr('data-rel') == "+"){
				$('#btn_demenageur').html('- Moins de choix');
				$(this).attr('data-rel', '-');
			}else {
				$('#btn_demenageur').html('+ Plus de choix');
				$(this).attr('data-rel', '+');
			}
		});
		$(document).on("click",'.btn_hour', function(){
			var currentTab = '#hours-' + $(this).data('id');
			$('.btn_hour').removeClass('actif').addClass('no_actif');
			$(this).removeClass('no_actif').addClass('actif');
			$('#prestation_2 .tab-pane').removeClass('active');
			$(currentTab).addClass('active');
			$('#p1_hour').val($(this).data('id'));
			if($(this).html() == '4h'){
				$('#hours').removeClass('hidden');
			}else{
				$('#hours').addClass('hidden');
			}
		});
		$("#btn_hour").on("click",function(){
			$('.hidden_user').toggleClass('hour-active');
			if($(this).attr('data-rel') == "+"){
				$('#btn_hour').html('- Moins de choix');
				$(this).attr('data-rel', '-');
			}else {
				$('#btn_hour').html('+ Plus de choix');
				$(this).attr('data-rel', '+');
			}
		});
		$(document).on("click",'.btn_vehicle', function(){
			if(($('.btn_demenageur.actif').data('id') == 0) && ($(this).data('id') == 3)){
				$('#userVehicle').modal('show');
			}else{
				var currentTab = '#vehicle-' + $(this).data('id');
				$('.btn_vehicle').removeClass('actif').addClass('no_actif');
				$(this).removeClass('no_actif').addClass('actif');
				$('#prestation_3 .tab-pane').removeClass('active');
				$(currentTab).addClass('active');
				$('#p1_vehicle').val($(this).data('id'));
			}


		});
		$("#btn_vehicle").on("click",function(){

			$('.vehicle-active').toggleClass('hidden_vehicle');
			if($(this).attr('data-rel') == "+"){
				$('#btn_hour').html('- Moins de choix');
				$(this).attr('data-rel', '-');
			}else {
				$('#btn_hour').html('+ Plus de choix');
				$(this).attr('data-rel', '+');
			}
		});
		$('.owl-item a').click(function(e){
			var hour_val = $('#p1_hour').val();
			if(hour_val==4){
				$('.nopadding').removeClass('hidden')
			}
			e.preventDefault();

			$('.owl-item').removeClass('error');
			var duree_select 	= $('div.class_2 .actif.btn').attr('id');
			var valeur 			= $(this).attr("name");
			var id 				= $(this).attr("id");
			var valeur_class 	= $(this).attr("class");


			$(".class_2 a.btn.barre_rouge").removeAttr("class").attr("class", "btn no_actif");


			$('.info_disponibilite').hide();
			$('#p1_day').val($(this).data('day'));
			$('.select_journee_news').hide();
			$('.demi_journee_news').val('');

			$(".selected").removeClass("selected");

			$('#date_choisie').val(valeur);
			$('.date_choisie').html(valeur);

			if(duree_select == 'id_2_1') {
				//$('.cal_'+valeur+' .select_journee_news').show();
				$('.select_journee_news').show();
			}

			if (valeur_class == 'selected') {
				$('.cal_'+valeur).removeClass("selected");
			}
			else {
				$('.cal_'+valeur).addClass("selected");

			}
			return false;
		});
		$('#distance').on("keyup", function () {
			var val = $('#distance').val();
			if($.isNumeric(val)){
				$('#distance').removeClass('red-input').addClass('green-input');
				$('#distance').css("border", "1px solid green");
				$('#p1_distance').val(val);
			}else{
				$('#distance').css("border", "1px solid red");
				$('#distance').val("").removeClass('green-input').addClass('red-input');
			}
		});
		$('#hours-select').on("change",function () {
			$('#p1_dayPart').val($('#hours-select').val());
		});


		$('.go_back_home').on("click",function(){
			$('#basket').removeClass('active');
			$('#go_coord').addClass('hidden');
			$('#go_basket').removeClass('hidden');
			$('#home').addClass('active');
		});


		$(document).on('click','.materiel_delete_basket',function(e){
			e.preventDefault();
			var id = $(this).attr('data-id');
			var type = $(this).attr('data-type');
			$.ajax({
				url:"<?=$this->CreateUrl('default/deleteProduct')?>",
				data:{key:id,type:type},
				type:"POST",
				success:function(result){
					console.log(result);
					if(result){
						$('.materiel_ligne_id_'+id).remove();
					}
				}
			})
		})

		$(document).on('click','.coordinates_btn',function(e){
			e.preventDefault();
			var materials = $('.materials_input');
			var services_option = $('.service_option');
			var services_count = $('.service_count');
			var all_materials = {};
			var all_services_count = {};
			var all_services_option = {};
			if(materials){
				$.each(materials,function(key,value){
					all_materials[$(value).attr('data-id')] = $(value).val();
				});
			}
			if(services_option){
				$.each(services_option,function(key,value){
					all_services_option[$(value).attr('data-id')] = $(value).val();
				});
			}
			if(services_count){
				$.each(services_count,function(key,value){
					all_services_count[$(value).attr('data-id')] = $(value).val();
				});
			}
			var assurance_id = $('input[name=option_assurance]:checked').val();
			$.ajax({
				url:'<?=$this->CreateUrl('default/addInfoCoord')?>',
				data:{assurance_id:assurance_id,all_materials:all_materials,all_services_option:all_services_option,all_services_count:all_services_count},
				type:'POST',
				dataType:'JSON',
				success:function(result){
					console.log(result);
				}
			})


			return;
		})


	});
</script>
<?php
	if(isset($_GET['tab'])):
?>
		<script>
			$('.tab-pane').removeClass('active');
			$('#<?=$_GET['tab']?>').addClass('active');
			$('#go_basket').addClass('hidden');
			$('#go_coord').removeClass('hidden');
		</script>
<?php
	endif;
?>
