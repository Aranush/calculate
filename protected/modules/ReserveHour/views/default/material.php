<section id="page-title" class="page-title-110">
    <div class="container clearfix">
        <h1>Achat de matériel déménagement</h1>
    </div>
</section>
<section id="content">
    <div class="content-wrap-padding-top-0 content-wrap">
        <div class="container clearfix">
            <div class="col-xs-12">&nbsp;</div>
            <div class="postcontent nobottommargin clearfix col_last">
                <div id="shop" class="shop grid-container clearfix" data-layout="fitRows" style="position: relative; height: 1302px;">

                    <?php
                        if(!empty($data['selectedCategory'])):
                            foreach($data['selectedCategory'] as $elems => $item):
                    ?>
                                <?=(isset($_GET['id'])) ? $item['categoryAdminLabel']['text'] : ""?>
                    <?php
                                 if(!empty($item['materials'])):?>
                                    <?php foreach($item['materials'] as $key=>$value):?>
                                         <div class="product clearfix">
                                             <div class="product-image">
                                                 <a href="<?=$this->CreateUrl('default/materialInfo',array('id' => $value['id']))?>">
                                                     <img src="<?=Yii::app()->request->BaseUrl?>/upload/materials/<?=$value['image']?>" />
                                                 </a>
                                                 <div class="product-overlay">
                                                     <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i><span>Voir</span></a>
                                                     <a href="#" class="item-quick-view"><i class="fa fa-plus"></i><span> Détails</span></a>
                                                 </div>
                                             </div>
                                             <div class="product-desc">
                                                 <div class="product-title">
                                                     <h2><a href="#"><?=$value['materialLabel']['name']?></a></h2>
                                                 </div>
                                                 <div class="product-price">
                                                     <?=$value['price']?> €
                                                 </div>
                                             </div>
                                         </div>
                                     <?php endforeach ?>
                    <?php
                                endif;
                            endforeach;
                        endif;
                    ?>

                </div>
            </div>
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">
                        <h3 class="font_20">
                            Shop Categories
                        </h3>
                            <ul class="widget_links">
                                <?php foreach($data['categories']as $key=>$value):?>
                                    <li><a href="<?=$this->CreateUrl('default/material',array('id'=>$value['id']))?>"><?=$value['categoryAdminLabel']['name']?></a></li>
                                <?php endforeach;?>
                            </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>