<section id="page-title" class="page-title-110">
    <div class="container clearfix">
        <h1><?=$data['materialsInfo']->materialLabel->name?></h1>
    </div>
</section>
<section id="content_">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col-xs-12">&nbsp;</div>
            <div class="postcontent nobottommargin clearfix col_last">
                <div class="single-product">
                    <div class="product">
                        <div class="col_half">
                            <div class="product-image">
                                <img src="<?=Yii::app()->request->BaseUrl?>/upload/materials/<?=$data['materialsInfo']['image']?>" />
                            </div>
                        </div>
                        <div class="col_half col_last product-desc">
                            <div class="product-price hide" itemprop="offers">
                                <ins itemprop="price" content="250.00">
                                    <span class="amount prix_details ">A partir de <?=$data['materialsInfo']['price']?></span>
                                    <span itemprop="priceCurrency"></span>
                                </ins>
                            </div>
                            <div class="clear"></div>
                            <div class="line hide"></div>
                            <div class="alert alert-info return_cart hide">
                                <i class="icon icon-thumbs-up">                                        Ajouté au panier
                                </i>
                            </div>
                            <form>
                                <div class="quantity" data-type = "<?=(!empty($data['materialsInfo']['serviceOption'])) ? "0" : "1"?>" >
                                    <?php if(!empty($data['materialsInfo']['serviceOption'])):?>
                                        <select class="form-control_3  sm-form-control product_value product_quantity" name="option_select_2">
                                            <option value="0">Sélectionner</option>
                                            <?php foreach($data['materialsInfo']['serviceOptions'] as $value):?>
                                                <option value="<?=$value['id']?>"><?=$value['serviceOptionLabels'][0]['text']?></option>
                                            <?php endforeach;?>
                                        </select>
                                    <?php else:?>
                                        <input type="button" class="plus_moins minus" value="-">
                                        <input type="text" class="input-text qty text product_value product_quantity" disabled="" title="Qty" value="1" name="quantite" id="quantite" min="1" step="1">
                                        <input type="button" class="plus_moins plus" value="+">

                                    <?php endif;?>
                                    <div class="col-md-12">&nbsp;</div>

                                    <button href="#" class="add-to-cart button nomargin" id="add_product">
                                        Ajouter au panier
                                    </button>
                                    <div class="col-md-12">&nbsp;</div>
                                    <div class="col-md-12">&nbsp;</div>

                                </div>
                            </form>
                            <div class="col-md-12"></div>
                            <div class="col-md-12 orange no-margin no-padding">
                                <div class="col-md-6 col-xs-12 font_16 no-margin no-padding">
                                    <a href="<?=$this->CreateUrl('default/material')?>" class="orange">
                                        <i class="fa fa-th-large"></i>
                                        Retourner au matériel
                                    </a>
                                </div>
                                <div class="col-md-6 col-xs-12 font_16 no-margin no-padding">
                                    <a href="<?=$this->CreateUrl('default/index',array('tab' => 'basket'))?>"  class="orange basket_link disabled_link">
                                        <i class="fa fa-shopping-cart"></i>
                                        Retourner au panier
                                    </a>
                                </div>
                                <br clear="all" />

                            </div>
                            <div class="clear"></div>
                            <div class="line"></div>
                            <p>
                                <?=$data['materialsInfo']['materialLabel']['small_description']?>
                            </p>

                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col_full nobottommargin">
                            <div class="tabs clearfix nobottommargin ui-tabs ui-widget ui-widget-content ui-corner-all" id="tab-1">
                                <ul class="tab-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                    <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                                        <a href="#tabs-1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">
                                            <i class="icon-align-justify2"></i>
                                            <span class="hidden-xs"> Description</span>
                                        </a>
                                    </li>
                                    <li class="ui-state-default ui-corner-top" role="tab">
                                        <a href="#tabs-2" class="ui-tabs-anchor" role="presentation1" tabindex="-1" id="ui-id-2">
                                            <i class="icon-align-justify2"></i>
                                            <span class="hidden-xs">  Information(s) complémentaire</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-container">
                                    <div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">

                                        <?=$data['materialsInfo']['materialLabel']['description']?>

                                    </div>
                                    <div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2" aria-labelledby="ui-id-2" role="tabpane2" aria-hidden="false">

                                        <?=$data['materialsInfo']['materialLabel']['additional_description']?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">
                        <h3 class="font_20">
                            Shop Categories
                        </h3>
                        <ul class="widget_links">
                            <?php foreach($data['categories']as $key=>$value):?>
                                <li><a href="<?=$this->CreateUrl('default/material',array('id'=>$value['id']))?>"><?=$value['categoryAdminLabel']['name']?></a></li>
                            <?php endforeach;?>
                        </ul>

                    </div>
                    <div class="widget clearfix">
                        <h4>Autres services</h4>
                        <div id="post-list-footer">
                            <?php if(isset($data['materials'])):?>
                                <?php foreach($data['materials'] as $value):?>
                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="<?=$this->CreateUrl('default/servicesInner',array('id'=>$value['id']))?>">
                                                <img src="<?=Yii::app()->request->BaseUrl?>/upload/materials/<?=$value['image']?>" alt="" />
                                            </a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="<?=$this->CreateUrl('default/materialInfo',array('id'=>$value['id']))?>"><?=$value['materialLabel']['name']?></a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li class="color"><?=$value['price']?> €</li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach?>
                            <?php endif?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        var id = '<?=$_GET['id']?>';
        $(document).on('click','#add_product',function(e){
            e.preventDefault();
            var data= $('.product_quantity').val();
            $.ajax({
                url:"<?=$this->CreateUrl('default/addMaterial')?>",
                data:{data:data, id:id},
                type:'POST',
                success:function (result) {
                    result = parseInt(result);
                    if(result){
                        $('.return_cart').html('Ajouté au panier');
                        $('.basket_link').removeClass('disabled_link');
                    }else{
                        $('.return_cart').html('Erreur');
                    }
                    $('.return_cart').removeClass('hide');
                }
            })
        })
    });

</script>