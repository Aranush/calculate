<section id="page-title" class="page-title-110">
    <div class="container clearfix">
        <h1>Nos services</h1>
    </div>
</section>
<section>
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col-xs-12">&nbsp;</div>
            <div class="col-md-12">
                <?php if(isset($services)):?>
                    <?php foreach($services as $value):?>
                        <div class="product clearfix col-md-3 col-sm-2 col-xs-8">
                            <div class="product-image">
                                <a href="<?=$this->CreateUrl('default/ServicesInner',array('id'=>$value['id']))?>">
                                    <img  src="<?=Yii::app()->request->BaseUrl?>/upload/services/<?=$value['image']?>" />
                                </a>
                                <div class="product-overlay">
                                    <a href="<?=$this->CreateUrl('default/ServicesInner',array('id'=>$value['id']))?>" class="add-to-cart">
                                        <i class="fa fa-shopping-cart"><span>Voir</span></i>
                                    </a>
                                    <a href="<?=$this->CreateUrl('default/ServicesInner',array('id'=>$value['id']))?>" class="item-quick-view">
                                        <i class="fa fa-plus"><span> Détails</span></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-desc">
                                <div class="product-title">
                                    <h3>
                                        <a href="<?=$this->CreateUrl('default/ServicesInner',array('id'=>$value['id']))?>"><?=$value['servicesLabel']['name']?></a>
                                    </h3>
                                    <div class="product-price">
                                       <?=$value['price']?> €
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endforeach ?>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>