<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Des bras en plus - Déménagement pas cher à la carte</title>
    <link rel="author" href="https://plus.google.com/102084316310845685116/posts" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="distribution" content="global" />
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="author" content="desbrasenplus.com">
    <link rel="shortcut icon" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/ico/favicon.ico">
    <meta name="description" content="éservez votre déménagement en ligne grâce à nos offres à la carte selon votre budget et selon vos besoins. Nombre de déménageurs, taille du camion, durée de la prestation"/>
    <meta name="keywords" content="" />
    <meta name="copyright" content="© 2014 Desbrasenplus" />
    <meta name="revisit-after" content="3 days" />
    <meta name="robots" content="index,follow,all" />
    <meta property="og:title" content="Des bras en plus - Déménagement pas cher à la carte" />
    <meta property="og:description" content="éservez votre déménagement en ligne grâce à nos offres à la carte selon votre budget et selon vos besoins. Nombre de déménageurs, taille du camion, durée de la prestation" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.desbrasenplus.com/reservation" />
    <meta property="og:image" content="http://www.desbrasenplus.comassets/images/logo.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:site_name" content="www.desbrasenplus.com" />
    <meta property="fb:fbid" content="593870139" />
    <!--
    *****************************************************
    *	Copyright © 2016 - Desbrasenplus.com
    *	Conception Technique : Jerome Arakelian
    *****************************************************
    -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '637418096397013');
        fbq('track', "PageView");


    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=637418096397013&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->




    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/swiper.css" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/responsive.css" type="text/css" />

    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/vendor/magnific-popup/magnific-popup.css" media="screen">
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/vendor/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/theme-responsive.css" />
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/theme-extension.css" />


    <link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/commande-style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/owl-carousel.min.css" media="screen">
    <link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/owl.theme.min.css" media="screen">
    <link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/jcalendar.css" rel="stylesheet" type="text/css" />
    <link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/slider.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
    <div id="gotoTop" class="fa fa-angle-up"></div>	<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/plugins2.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/functions.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/attributes.js"></script>
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/owl.carousel.js"></script>
    <script src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/bootbox/bootbox.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>


    <![endif]-->

</head>

<body>

<div class="container" id="page">
    <?php echo $content; ?>

    <div class="clear"></div>

</div><!-- page -->

</body>
</html>
