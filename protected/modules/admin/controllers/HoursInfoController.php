<?php

class HoursInfoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = "/layouts/main";

    public $canWithoutLogin = array();
    function init() {
        $this->canWithoutLogin = array('login');

        if(!isset(Yii::app()->session['admin'])) {
            if (!in_array($this->action, $this->canWithoutLogin)) {
                $this->redirect($this->CreateUrl('default/login'));
            }
        }
    }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new HoursInfo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HoursInfo']))
		{
			$model->attributes=$_POST['HoursInfo'];
            if($_FILES['HoursInfo']){
                $uploads_dir = UPLOADS.'hours/';
                $tmp_name = $_FILES["HoursInfo"]["tmp_name"]["image"];
                $model->image = md5(time()).".png";
                @move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
            }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HoursInfo']))
		{
            $oldImage = $model->image;
            $model->attributes = $_POST['HoursInfo'];
            $model->image = $oldImage;
            if(!empty($_FILES['HoursInfo']['name']['image'])){
                if(file_exists(UPLOADS.'/hours/'. $model->image)){
                    unlink(bDIR.'/upload/hours/'. $model->image);
                }
                $imageName = md5(time()).".png";
                $model->image = $imageName;
                $dir = UPLOADS."hours/";
                $tmp_name = $_FILES["HoursInfo"]["tmp_name"]["image"];
                $file = $dir.$imageName;

                move_uploaded_file($tmp_name, "$dir/$model->image");
            }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('HoursInfo');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new HoursInfo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['HoursInfo']))
			$model->attributes=$_GET['HoursInfo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return HoursInfo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=HoursInfo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param HoursInfo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='hours-info-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
