<?php

class MaterialsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = "/layouts/main";

    public $canWithoutLogin = array();
    function init() {
        $this->canWithoutLogin = array('login');

        if(!isset(Yii::app()->session['admin'])) {
            if (!in_array($this->action, $this->canWithoutLogin)) {
                $this->redirect($this->CreateUrl('default/login'));
            }
        }
    }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Materials;

        $criteria = new CDbCriteria();
        $criteria ->with = array(
            'categoryAdminLabel',
            'language_id' => 1,
        );

        $description = Category::model()->findAll($criteria);


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Materials']))
		{
//Helpers::dump($_POST['Materials']);die;
            $model->price=$_POST['Materials']['price'];
            $model->category_id=$_POST['Materials']['category_id'];
            if($_FILES['Materials']){
                $uploads_dir = UPLOADS.'materials/';
                $tmp_name = $_FILES["Materials"]["tmp_name"]["image"];
                $model->image = md5(time()).".png";
                @move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
            }
			if($model->save()){
                foreach (Helpers::languages() as $key=>$value){
                    $modelLabel= new MaterialLabel();
                    $modelLabel->language_id = $value->id;
                    $modelLabel->material_id = $model->id;
                    $modelLabel->name = $_POST['MaterialLabel']['name'][$value->id];
                    $modelLabel->description = $_POST['MaterialLabel']['description'][$value->id];
                    $modelLabel->additional_description = $_POST['MaterialLabel']['additional_description'][$value->id];
                    $modelLabel->small_description = $_POST['MaterialLabel']['small_description'][$value->id];
//                    var_dump($modelLabel);die;

                    $modelLabel->save(false);

                }
                $this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,'category'=>$description
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $criteria = new CDbCriteria();
        $criteria ->with = array(
            'categoryAdminLabel',
            'language_id' => 1,
        );

        $description = Category::model()->findAll($criteria);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Materials']))
		{

            $oldImage = $model->image;
            $model->image = $oldImage;
            $model->category_id = $_POST['Materials']['category_id'] ;
            $model->price = $_POST['Materials']['price'] ;
            if(!empty($_FILES['Materials']['name']['image'])){
                if(file_exists(UPLOADS.'/materials/'. $model->image)){
                    unlink(bDIR.'/upload/materials/'. $model->image);
                }
                $imageName = md5(time()).".png";
                $model->image = $imageName;
                $dir = UPLOADS."materials/";
                $tmp_name = $_FILES["Materials"]["tmp_name"]["image"];
                $file = $dir.$imageName;

                move_uploaded_file($tmp_name, "$dir/$model->image");
            }

            if($model->save()){
                foreach (Helpers::languages() as $key=>$value){
                    $modelLabel = MaterialLabel::model()->findByAttributes(array('material_id' => $id,'language_id' => $value->id));
                    $modelLabel->language_id = $value->id;
                    $modelLabel->material_id = $model->id;
                    $modelLabel->name = $_POST['MaterialLabel']['name'][$value->id];
                    $modelLabel->description = $_POST['MaterialLabel']['description'][$value->id];
                    $modelLabel->additional_description = $_POST['MaterialLabel']['additional_description'][$value->id];
                    $modelLabel->small_description = $_POST['MaterialLabel']['small_description'][$value->id];
                    $modelLabel->save();
                }

                $this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
            'category'=>$description
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Materials');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Materials('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Materials']))
			$model->attributes=$_GET['Materials'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Materials the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Materials::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Materials $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='materials-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
