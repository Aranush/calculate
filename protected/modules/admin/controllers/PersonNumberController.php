<?php

class PersonNumberController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = "/layouts/main";

    public $canWithoutLogin = array();
    function init() {
        $this->canWithoutLogin = array('login');

        if(!isset(Yii::app()->session['admin'])) {
            if (!in_array($this->action, $this->canWithoutLogin)) {
                $this->redirect($this->CreateUrl('default/login'));
            }
        }
    }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PersonNumber;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PersonNumber']))
		{
            $model->attributes=$_POST['PersonNumber'];
            if($_FILES['PersonNumber']){
                $uploads_dir = UPLOADS.'persones/';
                $tmp_name = $_FILES["PersonNumber"]["tmp_name"]["image"];
                $tmp_name1 = $_FILES["PersonNumber"]["tmp_name"]["history_image"];
                $model->image = md5(time()).".png";
                $model->history_image = md5(time()+1).".png";
                @move_uploaded_file($tmp_name, "$uploads_dir/$model->image");
                @move_uploaded_file($tmp_name1, "$uploads_dir/$model->history_image");
            }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PersonNumber']))
		{

            $oldImage = $model->image;
            $oldImage1 = $model->history_image;
            $model->attributes = $_POST['PersonNumber'];
            $model->image = $oldImage;
            $model->history_image = $oldImage1;
            if(!empty($_FILES['PersonNumber']['name']['image'])){
                if(file_exists(UPLOADS.'/persones/'. $model->image)){
                    unlink(bDIR.'/upload/persones/'. $model->image);
                }
                $imageName = md5(time()).".png";
                $model->image = $imageName;
                $dir = UPLOADS."persones/";
                $tmp_name = $_FILES["PersonNumber"]["tmp_name"]["image"];
                $file = $dir.$imageName;

                move_uploaded_file($tmp_name, "$dir/$model->image");
            }
            if(!empty($_FILES['PersonNumber']['name']['history_image'])){
                if(file_exists(UPLOADS.'/persones/'. $model->history_image)){
                    unlink(bDIR.'/upload/persones/'. $model->history_image);
                }
                $imageName = md5(time()+1).".png";
                $model->history_image = $imageName;
                $dir = UPLOADS."persones/";
                $tmp_name = $_FILES["PersonNumber"]["tmp_name"]["history_image"];
                $file = $dir.$imageName;

                move_uploaded_file($tmp_name, "$dir/$model->history_image");
            }

            if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PersonNumber');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PersonNumber('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PersonNumber']))
			$model->attributes=$_GET['PersonNumber'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PersonNumber the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PersonNumber::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PersonNumber $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='person-number-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
