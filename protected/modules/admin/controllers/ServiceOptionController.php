<?php

class ServiceOptionController extends Controller
{
    public $layout = "/layouts/main";

    public $canWithoutLogin = array();
    function init() {
        $this->canWithoutLogin = array('login');

        if(!isset(Yii::app()->session['admin'])) {
            if (!in_array($this->action, $this->canWithoutLogin)) {
                $this->redirect($this->CreateUrl('default/login'));
            }
        }
    }

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($service_id)
	{
		$model=new ServiceOption;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['rowGroup']))
		{
            foreach ($_POST['rowGroup'] as $key=>$value){
                $model=new ServiceOption();
                $model->price = $value['price'];
                $model->service_id = $service_id;
                if($model->save()){
                    foreach ($value['text'] as $k=>$v){

                        $modelLabel = new ServiceOptionLabel();
                        $modelLabel->option_id =$model->id;
                        $modelLabel->language_id =$k;
                        $modelLabel->text=$v;
                        $modelLabel->save();
                    }
                }
            }
            $this->redirect(array('admin','id'=>$service_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        if(isset($_POST['rowGroup']))
        {

            $model->attributes=$_POST['rowGroup'];
            if($model->save()){

                $languages = Helpers::languages();
                if(isset($languages)){
                    foreach($languages as $key=>$value){

                        $modelLabel = ServiceOptionLabel::model()->findByAttributes(array('option_id' => $id,'language_id' =>$value->id));
                        $modelLabel->text = $_POST['rowGroup']['text'][$value->id];

                        $modelLabel->save();
                    }

                }


                $this->redirect(array('view','id'=>$model->id));

            }
        }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ServiceOption']))
		{
			$model->attributes=$_POST['ServiceOption'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ServiceOption');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ServiceOption('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ServiceOption']))
			$model->attributes=$_GET['ServiceOption'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    public function actionServices()
	{
        $criteria = new CDbCriteria();
        $criteria->with=array(
            'servicesLabel'=>array(
                'alias' => 't1',
                'condition' => 't1.language_id = 1',
            )
        );
        $services = Services::model()->findAll($criteria);

		$this->render('services',array('services'=>$services));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ServiceOption the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ServiceOption::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ServiceOption $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='service-option-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
