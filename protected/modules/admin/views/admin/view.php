<section class="content-header">
	<h1>
		Admin
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Admin #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('admin/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('admin/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							'id',
							'username',
							'password',
							'first_name',
							'last_name',
							'phone',
							'last_activity',
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
