<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'category-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>

					<div class="form-group">

						<?php
						$languages = Helpers::languages();
						if(!empty($languages)):
							foreach($languages as $key => $value):
								?>
								<?php
								$active = "";
								if($key == 0){
									$active = "active";
								}
								$content = CategoryAdminLabel::model()->findByAttributes(array('language_id' => $value->id,'category_id' => $model->id));
								$val = "";
								$description = "";
								if(!empty($content)){
									$val = $content->name;
									$description = $content->text;
								}
								?>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Name (<?=$value->name?>)</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="CategoryLabel[name][<?=$value->id?>]" value="<?=$val?>" required/>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-2 control-label">Description (<?=$value->name?>)</label>
									<div class="col-sm-10">
										<textarea type="text" class="form-control" name="CategoryLabel[description][<?=$value->id?>]" id="editor<?=$value['id']?>" required><?=$description?></textarea>
									</div>
								</div>
								<?php
							endforeach;
						endif;
						?>
					</div>

					<?php echo $form->errorSummary($model); ?>
					<div class="row form-group">
						<?php echo $form->labelEx($model,'status',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<select class="form-control" name="Category[status]">
								<option selected value="0">active</option>
								<option value="1">disable</option>
							</select>
						</div>
						<?php echo $form->error($model,'status'); ?>
					</div>
					<div class="row form-group">
						<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($model,'sort_order',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'sort_order'); ?>
					</div>


					<div class="form-group buttons">
						<div class="col-sm-12">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
						</div>
					</div>


				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>

	});
</script>