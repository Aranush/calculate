<section class="content-header">
	<h1>
		Person
		<small>#<?php  echo $model->id; ?></small>
	</h1>

</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Category #<?php  echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('category/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('category/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>

				<?php $this->widget('zii.widgets.CDetailView', array(
					'data'=>$model,
					'htmlOptions' => array('class' => 'table table-bordered table-hover'),
					'attributes'=>array(
						array(
							'name'=>'status',
							'value'=> ($model->status == 0) ? 'active':'disable',
						),
						array(
							'name' 	=> 'name',
							'value'	=> ($model->categoryAdminLabel) ? $model->categoryAdminLabel->name : ""
						),
						'sort_order',
					),
				)); ?>
			</div>
		</div>
	</div>
	</div>
</section>
