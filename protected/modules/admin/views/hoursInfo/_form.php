<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'hours-info-form',
					'htmlOptions' => array(
						'class' =>'form-horizontal',
						"enctype" => "multipart/form-data"
					),
					'enableAjaxValidation'=>false,
				)); ?>

					<?php echo $form->errorSummary($model); ?>
					<div class="row form-group">
						<?php echo $form->labelEx($model,'hours',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($model,'hours',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'hours'); ?>
					</div>
					<div class="row form-group">
						<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'price'); ?>
					</div>

					<div class="row form-group">
						<?php echo $form->labelEx($model,'image',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->fileField($model,'image',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'image'); ?>
					</div>

					<div class="form-group buttons">
						<div class="col-sm-12">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
						</div>
					</div>

				<?php $this->endWidget(); ?>


			</div>
		</div>
	</div>
</section>
