<?php
/* @var $this HoursInfoController */
/* @var $model HoursInfo */

$this->breadcrumbs=array(
	'Hours Infos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HoursInfo', 'url'=>array('index')),
	array('label'=>'Manage HoursInfo', 'url'=>array('admin')),
);
?>

<h1>Create HoursInfo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>