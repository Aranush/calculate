<?php
/* @var $this HoursInfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hours Infos',
);

$this->menu=array(
	array('label'=>'Create HoursInfo', 'url'=>array('create')),
	array('label'=>'Manage HoursInfo', 'url'=>array('admin')),
);
?>

<h1>Hours Infos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
