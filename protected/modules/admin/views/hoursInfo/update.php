<?php
/* @var $this HoursInfoController */
/* @var $model HoursInfo */

$this->breadcrumbs=array(
	'Hours Infos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List HoursInfo', 'url'=>array('index')),
	array('label'=>'Create HoursInfo', 'url'=>array('create')),
	array('label'=>'View HoursInfo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage HoursInfo', 'url'=>array('admin')),
);
?>

<h1>Update HoursInfo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>