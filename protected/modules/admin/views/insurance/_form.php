<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>


				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'insurance-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>

				<?php echo $form->errorSummary($model); ?>

				<div class="form-container row">
					<div class="col-xs-12">
						<div class="row form-group">
							<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php if($model->isNewRecord): ?>
									<?php echo $form->textField($model,'price',array('class' => 'form-control', 'name'=>'rowGroup[0][price]')); ?>

								<?php else: ?>
									<?php echo $form->textField($model,'price',array('class' => 'form-control', 'name'=>'rowGroup[price]')); ?>

								<?php endif;?>
							</div>
							<?php echo $form->error($model,'price'); ?>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									?>
									<?php
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = InsuranceLabel::model()->findByAttributes(array('language_id' => $value->id,'insurance_id' => $model->id));
									$name = "";
									if(!empty($content)){
										$name = $content->text;
									}
									?>
									<label class="col-sm-2 control-label"><?=$value->name?></label>
									<div class="col-sm-10">
										<div class="form-group row">
											<div class="col-sm-12">
												<?php 									if($model->isNewRecord):
													?>
													<textarea type="text" class="form-control margin_10" name="rowGroup[0][text][<?=$value->id?>]" id="editor-0-<?=$value->id?>"  required><?=$name?></textarea>
												<?php else: ?>
													<textarea type="text" class="form-control margin_10" name="rowGroup[text][<?=$value->id?>]" id="editor-0-<?=$value->id?>"  required><?=$name?></textarea>
												<?php endif?>
											</div>
										</div>
									</div>
									<?php
								endforeach;
							endif;
							?>
						</div>
					</div>
				</div>
				<div class="form-container rows row">

				</div>

				<div class="form-group">
					<?php if($model->isNewRecord): ?>

						<div class="col-sm-12">
							<button class="btn btn-primary pull-right" data-id="1" id="plus">+</button>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-xs-12">
				</div>
				<div class="form-group buttons">
					<div class="col-sm-12">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
					</div>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>
<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor-0-<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>
	});
</script>

<script>
	$(document).ready(function () {
		$('#plus').on("click",function(e){
			e.preventDefault();
			var current = $(this).attr('data-id');
			var strVar="";
			strVar += "<div class=\"container-"+current+"\" style = 'margin-top: 50px;float: left;width: 100%;'>";
			strVar += "							<div class=\"col-xs-12\">";
			strVar += "								<div class=\"row form-group\">";
			strVar += "									<label class=\"col-sm-2 control-label\">Price *<\/label>";
			strVar += "									<div class=\"col-xs-10\">";
			strVar += "										<input name=\"rowGroup["+current+"][price]\" class=\"form-control\" \/>";
			strVar += "									<\/div>";
			strVar += "								<\/div>";
			<?php
			$language = Helpers::languages();
			if(!empty($language)):
			foreach($language as $key => $value):
			?>
			strVar += "								<div class=\"row form-group\">";
			strVar += "									<label class=\"col-sm-2 control-label\">Text * <?=$value->name?> <\/label>";
			strVar += "									<div class=\"col-xs-10\">";
			strVar += "										<textarea name=\"rowGroup["+current+"][text][<?=$value->id?>]\" id=\"editor-"+current+"-<?=$value->id?>\" class=\"form-control\" ></textarea>";
			strVar += "									<\/div>";
			strVar += "								<\/div>";
			strVar += "							<\/div>";
			<?php
			endforeach;
			endif;
			?>

			strVar += "						<\/div>";

			$('.rows').append(strVar);

			$(function () {
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
				<?php
				$language = Helpers::languages();
				if(!empty($language)):
				foreach($language as $key => $value):
				?>
				CKEDITOR.replace( 'editor-'+current+'-<?=$value->id?>', {});
				//bootstrap WYSIHTML5 - text editor
				$(".textarea").wysihtml5();
				<?php
				endforeach;
				endif;
				?>
			});

			current = parseInt(current)+1;
			$(this).attr('data-id',current);
		});
	});
</script>
