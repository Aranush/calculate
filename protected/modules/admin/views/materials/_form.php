<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'materials-form',
					'htmlOptions' => array(
						'class' =>'form-horizontal',
						"enctype" => "multipart/form-data"
					),
					'enableAjaxValidation'=>false,
				)); ?>

						<div class="form-group">
						<?php
						$languages = Helpers::languages();
						if(!empty($languages)):
							foreach($languages as $key => $value):
								?>
								<?php
								$active = "";
								if($key == 0){
									$active = "active";
								}
								$content = MaterialLabel::model()->findByAttributes(array('language_id' => $value->id,'material_id' => $model->id));
								$name = "";
								$description = "";
								$additional_description = "";
								$small_description = "";
								if(!empty($content)){
									$name = $content->name;
									$description = $content->description;
									$additional_description = $content->additional_description;
									$small_description = $content->small_description;

								}
								?>
									<label class="col-sm-2 control-label"><?=$value->name?></label>
									<div class="col-sm-10">
										<div class="form-group row">
											<label class="col-sm-2 control-label">Name*</label>
											<div class="col-sm-10">
												<input type="text" class="form-control margin_10" name="MaterialLabel[name][<?=$value->id?>]" value="<?=$name?>" required/>
											</div>
											<label class="col-sm-2 control-label">Description*</label>
											<div class="col-sm-10">
												<textarea type="text" class="form-control margin_10" name="MaterialLabel[description][<?=$value->id?>]" id="editor<?=$value['id']?>"  required><?=$description?></textarea>
											</div>
											<label class="col-sm-2 control-label">Additional Description</label>
											<div class="col-sm-10">
												<textarea type="text" class="form-control margin_10" name="MaterialLabel[additional_description][<?=$value->id?>]" id="editorDesc<?=$value['id']?>"  required><?=$additional_description?></textarea>
											</div>
											<label class="col-sm-2 control-label">Small Description</label>
											<div class="col-sm-10">
												<textarea type="text" class="form-control margin_10" name="MaterialLabel[small_description][<?=$value->id?>]" id="editorDesc1<?=$value['id']?>"  required><?=$small_description?></textarea>
											</div>

										</div>
									</div>
								<?php
							endforeach;
						endif;
						?>
						</div>

					<div class="row form-group">
						<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'price'); ?>
					</div>
					<div class="row form-group">
						<?php echo $form->labelEx($model,'image',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->fileField($model,'image',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'image'); ?>
					</div>

					<div class="row form-group">
						<?php echo $form->labelEx($model,'category_id',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<select class="form-control" name="Materials[category_id]">
								<?php foreach ($category as $key=>$value):?>
									<option selected</option>
									<option value ="<?=$value['id']?>"><?=$value['categoryAdminLabel']['name']?></option>
								<?php endforeach;?>
							</select>
						</div>
						<?php echo $form->error($model,'category_id'); ?>
					</div>


				<div class="form-group buttons">
						<div class="col-sm-12">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
						</div>
					</div>

				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor<?=$value->id?>', {});
		CKEDITOR.replace( 'editorDesc<?=$value->id?>', {});
		CKEDITOR.replace( 'editorDesc1<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>

	});
</script>