
<section class="content-header">
	<h1>
		Materials
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Materials</h3>
					<a href="<?=$this->CreateUrl('materials/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'materials-grid',
							'itemsCssClass' => 'table table-bordered table-hover',
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								array(
									'name'=>'name',
									'value'=>'$data->materialLabel ? $data->materialLabel->name :""'
								),
//								'category_id',
								array(
									'name' 	=> 'category_id',
									'filter' => CHTml::dropDownList("Materials[category_id]",$model->category_id,
										CHtml::listData(CategoryAdminLabel::model()->findAllByAttributes(array('language_id' => 1)), 'category_id', 'name'),
										array('empty' => 'All', 'class' => 'form-control')
									),

									'value'	=> '$data->category->categoryAdminLabel->name'
								),
								'price',
								array(
									'name'=>'image',
									'type' => 'raw',
									'htmlOptions'=>array(
										'class' => 'admin_img'
									),
									'value' => 'CHtml::image(Yii::app()->baseUrl . "/upload/materials/" . $data->image)'
								),
								array(
									'class' => 'zii.widgets.grid.CButtonColumn',
									'htmlOptions' => array('style' => 'white-space: nowrap'),
									'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
									'buttons' => array(
										'view' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-eye"></i>',
											'imageUrl' => false,
										),
										'update' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-pencil"></i>',
											'imageUrl' => false,
										),
										'delete' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn'),
											'label' => '<i class="fa fa-times"></i>',
											'imageUrl' => false,
										)
									)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
