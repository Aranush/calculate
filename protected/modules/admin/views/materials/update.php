<?php
/* @var $this MaterialsController */
/* @var $model Materials */

$this->breadcrumbs=array(
	'Materials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Materials', 'url'=>array('index')),
	array('label'=>'Create Materials', 'url'=>array('create')),
	array('label'=>'View Materials', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Materials', 'url'=>array('admin')),
);
?>

<h1>Update Materials <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'category'=>$category)); ?>