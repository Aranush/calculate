<?php
/* @var $this PdfFormsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pdf Forms',
);

$this->menu=array(
	array('label'=>'Create PdfForms', 'url'=>array('create')),
	array('label'=>'Manage PdfForms', 'url'=>array('admin')),
);
?>

<h1>Pdf Forms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
