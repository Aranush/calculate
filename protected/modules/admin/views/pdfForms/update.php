<?php
/* @var $this PdfFormsController */
/* @var $model PdfForms */

$this->breadcrumbs=array(
	'Pdf Forms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PdfForms', 'url'=>array('index')),
	array('label'=>'Create PdfForms', 'url'=>array('create')),
	array('label'=>'View PdfForms', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PdfForms', 'url'=>array('admin')),
);
?>

<h1>Update PdfForms <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>