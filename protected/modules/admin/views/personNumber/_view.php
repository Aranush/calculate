<?php
/* @var $this PersonNumberController */
/* @var $data PersonNumber */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('person_number')); ?>:</b>
	<?php echo CHtml::encode($data->person_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('history_image')); ?>:</b>
	<?php echo CHtml::encode($data->history_image); ?>
	<br />


</div>