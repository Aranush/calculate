<?php
/* @var $this PersonNumberController */
/* @var $model PersonNumber */

$this->breadcrumbs=array(
	'Person Numbers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PersonNumber', 'url'=>array('index')),
	array('label'=>'Manage PersonNumber', 'url'=>array('admin')),
);
?>

<h1>Create PersonNumber</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>