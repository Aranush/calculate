<?php
/* @var $this PersonNumberController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Person Numbers',
);

$this->menu=array(
	array('label'=>'Create PersonNumber', 'url'=>array('create')),
	array('label'=>'Manage PersonNumber', 'url'=>array('admin')),
);
?>

<h1>Person Numbers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
