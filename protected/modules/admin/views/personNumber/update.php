<?php
/* @var $this PersonNumberController */
/* @var $model PersonNumber */

$this->breadcrumbs=array(
	'Person Numbers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PersonNumber', 'url'=>array('index')),
	array('label'=>'Create PersonNumber', 'url'=>array('create')),
	array('label'=>'View PersonNumber', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PersonNumber', 'url'=>array('admin')),
);
?>

<h1>Update PersonNumber <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>