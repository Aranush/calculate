<section class="content-header">
	<h1>
		Person
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Person #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('personNumber/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('personNumber/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>

					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							'person_number',
							'price',
							array(
								'name'=>'image',
								'type'=>'raw',
								'value'=>CHtml::image(Yii::app()->baseUrl . "/upload/persones/" . $model->image)
							),
							array(
								'name'=>'history_image',
								'type'=>'raw',
								'value'=>CHtml::image(Yii::app()->baseUrl . "/upload/persones/" . $model->history_image)
							),
						),
					)); ?>
			</div>
		</div>
	</div>
	</div>
</section>
