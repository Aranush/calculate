<?php
/* @var $this ServiceOptionController */
/* @var $model ServiceOption */

$this->breadcrumbs=array(
	'Service Options'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ServiceOption', 'url'=>array('index')),
	array('label'=>'Manage ServiceOption', 'url'=>array('admin')),
);
?>

<h1>Create ServiceOption</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>