<?php
/* @var $this ServiceOptionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Service Options',
);

$this->menu=array(
	array('label'=>'Create ServiceOption', 'url'=>array('create')),
	array('label'=>'Manage ServiceOption', 'url'=>array('admin')),
);
?>

<h1>Service Options</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
