<div class="container">
    <div class="row">
        <div class="col-xs-12" style="margin-top: 50px">
            <h1>Please, select service</h1>
            <?php if(isset($services)):?>
                <?php foreach ($services as $value):?>
                    <div class="list-group">
                        <a href="<?=$this->CreateUrl('serviceOption/admin',array('id'=>$value['id']))?>" class="list-group-item"><?=$value['servicesLabel']['name']?></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
</div>