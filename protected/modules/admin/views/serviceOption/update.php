<?php
/* @var $this ServiceOptionController */
/* @var $model ServiceOption */

$this->breadcrumbs=array(
	'Service Options'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ServiceOption', 'url'=>array('index')),
	array('label'=>'Create ServiceOption', 'url'=>array('create')),
	array('label'=>'View ServiceOption', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ServiceOption', 'url'=>array('admin')),
);
?>

<h1>Update ServiceOption <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>