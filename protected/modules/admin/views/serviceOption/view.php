<?php
/* @var $this ServiceOptionController */
/* @var $model ServiceOption */

$this->breadcrumbs=array(
	'Service Options'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ServiceOption', 'url'=>array('index')),
	array('label'=>'Create ServiceOption', 'url'=>array('create')),
	array('label'=>'Update ServiceOption', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ServiceOption', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ServiceOption', 'url'=>array('admin')),
);
?>

<h1>View ServiceOption #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'service_id',
		'price',
	),
)); ?>
