<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'services-form',
					'htmlOptions' => array(
						'class' =>'form-horizontal',
						"enctype" => "multipart/form-data"
					),
					'enableAjaxValidation'=>false,
				)); ?>

				<p class="note">Fields with <span class="required">*</span> are required.</p>

				<?php echo $form->errorSummary($model); ?>
				<div class="form-group">
					<?php
					$languages = Helpers::languages();
					if(!empty($languages)):
						foreach($languages as $key => $value):
							?>
							<?php
							$active = "";
							if($key == 0){
								$active = "active";
							}
							$content = ServicesLabel::model()->findByAttributes(array('language_id' => $value->id,'services_id' => $model->id));
							$name = "";
							$description = "";
							$small_description = "";
							$additional_description = "";
							if(!empty($content)){
								$name = $content->name;
								$description = $content->description;
								$small_description = $content->small_description;
							}
							?>
							<label class="col-sm-2 control-label"><?=$value->name?></label>
							<div class="col-sm-10">
								<div class="form-group row">
									<label class="col-sm-2 control-label">Name*</label>
									<div class="col-sm-10">
										<input type="text" class="form-control margin_10" name="ServiceLabel[name][<?=$value->id?>]" value="<?=$name?>" required/>
									</div>
									<label class="col-sm-2 control-label">Small Description*</label>
									<div class="col-sm-10">
										<textarea type="text" class="form-control margin_10" name="ServiceLabel[small_description][<?=$value->id?>]"  required><?=$small_description?></textarea>
									</div>
									<label class="col-sm-2 control-label">Description*</label>
									<div class="col-sm-10">
										<textarea type="text" class="form-control margin_10" name="ServiceLabel[description][<?=$value->id?>]" id="editor1"  required><?=$description?></textarea>
									</div>


								</div>
							</div>

							<?php
						endforeach;
					endif;
					?>
				</div>
				<div class="row form-group">
					<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
					<div class="col-sm-10">
						<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
					</div>
					<?php echo $form->error($model,'price'); ?>
				</div>

				<div class="row form-group">
					<?php echo $form->labelEx($model,'image',array('class'=>'col-sm-2 control-label')); ?>
					<div class="col-sm-10">
						<?php echo $form->fileField($model,'image',array('class' => 'form-control')); ?>
					</div>
					<?php echo $form->error($model,'image'); ?>
				</div>


				<div class="form-container rows row">
					<div class="container-0" style = 'margin-top: 50px;float: left;width: 100%;'>
						<div class="col-xs-12">
							<div class="row form-group">
								<label class="col-sm-2 control-label">Price *</label>
								<div class="col-xs-10">
									<input name = "rowGroup[0][price]" class="form-control" />
								</div>
							</div>

						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-sm-12">
						<button class="btn btn-primary pull-right" data-id="1" id="plus">+</button>
					</div>
				</div>

				<div class="form-group buttons">
					<div class="col-sm-12">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
					</div>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor<?=$value->id?>', {});
		CKEDITOR.replace( 'editorDesc<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>

	});
</script>
<script>


	$(document).ready(function () {
		$('#plus').on("click",function(e){
			e.preventDefault();
			var current = $(this).attr('data-id');
			var strVar="";
			strVar += "<div class=\"container-"+current+"\" style = 'margin-top: 50px;float: left;width: 100%;'>";
			strVar += "							<div class=\"col-xs-12\">";
			strVar += "								<div class=\"row form-group\">";
			strVar += "									<label class=\"col-sm-2 control-label\">Price *<\/label>";
			strVar += "									<div class=\"col-xs-10\">";
			strVar += "										<input name=\"rowGroup["+current+"][price]\" class=\"form-control\" \/>";
			strVar += "									<\/div>";
			strVar += "								<\/div>";
			<?php
			$language = Helpers::languages();
			if(!empty($language)):
			foreach($language as $key => $value):
			?>
			strVar += "								<div class=\"row form-group\">";
			strVar += "									<label class=\"col-sm-2 control-label\">Text * <?=$value->name?> <\/label>";
			strVar += "									<div class=\"col-xs-10\">";
			strVar += "										<textarea name=\"rowGroup["+current+"][text][<?=$value->id?>]\" id=\"editor-"+current+"-<?=$value->id?>\" class=\"form-control\" ></textarea>";
			strVar += "									<\/div>";
			strVar += "								<\/div>";
			strVar += "							<\/div>";
			<?php
			endforeach;
			endif;
			?>

			strVar += "						<\/div>";

			$('.rows').append(strVar);

			$(function () {
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
				<?php
				$language = Helpers::languages();
				if(!empty($language)):
				foreach($language as $key => $value):
				?>
				CKEDITOR.replace( 'editor-'+current+'-<?=$value->id?>', {});
				//bootstrap WYSIHTML5 - text editor
				$(".textarea").wysihtml5();
				<?php
				endforeach;
				endif;
				?>
			});

			current = parseInt(current)+1;
			$(this).attr('data-id',current);
		});
	});
</script>