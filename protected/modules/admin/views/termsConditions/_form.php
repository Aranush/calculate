<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'terms-conditions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

				<div class="form-group">
					<?php
					$languages = Helpers::languages();
					if(!empty($languages)):
						foreach($languages as $key => $value):
							?>
							<?php
							$active = "";
							if($key == 0){
								$active = "active";
							}
							$content = TermsContaitionsLabel::model()->findByAttributes(array('language_id' => $value->id,'terms_id' => $model->id));
							$name = "";
							$description = "";
							$small_description = "";
							$additional_description = "";
							if(!empty($content)){
								$name = $content->text;
							}
							?>
							<label class="col-sm-2 control-label"><?=$value->name?></label>
							<div class="col-sm-10">
								<div class="form-group row">
									<label class="col-sm-2 control-label">Text*</label>
									<div class="col-sm-10">
										<textarea type="text" class="form-control margin_10" name="TermsConditions[text][<?=$value->id?>]"   id="editor1" required><?=$name?></textarea>
									</div>
								</div>
							</div>

							<?php
						endforeach;
					endif;
					?>
				</div>
				<div class="form-group buttons">
					<div class="col-sm-12">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
					</div>
				</div>
<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>

	});
</script>
<script>
