<?php
/* @var $this TermsConditionsController */
/* @var $model TermsConditions */

$this->breadcrumbs=array(
	'Terms Conditions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TermsConditions', 'url'=>array('index')),
	array('label'=>'Manage TermsConditions', 'url'=>array('admin')),
);
?>

<h1>Create TermsConditions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>