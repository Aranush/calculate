<?php
/* @var $this TermsConditionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Terms Conditions',
);

$this->menu=array(
	array('label'=>'Create TermsConditions', 'url'=>array('create')),
	array('label'=>'Manage TermsConditions', 'url'=>array('admin')),
);
?>

<h1>Terms Conditions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
