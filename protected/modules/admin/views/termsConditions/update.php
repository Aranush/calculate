<?php
/* @var $this TermsConditionsController */
/* @var $model TermsConditions */

$this->breadcrumbs=array(
	'Terms Conditions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TermsConditions', 'url'=>array('index')),
	array('label'=>'Create TermsConditions', 'url'=>array('create')),
	array('label'=>'View TermsConditions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TermsConditions', 'url'=>array('admin')),
);
?>

<h1>Update TermsConditions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>