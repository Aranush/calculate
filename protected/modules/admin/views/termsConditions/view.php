<?php
/* @var $this TermsConditionsController */
/* @var $model TermsConditions */

$this->breadcrumbs=array(
	'Terms Conditions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TermsConditions', 'url'=>array('index')),
	array('label'=>'Create TermsConditions', 'url'=>array('create')),
	array('label'=>'Update TermsConditions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TermsConditions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TermsConditions', 'url'=>array('admin')),
);
?>

<h1>View TermsConditions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
	),
)); ?>
