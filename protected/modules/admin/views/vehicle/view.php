<section class="content-header">
	<h1>
		Vehicle
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Vehicle #<?php echo $model->id; ?></h3>
					<a href="<?=$this->CreateUrl('vehicle/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
					<a href="<?=$this->CreateUrl('vehicle/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>

				<?php $this->widget('zii.widgets.CDetailView', array(
					'data'=>$model,
					'htmlOptions' => array('class' => 'table table-bordered table-hover'),
					'attributes'=>array(
						'id',
						'vehicle_volume',
						'price',
						array(
							'name'=>'image',
							'type'=>'raw',
							'value'=>CHtml::image(Yii::app()->baseUrl . "/upload/vehicle/" . $model->image)
						),
						array(
							'name'=>'history_image',
							'type'=>'raw',
							'value'=>CHtml::image(Yii::app()->baseUrl . "/upload/vehicle/" . $model->history_image)
						),
					),
				)); ?>
			</div>
		</div>
	</div>
	</div>
</section>
