<?php

class BuildingTypeController extends Controller
{


	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}




	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BuildingType;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BuildingType']))
		{
			$model->attributes=$_POST['BuildingType'];
			$model->created_date=Helpers::date();
			if($model->save())
				$languages = Helpers::languages();
					if(!empty($languages)){
						foreach($languages as $key => $value){
							$model_label = new BuildingTypeLabel();
							$model_label->building_type_id = $model->id;
							$model_label->language_id = $value->id;
							$model_label->name = $_POST['BuildingTypeLabel']['name'][$value->id];
							$model_label->save();
						}
					}
					if(!empty($_POST['BuildingTypeLabelPlus']['name'])){
						foreach($_POST['BuildingTypeLabelPlus']['name'] as $key => $value){
							$model_plus = new BuildingType();
							$model_plus->parent_id = $_POST['BuildingTypePlus']['parent_id'][$key];
							$model_plus->price = $_POST['BuildingTypePlus']['price'][$key];
							$model_plus->created_date = Helpers::date();
							$model_plus->sort_order = $_POST['BuildingTypePlus']['sort_order'][$key];
							$model_plus->save();
							foreach($value as $k => $v){
								$model_label_plus = new BuildingTypeLabel();
								$model_label_plus->building_type_id = $model_plus->id;
								$model_label_plus->language_id = $k;
								$model_label_plus->name = $v;
								$model_label_plus->save();
							}

						}
					}

				$this->redirect(array('view','id'=>$model->id));
		}
		$types = BuildingType::model()->with('buildingTypeLabel')->findAll();
		$this->render('create',array(
			'model'=>$model,
			'types' => $types
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BuildingType']))
		{

			if($id == 0){
				$this->redirect(array('admin'));
			}
			$model->attributes=$_POST['BuildingType'];
			if($model->save())
				$languages = Helpers::languages();
					if(!empty($languages)){
						foreach($languages as $key => $value){
							$model_label = BuildingTypeLabel::model()->findByAttributes(array('building_type_id' => $model->id,'language_id' => $value->id));
							if(empty($model_label)){
								$model_label = new BuildingTypeLabel();
							}
							$model_label->building_type_id = $model->id;
							$model_label->language_id = $value->id;
							$model_label->name = $_POST['BuildingTypeLabel']['name'][$value->id];
							$model_label->save();
						}
					}
				$this->redirect(array('view','id'=>$model->id));
		}
		$types = BuildingType::model()->with('buildingTypeLabel')->findAll();

		$this->render('update',array(
			'model'=>$model,
			'types' => $types
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if($id == 0){
			$this->redirect(array('admin'));
		}
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('BuildingType');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BuildingType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BuildingType']))
			$model->attributes=$_GET['BuildingType'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BuildingType the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BuildingType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BuildingType $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='building-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
