<?php

class DefaultController extends Controller
{
	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->actionLogin());
			}
		}
	}


	public function actionIndex()
	{
		$this->redirect($this->actionLogin());die;
	}

	public function actionLogin(){
        $this->layout = false;
		if(!Yii::app()->session){
			$this->redirect($this->CreateUrl('/staff/admin/default'));
		}
		if(isset($_POST['admin'])){
			$model = new LoginForm();
			$username= $_POST['admin']['username'];
			$password =hash('sha256',$_POST['admin']['password']);
			$model->username = $username;
			$model->password = $password;

			if($model->validate() && $model->login()){
				$admin = Admin::model()->findByAttributes(array('username'=>$model->username));
				$admin->last_activity = date('Y-m-d H:i:s');
				$admin->save();
				Yii::app()->session['stuff'] = $admin;
				$this->redirect($this->CreateUrl('/staff/admin/admin'));
			}else{
				Yii::app()->user->setFlash('danger', 'Username/password  incorrect');
				$this->redirect($this->CreateUrl('/staff/default/login'));
			}
		}
		$this->render('login');
	}
}