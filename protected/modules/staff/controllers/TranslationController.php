<?php

class TranslationController extends Controller
{

	public $layout = "/layouts/main";

	public $canWithoutLogin = array();
	function init() {
		$this->canWithoutLogin = array('login');

		if(!isset(Yii::app()->session['stuff'])) {
			if (!in_array($this->action, $this->canWithoutLogin)) {
				$this->redirect($this->CreateUrl('default/login'));
			}
		}
	}



	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Translation;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Translation']))
		{
			$model->attributes=$_POST['Translation'];
			if($model->save())
				$languages = Helpers::languages();
					foreach($languages as $key => $value){
						$model_label = new TranslationLabel();
						$model_label->translation_id = $model->id;
						$model_label->language_id = $value->id;
						$model_label->value = $_POST['TranslationLabel']['content'][$value->id];
						$model_label->save();
					}
			$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Translation']))
		{
			$model->attributes=$_POST['Translation'];
			if($model->save())
				$languages = Helpers::languages();
			foreach($languages as $key => $value){
				$model_label = TranslationLabel::model()->findByAttributes(array('language_id' => $value->id,'translation_id' => $id));
				if(empty($model_label)){
					$model_label = new TranslationLabel();
				}
				$model_label->translation_id = $model->id;
				$model_label->language_id = $value->id;
				$model_label->value = $_POST['TranslationLabel']['content'][$value->id];
				$model_label->save();
			}
			$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Translation');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Translation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Translation']))
			$model->attributes=$_GET['Translation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Translation the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Translation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Translation $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='translation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
