<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'admin-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array(
							'class' =>'form-horizontal'
						)
					)); ?>
						<?php echo $form->errorSummary($model); ?>

						<div class="form-group">
							<?php echo $form->labelEx($model,'username',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'username'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'password',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>250,'class' => 'form-control','value' => '')); ?>
							</div>
							<?php echo $form->error($model,'password'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'first_name',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'first_name'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'last_name',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'last_name'); ?>
						</div>

						<div class="form-group">
							<?php echo $form->labelEx($model,'phone',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>250,'class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'phone'); ?>
						</div>

						<div class="form-group hide">
							<?php echo $form->labelEx($model,'last_activity',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'last_activity'); ?>
							</div>
							<?php echo $form->error($model,'last_activity'); ?>
						</div>

						<div class="form-group buttons">
							<div class="col-sm-12">
								<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
							</div>
						</div>

					<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>