
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					If you don't choose parent, the current field assumed as a parent
				</div>
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'building-type-form',
							'enableAjaxValidation'=>false,
							'htmlOptions' => array(
								'class' =>'form-horizontal'
							)
						)); ?>

							<?php echo $form->errorSummary($model); ?>

							<div class="form-group">
								<label class="col-sm-2 control-label">Parent</label>
								<div class="col-sm-10">
									<select class="form-control" name="BuildingType[parent_id]">
										<option value="0">Select Parent</option>
										<?php
											if(!empty($types)):
												foreach($types as $key => $value):
													if($value->buildingTypeLabel):
										?>
													<option <?php if($value->id == $model->parent_id):?>selected<?php endif;?> value="<?=$value->id?>"><?=$value->buildingTypeLabel->name?></option>
										<?php
														endif;
												endforeach;
											endif;
										?>

									</select>
								</div>
								<?php echo $form->error($model,'parent_id'); ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->textField($model,'sort_order',array('class' => 'form-control')); ?>
								</div>
								<?php echo $form->error($model,'sort_order'); ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
								<div class="col-sm-10">
									<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
								</div>
								<?php echo $form->error($model,'price'); ?>
							</div>

							<div class="form-group">

								<?php
								$languages = Helpers::languages();
								if(!empty($languages)):
									foreach($languages as $key => $value):
								?>
								<?php
										$active = "";
										if($key == 0){
											$active = "active";
										}
										$content = BuildingTypeLabel::model()->findByAttributes(array('language_id' => $value->id,'building_type_id' => $model->id));
										$val = "";
										if(!empty($content)){
											$val = $content->name;
										}
										?>
											<div class="form-group">
												<label class="col-sm-2 control-label">Name (<?=$value->name?>)</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="BuildingTypeLabel[name][<?=$value->id?>]" value="<?=$val?>" required/>
												</div>
											</div>

										<?php
									endforeach;
								endif;
								?>
							</div>
							<?php
								if($model->isNewRecord):
							?>
									<div class="form-group">
										<div class="col-sm-12">
											<button class="btn btn-primary pull-right" data-id="0" id="plus">+</button>
										</div>
									</div>
							<?php
								endif;
							?>
							<div class="type_add">
							</div>
							<div class="form-group buttons">
								<div class="col-sm-12">
									<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
								</div>
							</div>

						<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</section>
<script>
	$(document).on('click','#plus', function(e){
		e.preventDefault();
		var data_id = $(this).attr('data-id');
		var strVar="";
		strVar +="<div class=\"plus_"+data_id+"\">";
		strVar +="<button class=\"btn btn-danger minus\" id="+data_id+">-</button>";
		strVar += "<div class=\"form-group\">";
		strVar += "								<label class=\"col-sm-2 control-label required\" for=\"BuildingType_parent_id\">Parent <span class=\"required\">*<\/span><\/label>								<div class=\"col-sm-10\">";
		strVar += "									<select class=\"form-control\" name=\"BuildingTypePlus[parent_id][]\">";
		strVar += "										<option value=\"0\">Select Parent<\/option>";
		<?php
		if(!empty($types)):
			foreach($types as $key => $value):
				if($value->buildingTypeLabel):
		?>
				strVar += "<option value=\"<?=$value->id?>\"><?=$value->buildingTypeLabel->name?></option>";
		<?php
				endif;
			endforeach;
		endif;
		?>
		strVar += "									<\/select>";
		strVar += "								<\/div>";
		strVar += "															<\/div>";
		strVar += "							<div class=\"form-group\">";
		strVar += "								<label class=\"col-sm-2 control-label\" for=\"BuildingType_sort_order\">Sort Order<\/label>								<div class=\"col-sm-10\">";
		strVar += "									<input class=\"form-control\" name=\"BuildingTypePlus[sort_order][]\" id=\"BuildingType_sort_order\" type=\"text\" value=\"0\" \/>								<\/div>";
		strVar += "									<input class=\"form-control hidden\" name=\"BuildingTypePlus[count][]\" id=\"BuildingType_sort_order\" type=\"text\" value=\"0\" \/>								<\/div>";
		strVar += "															<\/div>";
		strVar +="<div class=\"plus_"+data_id+"\">";
		strVar += "							<div class=\"form-group\">";
		strVar += "								<label class=\"col-sm-2 control-label\" for=\"BuildingType_price\">Price<\/label>								<div class=\"col-sm-10\">";
		strVar += "									<input class=\"form-control\" name=\"BuildingTypePlus[price][]\" id=\"BuildingType_price\" type=\"text\" \/>								<\/div>";
		strVar += "															<\/div>";
		strVar += "";

		strVar += "							<div class=\"form-group\">";
		<?php
		$languages = Helpers::languages();
			if(!empty($languages)):
				foreach($languages as $key => $value):
		?>
		<?php
			$active = "";
			if($key == 0){
				$active = "active";
			}
			$content = TranslationLabel::model()->findByAttributes(array('language_id' => $value->id,'translation_id' => $model->id));
			$val = "";
			if(!empty($content)){
				$val = $content->value;
			}
		?>
		strVar += "";
		strVar += "							<div class=\"form-group\">";
		strVar += "												<label class=\"col-sm-2 control-label\">Name (fran?ais)<\/label>";
		strVar += "												<div class=\"col-sm-10\">";
		strVar += "													<input type=\"text\" class=\"form-control\" name=\"BuildingTypeLabelPlus[name][][<?=$value->id?>]\"  required \/>";
		strVar += "												<\/div>";
		strVar += "							<\/div>";
		strVar += "	<\/div>";
		strVar += "";
		<?php
				endforeach;
			endif;
		?>
		strVar += "";
		$(this).attr('data-id',parseInt(data_id)+1);
		$('.type_add').append(strVar);
	});
	$(document).on('click','.minus',function(e){
		e.preventDefault();
		var id = $(this).attr('id');
		$('.plus_'+id).remove();
	});
</script>