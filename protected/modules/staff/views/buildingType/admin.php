
<section class="content-header">
	<h1>
		Building Type
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Building Type</h3>
					<a href="<?=$this->CreateUrl('buildingType/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'building-type-grid',
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'itemsCssClass' => 'table table-bordered table-hover',
							'columns'=>array(
								'id',
								array(
									'name' 	=> 'parent_id',
									'filter' => CHTml::dropDownList("BuildingType[parent_id]",$model->parent_id,
										CHtml::listData(BuildingTypeLabel::model()->findAllByAttributes(array('language_id' =>1)), 'building_type_id', 'name'),
										array('empty' => 'All', 'class' => 'form-control')
									),
									'value'	=> '($data->parent->buildingTypeLabel) ? $data->parent->buildingTypeLabel->name : ""'
								),
								array(
									'name' 	=> 'name',
									'value'	=> '($data->buildingTypeLabel) ? $data->buildingTypeLabel->name : ""'
								),
								'price',
								'sort_order',
								array(
									'class' => 'zii.widgets.grid.CButtonColumn',
									'htmlOptions' => array('style' => 'white-space: nowrap'),
									'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
									'buttons' => array(
										'view' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-eye"></i>',
											'imageUrl' => false,
										),
										'update' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
											'label' => '<i class="fa fa-pencil"></i>',
											'imageUrl' => false,
										),
										'delete' => array(
											'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn'),
											'label' => '<i class="fa fa-times"></i>',
											'imageUrl' => false,
											'visible'=>'$data->id > 0',
										)
									)
								),
							),
						)); ?>
				</div>
			</div>
		</div>
	</div>
</section>