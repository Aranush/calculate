<?php
/* @var $this BuildingTypeController */
/* @var $model BuildingType */

$this->breadcrumbs=array(
	'Building Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BuildingType', 'url'=>array('index')),
	array('label'=>'Manage BuildingType', 'url'=>array('admin')),
);
?>

<h1>Create BuildingType</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'types' => $types)); ?>