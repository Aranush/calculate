<?php
/* @var $this BuildingTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Building Types',
);

$this->menu=array(
	array('label'=>'Create BuildingType', 'url'=>array('create')),
	array('label'=>'Manage BuildingType', 'url'=>array('admin')),
);
?>

<h1>Building Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
