<?php
/* @var $this BuildingTypeController */
/* @var $model BuildingType */

$this->breadcrumbs=array(
	'Building Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BuildingType', 'url'=>array('index')),
	array('label'=>'Create BuildingType', 'url'=>array('create')),
	array('label'=>'View BuildingType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BuildingType', 'url'=>array('admin')),
);

?>

<h1>Update BuildingType <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'types' => $types)); ?>