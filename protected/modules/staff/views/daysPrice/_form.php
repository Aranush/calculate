
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

				<div class="form">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'days-price-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation'=>false,
				)); ?>
					<?php echo $form->errorSummary($model); ?>
					<div class="form-group row">
						<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
						</div>
						<?php echo $form->error($model,'price'); ?>
					</div>

					<div class="form-group buttons">
						<div class="col-sm-12">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
						</div>
					</div>
				<?php $this->endWidget(); ?>

				</div><!-- form -->
			</div>
		</div>
	</div>
</section>