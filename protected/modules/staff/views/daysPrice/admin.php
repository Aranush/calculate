
<section class="content-header">
	<h1>
		Days Price
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'days-price-grid',
						'itemsCssClass' => 'table table-bordered table-hover',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'columns'=>array(
							array(
								'header' => 'week_day',
								'name' => 	'week_day',
								'value' => 'Yii::app()->params["weekDays"][$data->week_day]',
								'cssClassExpression'=> '',
							),
							'price',
							array(
								'class' => 'zii.widgets.grid.CButtonColumn',
								'htmlOptions' => array('style' => 'white-space: nowrap'),
								'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
								'buttons' => array(
									'view' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
										'label' => '<i class="fa fa-eye"></i>',
										'imageUrl' => false,
									),
									'update' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
										'label' => '<i class="fa fa-pencil"></i>',
										'imageUrl' => false,
									),
									'delete' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn hide'),
										'label' => '<i class="fa fa-times"></i>',
										'imageUrl' => false,
									)
								)
							),
						),
					));
					?>
				</div>
			</div>
		</div>
	</div>
</section>