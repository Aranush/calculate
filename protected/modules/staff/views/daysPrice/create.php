<?php
/* @var $this DaysPriceController */
/* @var $model DaysPrice */

$this->breadcrumbs=array(
	'Days Prices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DaysPrice', 'url'=>array('index')),
	array('label'=>'Manage DaysPrice', 'url'=>array('admin')),
);
?>

<h1>Create DaysPrice</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>