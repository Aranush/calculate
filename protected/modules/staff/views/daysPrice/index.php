<?php
/* @var $this DaysPriceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Days Prices',
);

$this->menu=array(
	array('label'=>'Create DaysPrice', 'url'=>array('create')),
	array('label'=>'Manage DaysPrice', 'url'=>array('admin')),
);
?>

<h1>Days Prices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
