<?php
/* @var $this DaysPriceController */
/* @var $model DaysPrice */

$this->breadcrumbs=array(
	'Days Prices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DaysPrice', 'url'=>array('index')),
	array('label'=>'Create DaysPrice', 'url'=>array('create')),
	array('label'=>'View DaysPrice', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DaysPrice', 'url'=>array('admin')),
);
?>

<h1 class="text-capitalize">Update <?php echo Yii::app()->params["weekDays"][$model->week_day]; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>