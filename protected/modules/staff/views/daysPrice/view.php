
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<a href="<?=$this->CreateUrl('daysPrice/update/id/'.$model->id)?>" class="btn btn-success pull-right" style="margin-left: 15px">Edit</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							array(
								'name'=>'week_day',
								'value'=>Yii::app()->params["weekDays"][$model->week_day]
							),
							'price',
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>
