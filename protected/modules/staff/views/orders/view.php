<section class="content-header">
	<h1>
		Orders
		<small>#<?php echo $model->id; ?></small>
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">

				<!-- /.box-header -->
				<div class="box-body">
					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table-bordered table-hover'),
						'attributes'=>array(
							'id',
							'invoice_number',
							'download',
							'information',
							array(
								'name' => 'download',
								'value' => CHtml::link('Download',array('Orders/downloadPdf','id'=>$model->id)),
								'type' => 'raw'
							),
							array(
								'name' => 'information',
								'type'=>'raw',
								'value' => ($model->orderInfo) ? $model->orderInfo-> information_table : ""
							),

						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>

