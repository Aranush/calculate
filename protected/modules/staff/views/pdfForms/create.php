<?php
/* @var $this PdfFormsController */
/* @var $model PdfForms */

$this->breadcrumbs=array(
	'Pdf Forms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PdfForms', 'url'=>array('index')),
	array('label'=>'Manage PdfForms', 'url'=>array('admin')),
);
?>

<h1>Create PdfForms</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>