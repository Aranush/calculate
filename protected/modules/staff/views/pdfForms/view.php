<?php
/* @var $this PdfFormsController */
/* @var $model PdfForms */

$this->breadcrumbs=array(
	'Pdf Forms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PdfForms', 'url'=>array('index')),
	array('label'=>'Create PdfForms', 'url'=>array('create')),
	array('label'=>'Update PdfForms', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PdfForms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PdfForms', 'url'=>array('admin')),
);
?>
<h1>View PdfForms #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'information_table',
		'invoice_number',
		'pdf',
	),
)); ?>
