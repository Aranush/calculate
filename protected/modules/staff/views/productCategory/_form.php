
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'product-category-form',
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// There is a call to performAjaxValidation() commented in generated controller code.
						// See class documentation of CActiveForm for details on this.
						'enableAjaxValidation'=>false,
					)); ?>

						<p class="note">Fields with <span class="required">*</span> are required.</p>

						<?php echo $form->errorSummary($model); ?>


						<div class="form-group">

							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									?>
									<?php
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = CategoryLabel::model()->findByAttributes(array('language_id' => $value->id,'product_category_id' => $model->id));
									$val = "";
									if(!empty($content)){
										$val = $content->name;
									}
									?>
									<div class="form-group row">
										<label class="col-sm-2 control-label">Name (<?=$value->name?>)</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="CategoryLabel[name][<?=$value->id?>]" value="<?=$val?>" required/>
										</div>
									</div>

									<?php
								endforeach;
							endif;
							?>
						</div>

						<div class="form-group row">
							<?php echo $form->labelEx($model,'status',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<select class="form-control" name="ProductCategory[status]">
									<option selected value="0">visible</option>
									<option value="1">hidden</option>
								</select>
							</div>
							<?php echo $form->error($model,'parent_id'); ?>
						</div>
						<div class="form-group row">
							<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'sort_order',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'sort_order'); ?>
						</div>
						<div class="form-group buttons">
							<div class="col-sm-12">
								<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
							</div>
						</div>

					<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</section>