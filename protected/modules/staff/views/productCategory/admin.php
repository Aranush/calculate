
<section class="content-header">
	<h1>
		Categories
	</h1>

</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Category</h3>
					<a href="<?=$this->CreateUrl('productCategory/create')?>" class="btn btn-primary pull-right">Add New</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'product-category-grid',
						'itemsCssClass' => 'table table-bordered table-hover',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'columns'=>array(

							array(
								'name'=>'name',
								'value'=>'$data->categoryLabel ? $data->categoryLabel->name :""'
							),
							array(
								'name' => 'status',
								'filter' => CHtml::dropDownList('ProductCategory[status]', $model->status,
									array('0'=>'Visible', '1'=>'Hidden'),
									array(
										'empty' => 'All',
										'class' => 'form-control'
									)
								),
								'value' => '($data->status) ? "Hidden" : "Visible"'
							),
							'sort_order',
							array(
								'class' => 'zii.widgets.grid.CButtonColumn',
								'htmlOptions' => array('style' => 'white-space: nowrap'),
								'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
								'buttons' => array(
									'view' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'View'),'class'=>'img-btn'),
										'label' => '<i class="fa fa-eye"></i>',
										'imageUrl' => false,
									),
									'update' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Update'),'class'=>'img-btn'),
										'label' => '<i class="fa fa-pencil"></i>',
										'imageUrl' => false,
									),
									'delete' => array(
										'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete'),'class'=>'img-btn del-btn'),
										'label' => '<i class="fa fa-times"></i>',
										'imageUrl' => false,
									)
								)
							),
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</section>