
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'products-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array(
							'class' =>'form-horizontal',
							"enctype" => "multipart/form-data"
						)
					)); ?>

						<?php echo $form->errorSummary($model); ?>

						<div class="form-group row">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									?>
									<?php
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = ProductsLabel::model()->findByAttributes(array('language_id' => $value->id,'product_id' => $model->id));
									$val = "";
									if(!empty($content)){
										$val = $content->name;
									}
									?>
									<label class="col-sm-2 control-label">Name (<?=$value->name?>) *</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="ProductsLabel[name][<?=$value->id?>]" value="<?=$val?>" required/>
									</div>
									<?php
								endforeach;
							endif;
							?>
						</div>
						<div class="form-group row">
							<?php echo $form->labelEx($model,'image',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->fileField($model,'image',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'image'); ?>
						</div>
						<div class="form-group row">
							<?php echo $form->labelEx($model,'volume',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'volume',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'volume'); ?>
						</div>

						<div class="form-group row">
							<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'price'); ?>
						</div>
						<div class="form-group row">
							<?php echo $form->labelEx($model,'category',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<select class="form-control" name="Products[category]">
									<?php foreach($categories as $key => $value):?>
										<option <?= $value->id == $model->id ? "selected" : ""?> value="<?=$value->id?>">
											<?=$value['categoryLabels'][0]['name']; ?>
										</option>
									<?php endforeach;	?>
								</select>
							</div>
							<?php echo $form->error($model,'id'); ?>
						</div>
						<div class="form-group row">
							<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'sort_order',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'sort_order'); ?>
						</div>
						<div class="form-group row">
							<?php echo $form->labelEx($model,'status',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<select class="form-control" name="Products[status]">
									<option selected value="0">visible</option>
									<option value="1">hidden</option>
								</select>
							</div>
							<?php echo $form->error($model,'parent_id'); ?>
						</div>
					<div class="form-group buttons">
						<div class="col-sm-12">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
						</div>
					</div>

					<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>