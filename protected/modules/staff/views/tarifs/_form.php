
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

					<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'tarifs-form',
						'htmlOptions' => array(
							'class' =>'form-horizontal'
						),
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// There is a call to performAjaxValidation() commented in generated controller code.
						// See class documentation of CActiveForm for details on this.
						'enableAjaxValidation'=>false,
					)); ?>


						<?php echo $form->errorSummary($model); ?>

						<div class="form-group row">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									?>
									<?php
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = TarifsLabel::model()->findByAttributes(array('language_id' => $value->id,'tarifs_id' => $model->id));
									$val = "";
									if(!empty($content)){
										$val = $content->name;
									}
									?>
									<label class="col-sm-2 control-label">Name (<?=$value->name?>) *</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="TarifsLabel[name][<?=$value->id?>]" value="<?=$val?>" required/>
									</div>
									<?php
								endforeach;
							endif;
							?>
						</div>
						<div class="form-group">
							<?php echo $form->labelEx($model,'price',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'price',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'price'); ?>
						</div>
						<div class="form-group row">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									?>
									<?php
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = TarifsLabel::model()->findByAttributes(array('language_id' => $value->id,'tarifs_id' => $model->id));
									$val = "";
									if(!empty($content)){
										$val = $content->features_user_Comment;
									}
									?>
									<label class="col-sm-2 control-label">Description text (<?=$value->name?>) *</label>
									<div class="col-sm-10">
										<textarea row="5" type="text" class="form-control" id="editor<?=$value->id?>" name="TarifsLabel[features_user_Comment][<?=$value->id?>]" value="<?=$val?>" required><?=$val?></textarea>
									</div>
									<?php
								endforeach;
							endif;
							?>
						</div>
						<div class="form-group row">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									?>
									<?php
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = TarifsLabel::model()->findByAttributes(array('language_id' => $value->id,'tarifs_id' => $model->id));
									$val = "";
									if(!empty($content)){
										$val = $content->comment;
									}
									?>
									<label class="col-sm-2 control-label">Comment (<?=$value->name?>) </label>
									<div class="col-sm-10">
										<input row="5" type="text" class="form-control"  name="TarifsLabel[comment][<?=$value->id?>]" value="<?=$val?>" />
									</div>
									<?php
								endforeach;
							endif;
							?>
						</div>

						<div class="form-group row">
							<?php echo $form->labelEx($model,'status',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<select class="form-control" name="Tarifs[status]">
									<option selected value="0">visible</option>
									<option value="1">hidden</option>
								</select>
							</div>
							<?php echo $form->error($model,'status'); ?>
						</div>
						<div class="form-group">
							<?php echo $form->labelEx($model,'sort_order',array('class'=>'col-sm-2 control-label')); ?>
							<div class="col-sm-10">
								<?php echo $form->textField($model,'sort_order',array('class' => 'form-control')); ?>
							</div>
							<?php echo $form->error($model,'sort_order'); ?>
						</div>

						<div class="form-group buttons">
							<div class="col-sm-12">
								<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
							</div>
						</div>

					<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		<?php
		$language = Helpers::languages();
		if(!empty($language)):
		foreach($language as $key => $value):
		?>
		CKEDITOR.replace( 'editor<?=$value->id?>', {});
		//bootstrap WYSIHTML5 - text editor
		$(".textarea").wysihtml5();
		<?php
		endforeach;
		endif;
		?>



	});
</script>