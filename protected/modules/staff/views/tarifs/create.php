<?php
/* @var $this TarifsController */
/* @var $model Tarifs */

$this->breadcrumbs=array(
	'Tarifs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tarifs', 'url'=>array('index')),
	array('label'=>'Manage Tarifs', 'url'=>array('admin')),
);
?>

<h1>Create Tarifs</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>