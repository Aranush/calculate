<?php
/* @var $this TarifsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tarifs',
);

$this->menu=array(
	array('label'=>'Create Tarifs', 'url'=>array('create')),
	array('label'=>'Manage Tarifs', 'url'=>array('admin')),
);
?>

<h1>Tarifs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
