<?php
/* @var $this TarifsController */
/* @var $model Tarifs */

$this->breadcrumbs=array(
	'Tarifs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tarifs', 'url'=>array('index')),
	array('label'=>'Create Tarifs', 'url'=>array('create')),
	array('label'=>'View Tarifs', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Tarifs', 'url'=>array('admin')),
);
?>

<h1>Update Tarifs <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>