<?php
/* @var $this TarifsController */
/* @var $model Tarifs */

$this->breadcrumbs=array(
	'Tarifs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Tarifs', 'url'=>array('index')),
	array('label'=>'Create Tarifs', 'url'=>array('create')),
	array('label'=>'Update Tarifs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Tarifs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tarifs', 'url'=>array('admin')),
);
?>

<h1>View Tarifs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'price',
		'status',
		'sort_order',
	),
)); ?>
