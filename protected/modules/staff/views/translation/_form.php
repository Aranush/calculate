<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><p class="note">Fields with <span class="required">*</span> are required.</p></h3>
				</div>

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'translation-form',
					'enableAjaxValidation'=>false,
					'htmlOptions' => array(
						'class' =>'form-horizontal'
					)
				)); ?>


				<?php echo $form->errorSummary($model); ?>

				<div class="form-group">
					<?php echo $form->labelEx($model,'key',array('class'=>'col-sm-2 control-label')); ?>
					<div class="col-sm-10">
						<?php
						$readonly = "";
						if(!$model->isNewRecord){
							$readonly = "readonly";
						}
						echo $form->textField($model,'key',array('size'=>60,'maxlength'=>250,'readonly' => $readonly,'class' => 'form-control'));
						?>
					</div>
					<?php echo $form->error($model,'key'); ?>
				</div>
				<div id="form-group">
					<label class="col-sm-2 control-label">Content</label>
					<div class="col-sm-10">
						<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									$active = "";
									if($key == 0){
										$active = "active";
									}
									?>
									<li class="<?=$active?>"><a href="#<?=$value->id?>" data-toggle="tab" ><?=$value->name?></a></li>
									<?php
								endforeach;
							endif;
							?>
						</ul>
						<div id="my-tab-content" class="tab-content">
							<?php
							$languages = Helpers::languages();
							if(!empty($languages)):
								foreach($languages as $key => $value):
									$active = "";
									if($key == 0){
										$active = "active";
									}
									$content = TranslationLabel::model()->findByAttributes(array('language_id' => $value->id,'translation_id' => $model->id));
									$val = "";
									if(!empty($content)){
										$val = $content->value;
									}
									?>
									<div class="tab-pane <?=$active?>" id="<?=$value->id?>" >
										<div class="form-group">
											<label class="col-sm-1 control-label">Content</label>
											<div class="col-sm-11">
												<textarea class="form-control" id="editor<?=$value->id?>" name = "TranslationLabel[content][<?=$value->id?>]"><?=$val?></textarea>
											</div>
										</div>
									</div>

									<?php
								endforeach;
							endif;
							?>
						</div>
					</div>
				</div>

				<div class="form-group buttons">
					<div class="col-sm-12">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary pull-right')); ?>
					</div>
				</div>

				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</section>

