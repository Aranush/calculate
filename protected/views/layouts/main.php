<!DOCTYPE html>
<html dir="ltr" lang="fr-FR">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Calcul du Volume de votre D?m?nagement - Des bras en</title>
	<link rel="author" href="https://plus.google.com/102084316310845685116/posts" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="distribution" content="global" />
	<meta http-equiv="imagetoolbar" content="no"/>
	<meta name="author" content="desbrasenplus.com">
	<link rel="shortcut icon" href="/assets/ico/favicon.ico">
	<meta name="description" content="Calculez le volume de votre d?m?nagement et r?servez facilement la prestation correspondante."/>
	<meta name="keywords" content="" />
	<meta name="copyright" content="? 2014 Desbrasenplus" />
	<meta name="revisit-after" content="3 days" />
	<meta name="robots" content="index,follow,all" />
	<meta property="og:title" content="Calcul du Volume de votre D?m?nagement - Des bras en plus" />
	<meta property="og:description" content="Calculez le volume de votre d?m?nagement et r?servez facilement la prestation correspondante." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.desbrasenplus.com/commande/inventaire" />
	<meta property="og:image" content="http://www.desbrasenplus.com/assets/images/logo.png" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:locale" content="fr_FR" />
	<meta property="og:site_name" content="www.desbrasenplus.com" />
	<meta property="fb:fbid" content="593870139" />
	<!--
*****************************************************
*	Copyright ? 2016 - Desbrasenplus.com
*	Conception Technique : Jerome Arakelian
*****************************************************
-->
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '637418096397013');
		fbq('track', "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=637418096397013&ev=PageView&noscript=1"/></noscript>
	<!-- End Facebook Pixel Code -->




	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/custom.css" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/responsive.css" type="text/css" />

	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/vendor/magnific-popup/magnific-popup.css" media="screen">
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/vendor/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/theme-responsive.css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/owl-carousel.min.css" />
	<link rel="stylesheet" href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/owl.theme.min.css" />


	<link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/commande-style.css" rel="stylesheet">
	<link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/slider.css" rel="stylesheet">
	<link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/jcalendar.css" rel="stylesheet" type="text/css" />
	<link href="<?=Yii::app()->request->BaseUrl?>/vendor/assets/css/theme-extension.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/jquery.js"></script>


	<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<!-- Hotjar Tracking Code for www.desbrasenplus.com -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:113544,hjsv:5};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

</head>
<body class="no-transition stretched device-lg">
<div id="wrapper" class="clearfix">
	<section class="bg_fond_comande" id="content_">
		<div class="content-wrap">
			<?php echo $content; ?>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/attributes.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmT_0fYuipjO10CKG5wuJnRwkmGwtQTLI&libraries=geometry,places&callback=initAutocomplete"
		async defer></script>

<script type="text/javascript" src="<?php echo Yii::app()->request->BaseUrl?>/vendor/assets/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->BaseUrl?>/vendor/assets/js/functions.js"></script>
<!--<script type="text/javascript" src="http://www.desbrasenplus.com/assets/vendor/bootstrap/js/bootstrap.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->request->BaseUrl?>/vendor/assets/js/jquery.magnific.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->BaseUrl?>/vendor/assets/js/owl.carousel.js"></script>
<!--<script type="text/javascript" src="http://www.desbrasenplus.com/plugins/bootbox/bootbox.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->request->BaseUrl?>/vendor/assets/js/jcalendar.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->BaseUrl?>/vendor/assets/js/jcalendar.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function($){
		var $faqItems = $('#faqs .faq');
		if( window.location.hash != '' ) {
			var getFaqFilterHash = window.location.hash;
			var hashFaqFilter = getFaqFilterHash.split('#');
			if( $faqItems.hasClass( hashFaqFilter[1] ) ) {
				$('#portfolio-filter li').removeClass('activeFilter');
				$( '[data-filter=".'+ hashFaqFilter[1] +'"]' ).parent('li').addClass('activeFilter');
				var hashFaqSelector = '.' + hashFaqFilter[1];
				$faqItems.css('display', 'none');
				if( hashFaqSelector != 'all' ) {
					$( hashFaqSelector ).fadeIn(500);
				} else {
					$faqItems.fadeIn(500);
				}
			}
		}

		$('#portfolio-filter a').click(function(){
			$('#portfolio-filter li').removeClass('activeFilter');
			$(this).parent('li').addClass('activeFilter');
			var faqSelector = $(this).attr('data-filter');
			$faqItems.css('display', 'none');
			if( faqSelector != 'all' ) {
				$( faqSelector ).fadeIn(500);
			} else {
				$faqItems.fadeIn(500);
			}
			return false;
		});
	});
</script>

<script type="text/javascript">
	jQuery(document).ready(function($){

		jQuery('.btn_add_inventaire').click(function(){
			var id_categorie = jQuery(this).attr('id');
			jQuery('#id_categorie').val(id_categorie);
		});





		jQuery("a.bt-add-inventaire-manu").click(function() {
			var id_categorie  	= $(this).attr('id');
			jQuery('#id_categorie_add_inventaire').val(id_categorie);
			return false;
		});


		jQuery("form#form_add_inventaire_manuel").submit(function() {
			var id_categorie  	= jQuery('form#form_add_inventaire_manuel #id_categorie_add_inventaire').val();
			var libelle  		= jQuery('#libelle').val();
			var m3  			= jQuery('#m3').val();
			$.ajax({
				type : "POST",
				url : "/includes/inventaire/commande/inventaire-manuel.php",
				data : "id=999999&id_categorie="+id_categorie+"&libelle="+libelle+"&m3="+m3,
				error :function(msg){
					alert( "Erreur  : " + msg );
				},
				success : function(data){
					//$('input#date_dem').val(obj.valeur);
					var obj = jQuery.parseJSON(data);
					if(obj.result == 1) {
						$('.total_inventaire').html(''+obj.nb+'m&sup3;');
						$('.liste_inventaire').html(''+obj.liste_inventaire);
						$('.prix_total_inventaire').html(''+obj.prix_total_inventaire+'?');
						$('#categorie_inventaire_'+obj.categorie_inventaire).html(''+obj.div_liste_inventaire_manuel_categorie);
						//alert('#categorie_inventaire_'+obj.categorie_inventaire+'');
						if(obj.div_liste_inventaire > 0) { $('div#table_liste_inventaire').show(); }
						else { $('div#table_liste_inventaire').hide(); }
						jQuery('#largeur').val('');
						jQuery('#hauteur').val('');
						jQuery('#profondeur').val('');
						jQuery('#libelle').val('');
						jQuery('form#form_add_inventaire_manuel #id_categorie').val('0');
						$('.close').click();
					}
				}
			});

			return false;
		});



		$("#form_add_inventaire_manuel").keyup(function() {
			var hauteur = $('#hauteur').val();
			var largeur = $('#largeur').val();
			var profondeur = $('#profondeur').val();

			var hauteur = hauteur.replace(',','.');
			$('#hauteur').val(hauteur);

			var largeur = largeur.replace(',','.');
			$('#largeur').val(largeur);

			var profondeur = profondeur.replace(',','.');
			$('#profondeur').val(profondeur);


			if(hauteur > 0 && largeur > 0 && profondeur > 0){
				var volumObject = Math.round( (hauteur/100) * (largeur/100) * (profondeur/100) * 1000 ) / 1000;
				if(!isNaN(volumObject)) {
					var boxVolumObject = (parseFloat(volumObject) < 10 ? '' : '') + parseFloat(volumObject).toFixed(2).toString().replace('.', ',');
				}
				$(".result_volume").html(boxVolumObject);
				$("#m3").val(boxVolumObject);
				if(volumObject > 0){
					$('.error-msg').removeClass('show').addClass('hide');
					$('input.bt_add_inventaire').show();


				}
				else{
					$('.error-msg').removeClass('hide').addClass('show');
					$('input.bt_add_inventaire').hide();
				}
			}
		});

		$(".numbers-row-manuel").append("<div class=\"button_inc_manuel dec button_inc plus_moins minus col-md-6\">-</div><div class=\"button_inc_manuel dec button_inc plus_moins plus col-md-6\">+</div>");

		$(".button_inc_manuel").on("click", function () {
			var $button = $(this);
			//alert($button);
			//alert($button.toSource());
			var oldValue = $button.parent().find("input").val();
			//alert(oldValue);
			if ($button.text() == "+") {
				var newVal = parseFloat(oldValue) + 1;
			} else {
				if (oldValue > 1) {
					var newVal = parseFloat(oldValue) - 1;
				} else {
					newVal = 0;
				}
			}
			$button.parent().find("input").val(newVal);

			var id = $(this).parent().attr("id").split(" ");
			var valeur = newVal;
			if(newVal == 0) {
				$("#item_wind_"+id).html("");
				$("#item_wind_"+id).removeClass("item-wind bg_wind").addClass("item-wind");
			}
			else {
				$("#item_wind_"+id).html(valeur);
				$("#item_wind_"+id).addClass("bg_wind");
			}

			$.ajax({
				type : "POST",
				url : "/includes/inventaire/commande/commande-inventaire-manuel.php",
				data : "id="+id+"&valeur="+newVal,
				error :function(msg){
					alert( "Erreur  : " + msg );
				},
				success : function(data){
					//$("input#date_dem").val(obj.newVal);
					var obj = jQuery.parseJSON(data);
					if(obj.result == 1) {
						$(".total_inventaire").html(""+obj.nb+"m&sup3;");
						$(".liste_inventaire").html(""+obj.liste_inventaire);
						$(".prix_total_inventaire").html(""+obj.prix_total_inventaire+"?");
						if(obj.div_liste_inventaire > 0) { $("div#table_liste_inventaire").show(); }
						else { $("div#table_liste_inventaire").hide(); }
						if(obj.hide == 1) {
							$("#inventaire_manu_"+obj.id_hide).hide();
						}

					}
				}
			});
		});
	});
</script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/assets/js/tunnel-commande.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/validation/additional-methods.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery(".validation").validate();
	});
</script>

</body>
</html>