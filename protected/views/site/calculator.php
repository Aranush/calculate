<div id="wrapper" class="clearfix">
    <section class="bg_fond_comande" id="content_">
        <div class="content-wrap">
            <div class="container clearfix nopadding_responsive">

                <br clear="all">
                <div class="col-xs-12">
                    <div>

                    <!-- Tab panes -->


                    </div>
                    <section class="panel form-wizard" id="w4">
                        <div class="wizard-progress wizard-progress-lg">
                            <div class="steps-progress">
                                <div class="progress-indicator"></div>
                            </div>
                            <ul class="wizard-steps" role="tablist">
                                <li class="active tunnel first-step actived">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span>1</span>Client Information</a>
                                </li>
                                <li class="tunnel second-step">
                                    <a href="#inventory" aria-controls="inventory" role="tab" data-toggle="tab"><span>2</span>Inventory</a>
                                </li>
                                <li class=" tunnel third-step">
                                    <a href="#tarif" aria-controls="tarif" role="tab" data-toggle="tab"><span>3</span>Tarif</a>
                                </li>
                                 <li class=" tunnel fourth-step">
                                    <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab"><span>3</span>Payment</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <br clear="all">
            </div>

            <div class="container clearfix nopadding_responsive">
                <div class="col-md-9 commande nopadding_responsive">
                    <a href="tel:+37455422302">call</a>
                    <?php if(Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-danger calc-alert alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong> <?php echo Yii::app()->user->getFlash('success'); ?></strong>
                        </div>
                    <?php endif;?>
                    <div class="tab-content">
                        <div class="notification">
                        </div>
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div id="panier">
                                <?php $this->beginWidget('CActiveForm', array(
                                    'id'=> 'userInfo',
                                    'action' => $this->CreateUrl('site/index'),
                                    'enableClientValidation'=>true,
                                    'clientOptions'=>array(
                                        'validateOnSubmit'=>true,
                                    )
                                )); ?>
                                    <input type="hidden" name="name[add_information]" value="1">
                                    <input type="hidden" name="name[date_choisie]" id="date_choisie" value="" required/>
                                    <input type="hidden" name="name[depart_url]" id="depart_url" value="" />
                                    <input type="hidden" name="name[depart_geo]" id="depart_geo" value="" />
                                    <input type="hidden" name="name[arrivee_url]" id="arrivee_url" value="" />
                                    <input type="hidden" name="name[arrivee_geo]" id="arrivee_geo" value="" />
                                    <input type="hidden" id="depart_street_number" name="name[street_number_dep]" value="" />
                                    <input type="hidden" id="depart_route" name="name[route_dep]" value="" />
                                    <input type="hidden" id="depart_country" name="name[pays_dep]" value="" />
                                    <input type="hidden" id="arrivee_street_number" name="name[street_number_arr]" type="hidden" value="" />
                                    <input type="hidden" id="arrivee_route" name="name[route_arr]" value="" />
                                    <input type="hidden" id="arrivee_country" name="name[pays_arr]" value="" />
                                    <input type="hidden" id="distance" name="name[distance]" value="" />
                                    <div class="col-md-12 bg_white">
                                        <div class="col-md-12 encart_web">&nbsp;</div>
                                        <div class="form_title center padding">
                                            <h3 class="orange nopadding nomargin">Date de votre d?m?nagement</h3>
                                            <p>Choisissez la date de votre d?m?nagement, les dates vertes ?tant les plus accessibles</p>
                                        </div>
                                        <div class="step">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="resultat"></div>
                                                    <div id="calendrier" style="min-height:180px;">
                                                        <div id="owl-demo" class="owl-carousel owl-theme">
                                                            <?php
                                                            $begin =date('Y-m-d');
                                                            $end = date('Y-m-d', strtotime('+200 days'));
                                                            $days = Helpers::createDateRangeArray($begin,$end);
                                                            ?>
                                                            <?php
                                                            foreach($days as $key => $value):
                                                                ?>
                                                                <div id="col_<?=strtotime($value)?>" class="hover cal_<?=$value?> owl-item item date danger">
                                                                    <a href="#" id="" name="<?=$value?>" class="<?php if($empty):?>error<?php endif;?>">
                                                                        <div class="col-md-12 center">
                                                                            <h4 style="margin-bottom:10px;font-weight: 600;" class="gris"><?=strip_tags($this->translation['week_day_'.date('w',strtotime($value))])?></h4>
                                                                        </div>
                                                                        <div class="col-md-12 center vert <?php if((date('N',strtotime($value)) == 6) || (date('N',strtotime($value)) == 7)){echo "special-price";}?>">
                                                                            <h4 style="margin:10px;font-weight: 600;" class="chiffre"><?=date('d',strtotime($value))?></h4>
                                                                        </div>
                                                                        <div class="col-md-12 center">
                                                                            <p class="gris"><?=strip_tags($this->translation['month_'.date('m',strtotime($value))])?><br /> <?=date('Y',strtotime($value))?></p>
                                                                        </div>
                                                                    </a>
                                                                    <br clear="all" />
                                                                </div>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">&nbsp;</div>
                                    <div class="col-md-12 bg_white border_div">
                                        <div class="col-md-12 encart_web">&nbsp;</div>
                                        <div class="form_title center padding">
                                            <h3 class="orange nopadding nomargin ">Adresse de d?part</h3>
                                        </div>
                                        <div class="resultat">
                                        </div>
                                        <div class="step">
                                            <div class="row" style="margin-bottom:20px;">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <input placeholder="Adresse" type="text" name="name[adresse_depart]" id="autocomplete" class="required sm-form-control danger onFocus="geolocate()" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:20px;">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input placeholder="Code postal" type="text" class="required sm-form-control" name="name[cp_depart]" id="postal_code" value="">
                                                        <div class="reponse_cp_dep"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input placeholder="Ville" type="text" class="required sm-form-control" name="name[ville_depart]" id="locality" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:20px;">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <select name="name[type_logement_depart]" id="type_logement_depart" class="option danger required sm-form-control type_logement_depart build_select" data-id="1" rel="type_logement_depart">
                                                            <option selected="" value="999999">Type</option>
                                                            <?php
                                                            if($data['building_type']):
                                                                foreach($data['building_type'] as $key => $value):
                                                                    if($value->buildingTypeLabel):
                                                                        ?>
                                                                        <option value="<?=$value->id?>"><?=$value->buildingTypeLabel->name?></option>
                                                                        <?php
                                                                    endif;
                                                                endforeach;
                                                            endif;

                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="surface_depart_div col-md-6 col-sm-6 col-xs-6" style="display:none;">
                                                    <div class="form-group">
                                                        <input placeholder="surface m?" type="text" name="name[surface_depart]" class="surface_depart champ_depart sm-form-control" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id = "type_logement_depart_start_append">
                                            </div>

                                        </div>
                                        <div class="col-md-12 encart_web">&nbsp;</div>
                                    </div>

                                    <div class="col-md-12">&nbsp;</div>
                                    <div class="col-md-12 bg_white border_div">
                                        <div class="col-md-12 encart_web">&nbsp;</div>
                                        <div class="form_title center padding">
                                            <h3 class="orange nopadding nomargin">Adresse d'arriv?e</h3>

                                        </div>
                                        <div class="step">
                                            <div class="row" style="margin-bottom:20px;">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <input placeholder="Adresse" type="text" name="name[adresse_arrivee]" id="autocomplete_arrivee" onFocus="geolocate()"  class="required sm-form-control danger" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:20px;">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input placeholder="Code postal" type="text" class="required sm-form-control" name="name[cp_arrivee]" id="arrive_postal_code" value="">
                                                        <div class="reponse_cp_arr"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <input placeholder="Ville" type="text" class="required sm-form-control" name="name[ville_arrivee]" id="arrive_locality" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin-bottom:20px;">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <select name="name[type_logement_arrivee]" id="type_logement_arrivee" class="option danger required sm-form-control type_logement_depart build_select" data-id="2" rel="type_logement_arrivee">
                                                            <option selected="" value="999999">Type</option>
                                                            <?php
                                                            if($data['building_type']):
                                                                foreach($data['building_type'] as $key => $value):
                                                                    if($value->buildingTypeLabel):
                                                                        ?>
                                                                        <option value="<?=$value->id?>"><?=$value->buildingTypeLabel->name?></option>
                                                                        <?php
                                                                    endif;
                                                                endforeach;
                                                            endif;

                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="surface_arrivee_div col-md-6 col-sm-6 col-xs-6" style="display:none;">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Surface m?" name="name[surface_arrivee]" class="surface_arrivee champ_arrivee sm-form-control" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id = "type_logement_depart_end_append">
                                            </div>

                                        </div>

                                        <div class="col-md-12 row_alert">
                                        </div>

                                        <div class="step">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-12">&nbsp;</div>
                                                    <div id="policy">
                                                        <input type="submit" id="first_step_btn" class="bt-js-verif add-to-cart button nomargin" value="Etape suivante">
                                                    </div>
                                                    <div class="col-md-12">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $this->endWidget(); ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="inventory">
                            <div class="col-md-12 col-sm-12 col-xs-12 bg_white">
                                <div class="col-md-12 encart_web">&nbsp;</div>
                                <div class="heading-block center" style="margin:10px auto">
                                    <h1 class="orange nopadding nomargin font_20">Calculez le volume de votre d?m?nagement <br />et r?servez facilement la prestation correspondante</h1>
                                </div>
                                <div class="center">
                                    <p>1 - <b>Choisissez la quantit?</b> de meubles, appareils ?lectrom?nagers et cartons dont vous disposez.<br />
                                        2 - <b>Le volume de votre d?m?nagement s?affiche en temps r?el</b> dans la case ? Volume estim? ?<br />
                                        3 - Pour <b>r?server la prestation de d?m?nagement correspondante</b>, cliquez sur le bouton ?tape suivante</p>

                                </div>

                                <div class="col-md-3"></div>
                                <ul id="portfolio-filter" class="portfolio-filter clearfix center">
                                    <?php foreach($category as $key=>$value):?>
                                        <li class="<?php if($key == 0):?> activeFilter <?php endif;?>" ><a href="#" data-filter=".<?=$value['id'];?>"><?=$value['categoryLabels'][0]['name']?></a></li>
                                    <?php endforeach;?>
                                    <li><a href="#" data-filter=".cartons">Cartons</a></li>
                                </ul>
                                <div class="clear"></div>

                                <div id="faqs" class="faqs">

                                    <?php foreach($category as $key=>$value):?>
                                        <div class="item toggle faq <?php if($key == 0):?> activeFilter <?php endif;?> <?=$value['id'];?>" <?php if($key != 0):?>style = "display:none"<?php endif;?>>
                                            <div class="div_liste_inventaire_manuel_categorie" id="categorie_inventaire_<?=$key;?>">
                                            </div>
                                            <?php foreach($products as $k=>$v):?>
                                                <?php if($v['category']==$value['id']):?>
                                                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                                        <div class="col-md-12" style="padding:10px 0;">
                                                            <span class="item-wind" id="item_wind_<?=$v['id'];?>"></span>
                                                            <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/upload/products/<?=$v['image']?>'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                                            <div class="form-group center">
                                                                <div class="libelle_inventaire">
                                                                    <label class="label_inventaire" id="label-<?=$v['id'];?>"><?=$v['productsLabels'][0]['name']?></label>
                                                                </div>
                                                                <div class="numbers-row quantity_inventaire" id="<?=$v['id'];?>">
                                                                    <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                                    <input style="display:none;"  type="text" value="0" id="inventaire_<?=$v['id'];?>" class="qty2 form-control" name="inventaire_<?=$v['id'];?>" disabled>
                                                                    <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </div>
                                    <?php endforeach;?>
                                    <div class="item toggle faq cartons " style = "display:none">
                                        <div class="div_liste_inventaire_manuel_categorie" id="categorie_inventaire_<?=$key;?>">
                                        </div>
                                        <div class="item toggle faq cartons col-md-12" id="categorie_inventaire_4" style="display:none;">
                                            <h2>Nombre de cartons ? transporter durant votre d?m?nagement ?</h2>
                                            <p>Pour que votre inventaire soit le plus complet possible, n?oubliez pas de renseigner le nombre de cartons que vous avez ? transporter durant votre d?m?nagement. </p>
                                            <p>(Les cartons d?emballage ne sont pas inclus dans ce tarif, mais uniquement leur transport. Si vous souhaitez acheter des cartons, une option "Pack de mat?riel livr? ? domicile" est disponible ? l??tape Tarif de votre commande)</p>

                                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste no_padding_responsive">
                                                <div class="col-md-12" style="padding:10px 0;">
                                                    <span class="item-wind" id="item_wind_00"></span>
                                                    <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/47.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                                    <div class="form-group center">
                                                        <div class="libelle_inventaire">
                                                            <label class="label_inventaire">Carton</label>
                                                        </div>
                                                        <div class="numbers-row quantity_inventaire" id="00">
                                                            <input style="display:none;" type="text" value="0" id="inventaire_00" class="qty2 form-control" name="inventaire_00" disabled>
                                                            <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                            <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-6 col-sm-10 col-xs-12">
                                                <div class="col-md-12">&nbsp;</div>
                                                <p><strong>En moyenne, voici le nombre de cartons pr?par?s par nos clients selon leur surface :</strong></p>
                                                <ul class="list_ok">
                                                    <li>Une quinzaine de cartons pour un studio</li>
                                                    <li>Une trentaine de cartons pour un deux pi?ces</li>
                                                    <li>Une cinquantaine de cartons pour un trois pi?ces</li>
                                                    <li>Attention, n?oubliez pas votre cave et cagibi.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        <div role="tabpanel" class="tab-pane row" id="tarif">
                            <div class="col-md-12 col-sm-12 col-xs-12 bg_white">
                                <div class="col-md-12 encart_web">&nbsp;</div>
                                <div class="form_title center padding">
                                    <h3 class="orange nopadding nomargin">Choix de la cat?gorie de votre d?m?nagement</h3>
                                    <p> Je participe, un peu, beaucoup, ? la folie, pas du tout :)</p>
                                </div>

                                <div class="row">
                                    <?php foreach($tarifs as $key=>$value):?>
                                        <div class="col-md-4 col-sm-6 col-xs-12 tarif-block tarif-block-<?=$value['id']?>">
                                            <div class="plan pricing-box">
                                                <div class="pricing-title">
                                                    <h3 class="maj"><?= $value['tarifsLabels'][0]['name'];?></h3>
                                                    <?php if(!empty($value['tarifsLabels'][0]['comment'])):?>
                                                        <span><?= $value['tarifsLabels'][0]['comment'];?></span>
                                                    <?php endif;?>
                                                </div>
                                                <div class="pricing-price">
                                                    <b class="select-price-cat"><?=$value['price']?></b>
                                                    <span class="price-unit">&euro;</span>
                                                </div>
                                                <div class="pricing-features">
                                                    <?= $value['tarifsLabels'][0]['features_user_Comment'];?>
                                                </div>
                                                <div class="pricing-action">
                                                    <a href="#" class="select_categorie btn btn-silver btn-block btn-lg tarif" data-tarif="<?=$value['id']?>">S?lectionner cette cat?gorie</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                                <input class="hidden" id="choosen-tarif" value="" name="tarif" />
                                <a href="#" id="tarif-confirm" class="add-to-cart button">payment</a>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane row" id="payment">
                            <div class="col-md-12 bg_white">
                                <div class="col-md-12">&nbsp;</div>

                                <div class="form_title center">
                                    <h3 class="orange nopadding nomargin">Vos informations</h3>
                                    <p>&nbsp;</p>
                                </div>

                                <form method="post" action="<?=$this->CreateUrl('site/sendMail')?>" class="validation">
                                    <input type="hidden" name="add_users" value="1">

                                    <div class="row" style="margin-bottom:15px;">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <select name="statut" id="statut" class="required sm-form-control">
                                                    <option value="">Vous ?tes un</option>
                                                    <option value="0">Particulier</option>
                                                    <option value="1">Professionnel</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="Nom" type="text" name="nom" class="required sm-form-control" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="Pr?nom" type="text" class="required sm-form-control" name="prenom" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <input placeholder="Votre adresse email" type="email" class="required sm-form-control" name="email" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="T?l?phone portable" type="text" class="required sm-form-control" name="portable" data-mask="0999999999" value="" >
                                                <span class="help-block" style="font-style:italic; color:#ccc;">0612345678</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="T?l?phone secondaire" type="text" class="required sm-form-control" name="telephone" data-mask="0999999999" value="" >
                                                <span class="help-block" style="font-style:italic; color:#ccc;">0112345678</span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-12 center">
                                            <input type="submit" class="add-to-cart button" value="Proc?der au paiement">
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-3 center right_prix nopadding_responsive">
                    <div class="panel-body-">
                        <aside class="sidebar bg_rotate_prix padding_9">
                            <div class="bg_white single-product view_navigate">
                                <div class="center right_prix hidden">
                                    <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">Mon tarif</h3>
                                    <div class="prix view_navigate orange prix_total_inventaire"><br>
                                        <span id="price"></span> &#8364;
                                    </div>
                                    <br clear="all">
                                </div>
                            </div>
                            <div class="bg_white single-product">
                                <div class="center right_prix">
                                    <div id="table_liste_inventaire" >
                                        <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">My inventory</h3>
                                        <table class="table table_summary liste_inventaire">
                                            <tbody class="products"></tbody>
                                            <tr class="total hidden" id="volume-row">	<td class="left"><b class="orange">Volume estim?</b></td>	<td class="text-right"><b class="orange" id="volume"></b></td></tr>
                                            <tr class="total ">
                                                <td class="left"><b class="orange">Distance estim?</b></td>
                                                <td class="text-right"><b class="orange" id="distance-block">0</b></td>
                                            </tr>
                                    </div>
                                    </table>
                                </div>
                            </div>
                    </div>
                    </aside>
                    <a href ="#tarif" id="second-confirm" class="add-to-cart button nomargin hidden">Etape suivante <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
                </div>

            </div>
        </div>
</div>

<script src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/timepicker/bootstrap-datepicker.js"></script>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/timepicker/bootstrap-timepicker.js"></script>
<script>

    jQuery(document).ready(function($){

        var owl = $(".owl-carousel");
        owl.owlCarousel({
            items :7, //10 items above 1000px browser width
            itemsCustom : false,
            itemsDesktop : [1000,7], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,5], // 3 items betweem 900px and 601px
            itemsTablet: [600,3], //2 items between 600 and 0;
            itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
            navigation : true,
            navigationText : ["<i class=\"fa fa-chevron-left bg_bt_left\"></i>","<i class=\"fa fa-chevron-right bg_bt_right\"></i>"],
            rewindNav : true,
            scrollPerPage : false,
            pagination : false,
            dragBeforeAnimFinish :false,
            mouseDrag :true,
            touchDrag :true,
            lazyLoad : true
        });


    });

</script>
<script>
    $(document).ready(function () {

        $('.tarif').click(function (e) {
            e.preventDefault();
            var choosenTarif = $(this).data('tarif');
            $('.tarif-block').removeClass('choosen');
            $('#choosen-tarif').val(choosenTarif);
            $('.tarif-block-'+choosenTarif+'').addClass('choosen');

        });


        $('#tarif-confirm').click(function(){

            var choosenTarif = $('#choosen-tarif').val();

            $.ajax({
                type : "POST",
                url : "<?=$this->CreateUrl('site/chooseTarif')?>",
                data : "tarif="+choosenTarif,
                dataType:"JSON",
                error :function(msg){
                    alert( "Erreur  : " + msg );
                },
                success : function(result){
                    console.log(result);
                    if(result['response']){
                        $('#price').html(result.price);
                        $('.tunnel').removeClass('active');
                        $('.right_prix').removeClass('hidden');
                        $('.fourth-step').addClass('active actived');
                        $('.tab-pane').removeClass('active');
                        $('.notification').addClass('hidden');
                        $('#payment').addClass('active');
                    }else{
                        console.log(result['response']);
                        $('.notification').append('<div class="alert alert-danger calc-alert" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button><strong>Please, choose tarif !</strong></div>')
                    }
                }
            });

        });

        $('.button_inc').click(function(){

            var $button = $(this);
            //alert($button);
            //alert($button.toSource());
            var oldValue = $button.parent().find("input").val();
            //alert(oldValue);
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            $button.parent().find("input").val(newVal);


                var id = $(this).parent().attr('id').split(' ');
            if(id == '00'){
                var name = "Carton"
            }else {
                var name = $("#label-"+id).html();
            }




            var valeur = $('#inventaire_'+id).val();

            if(valeur == 0) {
                $("#item_wind_"+id).html('');
                $("#item_wind_"+id).removeClass("item-wind bg_wind").addClass("item-wind");
            }
            else {
                $("#item_wind_"+id).html(valeur);
                $("#item_wind_"+id).addClass("bg_wind");
            }

            var elem = '.' + id;

            if($('.products').children(elem).length){
                $('.product-item').each(function () {
                    if(($(this).data('product') == id) && (valeur > 0)){
                        $(this).html(' '+ name +' (x'+ valeur +')');
                    }
                    if (($(this).data('product') == id) && (valeur == 0)){
                        $(this).parent().remove();
                    }
                });
            }else{
                $('.products').append('<tr class = "items '+id+'" ><td colspan="2" data-product = '+ id +' class="product-item left"> '+ name +'  (x'+ valeur +')</td></tr>');
            }


            $.ajax({
                type : "POST",
                url : "<?=$this->CreateUrl('site/ProductsCount')?>",
                data : "id="+id+"&valeur="+valeur,
                error :function(msg){
                    alert( "Erreur  : " + msg );
                },
                success : function(result){

                    $('#volume').html(result +' m<sup>3</sup>');

                    }
            });
        });
    });
</script>
<script>

    $(document).on('change','.type_logement_depart',function(e){
        e.preventDefault();
        var option_val = $(this).val();
        var data_id = $(this).data('id');
        if(option_val == 999999){
            if(data_id == 1){
                $('#type_logement_depart_start_append').empty();
            }else{
                $('#type_logement_depart_end_append').empty();
            }
        }else{

            $.ajax({
                url:'<?=$this->CreateUrl('site/getBuildType')?>',
                data:{id:option_val},
                type:"POST",
                dataType:'JSON',
                success:function(result){
                    if(result){
                        if(data_id == 1){
                            $('#type_logement_depart_start_append').empty();
                        }else{
                            $('#type_logement_depart_end_append').empty();
                        }

                        var i = 1;
                        $.each(result, function(index,value){
                            var strVar="";
                            if(i%2 == 0) {
                                strVar += "<div class=\"row\" style=\"margin-bottom:20px;\" >";
                            }
                            strVar += "                                        <div class=\"etage_depart_div col-md-6 col-sm-6 col-xs-6\" style=\"display:block;\">";
                            strVar += "                                            <div class=\"form-group\">";
                            strVar += "                                                <select name=\"name[etage_depart][]\" id=\"\" class=\"option champ_depart sm-form-control build_select\" rel=\"etage_depart\">";
                            strVar += "                                                    <option selected=\"\" value=\""+value['id']+"\">"+value['name_label']+"<\/option>";
                            $.each(value['childs'], function(elems,item)
                            {
                                strVar += "                                                    <option value=\""+item['id']+"\">"+item['name_label']+"<\/option>";
                            });
                            strVar += "                                                <\/select>";
                            strVar += "                                            <\/div>";
                            strVar += "                                        <\/div>";
                            if(i%2 != 0){
                                strVar += "                                    <\/div>";
                            }

                            var strVarArr="";
                            if(i%2 == 0) {
                                strVarArr += "<div class=\"row\" style=\"margin-bottom:20px;\" >";
                            }
                            strVarArr += "                                        <div class=\"etage_arrive_div col-md-6 col-sm-6 col-xs-6\" style=\"display:block;\">";
                            strVarArr += "                                            <div class=\"form-group\">";
                            strVarArr += "                                                <select name=\"name[etage_arrive][]\" id=\"\" class=\"option champ_depart sm-form-control build_select\" rel=\"etage_arrive\">";
                            strVarArr += "                                                    <option selected=\"\" value=\""+value['id']+"\">"+value['name_label']+"<\/option>";
                            $.each(value['childs'], function(elems,item)
                            {
                                strVarArr += "                                                    <option value=\""+item['id']+"\">"+item['name_label']+"<\/option>";
                            });
                            strVarArr += "                                                <\/select>";
                            strVarArr += "                                            <\/div>";
                            strVarArr += "                                        <\/div>";
                            if(i%2 != 0){
                                strVarArr += "                                    <\/div>";
                            }



                            if(data_id == 1){
                                $('#type_logement_depart_start_append').append(strVar);
                            }else{
                                $('#type_logement_depart_end_append').append(strVarArr);
                            }
                            i++;
                        })
                    }
                }
            })
        }
    });



    $(document).on('click','#first_step_btn', function(e){
        e.preventDefault();

        if(lat1,lat2,lng1,lng2) {
            var destination_two_points;
            var origin1 = {lat: lat1, lng: lng1};
            var destinationB = {lat: lat2, lng: lng2};
            var x;
            var service = new google.maps.DistanceMatrixService;
            x = service.getDistanceMatrix({
                origins: [origin1],
                destinations: [destinationB],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function AR(response, status) {
                if (status !== 'OK') {
                    alert('Error was: ' + status);
                } else {
                    var originList = response.originAddresses;
                    for (var i = 0; i < originList.length; i++) {
                        var results = response.rows[i].elements;
                        for (var j = 0; j < results.length; j++) {
                            destination_two_points = results[j].distance.text;
                            $('#distance-block').html(destination_two_points);
                            $('#distance').val(destination_two_points);
                        }
                    }
                }


            });
        }

            var x = [];
            $(".build_select").each(function () {
                x.push(this.value);
            });


            $.ajax({
                url: '<?=$this->CreateUrl('site/PriceList')?>',
                data: $("#userInfo").serialize(),
                type: "POST",
                dataType: 'JSON',
                success: function (result) {

                    if (result['response'] == 'true') {
                        $('.danger').removeClass('error');
                        $('.tunnel').removeClass('active');
                        $('.second-step').addClass('active actived');
                        $('#volume-row').removeClass('hidden');
                        $('#second-confirm').removeClass('hidden');
                        $('.tab-pane').removeClass('active');
                        $('.notification').addClass('hidden');
                        $('#inventory').addClass('active');
                    } else {

                        $('.danger').addClass('error');

                        $('.notification').append('<div class="alert alert-danger calc-alert" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button><strong>'+result["error_message"]+'</strong></div>');

                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        return false;
                    }

                }
            });

    });

    $('#second-confirm').on("click", function () {

        if($('.products').children('.items').length){
            $('.calc-alert').remove();
            $('.tunnel').removeClass('active');
            $('.third-step').addClass('active actived');
            $('.tab-pane').removeClass('active');
            $('.notification').addClass('hidden');
            $('#tarif').addClass('active');
            $('#second-confirm').addClass('hidden');
        }else{
            $('.calc-alert').remove();
            $('.notification').append('<div class="alert alert-danger calc-alert" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button><strong>Vous devez faire votre inventaire !</strong></div>');
        }



    });

    $('.owl-item a').click(function(e){
        e.preventDefault();

        $('.owl-item').removeClass('error');
        var duree_select 	= $('div.class_2 .actif.btn').attr('id');
        var valeur 			= $(this).attr("name");
        var id 				= $(this).attr("id");
        var valeur_class 	= $(this).attr("clas    s");


        $(".class_2 a.btn.barre_rouge").removeAttr("class").attr("class", "btn no_actif");


        $('.info_disponibilite').hide();

        $('.select_journee_news').hide();
        $('.demi_journee_news').val('');

        //$(".encart_home_texte div.horaire a#matin").removeClass("no_actif_large actif_large").addClass("no_actif_large");
        //$(".encart_home_texte div.horaire a#aprem").removeClass("no_actif_large actif_large").addClass("no_actif_large");
        $(".selected").removeClass("selected");
        //$(".prix").css("background-color","#f3f3f3");

        $('#date_choisie').val(valeur);
        $('.date_choisie').html(valeur);

        // Si la dur??e est de 4 alors, alors on affiche le select Matin - Apres-midi //
        //alert(duree_select);

        if(duree_select == 'id_2_1') {
            //$('.cal_'+valeur+' .select_journee_news').show();
            $('.select_journee_news').show();

        }

        if (valeur_class == 'selected') {
            $('.cal_'+valeur).removeClass("selected");

        }
        else {
            $('.cal_'+valeur).addClass("selected");
        }
        return false;
    })


</script>

