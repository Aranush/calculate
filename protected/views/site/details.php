<div id="wrapper" class="clearfix">
    <section class="bg_fond_comande" id="content_">
        <div class="content-wrap">
            <div class="container clearfix nopadding_responsive">
                <br clear="all">
                <div class="col-xs-12">
                    <section class="panel form-wizard" id="w4">
                        <div class="wizard-progress wizard-progress-lg">
                            <div class="steps-progress">
                                <div class="progress-indicator"></div>
                            </div>
                            <ul class="wizard-steps">
                                <li class="active completed tunnel">
                                    <a href="<?=$this->CreateUrl('site/index')?>"><span>1</span>Client Information</a>
                                </li>
                                <li class="tunnel">
                                    <a href="<?=$this->CreateUrl('site/inventory')?>"><span>2</span>Inventory</a>
                                </li>
                                <li class=" tunnel">
                                    <a href="#" onclick="return false"><span>3</span>Payment</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <br clear="all">
            </div>
            <div class="container clearfix nopadding_responsive">



                <div class="col-md-9 commande nopadding_responsive">

                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12 bg_white border_div div_aide div_aide_1" style="display:none;">
                        <div class="col-md-12 right">
                            <a href="#" class="ferme_aide font_12 italic">fermer</a>
                        </div>
                        <div class="col-md-12">
                            <h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Nombre de d?m?nageurs</h4>
                            <p><span style="font-family: Arial; ">Un doute sur la personnalisation de votre formule? Voici quelques crit&egrave;res &agrave; prendre en consid&eacute;ration:</span></p>
                            <ul>
                                <li style="margin: 0px; font-family: Arial; ">Combien de personnes pourront pr&ecirc;ter main forte aux d&eacute;m&eacute;nageurs ?</li>
                                <li style="margin: 0px; font-family: Arial; ">Quelles sont les difficult&eacute;s (acc&egrave;s, &eacute;tage, pr&eacute;sence d&#39;un ascenseur ...) ?</li>
                                <li style="margin: 0px; font-family: Arial; ">O&ugrave; en serez-vous le jour du d&eacute;m&eacute;nagement (avancement de l&#39;emballage, d&eacute;montage/remontage &agrave; pr&eacute;voir...) ?</li>
                                <li style="margin: 0px; font-family: Arial; ">A quelles t&acirc;ches seront affect&eacute;s les d&eacute;m&eacute;nageurs (chargement, emballage, d&eacute;montage ...) ?</li>
                            </ul>
                            <p style="margin: 0px 0px 12px; font-family: Arial; ">Cas pratique: 2 d&eacute;m&eacute;nageurs sur 1/2 journ&eacute;e pour un studio de 20 &agrave; 25m<span style="font-size: 10px; "><sup>2</sup></span> peut se transformer en 1 d&eacute;m&eacute;nageur + 2 copains (selon le copain ;-) c&rsquo;est &ccedil;a le d&eacute;m&eacute;nagement participatif !</p>
                            <p style="margin: 0px 0px 12px; font-size: 10px; font-family: Arial; ">(Cas pratique &agrave; titre d&#39;exemple uniquement)</p>
                            <br clear="all">
                        </div>
                    </div>
                    <div class="col-md-12 div_aide div_aide_1" style="display:none;">&nbsp;</div>
                    <div class="col-md-12 bg_white border_div div_aide div_aide_2" style="display:none;">




                        <div class="col-md-12 right">
                            <a href="#" class="ferme_aide font_12 italic">fermer</a>
                        </div>
                        <div class="col-md-12">
                            <h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Dur?e de la prestation</h4>
                            <p>Dans un d&eacute;lai de 48h apr&egrave;s votre commande, un conseiller <strong>Des bras en plus</strong> vous contactera pour convenir avec vous de l&rsquo;horaire de votre prestation.</p>
                            <p>Besoin de plus de temps le jour du d&eacute;m&eacute;nagement ?&nbsp;Pas de probl&egrave;me, il vous est possible de prolonger la dur&eacute;e de votre prestation le jour m&ecirc;me au prix de 70 &euro; TTC par heure (non divisible et par d&eacute;m&eacute;nageur)</p>
                            <br clear="all">
                        </div>
                    </div>
                    <div class="col-md-12 div_aide div_aide_2" style="display:none;">&nbsp;</div>
                    <div class="col-md-12 bg_white border_div div_aide div_aide_3" style="display:none;">
                        <div class="col-md-12 right">
                            <a href="#" class="ferme_aide font_12 italic">fermer</a>
                        </div>
                        <div class="col-md-12">
                            <h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Taille du camion</h4>
                                <p style="text-align: justify; "><span style="font-family: Arial; ">Le camion de 22m</span><span style="font-family: Arial; font-size: 10px; "><sup>3</sup></span><span style="font-family: Arial; "> est le v&eacute;hicule le plus couramment utilis&eacute; dans le d&eacute;m&eacute;nagement. Il peut contenir en moyenne l&#39;&eacute;quivalent d&#39;un appartement de 40/45m</span><span style="font-family: Arial; font-size: 10px; "><sup>2</sup></span><span style="font-family: Arial; ">.</span></p>
                            <p style="margin: 0px 0px 12px; font-family: Arial; ">Une camionnette de 12m<span style="font-size: 10px; "><sup>3</sup></span> suffit en moyenne &agrave; transporter l&#39;&eacute;quivalent d&#39;un studio de 20/25m<span style="font-size: 10px; "><sup>2</sup></span>.</p>
                            <p style="margin: 0px 0px 12px; font-family: Arial; ">Caract&eacute;ristiques des v&eacute;hicules (donn&eacute;es &agrave; titre indicatif)</p>
                            <table border="1" cellpadding="0" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="col-md-4"><p align="center">&nbsp;</p></th>
                                    <th class="col-md-4"><p align="center">12 m<sup>3</sup></p></th>
                                    <th class="col-md-4"><p align="center">22 m<sup>3</sup></p></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="col-md-4"><p>Dimensions ext&eacute;rieures (m)</p></td>
                                    <td class="col-md-4">
                                        <p>Longueur 6,00</p>
                                        <p>Hauteur 2,80</p>
                                        <p>Largeur 2,00</p>
                                    </td>
                                    <td class="col-md-4">
                                        <p>Longueur 6,85</p>
                                        <p>Hauteur 3,40</p>
                                        <p>Largeur 2,30</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-4"><p>Dimensions int&eacute;rieures (m)</p></td>
                                    <td class="col-md-4">
                                        <p>Longueur 3,52</p>
                                        <p>Hauteur 1,90</p>
                                        <p>Largeur 1,80</p>
                                    </td>
                                    <td class="col-md-4">
                                        <p>Longueur 4,28</p>
                                        <p>Hauteur 2,20</p>
                                        <p>Largeur 2,02</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-4">
                                        <p>Visuel des v&eacute;hicules</p>
                                        <p>(photos non contractuelles)</p>
                                    </td>
                                    <td class="col-md-4">
                                        <img class="col-md-12" src="/assets/images/site/Master12m3.jpg" alt="Camion 12m3" />
                                        <p>(c) image constructeur</p>
                                    </td>
                                    <td class="col-md-4">
                                        <img class="col-md-12" alt="Camion demenagement 22m3" src="/assets/images/site/renault-master_22m3.jpg" />
                                        <p>(c) image constructeur</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <p style="text-align: justify; ">Astuce : Si vous h&eacute;sitez entre les deux tailles de camion, prenez la taille la plus grande, cela vous &eacute;vitera de perdre du temps &agrave; faire des allers-retours.</p>
                            <br clear="all">
                        </div>
                    </div>
                    <div class="col-md-12 div_aide div_aide_3" style="display:none;">&nbsp;</div>
                    <div class="col-md-12 bg_white border_div div_aide div_aide_4" style="display:none;">
                        <div class="col-md-12 right">
                            <a href="#" class="ferme_aide font_12 italic">fermer</a>
                        </div>
                        <div class="col-md-12">
                            <h4 class="gris maj border_bottom_gris"><i class="fa fa-plus-square-o"></i> Distance</h4>
                            <p> La distance vous est demand?e uniquement si vous avez besoin d'un v?hicule.</p>
                            <p>30 kilom?tres sont inclus ? votre prestation sans suppl?ment de prix. Le km suppl?mentaire est quand ? lui factur? 3? TTC .</p>
                            <p>S'il y a plusieurs adresses de chargement/livraison, indiquez la distance totale ? parcourir avec le v?hicule.</p>
                            <p>Si vous ne connaissez pas la distance entre votre adresse de d?part et d'arriv?e, n'h?sitez pas ? utiliser notre outil d'estimation de distance.</p>                                    <br clear="all">
                            <div class="col-md-12">
                                <form name="form_outils" id="form_outils" class="form_info validation form-horizontal row-border" method="post" action="#">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control required" name="cp_depart" id="cp_depart" placeholder="Code postal de d?part - ex : 75001" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control required" name="cp_arrivee" id="cp_arrivee" placeholder="Code postal d'arriv?e - ex : 13001"/>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="submit" value="ESTIMER MA DISTANCE" class="height_30 btn btn-block btn-primary">
                                    </div>
                                </form>
                                <h4 style="display:none" class="resultat_estimation_km center"></h4>
                            </div>
                            <div class="col-md-12">&nbsp;</div>
                            <br clear="all">
                        </div>
                    </div>
                    <div id="panier">
                        <form action="/" method="post" class="validation nopadding nomargin form-inventaire-details">
                            <input type="hidden" name="add_information" value="1">
                            <input type="hidden" name="date_choisie" id="date_choisie" value="" />
                            <input type="hidden" name="distance" value="" />
                            <input type="hidden" name="depart_url" id="depart_url" value="" />
                            <input type="hidden" name="depart_geo" id="depart_geo" value="" />
                            <input type="hidden" name="arrivee_url" id="arrivee_url" value="" />
                            <input type="hidden" name="arrivee_geo" id="arrivee_geo" value="" />
                            <input type="hidden" id="depart_street_number" name="street_number_dep" value="" />
                            <input type="hidden" id="depart_route" name="route_dep" value="" />
                            <input type="hidden" id="depart_country" name="pays_dep" value="" />
                            <input type="hidden" id="arrivee_street_number" name="street_number_arr" type="hidden" value="" />
                            <input type="hidden" id="arrivee_route" name="route_arr" value="" />
                            <input type="hidden" id="arrivee_country" name="pays_arr" value="" />
                            <div class="col-md-12 bg_white">
                                <div class="col-md-12 encart_web">&nbsp;</div>
                                <div class="form_title center padding">
                                    <h3 class="orange nopadding nomargin">Date de votre d?m?nagement</h3>
                                    <p>Choisissez la date de votre d?m?nagement, les dates vertes ?tant les plus accessibles</p>
                                </div>
                                <div class="step">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="resultat"></div>
                                            <div id="calendrier" style="min-height:180px;">
                                                <input type="hidden" name="date" id="date_choisie" value="" />
                                                <div id="owl-demo" class="owl-carousel owl-theme">
                                                    <?php
                                                        $begin =date('Y-m-d');
                                                        $end = date('Y-m-d', strtotime('+200 days'));
                                                        $days = Helpers::createDateRangeArray($begin,$end);
                                                    ?>

                                                    <?php

                                                        foreach($days as $key => $value):
                                                    ?>

                                                            <div id="col_<?=strtotime($value)?>" class="hover cal_<?=$value?> item date ">
                                                                <a href="#" id="" name="<?=$value?>">
                                                                    <div class="col-md-12 center">
                                                                        <h4 style="margin-bottom:10px;font-weight: 600;" class="gris"><?=strip_tags($this->translation['week_day_'.date('w',strtotime($value))])?></h4>
                                                            </div>
                                                                    <div class="col-md-12 center vert <?php if((date('N',strtotime($value)) == 6) || (date('N',strtotime($value)) == 7)){echo "special-price";}?>">
                                                                        <h4 style="margin:10px;font-weight: 600;" class="chiffre"><?=date('d',strtotime($value))?></h4>
                                                                    </div>
                                                                    <div class="col-md-12 center" style="margin-top:10px;">
                                                                        <p class="gris"><?=strip_tags($this->translation['month_'.date('m',strtotime($value))])?> <?=date('Y',strtotime($value))?></p>
                                                                    </div>
                                                                </a>
                                                                <br clear="all" />
                                                            </div>
                                                    <?php
                                                        endforeach;
                                                    ?>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">&nbsp;</div>
                            <div class="col-md-12 bg_white border_div">
                                <div class="col-md-12 encart_web">&nbsp;</div>
                                <div class="form_title center padding">
                                    <h3 class="orange nopadding nomargin ">Adresse de d?part</h3>

                                </div>
                                <div class="resultat">
                                </div>

                                <div class="step">
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <input placeholder="Adresse" type="text" name="adresse_depart" id="autocomplete" class="required sm-form-control" onFocus="geolocate()" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="Code postal" type="text" class="required sm-form-control" name="cp_depart" id="postal_code" value="">
                                                <div class="reponse_cp_dep"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="Ville" type="text" class="required sm-form-control" name="ville_depart" id="locality" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <select name="type_logement_depart" id="type_logement_depart" class="option required sm-form-control type_logement_depart build_select" data-id="1" rel="type_logement_depart">
                                                    <option selected="" value="999999">Type</option>
                                                    <?php
                                                        if($data['building_type']):
                                                            foreach($data['building_type'] as $key => $value):
                                                                if($value->buildingTypeLabel):
                                                    ?>
                                                                <option value="<?=$value->id?>"><?=$value->buildingTypeLabel->name?></option>
                                                    <?php
                                                                endif;
                                                            endforeach;
                                                        endif;

                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="surface_depart_div col-md-6 col-sm-6 col-xs-6" style="display:none;">
                                            <div class="form-group">
                                                <input placeholder="surface m?" type="text" name="surface_depart" class="surface_depart champ_depart sm-form-control" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div id = "type_logement_depart_start_append">
                                    </div>

                                </div>
                                <div class="col-md-12 encart_web">&nbsp;</div>
                            </div>

                            <div class="col-md-12">&nbsp;</div>
                            <div class="col-md-12 bg_white border_div">
                                <div class="col-md-12 encart_web">&nbsp;</div>
                                <div class="form_title center padding">
                                    <h3 class="orange nopadding nomargin">Adresse d'arriv?e</h3>

                                </div>
                                <div class="step">
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <input placeholder="Adresse" type="text" name="adresse_arrivee" id="autocomplete_arrivee" onFocus="geolocate()"  class="required sm-form-control" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="Code postal" type="text" class="required sm-form-control" name="cp_arrivee" id="arrive_postal_code" value="">
                                                <div class="reponse_cp_arr"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input placeholder="Ville" type="text" class="required sm-form-control" name="ville_arrivee" id="arrive_locality" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <select name="type_logement_arrivee" id="type_logement_arrivee" class="option required sm-form-control type_logement_depart build_select" data-id="2" rel="type_logement_arrivee">
                                                    <option selected="" value="999999">Type</option>
                                                    <?php
                                                    if($data['building_type']):
                                                        foreach($data['building_type'] as $key => $value):
                                                            if($value->buildingTypeLabel):
                                                                ?>
                                                                <option value="<?=$value->id?>"><?=$value->buildingTypeLabel->name?></option>
                                                                <?php
                                                            endif;
                                                        endforeach;
                                                    endif;

                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="surface_arrivee_div col-md-6 col-sm-6 col-xs-6" style="display:none;">
                                            <div class="form-group">
                                                <input type="text" placeholder="Surface m?" name="surface_arrivee" class="surface_arrivee champ_arrivee sm-form-control" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div id = "type_logement_depart_end_append">
                                    </div>

                                </div>

                                <div class="col-md-12 row_alert">
                                </div>

                                <div class="step">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">&nbsp;</div>
                                            <div id="policy">
                                                <input type="submit" id="first_step_btn" class="bt-js-verif add-to-cart button nomargin" value="Etape suivante">
                                            </div>
                                            <div class="col-md-12">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 center right_prix nopadding_responsive">
                    <div class="panel-body-">
                        <aside class="sidebar bg_rotate_prix padding_9">
                            <div class="col-md-12">&nbsp;</div>
                            <br clear="all">
                            <div class="bg_white single-product">
                                <div class="center right_prix">
                                    <div id="table_liste_inventaire" >
                                        <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">My inventory</h3>
                                        <table class="table table_summary liste_inventaire">
                                            <tbody><tr>	<td colspan="2" class="left">Lit simple (x1)</td></tr><tr>	<td colspan="2" class="left">Lit double (x1)</td></tr><tr class="total">	<td class="left"><b class="orange">Volume estim?</b></td>	<td class="text-right"><b class="orange">4.50m<sup>3</sup></b></td></tr></tbody>                                                <tr class="total ">
                                                <td class="left"><b class="orange">Distance estim?</b></td>
                                                <td class="text-right"><b class="orange" id="distance-block">0 km</b></td>
                                            </tr>
                                    </div>


                                    </table>
                                </div>
                            </div>
                    </div>




                    </aside>
                </div>
            </div>
        </div>
</div>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/timepicker/bootstrap-datepicker.js"></script>
<script src="<?=Yii::app()->request->BaseUrl?>/vendor/plugins/timepicker/bootstrap-timepicker.js"></script>
<script>

    jQuery(document).ready(function($){
        var owl = $(".owl-carousel");
        console.log(owl);
        owl.owlCarousel({
            items :7, //10 items above 1000px browser width
            itemsCustom : false,
            itemsDesktop : [1000,7], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,5], // 3 items betweem 900px and 601px
            itemsTablet: [600,3], //2 items between 600 and 0;
            itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
            navigation : true,
            navigationText : ["<i class=\"fa fa-chevron-left bg_bt_left\"></i>","<i class=\"fa fa-chevron-right bg_bt_right\"></i>"],
            rewindNav : true,
            scrollPerPage : false,
            pagination : false,
            dragBeforeAnimFinish :false,
            mouseDrag :true,
            touchDrag :true,
            lazyLoad : true
        });


    });

</script>
<script>

    $(document).on('change','.type_logement_depart',function(e){
        e.preventDefault();
        var option_val = $(this).val();
        var data_id = $(this).data('id');
        if(option_val == 999999){
            if(data_id == 1){
                $('#type_logement_depart_start_append').empty();
            }else{
                $('#type_logement_depart_end_append').empty();
            }
        }else{

            $.ajax({
                url:'<?=$this->CreateUrl('site/getBuildType')?>',
                data:{id:option_val},
                type:"POST",
                dataType:'JSON',
                success:function(result){
                    if(result){
                        if(data_id == 1){
                            $('#type_logement_depart_start_append').empty();
                        }else{
                            $('#type_logement_depart_end_append').empty();
                        }
                        var i = 1;
                        $.each(result, function(index,value){
                            var strVar="";
                            if(i%2 == 0) {
                                strVar += "<div class=\"row\" style=\"margin-bottom:20px;\" >";
                            }
                            strVar += "                                        <div class=\"etage_depart_div col-md-6 col-sm-6 col-xs-6\" style=\"display:block;\">";
                            strVar += "                                            <div class=\"form-group\">";
                            strVar += "                                                <select name=\"etage_depart\" id=\"\" class=\"option champ_depart sm-form-control build_select\" rel=\"etage_depart\">";
                            strVar += "                                                    <option selected=\"\" value=\""+value['id']+"\">"+value['name_label']+"<\/option>";
                            $.each(value['childs'], function(elems,item)
                            {
                                strVar += "                                                    <option value=\""+item['id']+"\">"+item['name_label']+"<\/option>";
                            });
                            strVar += "                                                <\/select>";
                            strVar += "                                            <\/div>";
                            strVar += "                                        <\/div>";
                            if(i%2 != 0){
                                strVar += "                                    <\/div>";
                            }
                            if(data_id == 1){
                                $('#type_logement_depart_start_append').append(strVar);
                            }else{
                                $('#type_logement_depart_end_append').append(strVar);
                            }
                            i++;
                        })
                    }
                }
            })
        }
    });


    $(document).on('click','#first_step_btn', function(e){
        e.preventDefault();
        var destination_two_points;
        var origin1 = {lat: lat1, lng: lng1};
        var destinationB = {lat: lat2, lng: lng2};
        var x;
        var service = new google.maps.DistanceMatrixService;
        x = service.getDistanceMatrix({
            origins: [origin1],
            destinations: [destinationB],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function AR(response, status) {
            if (status !== 'OK') {
                alert('Error was: ' + status);
            } else {
                var originList = response.originAddresses;
                for (var i = 0; i < originList.length; i++) {
                    var results = response.rows[i].elements;
                    for (var j = 0; j < results.length; j++) {
                       destination_two_points = results[j].distance.value;
                    }
                }
            }


        });


        console.log(service.getDistanceMatrix.AR());

            var x = [];
            $(".build_select").each(function(){
                x.push(this.value);
            });


            $.ajax({
                url:'<?=$this->CreateUrl('site/PriceList')?>',
                data:{building:x,destination_two_points_mets:destination_two_points},
                type:"POST",
                success:function(result){

                }

            })
    })


    $('.owl-item a').click(function(){


        var duree_select 	= $('div.class_2 .actif.btn').attr('id');
        var valeur 			= $(this).attr("name");
        var id 				= $(this).attr("id");
        var valeur_class 	= $(this).attr("class");



        $(".class_2 a.btn.barre_rouge").removeAttr("class").attr("class", "btn no_actif");


        $('.info_disponibilite').hide();

        $('.select_journee_news').hide();
        $('.demi_journee_news').val('');

        //$(".encart_home_texte div.horaire a#matin").removeClass("no_actif_large actif_large").addClass("no_actif_large");
        //$(".encart_home_texte div.horaire a#aprem").removeClass("no_actif_large actif_large").addClass("no_actif_large");
        $(".selected").removeClass("selected");
        //$(".prix").css("background-color","#f3f3f3");

        $('#date_choisie').val(valeur);
        $('.date_choisie').html(valeur);

        // Si la dur??e est de 4 alors, alors on affiche le select Matin - Apres-midi //
        //alert(duree_select);

        if(duree_select == 'id_2_1') {
            //$('.cal_'+valeur+' .select_journee_news').show();
            $('.select_journee_news').show();

        }

        if (valeur_class == 'selected') {
            $('.cal_'+valeur).removeClass("selected");

        }
        else {
            $('.cal_'+valeur).addClass("selected");






            //$.ajax({
            //	type : "POST",
            //	url : "/includes/cal/select-date.php",
            //	data : "valeur="+valeur,
            //	//send : jQuery('.affiche_result').html('<img src="../images/ajax-loader.gif" alt="Chargement" style="margin:0 auto; display:block;" />'),
            //	error :function(msg){
            //		alert( "Erreur  : " + msg );
            //	},
            //	success : function(data){
            //		//$('input#date_dem').val(obj.valeur);
            //		var obj = jQuery.parseJSON(data);
            //		if(obj.result == 1) {
            //			$('.date_select_up').html('<b> : '+obj.date_select_up+'</b>');
            //		}
            //	}
            //});
            //
            //var info_demenageur		= $('.class_1 a.no_actif actif').attr('id');
            //var info_click 			= $('.class_3 a.actif').attr('id');
            //var horaire_select 		= $('.class_2 a.actif').attr('id');
            //var produit_select 		= $('.class_3 a.actif').attr('id');
            //
            //if(horaire_select) {
            //	var horaire_select = horaire_select;
            //}
            //else {
            //	var horaire_select = '';
            //}
            //
            ///*
            //$.ajax({
            //	type : "POST",
            //	url : "/includes/cal/select-date-disponibilite.php",
            //	data : "valeur="+valeur+"&horaire_select="+horaire_select+"&produit_select="+produit_select+"&info_click="+info_click+"&info_demenageur="+info_demenageur,
            //	error :function(msg){
            //		alert( "Erreur  : " + msg );
            //	},
            //	success : function(data){
            //		//alert(data);
            //		$('.reponse_verif').html(data);
            //	}
            //});
            //*/
            //
            //$.ajax({
            //	type : "POST",
            //	url : "/includes/load/update_prix_date.php",
            //	data : "valeur="+valeur,
            //	error :function(msg){
            //		alert( "Erreur  : " + msg );
            //	},
            //	success : function(data){
            //		var obj = jQuery.parseJSON(data);
            //		if(obj.result == 1) {
            //			$('.prix').html(obj.prix_ttc)
            //			//$('.date_select_up').html('<b> : '+obj.date_select_up+'</b>');
            //		}
            //		else {}
            //	}
            //});
            //
            //var produit_select 		= $('.class_3 a.no_actif actif').attr('id');
            //$.ajax({
            //	type : "POST",
            //	url : "/includes/load/add_panier_produit.php",
            //	data : "id="+produit_select,
            //	error :function(msg){
            //		alert( "Erreur  : " + msg );
            //	},
            //	success : function(data){
            //		//$(".prix").css("background-color","#fc6720");
            //		var obj = jQuery.parseJSON(data);
            //		if(obj.result == 1) {
            //			$('.prix').html(obj.prix_ttc)
            //		}
            //		else {
            //			alert('Une erreur est survene ! [jcalendat.js - 140]');
            //		}
            //
            //
            //		//$(".prix").load('/includes/load/prix.php');
            //	}
            //});


        }
        //$.fancybox.hideLoading();
        return false;
    })


</script>

