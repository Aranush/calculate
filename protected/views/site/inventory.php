<div class="container clearfix">
                <br clear="all">
                <div class="col-xs-12">
                    <section class="panel form-wizard" id="w4">
                        <div class="wizard-progress wizard-progress-lg">
                            <div class="steps-progress">
                                <div class="progress-indicator"></div>
                            </div>
                            <ul class="wizard-steps">
                                <li class="active completed tunnel">
                                    <a href="<?=$this->CreateUrl('site/index')?>"><span>1</span>Client Information</a>
                                </li>
                                <li class="active tunnel">
                                    <a href="<?=$this->CreateUrl('site/inventory')?>"><span>2</span>Inventory</a>
                                </li>
                                <li class="tunnel">
                                    <a href="#" onclick="return false"><span>3</span>Payment</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <br clear="all">
            </div>

            <div class="container clearfix nopadding_responsive">
                <div class="col-md-9 nopadding_responsive">
                    <div class="col-md-12 col-sm-12 col-xs-12 bg_white">

                        <div class="col-md-12 encart_web">&nbsp;</div>
                        <div class="heading-block center" style="margin:10px auto">
                            <h1 class="orange nopadding nomargin font_20">Calculez le volume de votre d?m?nagement <br />et r?servez facilement la prestation correspondante</h1>
                        </div>
                        <div class="center">
                            <p>1 - <b>Choisissez la quantit?</b> de meubles, appareils ?lectrom?nagers et cartons dont vous disposez.<br />
                                2 - <b>Le volume de votre d?m?nagement s?affiche en temps r?el</b> dans la case ? Volume estim? ?<br />
                                3 - Pour <b>r?server la prestation de d?m?nagement correspondante</b>, cliquez sur le bouton ?tape suivante</p>

                        </div>

                        <div class="col-md-3"></div>
                        <ul id="portfolio-filter" class="portfolio-filter clearfix center">
                            <li class="activeFilter"><a href="#" data-filter=".meubles">Mobilier</a></li>
                            <li ><a href="#" data-filter=".electromenager">Appareils</a></li>
                            <li ><a href="#" data-filter=".autres">Divers</a></li>
                            <li ><a href="#" data-filter=".cartons">Cartons</a></li>
                        </ul>
                        <div class="clear"></div>

                        <div id="faqs" class="faqs">
                            <div class="item toggle faq meubles activeFilter">
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_9999999999"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/element-plus.jpg'); background-position:center top; background-repeat:no-repeat;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Nouvel objet</label>
                                            </div>
                                            <a class="bt-add-inventaire-manu add-to-cart button_add button nomargin" data-lightbox="inline" href="#add_inventaire" id="1">Ajouter</a>

                                        </div>
                                    </div>
                                </div>
                                <div class="div_liste_inventaire_manuel_categorie" id="categorie_inventaire_1">
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_1"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/1.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Lit simple</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="1">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_1" class="qty2 form-control" name="inventaire_1" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_2"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/2.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Lit double</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="2">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_2" class="qty2 form-control" name="inventaire_2" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_3"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/3.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Lit superpos?</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="3">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_3" class="qty2 form-control" name="inventaire_3" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_4"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/4.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Lit b?b?</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="4">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_4" class="qty2 form-control" name="inventaire_4" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_5"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/5.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Canap? 2 places</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="5">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_5" class="qty2 form-control" name="inventaire_5" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_6"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/6.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Canap? 3 places</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="6">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_6" class="qty2 form-control" name="inventaire_6" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_7"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/7.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Canap? d'angle</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="7">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_7" class="qty2 form-control" name="inventaire_7" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_8"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/8.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Fauteuil</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="8">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_8" class="qty2 form-control" name="inventaire_8" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_9"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/9.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Chaise</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="9">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_9" class="qty2 form-control" name="inventaire_9" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_10"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/10.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Luminaire</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="10">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_10" class="qty2 form-control" name="inventaire_10" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_11"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/11.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Petite table</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="11">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_11" class="qty2 form-control" name="inventaire_11" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_12"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/12.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Moyenne table</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="12">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_12" class="qty2 form-control" name="inventaire_12" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_13"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/13.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Grande table</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="13">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_13" class="qty2 form-control" name="inventaire_13" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_14"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/14.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Table basse</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="14">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_14" class="qty2 form-control" name="inventaire_14" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_15"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/15.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Biblioth?que</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="15">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_15" class="qty2 form-control" name="inventaire_15" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_16"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/16.jpg'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Bureau</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="16">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_16" class="qty2 form-control" name="inventaire_16" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_17"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/17.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Placard 1 porte</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="17">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_17" class="qty2 form-control" name="inventaire_17" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_18"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/18.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Placard 2 portes</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="18">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_18" class="qty2 form-control" name="inventaire_18" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_19"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/19.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Placard 3 portes</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="19">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_19" class="qty2 form-control" name="inventaire_19" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_20"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/20.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Armoire ancienne</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="20">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_20" class="qty2 form-control" name="inventaire_20" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_21"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/21.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Placard cuisine</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="21">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_21" class="qty2 form-control" name="inventaire_21" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_22"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/22.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Buffet</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="22">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_22" class="qty2 form-control" name="inventaire_22" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_23"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/23.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Vaisselier</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="23">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_23" class="qty2 form-control" name="inventaire_23" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_24"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/24.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Commode</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="24">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_24" class="qty2 form-control" name="inventaire_24" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_25"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/25.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Meuble TV bas</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="25">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_25" class="qty2 form-control" name="inventaire_25" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_26"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/26.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Meuble TV combin?</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="26">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_26" class="qty2 form-control" name="inventaire_26" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_27"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/27.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Bo?te/panier</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="27">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_27" class="qty2 form-control" name="inventaire_27" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_28"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/28.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Coffre ? linge</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="28">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_28" class="qty2 form-control" name="inventaire_28" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_29"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/29.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Banc</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="29">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_29" class="qty2 form-control" name="inventaire_29" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item toggle faq electromenager "style="display:none;">
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_9999999999"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/element-plus.jpg'); background-position:center top; background-repeat:no-repeat;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Nouvel objet</label>
                                            </div>
                                            <a class="bt-add-inventaire-manu add-to-cart button_add button nomargin" data-lightbox="inline" href="#add_inventaire" id="2">Ajouter</a>

                                        </div>
                                    </div>
                                </div>
                                <div class="div_liste_inventaire_manuel_categorie" id="categorie_inventaire_2">
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_30"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/30.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Cong?lateur bac</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="30">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_30" class="qty2 form-control" name="inventaire_30" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_31"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/31.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Frigo-cong?lateur</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="31">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_31" class="qty2 form-control" name="inventaire_31" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_33"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/33.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Frigo simple</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="33">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_33" class="qty2 form-control" name="inventaire_33" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_34"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/34.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Frigo am?ricain</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="34">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_34" class="qty2 form-control" name="inventaire_34" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_35"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/35.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Gazini?re</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="35">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_35" class="qty2 form-control" name="inventaire_35" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_36"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/36.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Lave vaisselle</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="36">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_36" class="qty2 form-control" name="inventaire_36" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_37"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/37.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Lave linge</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="37">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_37" class="qty2 form-control" name="inventaire_37" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_38"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/38.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">S?che linge</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="38">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_38" class="qty2 form-control" name="inventaire_38" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_39"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/39.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">T?l?vision</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="39">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_39" class="qty2 form-control" name="inventaire_39" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item toggle faq autres "style="display:none;">
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_9999999999"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/element-plus.jpg'); background-position:center top; background-repeat:no-repeat;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Nouvel objet</label>
                                            </div>
                                            <a class="bt-add-inventaire-manu add-to-cart button_add button nomargin" data-lightbox="inline" href="#add_inventaire" id="3">Ajouter</a>

                                        </div>
                                    </div>
                                </div>
                                <div class="div_liste_inventaire_manuel_categorie" id="categorie_inventaire_3">
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_40"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/40.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Cave ? vin</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="40">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_40" class="qty2 form-control" name="inventaire_40" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_41"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/41.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Poussette</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="41">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_41" class="qty2 form-control" name="inventaire_41" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_42"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/42.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">V?lo d'int?rieur</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="42">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_42" class="qty2 form-control" name="inventaire_42" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_43"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/43.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">V?lo</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="43">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_43" class="qty2 form-control" name="inventaire_43" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_44"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/44.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Plante en pot</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="44">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_44" class="qty2 form-control" name="inventaire_44" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_45"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/45.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Barbecue</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="45">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_45" class="qty2 form-control" name="inventaire_45" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_77"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/77.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Piano droit</label>
                                            </div>

                                            <div class="numbers-row quantity_inventaire" id="77">
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <input style="display:none;"  type="text" value="0" id="inventaire_77" class="qty2 form-control" name="inventaire_77" disabled>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item toggle faq cartons col-md-12" id="categorie_inventaire_4" style="display:none;">
                                <h2>Nombre de cartons ? transporter durant votre d?m?nagement ?</h2>
                                <p>Pour que votre inventaire soit le plus complet possible, n?oubliez pas de renseigner le nombre de cartons que vous avez ? transporter durant votre d?m?nagement. </p>
                                <p>(Les cartons d?emballage ne sont pas inclus dans ce tarif, mais uniquement leur transport. Si vous souhaitez acheter des cartons, une option "Pack de mat?riel livr? ? domicile" est disponible ? l??tape Tarif de votre commande)</p>

                                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 liste no_padding_responsive">
                                    <div class="col-md-12" style="padding:10px 0;">
                                        <span class="item-wind" id="item_wind_47"></span>
                                        <span class="item-icon" style="background-image:url('<?=Yii::app()->request->BaseUrl?>/vendor/assets/images/inventaire/47.png'); background-position:center top; background-repeat:no-repeat; background-size:cover;"></span>
                                        <div class="form-group center">
                                            <div class="libelle_inventaire">
                                                <label class="label_inventaire">Carton</label>
                                            </div>
                                            <div class="numbers-row quantity_inventaire" id="47">
                                                <input style="display:none;" type="text" value="0" id="inventaire_47" class="qty2 form-control" name="inventaire_47" disabled>
                                                <div class="dec button_inc plus_moins minus col-md-6">-</div>
                                                <div class="inc button_inc plus_moins plus col-md-6">+</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-6 col-sm-10 col-xs-12">
                                    <div class="col-md-12">&nbsp;</div>
                                    <p><strong>En moyenne, voici le nombre de cartons pr?par?s par nos clients selon leur surface :</strong></p>
                                    <ul class="list_ok">
                                        <li>Une quinzaine de cartons pour un studio</li>
                                        <li>Une trentaine de cartons pour un deux pi?ces</li>
                                        <li>Une cinquantaine de cartons pour un trois pi?ces</li>
                                        <li>Attention, n?oubliez pas votre cave et cagibi.</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="col-md-3 col-sm-12 col-xs-12 center right_prix">
                    <div class="panel-body-">
                        <aside class="sidebar bg_rotate_prix padding_9">

                            <div class="col-md-12 right_prix">&nbsp;</div>
                            <br clear="all" class="right_prix">
                            <div class="bg_white single-product">
                                <div class="center right_prix">
                                    <div id="table_liste_inventaire" style="display:none;">
                                        <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">Mon inventaire</h3>
                                        <table class="table table_summary liste_inventaire">
                                            <tbody><tr class="total">	<td class="left"><b class="orange">Volume estim?</b></td>	<td class="text-right"><b class="orange">0m<sup>3</sup></b></td></tr></tbody>											</table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">&nbsp;</div>
                            <br clear="all">
                            <div class="bg_white single-product">
                                <div class="center right_prix">
                                    <div id="table_liste_inventaire" >
                                        <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">My inventory</h3>
                                        <table class="table table_summary liste_inventaire">
                                            <tbody><tr>	<td colspan="2" class="left">Lit simple (x1)</td></tr><tr>	<td colspan="2" class="left">Lit double (x1)</td></tr><tr class="total">	<td class="left"><b class="orange">Volume estim?</b></td>	<td class="text-right"><b class="orange">4.50m<sup>3</sup></b></td></tr></tbody>                                                <tr class="total ">
                                                <td class="left"><b class="orange">Distance estim?</b></td>
                                                <td class="text-right"><b class="orange" id="distance-block">0 km</b></td>
                                            </tr>
                                    </div>


                                    </table>
                                </div>
                            </div>

                        </aside>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12">
                    <p>La r?servation d?une prestation de d?m?nagement directement en ligne est encore plus facile aujourd?hui gr?ce au calculateur de volume de d?m?nagement Des bras en plus.</p>
                    <p>Le calcul de volume cr?? par le ? Calculateur de volume Des bras en plus ? est donn? ? titre indicatif. Nous am?liorons sans cesse notre intelligence artificielle qui est le fruit de l?analyse de plusieurs dizaines de milliers de prestation. Notre objectif est de rendre le cubage de votre d?m?nagement facile, mais surtout de vous permettre de r?server facilement la prestation de d?m?nagement correspondante. Bien que le calcul de votre volume devrait ?tre relativement proche du r?el, nous vous conseillons de pr?voir large afin de palier ? un ?ventuel impr?vu.</p>
                </div>
            </div>



    <div class="modal1 mfp-hide" id="add_inventaire">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px;">
            <div class="center" style="padding:20px;">
                <h3>Personnalisez votre meuble</h3>
                <div id="message-review">
                    <p>Votre meuble ne figure pas dans la liste, pas de probl?me. Cr?ez le en remplissant les informations suivantes</p>
                </div>

                <div class="style-msg errormsg" id="pass-info" style="display:none;">
                    <div class="sb-msg">Votre objet doit faire au minimum 0,001m<sup>3</sup></div>
                </div>


                <form method="post" action="#" name="add_inventaire" id="form_add_inventaire_manuel" class="validation" style="margin-bottom:10px;">
                    <input type="hidden" name="id_categorie" id="id_categorie_add_inventaire" value="0" />
                    <input type="hidden" name="m3" id="m3" value="0" />
                    <div class="row" style="margin-bottom:10px;">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <label>Hauteur (en cm)</label>
                                <input maxlength="9" name="hauteur" id="hauteur" type="text" placeholder="0" class="required number form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <label>Largeur (en cm)</label>
                                <input maxlength="9" name="largeur" id="largeur" type="text" placeholder="0" class="required number form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom:10px;">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <label>Profondeur (en cm)</label>
                                <input maxlength="9" name="profondeur" id="profondeur" type="text" placeholder="0" class="required number form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <label>Nom de votre objet</label>
                                <input name="nom" id="libelle" type="text" placeholder="" class="required form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Enregistrer" class="add-to-cart button nomargin bt_add_inventaire" id="submit-review" style="display:none;">
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <h4 style="padding-bottom:10px; margin-bottom:0px;"><b>Volume : <span class="result_volume">0,00</span>m<sup>3</sup></b></h4>
                    </div>
                </div>

            </div>
            <div class="section center nomargin" style="padding:10px;">
                <a href="#" class="button" onClick="$.magnificPopup.close();return false;">Fermer la fen?tre</a>
            </div>
        </div>
    </div>


    <script type="text/javascript" charset="utf-8">var ju_num="AA736390-0E3A-4FBD-8050-5FBB98799D92";var asset_host=(("https:"==document.location.protocol)?"https":"http")+'://d2j3qa5nc37287.cloudfront.net/';(function() {var s=document.createElement('script');s.type='text/javascript';s.async=true;s.src=asset_host+'coupon_code1.js';var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);})();</script>
