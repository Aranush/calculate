
            <div class="container clearfix">
                <br clear="all">
                <div class="col-xs-12">
                    <section class="panel form-wizard" id="w4">
                        <div class="wizard-progress wizard-progress-lg">
                            <div class="steps-progress">
                                <div class="progress-indicator"></div>
                            </div>
                            <ul class="wizard-steps">
                                <li class="completed tunnel">
                                    <a href="inventaire"><span>1</span>Inventaire</a>
                                </li>
                                <li class="completed tunnel">
                                    <a href="inventaire-details"><span>2</span>Information Client</a>
                                </li>
                                <li class="completed tunnel">
                                    <a href="#" onclick="return false"><span>3</span>Tarif</a>
                                </li>
                                <li class="active tunnel">
                                    <a href="#" onclick="return false"><span>4</span>Paiement</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <br clear="all">
            </div>

            <div class="container clearfix nopadding_responsive">
                <div class="col-md-9 nopadding_responsive">
                    <div class="col-md-12 bg_white">
                        <div class="col-md-12">&nbsp;</div>

                        <div class="form_title center">
                            <h3 class="orange nopadding nomargin">Vos informations</h3>
                            <p>&nbsp;</p>
                        </div>

                        <form method="post" action="/commande/inventaire-paiement" class="validation">
                            <input type="hidden" name="add_users" value="1">

                            <div class="row" style="margin-bottom:15px;">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select name="statut" id="statut" class="required sm-form-control">
                                            <option value="">Vous ?tes un</option>
                                            <option value="0">Particulier</option>
                                            <option value="1">Professionnel</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>


                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input placeholder="Nom" type="text" name="nom" class="required sm-form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input placeholder="Pr?nom" type="text" class="required sm-form-control" name="prenom" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input placeholder="Votre adresse email" type="text" class="required sm-form-control" name="email" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input placeholder="T?l?phone portable" type="text" class="required sm-form-control" name="portable" data-mask="0999999999" value="" >
                                        <span class="help-block" style="font-style:italic; color:#ccc;">0612345678</span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input placeholder="T?l?phone secondaire" type="text" class="required sm-form-control" name="telephone" data-mask="0999999999" value="" >
                                        <span class="help-block" style="font-style:italic; color:#ccc;">0112345678</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-md-12 center">
                                    <input type="submit" class="add-to-cart button" value="Proc?der au paiement">
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12 bg_white">
                    </div>
                    <br clear="all">
                </div>

                <div class="col-md-3 center right_prix nopadding_responsive">
                    <div class="panel-body-">
                        <aside class="sidebar bg_rotate_prix padding_9">
                            <div class="bg_white single-product view_navigate">
                                <div class="center right_prix">
                                    <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">Mon tarif</h3>
                                    <div class="prix view_navigate orange prix_total_inventaire"><br>
                                        80?                                        </div>
                                    <br clear="all">
                                </div>
                            </div>
                            <br clear="all">
                            <div class="bg_white single-product">
                                <div class="center right_prix">
                                    <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">Informations</h3>
                                    <table class="table table_summary">
                                        <tbody>
                                        <tr>
                                            <td>Distance</td>
                                            <td class="text-right" id="distanceKm">
                                                0km
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date</td>
                                            <td class="text-right" id="date_right">25-10-2016</td>
                                        </tr>



                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <br clear="all">

                            <div class="bg_white single-product">
                                <div class="center right_prix">
                                    <div id="table_liste_inventaire" >
                                        <h3 class="border_bottom_orange_1 left" style="margin-left:20px;">Mon inventaire</h3>
                                        <table class="table table_summary liste_inventaire">
                                            <tbody><tr>	<td colspan="2" class="left"> (x1)</td></tr><tr>	<td colspan="2" class="left">Lit simple (x1)</td></tr><tr class="total">	<td class="left"><b class="orange">Volume estim?</b></td>	<td class="text-right"><b class="orange">2.00m<sup>3</sup></b></td></tr></tbody>											</table>
                                    </div>
                                </div>
                            </div>

                            <br clear="all">
                            <div class="border_div bg_white single-product">
                                <div class="center right_prix">
                                    <div class="single-product">
                                        <div class="col-md-12">&nbsp;</div>
                                        <div class="col-md-12">
                                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                                <div class="col-md-2 col-xs-2">
                                                    <div class="fbox-icon">
                                                        <i class="fa fa-question-circle-o"></i>
                                                    </div>
                                                    <br clear="all" />
                                                </div>
                                                <div class="col-md-10 col-xs-10">
                                                    <h3>Une question? </h3>
                                                    <p class="nomargin nopadding">09.72.47.50.47</p>
                                                </div>
                                            </div>
                                            <br clear="all" />
                                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                                <div class="col-md-2 col-xs-2">
                                                    <div class="fbox-icon">
                                                        <i class="fa fa-thumbs-up"></i>
                                                    </div>
                                                    <br clear="all" />
                                                </div>
                                                <div class="col-md-10 col-xs-10">
                                                    <h3>Paiement 100% s?curis?</h3>
                                                </div>
                                            </div>
                                            <br clear="all" />
                                            <div class="feature-box fbox-plain fbox-dark fbox-small">
                                                <div class="col-md-2 col-xs-2">
                                                    <div class="fbox-icon">
                                                        <i class="fa fa-undo"></i>
                                                    </div>
                                                    <br clear="all" />
                                                </div>
                                                <div class="col-md-10 col-xs-10">
                                                    <h3>ANNULATION POSSIBLE</h3>
                                                </div>
                                            </div>
                                            <br clear="all" />

                                        </div>
                                        <br clear="all">
                                    </div>
                                </div>
                            </div>



                        </aside>
                    </div>
                </div>
            </div>
