// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete, autocomplete_arrive;
var componentForm = {
    locality: 'long_name',
    postal_code: 'short_name'
};
var componentFormArrive = {
    arrive_locality: 'long_name',
    arrive_postal_code: 'short_name'
};
var lat1;
var lat2;
var lng1;
var lng2;
function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete')),
        {
            types: ['geocode'],
            componentRestrictions: {country: 'fr'}
        });
    autocomplete_arrive = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete_arrivee')),
        {
            types: ['geocode'],
            componentRestrictions: {country: 'fr'}
        });

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    autocomplete_arrive.addListener('place_changed', fillInAddressArrive);

}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }
    lat1 = place.geometry.location.lat();
    lng1 = place.geometry.location.lng();


    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
}
function fillInAddressArrive() {
    // Get the place details from the autocomplete object.
    var place = autocomplete_arrive.getPlace();

    for (var component in componentFormArrive) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    lat2 = place.geometry.location.lat();
    lng2 = place.geometry.location.lng();

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentFormArrive['arrive_'+addressType]) {
            var val = place.address_components[i][componentFormArrive['arrive_'+addressType]];

            document.getElementById('arrive_'+addressType).value = val;
        }
    }
}

$(document).ready(function(){
    $(document).on('click','.plus_moins_basket',function(){
        var value = $(this).val();
        var parent = $(this).parent('.quantity');
        //console.log(parent.find('input-text-basket'));
        var value_input = parent.find('.input-text-basket');
        var input_value = parseInt(parent.find('.input-text-basket').val());
        if(value == '+'){
            $(value_input).val(input_value+1);
        }else{
            if(input_value != 1){
                $(value_input).val(input_value-1);
            }
        }

    })


})
