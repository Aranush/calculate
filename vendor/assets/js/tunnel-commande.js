$(document).ready(function() {
	// CHANGE NB AVEC + ET - SUR LES INPUT 

	$(".paiement-commande").submit(function() {
		$('iframe#myform').show();
	});
	
	
	$(".bt-coordonnee").click(function() {
		$('form.coordonnee').submit();
		return false;
	});
	
	
	$(".ferme_aide").click(function() {
		//$.fancybox.showLoading();
		$('.div_aide').slideUp(200);
		//$.fancybox.hideLoading();
		return false;
	});
	
	
	$("a.codepromo").click(function() {
		$('#codepromo').slideDown(400);
		return false;
	});
	
	
	$(".aide").click(function() {
		//$.fancybox.showLoading();
		var id = $(this).attr('id');
		$('.div_aide').slideUp(400);
		$('.'+id).slideDown(400);
		//$.fancybox.hideLoading();
		return false;
	});
	
	
	$(".panier").click(function() {
		//$.fancybox.showLoading();
		var date = $('#date_choisie').val();
		var h_dispo = $('#4h_dispo').val();
		var distance = $('#nb_km').val();
		
		var camion = $('#id_3_1').attr('class');
		
		if(date == '22-02-2012') {
			var date = '';
		}
		else {
			var date = date;
		}
		
		
		//var camion = $.trim(camion);
		
		
		
		//alert(camion);
		
		if(camion == 'btn_camion btn actif' || camion == 'btn_camion btn actif ' || camion == 'btn_camion  btn actif') {
			var message_camion = '0';
		}
		else {
			var message_camion = '1';
		}
		
		
		
		
		if(message_camion == 1) {
			if(distance == 0) {
				var erreur_distance = '1';
				var erreur_message_distance = '<div style=\"margin:5px 5px 5px 15px; display:block;\">Veuillez remplir la distance</div>';
			}
			else {
				var erreur_distance = '0';
				var erreur_message_distance = '';
			}
		}
		else {
			var erreur_distance = '0';
			var erreur_message_distance = '';
		}
				
		if(date) {
			var erreur_date = '0';
			var erreur_message_date = '';
			
			if(h_dispo == 1) {
				
				var var_select = $('.demi_journee_val').val();
				//var var_select = $('.cal_'+date+' .demi_journee_news').val();
				if(var_select) { 
					var erreur_horaire = '0';
					var erreur_message_horaire = '';
					//window.location.href="/panier";
				}
				else {
					var erreur_horaire = '1';
					var erreur_message_horaire = '<div style=\"margin:5px 5px 5px 15px; display:block;\">Vous n\'avez pas sélectionné la plage horaire</div>';
					//alert('Vous n\'avez pas sélectionner la plage horaire'); }
				}
			}
			else {
				var erreur_horaire = '0';
				var erreur_message_horaire = '';
				//window.location.href="/panier";
			}
		}
		else {
			var erreur_date = '1';
			var erreur_message_date = '<div style=\"margin:5px 5px 5px 15px; display:block;\">Vous n\’avez pas sélectionné de date pour votre déménagement</div>';
			//alert('Vous n\'avez pas sélectionner de date pour votre déménagement');
			var erreur_message_horaire = '';
		}
		
		if(erreur_distance == 1 || erreur_date == 1 || erreur_horaire == 1) {
			if(erreur_distance == 1) {}
			if(erreur_date == 1) {}
			if(erreur_horaire == 1) {}
			
			var box = ""
				//+"<div class=\"modal_\">"
				//+"	<div class=\"modal-dialog\">"
				//+"		<div class=\"modal-content\">"
				+"			<div class=\"modal-header\">"
				+"				<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><i class=\"fa fa-times-circle-o red\"></i></button>"
				+"				<h4 class=\"modal-title\"><i class=\"icon fa fa-exclamation-triangle red\"></i> Informations manquantes</h4>"
				+"			</div>"
				+"			"+erreur_message_distance+" "+erreur_message_date+" "+erreur_message_horaire+" "
				+"			</div>"
				//+"		</div>"
				//+"	</div>"
				//+"</div>"
			var box = bootbox.alert(box);
		}
		else {
			window.location.href="/panier"; 
		}
		
		//$.fancybox.hideLoading();
		return false;
	});
	
	$(".plus_moins").click(function() {

		//$.fancybox.showLoading();
		var val = $(this).val();
		var old = $('#quantite').val();
		if(val == '-') {
			if(old == '1') { var news = '1'; }
			else { var news = (parseInt(old)-parseInt(1)); }
		}
		else { var news = (parseInt(old)+parseInt(1)); }
		$('#quantite').val(news);
		//$.fancybox.hideLoading();
	});
	
	
	jQuery("form#form_simulation").submit(function() {
		//$.fancybox.showLoading(); 
		$.ajax({
            type : "POST",
            url : "/includes/load/enregistre-simulation.php",
			data		: $(this).serializeArray(),
            error :function(msg){
           	   	alert( "Erreur  : " + msg );
           	},
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) { $('.result_simulation').html(obj.html).fadeIn('slow').delay(10000).fadeOut('slow'); }
				else { $('.result_simulation').html(obj.html).fadeIn('slow').delay(10000).fadeOut('slow'); }
				
				//var simulation_div = '<div class="col-md-12"><a class="simulation bold" href="#">Enregistrer ma simulation dans mon espace client</a></div><div class="col-md-12 padding result_simulation"></div><br clear="all">';
				//$('.simulation_div').html(simulation_div);							
			}
		});
		return false;
	});
	
	$(".simulation").click(function() {
		$.ajax({
            type : "POST",
            url : "/includes/load/enregistre-simulation.php",
	        error :function(msg){
           	   	alert( "Erreur  : " + msg );
           	},
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) { $('.result_simulation').html(obj.html).fadeIn('slow').delay(10000).fadeOut('slow'); }
				else { $('.result_simulation').html(obj.html).fadeIn('slow').delay(10000).fadeOut('slow'); }
			}
		});
		return false;
	});
	
	
	//$("form#add_service").submit(function() {
	//	//$.fancybox.showLoading();
	//	var id_send 	= $("#id_send").val();
	//	var quantite 	= $('.option_select_details').val();
	//	if(quantite) {}
	//	else { var quantite = $('.option_select_details_2').val(); }
	//	$.ajax({
     //       type : "POST",
     //       url : "/includes/load/add_panier.php",
     //       data : "id_send="+id_send+"&quantite="+quantite,
     //   	error :function(msg){
     //      	   	alert( "Erreur  : " + msg );
     //      	},
     //      	success : function(data){
	//			var obj = jQuery.parseJSON(data);
	//			if(obj.result == 1) {
	//				$('#quantity').val(obj.html);
	//				$('.return_cart').fadeIn('slow').delay(5000).fadeOut('slow');
	//				$('prix_details').html(obj.prix_details);
	//			}
	//			else {}
	//			//$.fancybox.hideLoading();
	//		}
	//	});
	//	return false;
	//});
	
	// SELECT CHANGE
	$(".option_select_details").change(function() {
		//jQuery.fancybox.showLoading();
		var val = $(this).val();
		var id = $(this).attr('id');
		var id_send = $(this).attr('title');
		$.ajax({
            type : "POST",
            url : "/includes/load/prix_service.php",
            data : "id_send="+id_send+"&quantite="+val,
        	error :function(msg){ alert( "Erreur  : " + msg ); },
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) { $('.prix_details').html(obj.prix_details); }				
			}
		});
		//$.fancybox.hideLoading();
		return false;
	});
	
	//$("form#add_materiel").submit(function() {
    	////$.fancybox.showLoading();
	//	var id_send 	= $("#id_send").val();
	//	var quantite 	= $('#quantite').val();
	//	$.ajax({
     //       type : "POST",
     //       url : "/includes/load/add_panier.php",
     //       data : "id_send="+id_send+"&quantite="+quantite,
     //   	error :function(msg){ alert( "Erreur  : " + msg ); },
     //      	success : function(data){
	//			var obj = jQuery.parseJSON(data);
	//			if(obj.result == 1) {
	//				$('#quantity').val(obj.html);
	//				$('.return_cart').fadeIn('slow').delay(5000).fadeOut('slow');
	//			}
	//			else {}
	//			//$.fancybox.hideLoading();
	//		}
	//	});
	//	return false;
	//});
	
	$("form#add_materiel_panier .plus_moins_panier").click(function() {
    	//$.fancybox.showLoading();
		var id 		= jQuery(this).attr('id');
		var valeur 	= jQuery(this).attr('value');		
		if(valeur == '+') { var action = 'plus'; } else { var action = 'moins'; }
		$.ajax({
            type : "POST",
            url : "/includes/load/modification_panier.php",
            data : "id="+id+"&action="+action,
        	error :function(msg){ alert( "Erreur  : " + msg ); },
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) {
					$('form.'+id+' #quantite').val(obj.html);
					$('.prix').html(obj.prix_ttc)
					$('.prix_materiel').html(obj.prix_materiel)
					$('.prix_service').html(obj.prix_service)					
					if(obj.count_materiel == 0) { $(".details_materiel").load('/includes/load/details-panier-services.php'); }
					if(obj.hover_materiel == 1) { $('.materiel').addClass('hover_panier'); }
				}
				else if(obj.result == 2) {
					$('tr.materiel_ligne_'+id+'').hide();
					$('.prix').html(obj.prix_ttc)
					if(obj.count_materiel == 0) {
						$(".details_materiel").load('/includes/load/details-panier-materiel.php');	
						$('.livraison').hide();
					}
					if(obj.hover_materiel == 1) { $('.materiel').addClass('hover_panier'); }					
				}
				else { var box = bootbox.alert(obj.html); }
			}
		});
		//$.fancybox.hideLoading();
		return false;
	});
	
	$("a.materiel_delete_panier").click(function() {
    	//$.fancybox.showLoading(); 
		var id 		= jQuery(this).attr('id');
		$.ajax({
            type : "POST",
            url : "/includes/load/modification_panier.php",
            data : "id="+id+"&action=delete",
        	error :function(msg){ alert( "Erreur  : " + msg ); },
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				$('tr.materiel_ligne_'+id+'').hide();
				$('.prix_materiel').html(obj.prix_materiel)
				if(obj.result == 1) { $('.prix').html(obj.prix_ttc) }
				else { alert('Une erreur est survene !'); }
				if(obj.count_materiel == 0) {
					$(".details_materiel").load('/includes/load/details-panier-materiel.php');	
					$('.livraison').hide();
				}
				if(obj.hover_materiel == 1) { $('.materiel').addClass('hover_panier'); }
			}
		});
		//$.fancybox.hideLoading();
		return false;
	});
	
	
	$("a.service_delete_panier").click(function() {
    	//$.fancybox.showLoading(); 
		var id 		= jQuery(this).attr('id');
		$.ajax({
            type : "POST",
            url : "/includes/load/modification_panier.php",
            data : "id="+id+"&action=delete",
        	error :function(msg){
           	   	alert( "Erreur  : " + msg );
           	},
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				$('tr.service_ligne_'+id+'').hide();
				$('.prix').html(obj.prix_ttc)
				$('.prix_service').html(obj.prix_service)
				if(obj.count_service == 0) { $(".details_service").load('/includes/load/details-panier-services.php'); }
				if(obj.hover_service == 1) { $('.service').addClass('hover_panier'); }
			}
		});
		//$.fancybox.hideLoading();
		return false;
	});
	
	// SELECT CHANGE
	$(".option_select").change(function() {
		//jQuery.fancybox.showLoading();
		var val = $(this).val();
		var id = $(this).attr('id');
		var id_send = $(this).attr('title');
		$.ajax({
            type : "POST",
            url : "/includes/load/add_panier.php",
            data : "id_send="+id_send+"&quantite="+val,
        	error :function(msg){ alert( "Erreur  : " + msg ); },
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				$('.prix').html(obj.prix_ttc);
				$('.prix_service').html(obj.prix_service);
			}
		});
		//$.fancybox.hideLoading();
		return false;	
	});
	

	// ETAPE 1,CHANGEMENT DE PRIX EN TEMPS REELLE DANS LE PANIER
	$('.demi_journee').click(function(){
		//jQuery.fancybox.showLoading(); 
		var valeur 			= $('#date_dem').val();
		var horaire_select = $('.class_2 a.actif').attr('id');
		var produit_select = $('.class_3 a.actif').attr('id');
		$.ajax({
            type : "POST",
            url : "/includes/cal/select-date-disponibilite.php",
            data : "valeur="+valeur+"&horaire_select="+horaire_select+"&produit_select="+produit_select,
		    error :function(msg){ alert( "Erreur  : " + msg ); },
           	success : function(data){ $('.reponse_verif').html(data); }
		});
		
		var id = $(this).attr('id');
		$.ajax({
        	type : "POST",
        	url : "/includes/cal/select-duree-matin-aprem.php",
        	data : "id="+id,
        	error :function(msg){ alert( "Erreur  : " + msg ); },
        	success : function(data){ $('.duree_update').html(data); }
		});
		//$.fancybox.hideLoading();
		return false;
	});	
	
	$('.demi_journee_news').change(function(){
		//jQuery.fancybox.showLoading(); 
		var val = $(this).val();
		if(val) {
			var valeur 			= $('#date_choisie').val();
			var horaire_select = $('.class_2 a.actif').attr('id');
			var produit_select = $('.class_3 a.actif').attr('id');
			
			
			$.ajax({
				type : "POST",
				url : "/includes/cal/select-date.php",
				data : "valeur="+valeur,
				//send : jQuery('.affiche_result').html('<img src="../images/ajax-loader.gif" alt="Chargement" style="margin:0 auto; display:block;" />'),
				error :function(msg){
					alert( "Erreur  : " + msg );
				},
				success : function(data){
					//$('input#date_dem').val(obj.valeur);
					var obj = jQuery.parseJSON(data);
					if(obj.result == 1) {
						$('.date_select_up').html('<b> : '+obj.date_select_up+'</b>');
						
						//alert('626');
					}
				}
			});
			
			$.ajax({
				type : "POST",
				url : "/includes/cal/select-periode-date.php",
				data : "valeur="+valeur+"&horaire_select="+horaire_select+"&produit_select="+produit_select+"&duree_select="+val,
				error :function(msg){ alert( "Erreur  : " + msg ); },
				success : function(data){ $('.resultat').html(data); }
			});
			
			
			$.ajax({
				type : "POST",
				url : "/includes/cal/select-date.php",
				data : "valeur="+valeur,
				//send : jQuery('.affiche_result').html('<img src="../images/ajax-loader.gif" alt="Chargement" style="margin:0 auto; display:block;" />'),
				error :function(msg){
					alert( "Erreur  : " + msg );
				},
				success : function(data){
					//$('input#date_dem').val(obj.valeur);
					var obj = jQuery.parseJSON(data);
					if(obj.result == 1) {
						$('.date_select_up').html('<b> : '+obj.date_select_up+'</b>');
						
						//alert('654');
						
					}
				}
			});
			
			
			var id = $(this).val();
			$.ajax({
				type : "POST",
				url : "/includes/cal/select-duree-matin-aprem.php",
				data : "id="+id,
				error :function(msg){ alert( "Erreur  : " + msg ); },
				success : function(data){ $('.duree_update').html(data); }
			});			
		}
		else { $('.info_disponibilite').hide(); }
		//jQuery.fancybox.hideLoading();	return false;
	});	
	
	// DESACTIVE OU PAS LE CHAMP KM SI AUCUN VEHICULE EST CHOISIE
	// $('.encart_home_texte a').click(function(){
	// 	var id = $(this).attr('id');
	// 	if (id == 'id_3_1') {
	// 		$('.distance_km').hide();
	// 		$('#nb_km').val('');
	// 	}
	// 	else if (id == 'id_3_2' || id == 'id_3_3' || id == 'id_3_4') {
	// 		$('.distance_km').show();
	// 		$('#nb_km').css("background","#FFF");
	// 	}
	// 	return false;
	// });
	//
	
	// MODIFICATION POUR LES OPTIONS DE LIVRAISON
	$('.checkbox_livraison').click(function() {
		//jQuery.fancybox.showLoading(); 
		var id = $(this).attr('id');
		$.ajax({
            type : "POST",
            url : "/includes/load/add_panier_option_livraison.php",
            data : "id="+id,
        	error :function(msg){
           	   	alert( "Erreur  : " + msg );
           	},
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) { $('.prix').html(obj.prix_ttc) }
				else { var box = bootbox.alert(obj.html); }
				//$.fancybox.hideLoading();
			}
		});
	});
	
	$('.checkbox_assurance').click(function() {
		//jQuery.fancybox.showLoading(); 
		var id = $(this).val();
		$.ajax({
            type : "POST",
            url : "/includes/load/add_panier_option_assurance.php",
            data : "id="+id,
        	error :function(msg){
           	   	alert( "Erreur  : " + msg );
           	},
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) { $('.prix').html(obj.prix_ttc); }
				else { var box = bootbox.alert(obj.html); }
				//$.fancybox.hideLoading();
			}
		});
	});
	
	
	$('.option_emmaus').click(function() {
		//jQuery.fancybox.showLoading(); 
		var id = $(this).val();
		$.ajax({
            type : "POST",
            url : "/includes/load/add_panier_option_emmaus.php",
            data : "id="+id,
        	error :function(msg){
           	   	alert( "Erreur  : " + msg );
           	},
           	success : function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1) { $('.prix').html(obj.prix_ttc); }
				else { var box = bootbox.alert(obj.html); }
				//$.fancybox.hideLoading();
			}
		});
	});
	
	// CALCUL DU KM EN PLUS
	jQuery('#nb_km').keyup( function(){
		//jQuery.fancybox.showLoading(); 
		jQueryfield = $(this);
 		//if(jQueryfield.val().length > 0 ) {
			$('.result_km').html('');
			$.ajax({
  				type : 'POST',
				url  : '/includes/load/update_km.php' ,
				data : 'nb_km='+$(this).val() ,
				beforeSend : function() {},
				success : function(data){
					var obj = jQuery.parseJSON(data);
					if(obj.result == 1) {
						$("span.km").html(obj.html);
						$('.prix').html(obj.prix_ttc);
						
						jQuery('.no-km').hide();
						jQuery('.yes-km').hide();
						jQuery('input#nb_km').css('border','1px solid green');
						jQuery('input#nb_km').css('color','green');
						jQuery('.yes-km').show().css('color','green');
					
					}
					else if(obj.result == 2) {
						$("span.km").html(obj.html);
						$('.prix').html(obj.prix_ttc);
						
						jQuery('input#nb_km').val('');
						jQuery('.no-km').hide();
						jQuery('.yes-km').hide();
						jQuery('input#nb_km').css('border','1px solid red');
						jQuery('input#nb_km').css('color','red');
						jQuery('.no-km').show().css('color','red');
						//var box = bootbox.alert(obj.html);
						
					}
					else {
						jQuery('input#nb_km').val('');
						jQuery('.no-km').hide();
						jQuery('.yes-km').hide();
						jQuery('input#nb_km').css('border','1px solid red');
						jQuery('input#nb_km').css('color','red');
						jQuery('.no-km').show().css('color','red');
						//var box = bootbox.alert(obj.html);
					}
				}
      		});
   		/*
		}
		else {
			jQuery('input#nb_km').val('');
			jQuery('.no-km').hide();
			jQuery('.yes-km').hide();
			jQuery('input#nb_km').css('border','1px solid red');
			jQuery('input#nb_km').css('color','red');
			jQuery('.no-km').show().css('color','red');
		}
		*/
		
		//jQuery.fancybox.hideLoading();	
	});
	
});
